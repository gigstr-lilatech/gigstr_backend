<?php

sendAndroidPush();

function sendAndroidPush() {
    $url = 'http://staging-gigster.efserver.net/androidpush.php';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $result = curl_exec($ch);
    if ($result === false)
        die('Curl failed ' . curl_error());

    curl_close($ch);
}
