<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Alaxos' => $baseDir . '/vendor/alaxos/cakephp3-libs/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'CakephpBlueimpUpload' => $baseDir . '/vendor/alaxos/cakephp3-blueimp-upload/',
        'Crud' => $baseDir . '/vendor/friendsofcake/crud/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Kerox/Push' => $baseDir . '/vendor/ker0x/cakephp-push/',
        'WyriHaximus/TwigView' => $baseDir . '/vendor/wyrihaximus/twig-view/'
    ]
];
