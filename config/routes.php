<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Plugin;
use Cake\Routing\Router;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');

Router::scope('/', function ($routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */

    $routes->connect('/simplepush.php', ['controller' => 'Push', 'action' => 'index']);
    $routes->connect('/androidpush.php', ['controller' => 'Push', 'action' => 'pushAndroid']);
    
    $routes->connect('/checkUnreadApple.php', ['controller' => 'Push', 'action' => 'getUnsentMessagesApple']);
    $routes->connect('/checkUnreadAndroid.php', ['controller' => 'Push', 'action' => 'getUnsentMessagesAndroid']);
    
    $routes->connect('/', ['prefix' => 'admin','controller' => 'User', 'action' => 'login']);


    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);


    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks('DashedRoute');
});

/**
 * API V2 Route
 */
Router::scope('/v2', function ($routes) {
    $routes->connect('/user/get_personal', ['controller' => 'User', 'action' => 'getNewPersonal']);
    $routes->connect('/user/add', ['controller' => 'User', 'action' => 'newAdd']);
    $routes->connect('/user/updateGetInTouch', ['controller' => 'User', 'action' => 'updateGetInTouch']);
    $routes->connect('/user/editProfileList', ['controller' => 'User', 'action' => 'editProfileList']);
    $routes->connect('/user/login', ['controller' => 'User', 'action' => 'newLogin']);
    $routes->connect('/user/ephemeral_keys', ['controller' => 'User', 'action' => 'ephemeralKeys']);
    $routes->connect('/user/fcm_token', ['controller' => 'User', 'action' => 'fcmToken']);
    $routes->connect('/user/app_mode', ['controller' => 'User', 'action' => 'appMode']);
    $routes->connect('/user/remove_applicant', ['controller' => 'User', 'action' => 'removeApplicant']);
    $routes->connect('/user/browse_details', ['controller' => 'User', 'action' => 'browseDetails']);
    $routes->connect('/user/search', ['controller' => 'User', 'action' => 'search']);
    $routes->connect('/user/gigstr_seen', ['controller' => 'User', 'action' => 'gigstrSeen']);
    $routes->connect('/user/set_chat', ['controller' => 'User', 'action' => 'setChat']);
    $routes->connect('/user/recommendation_visibility', ['controller' => 'User', 'action' => 'recommendationVisibility']);

    $routes->connect('/gig/add_gig', ['controller' => 'Gig', 'action' => 'addNewGig']);
    $routes->connect('/gig/list_gig', ['controller' => 'Gig', 'action' => 'listGigNew']);
    $routes->connect('/gig/list_all', ['controller' => 'Gig', 'action' => 'listAllNew']);
    $routes->connect('/gig/add_gig', ['controller' => 'Gig', 'action' => 'addNewGig']);
    $routes->connect('/gig/gig_products', ['controller' => 'Gig', 'action' => 'gigProducts']);
    $routes->connect('/gig/gig_skills', ['controller' => 'Gig', 'action' => 'gigSkills']);
    $routes->connect('/gig/myGigs', ['controller' => 'Gig', 'action' => 'myGigs']);
    $routes->connect('/gig/applicants', ['controller' => 'Gig', 'action' => 'applicants']);
    $routes->connect('/gig/apply', ['controller' => 'Gig', 'action' => 'applyNew']);
    $routes->connect('/gig/deactivate', ['controller' => 'Gig', 'action' => 'deactivate']);
    $routes->connect('/gig/search', ['controller' => 'Gig', 'action' => 'search']);
    $routes->connect('/gig/save_gig', ['controller' => 'Gig', 'action' => 'saveGig']);
    $routes->connect('/gig/draft_gig', ['controller' => 'Gig', 'action' => 'draftGig']);

    $routes->connect('/job_offer/create', ['controller' => 'JobOffer', 'action' => 'create']);
    $routes->connect('/job_offer/save', ['controller' => 'JobOffer', 'action' => 'save']);
    $routes->connect('/job_offer/offers_list', ['controller' => 'JobOffer', 'action' => 'offersList']);
    $routes->connect('/job_offer/view', ['controller' => 'JobOffer', 'action' => 'view']);
    $routes->connect('/job_offer/tax_rate', ['controller' => 'JobOffer', 'action' => 'taxRate']);
    $routes->connect('/job_offer/add_card', ['controller' => 'JobOffer', 'action' => 'addCard']);
    $routes->connect('/job_offer/accept', ['controller' => 'JobOffer', 'action' => 'accept']);
    $routes->connect('/job_offer/favorites', ['controller' => 'JobOffer', 'action' => 'favorites']);
    $routes->connect('/job_offer/cancel', ['controller' => 'JobOffer', 'action' => 'cancel']);
    $routes->connect('/job_offer/copy_offer', ['controller' => 'JobOffer', 'action' => 'copyOffer']);
    $routes->connect('/job_offer/get_card', ['controller' => 'JobOffer', 'action' => 'getCard']);
    $routes->connect('/job_offer/payout', ['controller' => 'JobOffer', 'action' => 'payout']);
    $routes->connect('/job_offer/payout_data', ['controller' => 'JobOffer', 'action' => 'payoutData']);
    $routes->connect('/job_offer/badge_icons', ['controller' => 'JobOffer', 'action' => 'badgeIcons']);
    $routes->connect('/job_offer/notification_history', ['controller' => 'JobOffer', 'action' => 'notificationHistory']);
    $routes->connect('/job_offer/event_count', ['controller' => 'JobOffer', 'action' => 'eventCount']);
    $routes->connect('/job_offer/create_new', ['controller' => 'JobOffer', 'action' => 'createNew']);
    $routes->connect('/job_offer/validate_coupon', ['controller' => 'JobOffer', 'action' => 'validateCoupon']);

    $routes->connect('/schedule/not_work', ['controller' => 'Schedule', 'action' => 'notWork']);
    $routes->connect('/schedule/checkout', ['controller' => 'Schedule', 'action' => 'checkoutNew']);

    $routes->connect('/chat/block_user', ['controller' => 'Chat', 'action' => 'blockUser']);
});

// Or with prefix()
Router::prefix('admin', function($routes) {
    // All routes here will be prefixed with ‘/admin‘
    // And have the prefix => admin route element added.
    $routes->connect(
        '/:controller',
        ['action' => 'index'],
        ['routeClass' => 'InflectedRoute']
    );
    $routes->connect(
        '/:controller/:action/*',
        [],
        ['routeClass' => 'InflectedRoute']
    );
});

/*Router::scope('/', function ($routes) {
    $routes->connect('/user/recommendations', ['controller' => 'User', 'action' => 'recommendations']);
});*/

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();


