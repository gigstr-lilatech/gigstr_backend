<?php
use Migrations\AbstractMigration;

class AddLngCountryToCandidates extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('candidate');
        $table->addColumn('lng_country', 'string', [
            'default' => null,
            'limit' => 3,
            'null' => false,
        ]);
        $table->update();
    }
}
