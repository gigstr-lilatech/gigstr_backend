<?php
use Migrations\AbstractMigration;

class AddCountryToJob extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('job');
        $table->addColumn('country', 'string', [
            'default' => null,
            'limit' => 3,
            'null' => false,
        ]);
        $table->update();
    }
}
