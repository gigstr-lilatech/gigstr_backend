<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return [
    'from_email' => 'info@gigstr.com',
    'from_site' => 'Gigstr',
    'reset_password_subject' => 'Reset Password',
    'gigs_list_per_page' => '5',
    'dev_base_url' => 'http://backoffice.gigstr.com/',
    'autoreply_message' => 'Thank you for your message! Our support is currently unmanned. We are here for you monday to friday between 9am and 17pm. Please call your contact at Gigstr for urgent matters. Best regards Team Gigstr.',
    'autoreply_message_sv' => 'Hej och tack för ditt meddelande! Vår support är för närvarande inte bemannad. Vi finns här för dig måndag till fredag mellan 09-17. För akuta ärenden hör av dig direkt till din kontakt inne på Gigstr. Allt gott Team Gigstr.',
    'autoreply_message_da' => 'Hej og tak for din meddelelse! Vores support er ikke bemandet lige nu. Vi står til rådighed for dig mandag til fredag mellem kl. 9-17. For hastesager, tag direkte kontakt med din kontakt hos Gigstr. Vh Team Gigstr.',
    'autoreply_message_nb' => 'Hej og takk for din melding! Vår support er for øyeblikket ikke bemannet. Vi er her for deg mandag til fredag mellom 09-17. Om det haster, hør av deg til din kontaktperson inne på Gigstr. Ha det strålende, Team Gigstr.',
    'job_application_message' => 'Hello and welcome to Gigstr. Thank you for applying to this gig. We will shortly provide you with more info.',
    'job_application_message_sv' => 'Hej och välkommen till Gigstr. Tack för visat intresse. Vi kontaktar dig inom kort med mer information.',
    'job_application_message_da' => 'Hej og velkommen til Gigstr. Tak for din interesse. Vi kontakter dig snarest med mere information.',
    'job_application_message_nb' => 'Hej og velkommen til Gigstr. Takk for at du viser interesse. Vi tar kontakt innom kort tid med mer informasjon.',
    'admin_id' => '28',
    // The APNS server that we will use
    'server' => 'ssl://gateway.push.apple.com:2195',
    // The SSL certificate that allows us to connect to the APNS servers
    //'certificate' => 'private/ck.pem', // Commented in production new cert from rakitha 05.042016
    //'passphrase' => 'abc123', // Commented in production new cert from rakitha 05.042016
//    'certificate' => 'private/production_com.se.Gigster.pem',
       'certificate' => 'private/pushcert.pem',
    'passphrase' => '',

    'logfile' => 'private/push_development.log',
    'MAX_MESSAGE_LENGTH' => 190,
    'google_api_key' => 'AIzaSyAgLuMkvv9AXKb7yNUYaNm03rk29FjarRY',
    'android_push_url' => 'androidpush.php',
//'stripe_api_version' => '2017-08-15',
//    'stripe_api_key' => 'sk_test_RTTYi5QR3KpSpmqbnvinjaCQ',
 //   'stripe_api_mode' => 'Test'
    'stripe_api_version' => '2018-02-06',
    'stripe_api_key' => 'sk_live_tVnP6JzdBbvW4kOcePZCqScq',
    'stripe_api_mode' => 'Live'

];

//define('RESET_EMAIL','noreply@gigstr.com');
