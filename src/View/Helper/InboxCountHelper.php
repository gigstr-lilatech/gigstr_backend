<?php
/**
 * Created by PhpStorm.
 * User: yasith
 * Date: 6/13/17
 * Time: 4:07 PM
 */

namespace App\View\Helper;


use Cake\Datasource\ConnectionManager;
use Cake\View\Helper;

class InboxCountHelper extends Helper {

    public function getNotificationCount() {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT
                            distinct c.id AS chat_id
                            FROM  chat c
                            INNER JOIN application a ON a.chat_id = c.id
                            INNER JOIN gig g ON g.id = a.gig_id
                            INNER JOIN user u ON u.id = a.user_id
                            LEFT JOIN admin_notification ad ON c.id = ad.chat_id
                            WHERE (g.status = 1) AND ad.type = "2"');

        $stmt->execute();
        $chats = $stmt->fetchAll('assoc');
        $notificationCount = count($chats);
        return $notificationCount;
    }

    /**
     * @return int
     * get pending gig count that created from APP
     */
    public function getPendingGigCount() {

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT id FROM gig WHERE status = "0"');
        $stmt->execute();
        $gigs = $stmt->fetchAll('assoc');
        $pendingGigCount =  count($gigs);

        return $pendingGigCount;
    }

}