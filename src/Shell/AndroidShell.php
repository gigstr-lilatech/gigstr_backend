<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\App;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\PushComponent;
use Cake\Core\Configure;

class AndroidShell extends Shell {

    public function initialize() {
        parent::initialize();
        $this->loadModel('PushQueue');
    }

    public function main() {
        //$this->Push = new PushComponent(new ComponentRegistry());

//        $recordsWithNull = $this->PushQueue->find()->where(['modified_and IS NULL'])->count();
//        if ($recordsWithNull > 0) {
//            $this->initiatePush();
//        } else {
//            return false;
//        }
    }

    public function initiatePush() {
       

            $url = Configure::read('dev_base_url') . '/androidpush.php';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            $result = curl_exec($ch);
            if ($result === false)
                die('Curl failed ' . curl_error());

            curl_close($ch);
    }

}
