<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\App;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\PushComponent;
use Cake\Core\Configure;

class AppleShell extends Shell {

    public function initialize() {
        parent::initialize();
        $this->loadModel('PushQueue');
    }

    public function main() {

        //mail('sudharshan.ars@gmail.com', 'sdfsdfsd', 'sdfdsfdsf');
        //$this->Push = new PushComponent(new ComponentRegistry());
//        $lastSentMessage = $this->PushQueue->find()->where(['modified_ios IS NOT NULL'])->order(['id' => 'desc'])->first();
//
//        $recordsWithNotNull = $this->PushQueue->find()->where(['modified_ios IS NULL'])->count();
//        $recordsInTable = $this->PushQueue->find()->count();
//
//        if (!empty($lastSentMessage)) {
//            $lastMessageSentTime = strtotime($lastSentMessage->modified_ios);
//
//            if (time() > ($lastMessageSentTime + (60 * 5))) {
//                $this->initiatePush();
//            }
//        } elseif ($recordsWithNotNull === $recordsInTable) {
//            $this->initiatePush();
//        } else {
//            return false;
//        }
    }

    public function initiatePush() {
        $query = $this->PushQueue->find()->where(['modified_ios IS NULL']);
        $messages = $query->all();

        if (!empty($messages)) {

            $url = Configure::read('dev_base_url') . '/simplepush.php';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            $result = curl_exec($ch);
            if ($result === false)
                die('Curl failed ' . curl_error());

            curl_close($ch);
        }
    }

}
