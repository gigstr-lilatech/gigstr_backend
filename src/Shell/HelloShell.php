<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

class HelloShell extends Shell {

    public function main() {
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('UPDATE push_queue SET modified_and = NOW()');
        $stmt->execute();
    }

}
