<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AddressTypeTranslation Entity
 *
 * @property int $id
 * @property string $area_local
 * @property string $area_english
 * @property string $area_type
 * @property string $place_id
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Place $place
 */
class AddressTypeTranslation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
