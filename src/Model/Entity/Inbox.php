<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inbox Entity.
 *
 * @property int $id
 * @property int $gig_id
 * @property \App\Model\Entity\Gig $gig
 * @property int $sender_id
 * @property \App\Model\Entity\Sender $sender
 * @property int $receiver_id
 * @property \App\Model\Entity\Receiver $receiver
 * @property string $message
 * @property string $added_time
 * @property string $is_read
 * @property string $status
 */
class Inbox extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
