<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ScheduleImage Entity.
 *
 * @property int $id
 * @property int $schedule_id
 * @property \App\Model\Entity\Schedule $schedule
 * @property int $gig_id
 * @property \App\Model\Entity\Gig $gig
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $image
 * @property string $thumbnail
 * @property \Cake\I18n\Time $created
 */
class ScheduleImage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
