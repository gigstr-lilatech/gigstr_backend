<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Pricing Entity.
 *
 * @property int $id
 * @property int $country_id
 * @property \App\Model\Entity\Country $country
 * @property float $minimum_salary
 * @property float $vat
 * @property \Cake\I18n\Time $created
 * @property \App\Model\Entity\Fee[] $fee
 * @property \App\Model\Entity\JobOffer[] $job_offer
 */
class Pricing extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
