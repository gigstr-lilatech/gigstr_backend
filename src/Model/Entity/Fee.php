<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Fee Entity.
 *
 * @property int $id
 * @property int $fee_type_id
 * @property \App\Model\Entity\FeeType $fee_type
 * @property int $pricing_id
 * @property \App\Model\Entity\Pricing $pricing
 * @property float $value
 * @property \Cake\I18n\Time $created
 */
class Fee extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
