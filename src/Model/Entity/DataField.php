<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DataField Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $created_by
 * @property int $modified_by
 * @property int $company_id
 * @property \Cake\I18n\Time $created
 * @property \App\Model\Entity\GigTemplateShiftData[] $gig_template_shift_data
 * @property \App\Model\Entity\Schedule[] $schedule
 */
class DataField extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
