<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CouponUser Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $coupon_code_id
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\CouponCode $coupon_code
 */
class CouponUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
