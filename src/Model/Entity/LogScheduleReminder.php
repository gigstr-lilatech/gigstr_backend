<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogScheduleReminder Entity
 *
 * @property int $id
 * @property int $schedule_id
 * @property int $type
 * @property int $push_status
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Schedule $schedule
 */
class LogScheduleReminder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'schedule_id' => true,
        'type' => true,
        'push_status' => true,
        'created' => true,
        'schedule' => true
    ];
}
