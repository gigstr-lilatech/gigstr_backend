<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Candidate Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property string $phone_number
 * @property string $address
 * @property string $address_co
 * @property string $zip_code
 * @property string $city
 * @property int $country
 * @property string $gender
 * @property string $bank_name
 * @property string $account
 * @property string $notes
 * @property string $person_number
 * @property string $clearing_number
 * @property string $image
 * @property string $candidate_rating
 * @property string $status
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Candidate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
