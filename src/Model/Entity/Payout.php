<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payout Entity.
 *
 * @property int $id
 * @property int $country_id
 * @property \App\Model\Entity\Country $country
 * @property string $type
 * @property string $value
 * @property string $validation
 * @property int $is_required
 * @property int $title_id
 * @property string $placeholder
 * @property \Cake\I18n\Time $created
 */
class Payout extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
