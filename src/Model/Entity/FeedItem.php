<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FeedItem Entity
 *
 * @property int $id
 * @property int $feed_id
 * @property string $title
 * @property string $description
 * @property string $link_type
 * @property string $url
 * @property string $image
 * @property string $image_thumbnail
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Feed $feed
 */
class FeedItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
