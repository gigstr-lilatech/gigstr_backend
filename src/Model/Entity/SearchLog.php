<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SearchLog Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $city
 * @property string $area
 * @property string $areatype
 * @property string $place_id_google
 * @property string $country_code
 * @property \Cake\I18n\Time $from_date
 * @property \Cake\I18n\Time $to_date
 * @property string $skills
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 */
class SearchLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'city' => true,
        'area' => true,
        'areatype' => true,
        'place_id_google' => true,
        'country_code' => true,
        'from_date' => true,
        'to_date' => true,
        'skills' => true,
        'created' => true,
        'user' => true
    ];
}
