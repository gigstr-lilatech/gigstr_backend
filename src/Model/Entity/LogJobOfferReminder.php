<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogJobOfferReminder Entity
 *
 * @property int $id
 * @property int $job_offer_id
 * @property int $schedule_id
 * @property int $type
 * @property int $push_status
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\JobOffer $job_offer
 * @property \App\Model\Entity\Schedule $schedule
 */
class LogJobOfferReminder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'job_offer_id' => true,
        'schedule_id' => true,
        'type' => true,
        'push_status' => true,
        'created' => true,
        'job_offer' => true,
        'schedule' => true
    ];
}
