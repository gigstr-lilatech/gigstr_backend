<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StripeLog Entity
 *
 * @property int $id
 * @property string $stripe_customer_id
 * @property int $user_id
 * @property string $request
 * @property string $response
 * @property string $endpoint
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 */
class StripeLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'card_token' => true,
        'user_id' => true,
        'request' => true,
        'response' => true,
        'endpoint' => true,
        'created' => true,
        'user' => true
    ];
}
