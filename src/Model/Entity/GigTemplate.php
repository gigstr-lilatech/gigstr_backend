<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Utility\Text;

/**
 * GigTemplate Entity.
 *
 * @property int $id
 * @property int $gig_id
 * @property \App\Model\Entity\Gig $gig
 * @property string $description
 * @property text $form_json
 * @property text $from_html
 * @property \Cake\I18n\Time $created
 * @property int $created_by
 * @property int $modified_by
 */
class GigTemplate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
