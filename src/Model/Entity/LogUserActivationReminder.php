<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogUserActivationReminder Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $type
 * @property int $app_mode_preference
 * @property int $push_status
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 */
class LogUserActivationReminder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'type' => true,
        'app_mode_preference' => true,
        'push_status' => true,
        'created' => true,
        'user' => true
    ];
}
