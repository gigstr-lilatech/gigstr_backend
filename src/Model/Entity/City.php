<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * City Entity
 *
 * @property int $id
 * @property string $city_name
 * @property string $area
 * @property string $area_type
 * @property int $country_id
 * @property string $country_code
 * @property string $place_id_google
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\GigstrSearchLog[] $gigstr_search_log
 */
class City extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
