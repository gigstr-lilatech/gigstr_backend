<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AppModeLog Entity
 *
 * @property int $id
 * @property string $place
 * @property string $mode
 * @property \Cake\I18n\Time $created
 */
class AppModeLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
