<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gig Entity.
 *
 * @property int $id
 * @property int $company_id
 * @property \App\Model\Entity\Company $company
 * @property string $title
 * @property string $gig_image
 * @property string $sub_title1
 * @property string $sub_title2
 * @property int $address_id
 * @property \App\Model\Entity\Addres $address
 * @property int $status
 * @property int $created_by
 * @property string $description
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Applicant[] $applicant
 * @property \App\Model\Entity\Inbox[] $inbox
 * @property \App\Model\Entity\Sentbox[] $sentbox
 */
class Gig extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
