<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GigTemplateShiftData Entity.
 *
 * @property int $id
 * @property int $gig_template_id
 * @property \App\Model\Entity\GigTemplate $gig_template
 * @property string $field_name
 * @property string $field_type
 * @property string $field_data
 * @property string $field_title
 * @property string $checkbox_value
 * @property string $required
 * @property int $is_delete
 * @property \Cake\I18n\Time $created
 * @property \App\Model\Entity\ScheduleDataField[] $schedule_data_field
 */
class GigTemplateShiftData extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
