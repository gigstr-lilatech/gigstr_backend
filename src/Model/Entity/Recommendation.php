<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Recommendation Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $company
 * @property string $email
 * @property string $recommendation
 * @property int $visible
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 */
class Recommendation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
