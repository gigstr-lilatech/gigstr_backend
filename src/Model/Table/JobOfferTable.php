<?php
namespace App\Model\Table;

use App\Model\Entity\JobOffer;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
//use Kerox\Push\Adapter\Fcm;
//use Kerox\Push\Push;

/**
 * JobOffer Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Company
 * @property \Cake\ORM\Association\BelongsTo $Gig
 * @property \Cake\ORM\Association\BelongsTo $User
 * @property \Cake\ORM\Association\BelongsTo $Address
 * @property \Cake\ORM\Association\BelongsTo $Agreement
 * @property \Cake\ORM\Association\BelongsTo $Skill
 * @property \Cake\ORM\Association\BelongsTo $Pricing
 * @property \Cake\ORM\Association\BelongsTo $Job
 * @property \Cake\ORM\Association\HasMany $GigOrder
 */
class JobOfferTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('job_offer');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Company', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Gig', [
            'foreignKey' => 'gig_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Address', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agreement', [
            'foreignKey' => 'agreement_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Skills', [
            'foreignKey' => 'skill_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Pricing', [
            'foreignKey' => 'pricing_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Job', [
            'foreignKey' => 'job_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('GigOrder', [
            'foreignKey' => 'job_offer_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('total_cost', 'valid', ['rule' => 'decimal'])
            ->requirePresence('total_cost', 'create')
            ->notEmpty('total_cost');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Company'));
        //$rules->add($rules->existsIn(['gig_id'], 'Gig'));
        $rules->add($rules->existsIn(['user_id'], 'User'));
        $rules->add($rules->existsIn(['address_id'], 'Address'));
        $rules->add($rules->existsIn(['agreement_id'], 'Agreement'));
        $rules->add($rules->existsIn(['skill_id'], 'Skills'));
        $rules->add($rules->existsIn(['pricing_id'], 'Pricing'));
        $rules->add($rules->existsIn(['job_id'], 'Job'));
        return $rules;
    }

    /**
     * @param $userId
     * @return \stdClass
     */
    public function badgeCount($userId) {

        $scheduleTable = TableRegistry::get('Schedule');
        $gigTable = TableRegistry::get('Gig');
        $applicationTable = TableRegistry::get('Application');
        $jobOfferTable = TableRegistry::get('JobOffer');
        $candidateTable = TableRegistry::get('Candidate');
        $eventTable = TableRegistry::get('Event');
        $unreadTable = TableRegistry::get('UnreadMessage');

        $gigs = $gigTable->find('all')
            ->where(['Gig.created_by' => $userId, 'Gig.status !=' => '4']);
        $applicants_count = 0;
        foreach($gigs as $gig) {
            $applicants = $applicationTable->find()
                ->join([
                    'alias' => 'g',
                    'table' => 'gig_candidate',
                    'type' => 'LEFT',
                    'conditions' => 'g.user_id = Application.user_id'
                ])
                ->where(['Application.gig_id' => $gig->id, 'Application.status' => '0',
                    'g.selection' => 'SHORTLIST', 'g.gig_id' => $gig->id])->count();
            $applicants_count = $applicants_count + $applicants;
        }

        $inbox = $unreadTable->find('all')->where(['user_id' => $userId])->group('chat_id')->count();

        $jobOffers = $jobOfferTable->find('all')
            ->where(['status' => 'PENDING', 'user_id' => $userId])
            ->count();

        $candidateObj = $candidateTable->findByUserId($userId)->first();
        $event_date = $candidateObj->event_date;
        $addCountry = $candidateObj->lng_country;
        if($event_date) {
            $select = ['user_id' => $userId, 'created >' => $event_date, 'is_read' => 0];
            $requires = ['user_id' => $userId, 'created >' => $event_date, 'is_read' => 0, 'requires_action' => 1];
            $addCount = $eventTable->find('all')->where(['created >' => $event_date, 'country_code' => $addCountry])->count();
        }
        else {
            $select = ['user_id' => $userId, 'is_read' => 0];
            $requires = ['user_id' => $userId, 'is_read' => 0, 'requires_action' => 1];
            $addCount = 0;
        }

        $eventCount = $eventTable->find('all')->where($select)->count();
        $requiresAction = $eventTable->find('all')->where($requires)->count();
        $eventCount = $eventCount + $addCount;

        $conn = ConnectionManager::get('default');
        $now = date('Y-m-d H:i:s');
        $sql = "SET @time_stamp = '". $now ."'; 
                SELECT schedule.id
                FROM schedule 
                WHERE DATE(schedule.shift_from) = DATE(@time_stamp) 
                AND @time_stamp > DATE_ADD(schedule.shift_from, INTERVAL 15 MINUTE) 
                AND schedule.status = 1 AND schedule.not_work = 0 AND schedule.gigstr = ". $userId ." 
                UNION
                SELECT schedule.id
                FROM schedule 
                WHERE DATE(schedule.shift_to) = DATE(@time_stamp) 
                AND @time_stamp > DATE_ADD(schedule.shift_to, INTERVAL 60 MINUTE) 
                AND schedule.status = 2 AND schedule.gigstr = ". $userId .";";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $schedules = $stmt->rowCount();

        $return = new \stdClass();
        $return->myGigs = $applicants_count;
        $return->jobOffers = $jobOffers;
        $return->schedules = $schedules;
        $return->notifications = $eventCount;
        $return->inbox = $inbox;

        $total = ($applicants_count + $jobOffers + $schedules + $inbox);
        $eventsTotal = $total + ($eventCount - $requiresAction);

        if($eventsTotal < $eventCount) {
            $return->total = $eventCount;
        }
        else {
            $return->total = $eventsTotal;
        }

        //$return->total = $eventsTotal;

        $return->events = ($eventCount - $requiresAction);
        $return->requiresAction = $requiresAction;

        return $return;
    }

    /**
     * DEPRECATED
     * @param $token
     * @param $title
     * @param $body
     * @return bool
     */
    public function pushSend($token, $title, $body) {
        $adapter = new Fcm();
        $adapter
            ->setTokens([$token])
            ->setNotification([
                'title' => $title,
                'body' =>  $body
            ])
            ->setDatas([
                'data-1' => 'Lorem ipsum',
                'data-2' => 1234,
                'data-3' => true
            ])
            ->setParameters([
                'dry_run' => true
            ]);

      /*  $push = new Push($adapter);

        // Make the push
        $result = $push->send();
        $response = $push->response();*/

       /* var_dump($result);
        var_dump($response);
        die();*/

        return true;
    }

}
