<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * StripeLog Model
 *
 * @property \App\Model\Table\UserTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\StripeLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\StripeLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StripeLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StripeLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StripeLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StripeLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StripeLog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StripeLogTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('stripe_log');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'User'));

        return $rules;
    }

    /**
     * create stripe log
     * @param $token
     * @param $user
     * @param $request
     * @param $response
     * @param $endpoint
     * @return bool
     */
    public function createLog($token, $user, $request, $response, $endpoint) {

        $stripeLog = TableRegistry::get('StripeLog');
        $log = $stripeLog->newEntity();

        $log->stripe_customer_id = $token;
        $log->user_id = $user;
        $log->request = $request;
        $log->response = $response;
        $log->endpoint = $endpoint;

        $stripeLog->save($log);

        return true;
    }

}
