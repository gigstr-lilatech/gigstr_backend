<?php
namespace App\Model\Table;

use App\Model\Entity\PushQueue;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PushQueue Model
 *
 */
class PushQueueTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('push_queue');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('param');

        $validator
            ->requirePresence('payload', 'create')
            ->notEmpty('payload');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->add('modified_ios', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('modified_ios');

        $validator
            ->add('modified_and', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('modified_and');

        return $validator;
    }
}
