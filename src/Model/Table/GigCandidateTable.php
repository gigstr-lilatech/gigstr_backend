<?php
namespace App\Model\Table;

use App\Model\Entity\GigCandidate;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GigCandidate Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Gigs
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class GigCandidateTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('gig_candidate');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Gig', [
            'foreignKey' => 'gig_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('selection', 'create')
            ->notEmpty('selection');

        $validator
            ->add('selection_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('selection_date', 'create')
            ->notEmpty('selection_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['gig_id'], 'Gig'));
        $rules->add($rules->existsIn(['user_id'], 'User'));
        return $rules;
    }
}
