<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * PushNotification Model
 *
 * @property \App\Model\Table\UserTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\PushNotification get($primaryKey, $options = [])
 * @method \App\Model\Entity\PushNotification newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PushNotification[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PushNotification|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PushNotification patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PushNotification[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PushNotification findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PushNotificationTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('push_notification');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
      //  $rules->add($rules->existsIn(['user_id'], 'User'));

        return $rules;
    }


    /**
     * @param $payload
     * @param $token
     * @param $user_id
     * @param $mode
     * @param $redirect
     * @param $notification
     * @param $badgeCount
     * @param $key_value
     * @param string $topic
     * @return bool
     */
    public function sendPushEvent($payload, $token, $user_id, $mode, $redirect, $notification, $badgeCount, $key_value, $topic = "") {

        $UserTable = TableRegistry::get('User');
        $pushTable = TableRegistry::get('PushNotification');

        if($user_id != 0 ) {
            $userObj  = $UserTable->get($user_id);
            $device_type = $userObj->device_type;
        }
        else {
            $device_type = 0;
        }

        $push = $pushTable->newEntity();

        $push->payload = $payload;
        $push->token = $token;
        $push->user_id = $user_id;
        $push->device_type = $device_type;
        $push->mode = $mode;
        $push->redirect = $redirect;
        $push->name = $notification;
        $push->badge_count = $badgeCount;
        $push->key_value = $key_value;
        $push->topic = $topic;

        if($pushTable->save($push)) {
            return true;
        }
        else {
            return false;
        }
    }
}
