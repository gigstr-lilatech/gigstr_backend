<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ScheduleGigTemplate Model
 *
 * @property \App\Model\Table\ScheduleTable|\Cake\ORM\Association\BelongsTo $Schedules
 *
 * @method \App\Model\Entity\ScheduleGigTemplate get($primaryKey, $options = [])
 * @method \App\Model\Entity\ScheduleGigTemplate newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ScheduleGigTemplate[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ScheduleGigTemplate|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ScheduleGigTemplate patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ScheduleGigTemplate[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ScheduleGigTemplate findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ScheduleGigTemplateTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('schedule_gig_template');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Schedule', [
            'foreignKey' => 'schedule_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('form_json', 'create')
            ->notEmpty('form_json');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['schedule_id'], 'Schedule'));

        return $rules;
    }
}
