<?php
namespace App\Model\Table;

use App\Model\Entity\Referal;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Referal Model
 *
 */
class ReferalTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('referal');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('referrer_user_id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('referrer_user_id', 'create')
            ->notEmpty('referrer_user_id');

        $validator
            ->requirePresence('device_id', 'create')
            ->notEmpty('device_id');

        return $validator;
    }
}
