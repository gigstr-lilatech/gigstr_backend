<?php

namespace App\Model\Table;

use App\Model\Entity\Gig;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Gig Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Company
 * @property \Cake\ORM\Association\BelongsTo $User
 * @property \Cake\ORM\Association\BelongsTo $Address
 * @property \Cake\ORM\Association\HasMany $Applicant
 * @property \Cake\ORM\Association\HasMany $Inbox
 * @property \Cake\ORM\Association\HasMany $Sentbox
 */
class GigTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('gig');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Company', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('User', [
            'foreignKey' => 'created_by'
        ]);
        $this->belongsTo('Address', [
            'foreignKey' => 'address_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Application', [
            'foreignKey' => 'gig_id',
        ]);
        $this->belongsTo('Skills', [
            'foreignKey' => 'skills_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create');

        /*$validator
                ->requirePresence('title', 'create')
                ->notEmpty('title');*/

        $validator
                ->requirePresence('gig_image', 'create')
                ->notEmpty('gig_image');

        $validator
                ->requirePresence('sub_title1', 'create')
                ->notEmpty('sub_title1');

        $validator
                ->requirePresence('sub_title2', 'create')
                ->notEmpty('sub_title2');

        $validator
                ->add('status', 'valid', ['rule' => 'numeric'])
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->add('created_by', 'valid', ['rule' => 'numeric'])
                ->requirePresence('created_by', 'create')
                ->notEmpty('created_by');

        $validator
                ->requirePresence('description', 'create')
                ->notEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['company_id'], 'Company'));
        return $rules;
    }

    public function deleteGig($id = null) {
        $gigTable = TableRegistry::get('Gig');
        $gig = $gigTable->get($id); // Return article with id 12

        $gig->status = '4';
        $gigTable->save($gig);
        
        $this->table('gig');
    }


    /**
     * @param $gigStatus
     * @param bool $pendingCheck
     * @return string
     */
    public function getGigStatus($gigStatus, $pendingCheck = false) {

        $status = "";
        switch ($gigStatus) {
            case "0" :
                if($pendingCheck) {
                    $status = 'Pending';
                } else {
                    $status = 'Pending - Waiting for review';
                }
                break;
            case "1" :
                $status = 'Published';
                break;
            case "2" :
                $status = 'Deactivated';
                break;
            case "3" :
                $status = 'Expired';
                break;
            case "4" :
                $status = 'Deleted';
                break;
            case "5" :
                $status = 'Declined';
                break;
            case "6" :
                $status = 'Draft';
                break;
        }

        return $status;
    }


    /**
    *@param $getAddress
     * @return int
     * create gig and company address
     */
    public function getAddress($getAddress) {

        $addressTable = TableRegistry::get('Address');
        $countryTable = TableRegistry::get('Country');

        $addressId = 0;
        if(!empty($getAddress)) {

            $translate = '';
            if(isset($getAddress['area'])) {
                if($getAddress['country_code'] == 'GB') {
                    $translate = $getAddress['area'];
                }
                else {
                    $translate = $this->translateAddress($getAddress['area'], $getAddress['area_type'],
                        $getAddress['place_id_google']);
                }
            }

            $newAddress = $addressTable->newEntity();
            $newAddress->street = $getAddress['street'];
            $newAddress->zip = $getAddress['zip_code'];
            $newAddress->city = $getAddress['city'];
            if(isset($getAddress['area'])) {
                $newAddress->place_id_google = $getAddress['place_id_google'];
                $newAddress->latitude = $getAddress['latitude'];
                $newAddress->longitude = $getAddress['longitude'];
                $newAddress->area_type = $getAddress['area_type'];
                $newAddress->area = $translate;
            }
            $newAddress->country_name = $getAddress['country'];
            $newAddress->country_code = $getAddress['country_code'];
            if(!empty($getAddress['country_code'])) {
                $countryCode = $countryTable->findByCountryCode($getAddress['country_code'])->first();
                $newAddress->country_id = $countryCode->id;
            }
            $address = $addressTable->save($newAddress);
            $addressId = $address->id;
        }
        return $addressId;
    }


    /**
     * @param $area
     * @param $areaType
     * @param $placeId
     */
    public function translateAddress($area, $areaType, $placeId) {

        $translationTable = TableRegistry::get('AddressTypeTranslation');
        $translationObj = $translationTable->find('all')
            ->where(['area_local' => $area])
            ->first();

        if($translationObj) {
            $area_english = $translationObj->area_english;
        }
        else {
            $areaSearch = str_replace(' ', '+',$area);

            $apiKeystg = 'AIzaSyDFFhQ3OM0EeaSLBTd5w7_zEy4JT_Ptb9s';
            $apiKey = 'AIzaSyDBDLZW07WO2pVd0DIi3Ce7AGT1ASYNJSI';
            $url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' . $areaSearch . '&lang‌​uage=en&key=' . $apiKey;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url
            ));
            $resp = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($resp,true);

            if(!empty($response['results'])) {
                $area_english = $response['results'][0]['name'];
            } else {
                $area_english = $area;
            }

            $translation = $translationTable->newEntity();
            $translation->area_local = $area;
            $translation->area_english = $area_english;
            $translation->area_type = $areaType;
            $translation->place_id_google = $placeId;
            $translationTable->save($translation);
        }
        return $area_english;
    }


}
