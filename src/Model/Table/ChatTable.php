<?php
namespace App\Model\Table;

use App\Model\Entity\Chat;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Chat Model
 *
 * @property \Cake\ORM\Association\HasMany $Application
 * @property \Cake\ORM\Association\HasMany $Message
 */
class ChatTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('chat');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Application', [
            'foreignKey' => 'chat_id'
        ]);

        $this->hasMany('ChatGroup', [
            'foreignKey' => 'chat_id'
        ]);

        $this->hasMany('Message', [
            'foreignKey' => 'chat_id',
            'sort' => 'id DESC'
        ]);
                
        $this->belongsTo('User', [
            'foreignKey' => 'created_by',
            'joinType' => 'INNER',
        ]);
        
        $this->hasMany('UnreadMessage', [
            'foreignKey' => 'user_id',
        ]);


    }
    
    

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');

        return $validator;
    }


    /**
     * @param $gigstr
     * @param $company
     * @return mixed
     */
    public function setChatGroup($gigstr, $company) {

        $chatGroupModel = TableRegistry::get('ChatGroup');
        $chatModel = TableRegistry::get('Chat');

        $chatEntry = $chatModel->newEntity();
        $chatEntry->created_by = $company;
        $chatModel->save($chatEntry);
        $chat_id = $chatEntry->id;

        $chatgroup = $chatGroupModel->newEntity();
        $chatgroup->chat_id = $chat_id;
        $chatgroup->user_id = $company;
        $chatgroup->status = 1;
        $chatGroupModel->save($chatgroup);

        $chatgroup2 = $chatGroupModel->newEntity();
        $chatgroup2->chat_id = $chat_id;
        $chatgroup2->user_id = $gigstr;
        $chatgroup2->status = 1;
        $chatGroupModel->save($chatgroup2);

        return $chat_id;
    }
}
