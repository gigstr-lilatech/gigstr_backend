<?php
namespace App\Model\Table;

use App\Model\Entity\GigTemplateShiftData;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GigTemplateShiftData Model
 *
 * @property \Cake\ORM\Association\BelongsTo $GigTemplate
 * @property \Cake\ORM\Association\HasMany $ScheduleDataField
 */
class GigTemplateShiftDataTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('gig_template_shift_data');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('GigTemplate', [
            'foreignKey' => 'gig_template_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ScheduleDataField', [
            'foreignKey' => 'gig_template_shift_data_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('field_name', 'create')
            ->notEmpty('field_name');

        $validator
            ->requirePresence('field_type', 'create')
            ->notEmpty('field_type');

        $validator
            ->requirePresence('field_data', 'create')
            ->notEmpty('field_data');

        $validator
            ->requirePresence('field_title', 'create')
            ->notEmpty('field_title');

        $validator
            ->requirePresence('checkbox_value', 'create')
            ->notEmpty('checkbox_value');

        $validator
            ->requirePresence('required', 'create')
            ->notEmpty('required');

        $validator
            ->add('is_delete', 'valid', ['rule' => 'numeric'])
            ->requirePresence('is_delete', 'create')
            ->notEmpty('is_delete');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['gig_template_id'], 'GigTemplate'));
        return $rules;
    }
}
