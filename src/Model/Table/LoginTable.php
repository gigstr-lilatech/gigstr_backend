<?php

namespace App\Model\Table;

use App\Model\Entity\Login;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Login Model
 *
 * @property \Cake\ORM\Association\BelongsTo $User
 */
class LoginTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('login');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('token', 'create')
                ->notEmpty('token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'User'));
        return $rules;
    }

    /**
     * Expire all other previous sessions by the user
     * 
     * @param $user_id The id of the user to expire the sessions.
     */
    public function expireAllSessions($user_id) {
        $this->updateAll(
                ['status' => 2], // fields
                ['user_id' => $user_id]); // conditions
    }

    public function isLoggedIn($token, $user_id) {
        // In a controller or table method.
        $query = $this->find('all', [
           'conditions' => ['user_id' => $user_id, 'token' => $token]
        ]);
        $row = $query->first();
        
        if(!empty($row)){
            return true;
        }else{
            return false;
        }
    }

}
