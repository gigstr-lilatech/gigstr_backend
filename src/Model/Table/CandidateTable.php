<?php
namespace App\Model\Table;

use App\Model\Entity\Candidate;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Candidate Model
 *
 * @property \Cake\ORM\Association\BelongsTo $User
 * @property \Cake\ORM\Association\BelongsTo $Application
 */
class CandidateTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('candidate');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('User', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('CandidateSkills', [
            'foreignKey' => 'candidate_id'
        ]);

//        $this->hasMany('Application', [
//            'foreignKey' => 'user_id'
//        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('phone_number');

        $validator
            ->allowEmpty('address');

        $validator
            ->allowEmpty('address_co');

        $validator
            ->allowEmpty('zip_code');

        $validator
            ->allowEmpty('city');

        $validator
            ->add('country', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('country');

        $validator
            ->allowEmpty('gender');

        $validator
            ->allowEmpty('bank_name');

        $validator
            ->allowEmpty('account');

        $validator
            ->allowEmpty('notes');

        $validator
            ->allowEmpty('person_number');

        $validator
            ->allowEmpty('clearing_number');

        $validator
            ->allowEmpty('image');

        $validator
            ->requirePresence('candidate_rating', 'create')
            ->notEmpty('candidate_rating');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'User'));
        return $rules;
    }


    public function getAvg($userId) {

        $my_model = TableRegistry::get('Schedule');
        $rating = $my_model->find()
            ->select('internal_rating')
            ->where(['gigstr' => $userId, 'internal_rating !=' => 0])
            ->toArray();

        $rates = 0;

        foreach($rating as $rate) {
            $rates +=  $rate->internal_rating;
        }
        $total = count($rating);

        if($total > 0) {
            $avg = round($rates / $total, 1);
            return $avg;

        } else {
            return 0;
        }
    }
}
