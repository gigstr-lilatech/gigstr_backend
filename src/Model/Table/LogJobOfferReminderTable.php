<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LogJobOfferReminder Model
 *
 * @property \App\Model\Table\JobOffersTable|\Cake\ORM\Association\BelongsTo $JobOffers
 * @property \App\Model\Table\SchedulesTable|\Cake\ORM\Association\BelongsTo $Schedules
 *
 * @method \App\Model\Entity\LogJobOfferReminder get($primaryKey, $options = [])
 * @method \App\Model\Entity\LogJobOfferReminder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LogJobOfferReminder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LogJobOfferReminder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LogJobOfferReminder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LogJobOfferReminder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LogJobOfferReminder findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LogJobOfferReminderTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('log_job_offer_reminder');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('JobOffer', [
            'foreignKey' => 'job_offer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Schedule', [
            'foreignKey' => 'schedule_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->requirePresence('push_status', 'create')
            ->notEmpty('push_status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['job_offer_id'], 'JobOffer'));
        $rules->add($rules->existsIn(['schedule_id'], 'Schedule'));

        return $rules;
    }
}
