<?php

namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Fbs
 * @property \Cake\ORM\Association\HasMany $Applicant
 * @property \Cake\ORM\Association\HasMany $Candidate
 */
class UserTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('user');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

//        $this->belongsTo('Fbs', [
//            'foreignKey' => 'fbid'
//        ]);
        $this->hasMany('Applicant', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Candidate', [
            'foreignKey' => 'user_id'
        ]);

        $this->hasMany('ChatCreator', [
            'foreignKey' => 'created_by',
            'className' => 'Chat'
        ]);
        $this->hasMany('ChatApplicant', [
            'foreignKey' => 'applicant',
            'className' => 'Chat'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create');

        $validator
                ->requirePresence('user_type', 'create')
                ->notEmpty('user_type');

        $validator
                ->requirePresence('name', 'create')
                ->notEmpty('name');

        $validator
                ->add('email', 'valid', ['rule' => 'email'])
                ->allowEmpty('email');

        $validator
                ->allowEmpty('password');

        $validator
                ->allowEmpty('fbtoken');

        $validator
                ->allowEmpty('token');

        $validator
                ->requirePresence('status', 'create')
                ->notEmpty('status');

        $validator
                ->allowEmpty('device_token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        //$rules->add($rules->existsIn(['fbid'], 'Fbs'));
        return $rules;
    }

    public function isLoggedIn($token, $user_id) {
        // In a controller or table method.
        
        $query = $this->find('all', [
            'conditions' => ['id' => $user_id, 'token' => $token]
        ]);
        $row = $query->first();

        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($event) {
        $entity = $event->data['entity'];

        if ($entity->isNew()) {
            $hasher = new DefaultPasswordHasher();

            // Generate an API 'token'
            $entity->token = $hasher->hash(sha1(Text::uuid()));
        }
        return true;
    }

    public function checkExisting($email) {
        $query = $this->find('all', [
            'conditions' => ['email' => $email]
        ]);

        $row = $query->first();

        if (!empty($row)) {
            return $row->id;
        } else {
            return -1;
        }
    }

    public function checkReferrer($device_id) {

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT referrer_user_id FROM referal
                            WHERE device_id = "' . $device_id . '"');
        $stmt->execute();
        $referrer = $stmt->fetch('assoc');

        if($referrer) {
            return $referrer['referrer_user_id'];
        } else {
            return "";
        }
    }


    /**return skill list
     * @param $candidateId
     * @return array
     */
    public function getSkillList($candidateId) {

        $skillsModel = TableRegistry::get('Skills');
        $CandidateSkillsModel = TableRegistry::get('CandidateSkills');
        $updateSkills = array();
        $skillsArr = $skillsModel->find()
            ->select(['id', 'title' =>'skill'])
            ->where(['active' => 1])
            ->order(['title' => 'ASC'])
            ->toArray();

        foreach($skillsArr as $skill) {
            $temp = [];
            $candidateSkill = $CandidateSkillsModel
                ->findByCandidateIdAndSkillsId($candidateId, $skill['id'])
                ->first();

            if($candidateSkill) {
                $temp['id'] = $candidateSkill->id;
                $temp['selected'] = true;
            } else {
                $temp['id'] = 0;
                $temp['selected'] = false;
            }
            $temp['title'] = $skill['title'];
            $temp['skill_id'] = $skill['id'];

            array_push($updateSkills, $temp);
        }

        foreach($updateSkills as $key => $val) {
            if($val['title'] == 'Other') {
              //  $item = $updateSkills[$key];
                unset($updateSkills[$key]);
               // array_push($updateSkills, $item);
                break;
            }
        }

        $skillArr = [];
        foreach ($updateSkills as $skill) {
            $temp = [];
            $temp['title'] = $skill['title'];
            $temp['id'] = $skill['id'];
            $temp['selected'] = $skill['selected'];
            $temp['skill_id'] = $skill['skill_id'];

            array_push($skillArr, $temp);
        }


        return $skillArr;
    }

    /** return languages list
     * @param $candidateId
     * @return array
     */
    public function getLanguagesList($candidateId) {

        $languagesModel = TableRegistry::get('Languages');
        $candidateLanguagesModel = TableRegistry::get('CandidateLanguages');
        $languages = array();
        $languageArr = $languagesModel->find()
            ->select(['id', 'title' =>'language'])
            ->where(['active' => 1])
            ->toArray();

        foreach($languageArr as $language) {
            $temp = [];
            $candidateLanguage = $candidateLanguagesModel
                ->findByCandidateIdAndLanguagesId($candidateId, $language['id'])
                ->first();

            if($candidateLanguage) {
                $temp['id'] = $candidateLanguage->id;
                $temp['selected'] = true;
            } else {
                $temp['id'] = 0;
                $temp['selected'] = false;
            }
            $temp['title'] = $language['title'];
            $temp['language_id'] = $language['id'];
            array_push($languages, $temp);
        }
        return $languages;
    }

    /** return education list
     * @param $candidateId
     * @return Query
     */
    public function getEducation($candidateId) {

        $candidateEducationModel = TableRegistry::get('CandidateEducation');
        $education = $candidateEducationModel->find()
            ->select(['id', 'title' => 'school', 'type', 'subject'])
            ->where(['candidate_id' => $candidateId])
         //   ->order(['id' => 'DESC'])
            ->toArray();

        $orderArr = ['University', 'College', 'Graduate School', 'Other', 'High School'];

        usort($education, function ($a, $b) use ($orderArr) {
            $pos_a = array_search($a['type'], $orderArr);
            $pos_b = array_search($b['type'], $orderArr);
            return $pos_a - $pos_b;
        });

        return $education;
    }

    /** return experinces list
     * @param $candidateId
     * @return Query
     */
    public function getExperiences($candidateId) {

        $candidateExperiencesModel = TableRegistry::get('CandidateExperiences');
        $experiences = $candidateExperiencesModel->find()
            ->select(['id', 'title' => 'employer', 'type' => 'position', 'start_date', 'end_date', 'present'])
            ->where(['candidate_id' => $candidateId])
            ->order(['start_date' => 'DESC']);

        foreach($experiences as $exp) {
            if($exp->start_date) {
                $exp->start_date = $exp->start_date->format("Y-m-d");
            }
            else {
                $exp->start_date = "";
            }

            if($exp->present == 1) {
                $exp->end_date = "Present";
            }
            else {
                if ($exp->end_date) {
                    $exp->end_date = $exp->end_date->format("Y-m-d");
                } else {
                    $exp->end_date = "";
                }
            }
        }

        return $experiences;
    }

    /** get wizard json
     * @param $wizard
     * @param bool $signUp
     * @return array
     */
    public function checkWizard($wizard, $signUp = false) {

        $wizard_enable = false;
        $wizardArr = [];
        $candidateSkillsModel = TableRegistry::get('CandidateSkills');
        $candidateExperiencesModel = TableRegistry::get('CandidateExperiences');
        $candidateEducationModel = TableRegistry::get('CandidateEducation');
        $skills = $candidateSkillsModel->findByCandidateId($wizard->id)->first();
        $exp = $candidateExperiencesModel->findByCandidateId($wizard->id)->first();
        $edu = $candidateEducationModel->findByCandidateId($wizard->id)->first();

        if(is_null($wizard->phone_number) || empty($wizard->phone_number)) {
            $wizard_enable = true;
            $wizardArr[0]['wizard_id'] = "w_email_telephone";
            $wizardArr[0]['wizard_key'] = "mobile";
            $wizardArr[0]['wizard_data'] = "";
        }
        if(is_null($wizard->image) || empty($wizard->image)) {
            $wizard_enable = true;
            $wizardArr[1]['wizard_id'] = "w_profile_image";
            $wizardArr[1]['wizard_key'] = "image";
            $wizardArr[1]['wizard_data'] = "";
        }
        if($signUp == true) {
            $wizard_enable = true;
            $wizardArr[2]['wizard_id'] = "w_account_selection";
            $wizardArr[2]['wizard_key'] = "";
            $wizardArr[2]['wizard_data'] = "";

            $wizardArr[3]['wizard_id'] = "w_account_gigstr";
            $wizardArr[3]['wizard_key'] = "";
            $wizardArr[3]['wizard_data'] = "";
        }
        if(is_null($wizard->city) || empty($wizard->city)) {
            $wizard_enable = true;
            $wizardArr[4]['wizard_id'] = "w_home_city";
            $wizardArr[4]['wizard_key'] = "city";
            $wizardArr[4]['wizard_data'] = "";
        }
        if(!$skills) {
            $wizard_enable = true;
            $skillList = $this->getSkillList($wizard->id);
            $wizardArr[5]['wizard_id'] = "w_skills";
            $wizardArr[5]['wizard_key'] = "skills";
            $wizardArr[5]['wizard_data'] = $skillList;
        }
        if(!$exp) {
            $wizard_enable = true;
            $wizardArr[6]['wizard_id'] = "w_work_experience";
            $wizardArr[6]['wizard_key'] = "experiences";
            $wizardArr[6]['wizard_data'] = array();
        }
        if(!$edu) {
            $wizard_enable = true;
            $wizardArr[7]['wizard_id'] = "w_education";
            $wizardArr[7]['wizard_key'] = "education";
            $wizardArr[7]['wizard_data'] = array();
        }
        if(is_null($wizard->pitch) || empty($wizard->pitch)) {
            $wizard_enable = true;
            $wizardArr[8]['wizard_id'] = "w_pitch";
            $wizardArr[8]['wizard_key'] = "pitch";
            $wizardArr[8]['wizard_data'] = "";
        }

        return array(
            'wizard_enable' => $wizard_enable,
            'wizardArr' => $wizardArr
        );
    }


    /**
     * @param int $user_id
     * @param int $companyUser
     * @return array
     */
    public function profileObj($user_id, $companyUser = 0) {

        $candidateModel = TableRegistry::get('Candidate');
        $userModel = TableRegistry::get('User');
        $newCandidate = $candidateModel->findByUserId($user_id)->first();
        $user = $userModel->findById($user_id)->first();

        $ratings = $this->getCandidateRating($user->id, $newCandidate->candidate_rating);
        $scheduleModel = TableRegistry::get('Schedule');
        $gig_completed = $scheduleModel->find('all')->where(['gigstr' => $user->id, 'status' => 3])->count();
        $chatId = $this->getChatGroup($companyUser, $user_id);

        $company = $this->getCompanyAddress($newCandidate->company_id);

        $country = "";
        if($newCandidate->country) {
            $countryModel = TableRegistry::get('Country');
            $countryObj = $countryModel->findByCountryCode($newCandidate->country)->first();
            $country = $countryObj->country_name;
        }

        if($companyUser == 0) {
            $view_contact = false;
        } else {
            $view_contact = $this->viewedUser($user_id, $companyUser);
        }

        $addressFix = (!empty($newCandidate->address_co) ? $newCandidate->address_co . ", " : "") . (!empty($newCandidate->zip_code) ? $newCandidate->zip_code .
                ", " : "") . ($newCandidate->city ? $newCandidate->city . ", " . $country : "");

        $recommendations = $this->getRecommendations($user->id);

        if(!empty($newCandidate->profile_link)) {
            $profile_link = $newCandidate->profile_link;
        } else {
            $profile_link = $this->getProfileBranchUrl($user->id);
        }

        $profileArr = [
            'user_id' => $user->id,
            'candidate_id' => $newCandidate->id,
            'name' => $user->name,
            'email' => $user->email,
            'mobile' => $newCandidate->phone_number ? $newCandidate->phone_number : "",
            'gender' => $newCandidate->gender ? $newCandidate->gender : "",
            'day' => $newCandidate->day ? $newCandidate->day : "",
            'month' => $newCandidate->month ? $newCandidate->month : "",
            'year'=> $newCandidate->year ? $newCandidate->year : "",
            'person_number' => $newCandidate->person_number ? $newCandidate->person_number : "",
            'candidate_rating' => $ratings->candidate_rating,
            'candidate_rating_count' => $ratings->candidate_rating_count,
            'image' => $this->getUserImage($newCandidate->image),
            'address' => $addressFix,
            'address_co' => $newCandidate->address_co ? $newCandidate->address_co : "",
            'city' => $newCandidate->city ? $newCandidate->city : "",
            'country' => $country,
            'country_code' => $newCandidate->country ? $newCandidate->country : "",
            'zip_code' => $newCandidate->zip_code ? $newCandidate->zip_code : "",
            'pitch' => $newCandidate->pitch ? $newCandidate->pitch : "",
            'skills' => $this->getSkillList($newCandidate->id),
            'language' => $this->getLanguagesList($newCandidate->id),
            'education' => $this->getEducation($newCandidate->id),
            'experiences' => $this->getExperiences($newCandidate->id),
            'website' => $newCandidate->website,
            'facebook' => $newCandidate->facebook,
            'linkedin' => $newCandidate->linkedin,
            'gigin' => "Since " . date("F Y", strtotime($newCandidate->created)),
            'with_gigstr' => date("M y", strtotime($newCandidate->created)),
            'verified' => $newCandidate->is_verified,
            'gig_completed' => $gig_completed,
            'chat_id' => $chatId,
            'company' => $company->company,
            'company_address' => $company->company_address,
            'company_website' => $company->website,
            'recommendation_link' => $recommendations['link'],
            'recommendations' => $recommendations['recommendations'],
            'recommendations_count' => $recommendations['count'],
            'app_mode_preference' => $newCandidate->app_mode_preference,
            'profile_link' => $profile_link,
            'viewed_user' => $view_contact
        ];

        return $profileArr;
    }


    /**
     * @param $user_id
     * @param $candidate_rating
     * @return \stdClass
     */
    public function getCandidateRating($user_id, $candidate_rating) {

        $ratingSchedule = TableRegistry::get('Schedule');

        $rating = $ratingSchedule->find()
            ->select('internal_rating')
            ->where(['gigstr' => $user_id, 'internal_rating !=' => 0])
            ->toArray();
        $rates = 0;
        foreach($rating as $rate) {
            $rates +=  $rate->internal_rating;
        }
        $total = count($rating);

        $results = new \stdClass();

        if($total > 0) {
            $avg = round($rates / $total, 1);
            $results->candidate_rating = $avg;
            $results->candidate_rating_count = $total;

        } else {
            $results->candidate_rating = $candidate_rating;
            $results->candidate_rating_count = 0;
        }

        return $results;
    }


    /**
     * @param $image
     * @return string
     */
    public function getUserImage($image) {

        if ($image != "") {
            if (preg_match('/facebook/', $image)) {
                $imageUrl = $image;
            } else {
                $imageUrl = Configure::read('dev_base_url') . 'img/user_images/' . $image;
            }
        }
        else {
            $imageUrl = Configure::read('dev_base_url') . 'img/user_images/user_placeholder.jpg';
        }
        return $imageUrl;
    }


    /**
     * @param $userId
     * @param $applicantId
     * @return int
     */
    public function getChatGroup($userId, $applicantId) {

        $chatGroupModel = TableRegistry::get('ChatGroup');
        $chatObj = $chatGroupModel->find('all')->where(['user_id' => $userId, 'status' => 1]);

        $chatId = -1;

        if($chatObj) {
            foreach ($chatObj as $chat) {
                $chat = $chatGroupModel->find('all')->where(['chat_id' => $chat->chat_id, 'user_id' => $applicantId, 'status' => 1])->first();

                if ($chat) {
                    $chatId = $chat->chat_id;
                }
            }
        }

        return $chatId;
    }


    /**
     * @param $company_id
     * @return \stdClass
     */
    public function getCompanyAddress($company_id) {

        $return = new \stdClass();

        $return->company_address = array(
            'street' => '',
            'zip_code' => '',
            'city' => '',
            'area' => '',
            'place_id_google' => '',
            'latitude' => '',
            'longitude' => '',
            'area_type' => '',
            'country' => '',
            'country_code' => ''
        );

        if($company_id == 0) {
            $return->company = "";
            $return->website = "";
        }
        else {
            $companyModel = TableRegistry::get('Company');
            $addressModel = TableRegistry::get('Address');

            $companyObj = $companyModel->findById($company_id)->first();
            $return->company = $companyObj->name;
            $return->website = $companyObj->website;

            if($companyObj->address_id != 0) {
                $address = $addressModel->findById($companyObj->address_id)->first();
                $return->company_address = array(
                    'street' => $address->street,
                    'zip_code' => $address->zip,
                    'city' => $address->city,
                    'area' => $address->area,
                    'place_id_google' => $address->place_id_google,
                    'latitude' => $address->latitude,
                    'longitude' => $address->longitude,
                    'area_type' => $address->area_type,
                    'country' => $address->country_name,
                    'country_code' => $address->country_code
                );
            }
        }

        return $return;
    }


    /**
     * @param $userId
     * @param string $type
     * @return array
     */
    public function getRecommendations($userId, $type = "USER") {

        $recommendationModel = TableRegistry::get('Recommendation');
        $candidateModel = TableRegistry::get('Candidate');

        if($type == "ME") {
            $condition = ['user_id' => $userId];
            $limit = 100;
        } else {
            $condition =  ['user_id' => $userId, 'visible' => 1];
            $limit = 6;
        }

        $recommendationObj = $recommendationModel->find('all')
            ->where($condition)
            ->order(['created' =>'DESC'])
            ->limit($limit);

        $recommendArr = [];
        foreach ($recommendationObj as $recommend) {
            $temp = [];
            $temp['id'] = $recommend->id;
            $temp['name'] = $recommend->name;
            $temp['company'] = $recommend->company;
            $temp['recommendation'] = $recommend->recommendation;
            $temp['visible'] = $recommend->visible;
            $temp['date'] = $recommend->created->format('Y-m-d');

            array_push($recommendArr, $temp);
        }

        $candidate = $candidateModel->findByUserId($userId)->first();

        if(empty($candidate->recommendation_link)) {
            $encryptId = urlencode(openssl_encrypt($userId, "AES-128-ECB", "user_id"));
            $recommendation_link = Configure::read('dev_base_url') . 'admin/user/recommendations?id='
                . $encryptId;
            $link = $this->getBranchUrl($recommendation_link);

            $candidateObj = $candidateModel->get($candidate->id);
            $candidateObj->recommendation_link = $link;
            $candidateModel->save($candidateObj);
        }
        else {
            $link = $candidate->recommendation_link;
        }

        return $recommend = array(
            'recommendations' => $recommendArr,
            'count' => $recommendationObj->count(),
            'link' => $link
        );
    }


    /**
     * @param $link
     * @return mixed
     */
    public function getBranchUrl($link) {

        $img = Configure::read('dev_base_url') .'img/gigstr_recommendation.jpg';
        $data = [
            "branch_key" => "key_live_jbxl88cdgfC29C2FbvS8TpffEulXn15t",
            "campaign" => "Recommendation",
            "data" => [
                '$canonical_identifier' => "Gigstr Recommendation",
                '$og_title' => "Gigstr Recommendation",
                '$og_description' => "Hi, I have just started working via the Gigstr app, the new way for students to work parallel to their studies. Would you recommend me to other companies?",
                '$og_image_url' => $img,
                '$desktop_url' => $link,
                '$android_url' => $link,
                '$ios_url' => $link,
                'type'=> 'recommendation',
                "custom_boolean" => true
            ]
        ];

        $payload = json_encode($data);
        $deepLink = $this->curlDeepLink($payload);

        return $deepLink;
    }


    /**
     * @param $userId
     * @return mixed
     */
    public function getProfileBranchUrl($userId) {

        $candidateModel = TableRegistry::get('Candidate');
        $candidate = $candidateModel->findByUserId($userId)->first();
        $img = Configure::read('dev_base_url') .'img/gigstr_recommendation.jpg';

        $data = [
            "branch_key" => "key_live_jbxl88cdgfC29C2FbvS8TpffEulXn15t",
            "campaign" => "Profile",
            "data" => [
                '$canonical_identifier' => "Gigstr Profile",
                '$og_title' => "Check this profile out on Gigstr!",
                '$og_description' => "Hi, thought you would be interested in this gigstr profile. Check it out on the gig platform Gigstr!",
                '$og_image_url' => $img,
                '$desktop_url' => "https://www.gigstr.com/",
                "custom_boolean" => true,
                'user_id' => $userId,
                'open_app' => true,
                'type'=> 'profile_share',
                '$always_deeplink' => true
            ]
        ];

        $payload = json_encode($data);
        $deepLink = $this->curlDeepLink($payload);

        $candidateObj = $candidateModel->get($candidate->id);
        $candidateObj->profile_link  = $deepLink;
        $candidateModel->save($candidateObj);

        return $deepLink;
    }


    /**
     * @param $user_id
     * @param $company_user_id
     * @return bool
     * in chat, both parties have to send msg each other
     * in job offer, has to accept the job offer
     */
    public function viewedUser($user_id, $company_user_id) {

        $jobOfferModel = TableRegistry::get('JobOffer');
        $messageModel = TableRegistry::get('Message');

        $jobOffer = $jobOfferModel->find('all')
            ->where(['user_id' => $user_id, 'created_by' => $company_user_id,
                'OR' => [['status' => 'HIRED'], ['status' => 'COMPLETED']]])
            ->count();

        $chat = false;
        $chatId = $this->getChatGroup($company_user_id, $user_id);
        if($chatId > 0) {
            $hasChat = $messageModel->find()->distinct(['created_by'])->where(['chat_id' => $chatId])->count();
            if($hasChat == 2) {
                $chat = true;
            }
        }

        if(($jobOffer > 0) || ($chat == true)) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * @param $fcm
     * @param $topic
     * @return mixed
     */
    public function subscribeToTopic($fcm, $topic) {

        $url = 'https://iid.googleapis.com/iid/v1/' . $fcm . '/rel/topics/' . $topic;
        $headers = array (
            'Authorization:key=AAAAr7ERGjM:APA91bHZs0mo5_4knH4_6UY1vy0ZLyeSYAgqTFZoXdwNzEbRaGNA1hOeUjwnZR-yE7L9QCUD-4bFBujq78fY0oG-duwvg2V5BoMZ6zgtRJq7zKaIgJLK4JvO2ZNoZn_0hydM7F5mHfQb',
            'Content-Type:application/json',
            'Content-Length: 0'
        );
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = json_decode(curl_exec ( $ch ));
        curl_close ( $ch );

        return $result;
    }

    /**
     * @param $fcm
     * @param $topic
     * @return mixed
     */
    public function unsubscribeToTopic($fcm, $topic) {

        $url = 'https://iid.googleapis.com/iid/v1:batchRemove';
        $headers = array (
            'Authorization:key=AAAAr7ERGjM:APA91bHZs0mo5_4knH4_6UY1vy0ZLyeSYAgqTFZoXdwNzEbRaGNA1hOeUjwnZR-yE7L9QCUD-4bFBujq78fY0oG-duwvg2V5BoMZ6zgtRJq7zKaIgJLK4JvO2ZNoZn_0hydM7F5mHfQb',
            'Content-Type:application/json'
        );

        $data = [
            "to" => "/topics/". $topic,
            "registration_tokens" => [$fcm]
        ];

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = json_decode(curl_exec ( $ch ));
        curl_close ( $ch );

        if(empty($result))
            return true;
        else
           return false;
    }


    /**
     * @param $area
     * @return mixed
     */
    public function convertSwedishLetters($area) {

        $engArea = str_replace('ö', 'o', str_replace('ä', 'a', str_replace('å', 'a', $area)));

        $replaceSw = str_replace('Å', 'A', str_replace('Ä', 'A', str_replace('Ö', 'O', $engArea)));

        return $replaceSw;
    }

    /**
     * @param $payload
     * @return mixed
     */
    private function curlDeepLink($payload) {
        $ch = curl_init( 'https://api.branch.io/v1/url');
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = curl_exec($ch);
        curl_close($ch);

        $link = json_decode($result);
        return $link->url;
    }


}
