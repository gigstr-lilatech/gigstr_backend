<?php

namespace App\Model\Table;

use App\Model\Entity\Schedule;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Gig Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Company
 * @property \Cake\ORM\Association\BelongsTo $User
 * @property \Cake\ORM\Association\HasMany $Applicant
 * @property \Cake\ORM\Association\HasMany $Inbox
 * @property \Cake\ORM\Association\HasMany $Sentbox
 */
class Latest extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('LATESTMSG');
        $this->name('Latest');
        $this->primaryKey('chat_id');

    }
}
