<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Inbox'), ['action' => 'edit', $inbox->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Inbox'), ['action' => 'delete', $inbox->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inbox->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Inbox'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Inbox'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="inbox view large-9 medium-8 columns content">
    <h3><?= h($inbox->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Added Time') ?></th>
            <td><?= h($inbox->added_time) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Read') ?></th>
            <td><?= h($inbox->is_read) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($inbox->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($inbox->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Gig Id') ?></th>
            <td><?= $this->Number->format($inbox->gig_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Sender Id') ?></th>
            <td><?= $this->Number->format($inbox->sender_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Receiver Id') ?></th>
            <td><?= $this->Number->format($inbox->receiver_id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($inbox->message)); ?>
    </div>
</div>
