<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $inbox->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $inbox->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Inbox'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="inbox form large-9 medium-8 columns content">
    <?= $this->Form->create($inbox) ?>
    <fieldset>
        <legend><?= __('Edit Inbox') ?></legend>
        <?php
            echo $this->Form->input('gig_id');
            echo $this->Form->input('sender_id');
            echo $this->Form->input('receiver_id');
            echo $this->Form->input('message');
            echo $this->Form->input('added_time');
            echo $this->Form->input('is_read');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
