<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Inbox'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inbox index large-9 medium-8 columns content">
    <h3><?= __('Inbox') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('gig_id') ?></th>
                <th><?= $this->Paginator->sort('sender_id') ?></th>
                <th><?= $this->Paginator->sort('receiver_id') ?></th>
                <th><?= $this->Paginator->sort('added_time') ?></th>
                <th><?= $this->Paginator->sort('is_read') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($inbox as $inbox): ?>
            <tr>
                <td><?= $this->Number->format($inbox->id) ?></td>
                <td><?= $this->Number->format($inbox->gig_id) ?></td>
                <td><?= $this->Number->format($inbox->sender_id) ?></td>
                <td><?= $this->Number->format($inbox->receiver_id) ?></td>
                <td><?= h($inbox->added_time) ?></td>
                <td><?= h($inbox->is_read) ?></td>
                <td><?= h($inbox->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $inbox->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $inbox->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $inbox->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inbox->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
