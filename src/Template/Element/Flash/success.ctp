<div class="alert alert-success" role="alert">
      <span class="glyphicon glyphicon-correct-sign" aria-hidden="true"></span>
      <?php echo h($message); ?>
</div>