<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Gig'), ['action' => 'edit', $gig->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gig'), ['action' => 'delete', $gig->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gig->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gig'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gig'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Applicant'), ['controller' => 'Applicant', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Applicant'), ['controller' => 'Applicant', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Inbox'), ['controller' => 'Inbox', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Inbox'), ['controller' => 'Inbox', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Sentbox'), ['controller' => 'Sentbox', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sentbox'), ['controller' => 'Sentbox', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gig view large-9 medium-8 columns content">
    <h3><?= h($gig->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($gig->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Gig Image') ?></th>
            <td><?= h($gig->gig_image) ?></td>
        </tr>
        <tr>
            <th><?= __('Sub Title1') ?></th>
            <td><?= h($gig->sub_title1) ?></td>
        </tr>
        <tr>
            <th><?= __('Sub Title2') ?></th>
            <td><?= h($gig->sub_title2) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($gig->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Company Id') ?></th>
            <td><?= $this->Number->format($gig->company_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $this->Number->format($gig->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Created By') ?></th>
            <td><?= $this->Number->format($gig->created_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($gig->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($gig->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($gig->description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Applicant') ?></h4>
        <?php if (!empty($gig->applicant)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Gig Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($gig->applicant as $applicant): ?>
            <tr>
                <td><?= h($applicant->id) ?></td>
                <td><?= h($applicant->user_id) ?></td>
                <td><?= h($applicant->gig_id) ?></td>
                <td><?= h($applicant->status) ?></td>
                <td><?= h($applicant->created) ?></td>
                <td><?= h($applicant->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Applicant', 'action' => 'view', $applicant->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Applicant', 'action' => 'edit', $applicant->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Applicant', 'action' => 'delete', $applicant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $applicant->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Inbox') ?></h4>
        <?php if (!empty($gig->inbox)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Gig Id') ?></th>
                <th><?= __('Sender Id') ?></th>
                <th><?= __('Receiver Id') ?></th>
                <th><?= __('Message') ?></th>
                <th><?= __('Added Time') ?></th>
                <th><?= __('Is Read') ?></th>
                <th><?= __('Status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($gig->inbox as $inbox): ?>
            <tr>
                <td><?= h($inbox->id) ?></td>
                <td><?= h($inbox->gig_id) ?></td>
                <td><?= h($inbox->sender_id) ?></td>
                <td><?= h($inbox->receiver_id) ?></td>
                <td><?= h($inbox->message) ?></td>
                <td><?= h($inbox->added_time) ?></td>
                <td><?= h($inbox->is_read) ?></td>
                <td><?= h($inbox->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Inbox', 'action' => 'view', $inbox->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Inbox', 'action' => 'edit', $inbox->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Inbox', 'action' => 'delete', $inbox->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inbox->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sentbox') ?></h4>
        <?php if (!empty($gig->sentbox)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Gig Id') ?></th>
                <th><?= __('Sender Id') ?></th>
                <th><?= __('Receiver Id') ?></th>
                <th><?= __('Message') ?></th>
                <th><?= __('Added Time') ?></th>
                <th><?= __('Is Read') ?></th>
                <th><?= __('Status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($gig->sentbox as $sentbox): ?>
            <tr>
                <td><?= h($sentbox->id) ?></td>
                <td><?= h($sentbox->gig_id) ?></td>
                <td><?= h($sentbox->sender_id) ?></td>
                <td><?= h($sentbox->receiver_id) ?></td>
                <td><?= h($sentbox->message) ?></td>
                <td><?= h($sentbox->added_time) ?></td>
                <td><?= h($sentbox->is_read) ?></td>
                <td><?= h($sentbox->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Sentbox', 'action' => 'view', $sentbox->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Sentbox', 'action' => 'edit', $sentbox->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sentbox', 'action' => 'delete', $sentbox->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sentbox->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
