<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Gig'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Applicant'), ['controller' => 'Applicant', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Applicant'), ['controller' => 'Applicant', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Inbox'), ['controller' => 'Inbox', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Inbox'), ['controller' => 'Inbox', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sentbox'), ['controller' => 'Sentbox', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sentbox'), ['controller' => 'Sentbox', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gig form large-9 medium-8 columns content">
    <?= $this->Form->create($gig) ?>
    <fieldset>
        <legend><?= __('Add Gig') ?></legend>
        <?php
            echo $this->Form->input('company_id');
            echo $this->Form->input('title');
            echo $this->Form->input('gig_image');
            echo $this->Form->input('sub_title1');
            echo $this->Form->input('sub_title2');
            echo $this->Form->input('status');
            echo $this->Form->input('created_by');
            echo $this->Form->input('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
