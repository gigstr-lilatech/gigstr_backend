<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Gig'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Applicant'), ['controller' => 'Applicant', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Applicant'), ['controller' => 'Applicant', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Inbox'), ['controller' => 'Inbox', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Inbox'), ['controller' => 'Inbox', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Sentbox'), ['controller' => 'Sentbox', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Sentbox'), ['controller' => 'Sentbox', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gig index large-9 medium-8 columns content">
    <h3><?= __('Gig') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('company_id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('gig_image') ?></th>
                <th><?= $this->Paginator->sort('sub_title1') ?></th>
                <th><?= $this->Paginator->sort('sub_title2') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gig as $gig): ?>
            <tr>
                <td><?= $this->Number->format($gig->id) ?></td>
                <td><?= $this->Number->format($gig->company_id) ?></td>
                <td><?= h($gig->title) ?></td>
                <td><?= h($gig->gig_image) ?></td>
                <td><?= h($gig->sub_title1) ?></td>
                <td><?= h($gig->sub_title2) ?></td>
                <td><?= $this->Number->format($gig->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $gig->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gig->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gig->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gig->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
