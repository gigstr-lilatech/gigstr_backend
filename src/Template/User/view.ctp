<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Applicant'), ['controller' => 'Applicant', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Applicant'), ['controller' => 'Applicant', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Login'), ['controller' => 'Login', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Login'), ['controller' => 'Login', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="user view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($user->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Fbid') ?></th>
            <td><?= h($user->fbid) ?></td>
        </tr>
        <tr>
            <th><?= __('Fbtoken') ?></th>
            <td><?= h($user->fbtoken) ?></td>
        </tr>
        <tr>
            <th><?= __('Token') ?></th>
            <td><?= h($user->token) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($user->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Phone') ?></th>
            <td><?= h($user->phone) ?></td>
        </tr>
        <tr>
            <th><?= __('Address') ?></th>
            <td><?= h($user->address) ?></td>
        </tr>
        <tr>
            <th><?= __('Device Token') ?></th>
            <td><?= h($user->device_token) ?></td>
        </tr>
        <tr>
            <th><?= __('Last Login') ?></th>
            <td><?= h($user->last_login) ?></td>
        </tr>
        <tr>
            <th><?= __('Tokenplain') ?></th>
            <td><?= h($user->tokenplain) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Applicant') ?></h4>
        <?php if (!empty($user->applicant)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Gig Id') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->applicant as $applicant): ?>
            <tr>
                <td><?= h($applicant->id) ?></td>
                <td><?= h($applicant->user_id) ?></td>
                <td><?= h($applicant->gig_id) ?></td>
                <td><?= h($applicant->status) ?></td>
                <td><?= h($applicant->created) ?></td>
                <td><?= h($applicant->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Applicant', 'action' => 'view', $applicant->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Applicant', 'action' => 'edit', $applicant->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Applicant', 'action' => 'delete', $applicant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $applicant->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Login') ?></h4>
        <?php if (!empty($user->login)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Token') ?></th>
                <th><?= __('Logintime') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->login as $login): ?>
            <tr>
                <td><?= h($login->id) ?></td>
                <td><?= h($login->user_id) ?></td>
                <td><?= h($login->token) ?></td>
                <td><?= h($login->logintime) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Login', 'action' => 'view', $login->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Login', 'action' => 'edit', $login->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Login', 'action' => 'delete', $login->id], ['confirm' => __('Are you sure you want to delete # {0}?', $login->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
