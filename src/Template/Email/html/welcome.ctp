<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<div style="width: 100%; min-height: 1000px; margin: 0 auto; padding: 0; background-color: #F8F7F2;">
    <div style="width: 600px; min-height: 1000px; margin: 0 auto; font-size: 0; background-color: #1D1F2B;">
        <a href="https://gigstr.app.link/icEzBOxjsA" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/email-1.jpg"></a>
        <img style="max-width: 100%;" src="<?= $base_url; ?>img/email/email-2.jpg">
        <div>
            <img style="max-width: 100%;" src="<?= $base_url; ?>img/email/email-3.jpg">
        </div>
        <img style="max-width: 100%;" src="<?= $base_url; ?>img/email/image-4.gif">
        <a href="mailto:support@gigstr.com?Subject=HI!%20I%20have%20a%20question%20for%20you..." target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/image-5.jpg"></a>
        <a href="https://intercom.help/gigstr/" target="_blank"><img style="max-width: 100%; padding: 0 0 60px 0;background-color: #1D1F2B;" src="<?= $base_url; ?>img/email/image-6.jpg"></a>
        <a href="https://youtu.be/t6_OEtzAUmg" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/image-7.jpg"></a>
        <a href="https://www.gigstr.com/" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/image-8.jpg"></a>

    </div>
</div>

</body>
</html>