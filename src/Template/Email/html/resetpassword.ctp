<div class="email-read col-lg-12">
    <div class="email-read-text">
        <p>
            Hello Gigstr,
        </p>
        <p>
          Please click the below link to reset your password
        </p>
        <p>
            <a href="<?= $base_url.$this->Url->build([
                    "controller" => "user",
                    "action" => "resetPassword",
                    "prefix" => "admin"
                    ]); ?>?token=<?= $token; ?>">Reset password</a>
        </p>
    </div>
    <img src="<?= $base_url; ?>img/logotype_backoffice_login.png" alt="Gigstr Logo"/>
 </div>