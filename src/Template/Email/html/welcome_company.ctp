<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<div style="width: 100%; min-height: 1000px; margin: 0 auto; padding: 0; background-color: #F8F7F2;">
    <div style="width: 600px; min-height: 1000px; margin: 0 auto; font-size: 0; background-color: #F8F7F2;">
        <img style="max-width: 100%;" src="<?= $base_url; ?>img/email/imageCompany-1.jpg">
        <a href="https://youtu.be/t6_OEtzAUmg" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/imageCompany-6.jpg"></a>
        <a href="https://intercom.help/gigstr/get-hired-via-gigstr" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/imageCompany-7.jpg"></a>
        <a href="https://www.instagram.com/gigstr_official/" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/imageCompany-8.jpg"></a>
        <div>
            <table style="width:100%">
                <tr>
                    <th><a href="https://www.facebook.com/gigstr/" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/fb.png"></a></th>
                    <th><a href="https://www.linkedin.com/company/gigstr-inc/" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/in.png"></a></th>
                    <th><a href="https://www.instagram.com/gigstr_official/" target="_blank"><img style="max-width: 100%;" src="<?= $base_url; ?>img/email/insta.png"></a></th>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>