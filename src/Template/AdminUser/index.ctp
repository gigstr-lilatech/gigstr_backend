<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Admin User'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="adminUser index large-9 medium-8 columns content">
    <h3><?= __('Admin User') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('user_name') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('password') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($adminUser as $adminUser): ?>
            <tr>
                <td><?= $this->Number->format($adminUser->id) ?></td>
                <td><?= h($adminUser->user_name) ?></td>
                <td><?= h($adminUser->email) ?></td>
                <td><?= h($adminUser->password) ?></td>
                <td><?= $this->Number->format($adminUser->status) ?></td>
                <td><?= h($adminUser->created) ?></td>
                <td><?= h($adminUser->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $adminUser->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $adminUser->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $adminUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $adminUser->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
