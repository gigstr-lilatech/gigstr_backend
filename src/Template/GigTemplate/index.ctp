<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Gig Template'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="gigTemplate index large-9 medium-8 columns content">
    <h3><?= __('Gig Template') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('gig_id') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('created_by') ?></th>
                <th><?= $this->Paginator->sort('modified_by') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gigTemplate as $gigTemplate): ?>
            <tr>
                <td><?= $this->Number->format($gigTemplate->id) ?></td>
                <td><?= $this->Number->format($gigTemplate->gig_id) ?></td>
                <td><?= h($gigTemplate->created) ?></td>
                <td><?= $this->Number->format($gigTemplate->created_by) ?></td>
                <td><?= $this->Number->format($gigTemplate->modified_by) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $gigTemplate->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gigTemplate->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gigTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gigTemplate->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
