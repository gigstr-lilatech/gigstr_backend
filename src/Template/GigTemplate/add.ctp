<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Gig Template'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="gigTemplate form large-9 medium-8 columns content">
    <?= $this->Form->create($gigTemplate) ?>
    <fieldset>
        <legend><?= __('Add Gig Template') ?></legend>
        <?php
            echo $this->Form->input('gig_id');
            echo $this->Form->input('description');
            echo $this->Form->input('created_by');
            echo $this->Form->input('modified_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
