<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Gig Template'), ['action' => 'edit', $gigTemplate->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Gig Template'), ['action' => 'delete', $gigTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gigTemplate->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Gig Template'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gig Template'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="gigTemplate view large-9 medium-8 columns content">
    <h3><?= h($gigTemplate->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($gigTemplate->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Gig Id') ?></th>
            <td><?= $this->Number->format($gigTemplate->gig_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created By') ?></th>
            <td><?= $this->Number->format($gigTemplate->created_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($gigTemplate->modified_by) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($gigTemplate->created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($gigTemplate->description)); ?>
    </div>
</div>
