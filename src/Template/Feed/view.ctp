<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Feed $feed
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Feed'), ['action' => 'edit', $feed->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Feed'), ['action' => 'delete', $feed->id], ['confirm' => __('Are you sure you want to delete # {0}?', $feed->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Feed'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Feed'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="feed view large-9 medium-8 columns content">
    <h3><?= h($feed->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Section') ?></th>
            <td><?= h($feed->section) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ui Type') ?></th>
            <td><?= h($feed->ui_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($feed->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($feed->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created By') ?></th>
            <td><?= $this->Number->format($feed->created_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified By') ?></th>
            <td><?= $this->Number->format($feed->modified_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($feed->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($feed->modified) ?></td>
        </tr>
    </table>
</div>
