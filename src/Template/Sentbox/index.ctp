<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Sentbox'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="sentbox index large-9 medium-8 columns content">
    <h3><?= __('Sentbox') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('gig_id') ?></th>
                <th><?= $this->Paginator->sort('sender_id') ?></th>
                <th><?= $this->Paginator->sort('receiver_id') ?></th>
                <th><?= $this->Paginator->sort('added_time') ?></th>
                <th><?= $this->Paginator->sort('is_read') ?></th>
                <th><?= $this->Paginator->sort('status') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($sentbox as $sentbox): ?>
            <tr>
                <td><?= $this->Number->format($sentbox->id) ?></td>
                <td><?= $this->Number->format($sentbox->gig_id) ?></td>
                <td><?= $this->Number->format($sentbox->sender_id) ?></td>
                <td><?= $this->Number->format($sentbox->receiver_id) ?></td>
                <td><?= h($sentbox->added_time) ?></td>
                <td><?= h($sentbox->is_read) ?></td>
                <td><?= h($sentbox->status) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $sentbox->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $sentbox->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $sentbox->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sentbox->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
