<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Sentbox'), ['action' => 'edit', $sentbox->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Sentbox'), ['action' => 'delete', $sentbox->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sentbox->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sentbox'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Sentbox'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sentbox view large-9 medium-8 columns content">
    <h3><?= h($sentbox->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Added Time') ?></th>
            <td><?= h($sentbox->added_time) ?></td>
        </tr>
        <tr>
            <th><?= __('Is Read') ?></th>
            <td><?= h($sentbox->is_read) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($sentbox->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($sentbox->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Gig Id') ?></th>
            <td><?= $this->Number->format($sentbox->gig_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Sender Id') ?></th>
            <td><?= $this->Number->format($sentbox->sender_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Receiver Id') ?></th>
            <td><?= $this->Number->format($sentbox->receiver_id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Message') ?></h4>
        <?= $this->Text->autoParagraph(h($sentbox->message)); ?>
    </div>
</div>
