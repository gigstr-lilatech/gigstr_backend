<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Sentbox'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="sentbox form large-9 medium-8 columns content">
    <?= $this->Form->create($sentbox) ?>
    <fieldset>
        <legend><?= __('Add Sentbox') ?></legend>
        <?php
            echo $this->Form->input('gig_id');
            echo $this->Form->input('sender_id');
            echo $this->Form->input('receiver_id');
            echo $this->Form->input('message');
            echo $this->Form->input('added_time');
            echo $this->Form->input('is_read');
            echo $this->Form->input('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
