<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Referal'), ['action' => 'edit', $referal->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Referal'), ['action' => 'delete', $referal->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referal->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Referal'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Referal'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="referal view large-9 medium-8 columns content">
    <h3><?= h($referal->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('DeviceId') ?></th>
            <td><?= h($referal->deviceId) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($referal->id) ?></td>
        </tr>
        <tr>
            <th><?= __('UserId') ?></th>
            <td><?= $this->Number->format($referal->userId) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($referal->created) ?></td>
        </tr>
    </table>
</div>
