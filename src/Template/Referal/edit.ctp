<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $referal->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $referal->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Referal'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="referal form large-9 medium-8 columns content">
    <?= $this->Form->create($referal) ?>
    <fieldset>
        <legend><?= __('Edit Referal') ?></legend>
        <?php
            echo $this->Form->input('userId');
            echo $this->Form->input('deviceId');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
