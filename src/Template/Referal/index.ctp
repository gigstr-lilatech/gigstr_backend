<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Referal'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="referal index large-9 medium-8 columns content">
    <h3><?= __('Referal') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('userId') ?></th>
                <th><?= $this->Paginator->sort('deviceId') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($referal as $referal): ?>
            <tr>
                <td><?= $this->Number->format($referal->id) ?></td>
                <td><?= $this->Number->format($referal->userId) ?></td>
                <td><?= h($referal->deviceId) ?></td>
                <td><?= h($referal->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $referal->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $referal->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $referal->id], ['confirm' => __('Are you sure you want to delete # {0}?', $referal->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
