<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Schedule Image'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="scheduleImage form large-9 medium-8 columns content">
    <?= $this->Form->create($scheduleImage) ?>
    <fieldset>
        <legend><?= __('Add Schedule Image') ?></legend>
        <?php
            echo $this->Form->input('schedule_id');
            echo $this->Form->input('gig_id');
            echo $this->Form->input('user_id');
            echo $this->Form->input('image');
            echo $this->Form->input('thumbnail');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
