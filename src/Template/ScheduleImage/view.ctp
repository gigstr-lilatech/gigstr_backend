<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Schedule Image'), ['action' => 'edit', $scheduleImage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Schedule Image'), ['action' => 'delete', $scheduleImage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $scheduleImage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Schedule Image'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Schedule Image'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="scheduleImage view large-9 medium-8 columns content">
    <h3><?= h($scheduleImage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Image') ?></th>
            <td><?= h($scheduleImage->image) ?></td>
        </tr>
        <tr>
            <th><?= __('Thumbnail') ?></th>
            <td><?= h($scheduleImage->thumbnail) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($scheduleImage->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Schedule Id') ?></th>
            <td><?= $this->Number->format($scheduleImage->schedule_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Gig Id') ?></th>
            <td><?= $this->Number->format($scheduleImage->gig_id) ?></td>
        </tr>
        <tr>
            <th><?= __('User Id') ?></th>
            <td><?= $this->Number->format($scheduleImage->user_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($scheduleImage->created) ?></td>
        </tr>
    </table>
</div>
