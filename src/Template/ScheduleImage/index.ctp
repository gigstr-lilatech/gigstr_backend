<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Schedule Image'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="scheduleImage index large-9 medium-8 columns content">
    <h3><?= __('Schedule Image') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('schedule_id') ?></th>
                <th><?= $this->Paginator->sort('gig_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('image') ?></th>
                <th><?= $this->Paginator->sort('thumbnail') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($scheduleImage as $scheduleImage): ?>
            <tr>
                <td><?= $this->Number->format($scheduleImage->id) ?></td>
                <td><?= $this->Number->format($scheduleImage->schedule_id) ?></td>
                <td><?= $this->Number->format($scheduleImage->gig_id) ?></td>
                <td><?= $this->Number->format($scheduleImage->user_id) ?></td>
                <td><?= h($scheduleImage->image) ?></td>
                <td><?= h($scheduleImage->thumbnail) ?></td>
                <td><?= h($scheduleImage->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $scheduleImage->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $scheduleImage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $scheduleImage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $scheduleImage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
