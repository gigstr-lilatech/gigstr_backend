 <!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
                            <div class="page-header">
                                <h2><?= __('Gigs'); ?></h2>
                                <span class="txt"><?= __('This page lists all gigs available in the database'); ?></span>
                             </div>
                            
                        </div>
                        <!-- Start .row -->       
<div class="row">
                            <!-- Start .row -->
                            <div class="col-lg-12">
                                <!-- col-lg-12 start here -->
                                <?= $this->Flash->render(); ?>
                                <div class="panel panel-default toggle panelClose panelRefresh">
                                    <!-- Start .panel -->
                                    <div class="panel-heading" id="addgigbutton">
                                        <h4 class="panel-title pull-left"><?= __('All Gigs'); ?></h4>
                                        <div class="pull-right" style="margin-top:6px;">
                                             <?php echo $this->Form->submit('New Gig',array('class'=>'btn btn-success' ,'id'=> 'idaddgigbutton')); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="panel-body" style="padding-top:4px; padding-bottom:4px; display: none;" id="addgig">
            <?php echo $this->Form->create('Job', array('type' => 'post', 'class'=> 'form-inline', 'id' => 'create_job'));?>
 <div class="alert alert-danger" id="error-message" style="display:none;"></div>
<div class="alert alert-success" id="success-message" style="display:none;"></div>
<div class="form-group">
        <div class="form-group">
            <span style="font-size:12px; display: block;">Title</span>
       <input type="text" name="title" id="title" class="form-control input-sm required" style="width:159px;">
    </div>
    <div class="form-group">
        <span style="font-size:12px; display: block;">Country</span>
        <select name="country" id="country" class="form-control" id="country">
        <?php

        foreach ($country as $value){ ?>
            <option value="<?= $value->country_code; ?>"><?= $value->country_name; ?></option>
        <?php } ?>
        </select>
        </div>
     <div class="form-group">
<span style="font-size:12px; display: block;">Company</span>
      <?php
                            $companyarray = array();
                                foreach ($companies as $company)
                                {
                                    $companyarray[$company->id] = $company->name;
                                }
                            echo $this->Form->select('company', $companyarray, array('id' => 'company','escape' => false,'class'=> 'form-control input-sm required'));
                        ?>
    </div>
    
  
    <div class="form-group" style="padding: 0 0; margin-top:15px;">
<button class="btn btn-success pull-right" id="save_job_ajax" disabled onclick="return false;" style="height:2em;">SAVE</button>
    </div>
                </div>
  <?php echo $this->Form->end() ?>
                                        </div>

                                    <div class="panel-body">
                                        
                                        <table id="basic-datatables-jobs" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="per5">
                                                        <?= __('ID'); ?>
                                                    </th>
                                                    <th class="per15"><?= __('Title'); ?></th>
                                                    <th class="per15"><?= __('Company'); ?></th>
                                                    <th class="per15"><?= __('Country'); ?></th>
                                                    <th class="per15"><?= __('Created date'); ?></th>
                                                    <!--<th class="per15">Delete</th>-->
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                                <?php 
                                            //    $i = 1;
                                                foreach ($jobs as $job): 
                                                   
                                                 ?>
                                                 <tr>
                                                 <td><?= $job->id ?></td>
                                                 <td><?= $job->title ?></td>
                                                 <td><?= $job['company']['name'] ?> </td>                                                                         <td><?= $job->country ?></td>
                                                 <td>
                                                 <?php echo date('Y-m-d h:i',strtotime($job->create_date )) ?>
                                                 </td>
                                          </tr>
                                        <?php
                                     //   $i++;
                                        endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- End .panel -->
                            </div>
                            <!-- col-lg-12 end here -->
                        </div>
</div>
<script>
                    var base_url = '<?php echo $base_url; ?>';
        </script>
</div>


        