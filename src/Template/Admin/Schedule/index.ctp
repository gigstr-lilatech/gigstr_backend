<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Schedule</h2>
        <span class="txt">This page lists all Schedule available in the database</span>
    </div>

</div>
<div class="pull-right" style="padding-bottom:10px;">
<?= $this->Html->link('Download', ['action' => 'export'],['class'=>'btn btn-success create-gig-button', 'style' => 'margin-right:5px;']) ?>
<?= $this->Html->link('Add Shift', ['action' => 'add'],['class'=>'btn btn-success create-gig-button']) ?>
</div>
<!-- Start .row -->       
<div class="row">
    <!-- Start .row -->
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
         <div class="panel-body" style="padding-top: 8px;padding-bottom: 0px;">
            <?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'search', 'class'=> 'form-inline'));?>
    <div class="form-group">
             <div class="form-group" style="padding: 0 0;">
      <span style="font-size:12px; display: block;">Company</span>
      <?php
                            $companyarray = array();
                            $companyarray["all"]= "All";
                                foreach ($companies as $company)
                                {
                                    $companyarray[$company->id] = $company->name;
                                }
                            echo $this->Form->select('company', $companyarray, array('id' => 'company','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px'));
                        ?>
    </div>
    <div class="form-group" id="gigselect" style="padding: 0 10px;">
       <span style="font-size:12px; display: block;">Gig</span>
      <?php
                            $searchgigarray = array();
                            $searchgigarray["all"]= "All";
                                foreach ($searchgigs as $searchgig)
                                {
                                    $searchgigarray[$searchgig->id] = $searchgig->title;
                                }
                            echo $this->Form->select('gig', $searchgigarray, array('id' => 'gig','escape' => false,'class'=> 'form-control input-sm','style'=>'width:160px'));
                        ?>
    </div>
    <div class="form-group" id="response" style="padding: 0 10px; display:none;">
    <input type="hidden" value="<?php echo isset($_POST["company"])?>">
    </div>
    <?php 
    if (isset($_POST["from"])) {
        $backdate = date('Y-m-d', strtotime($_POST["from"]));
    }
    else
    {
        $backdate = date('Y-m-d', strtotime(date("Y-m-d")));
    }
    
    if (isset($_POST["to"])) {
        $frontdate = date('Y-m-d', strtotime($_POST["to"]));
    }
    else
    {
        $frontdate =date('Y-m-d', strtotime(date("Y-m-d") . " +30 days"));
    }
        
        
    ?>
    <div class="form-group" style="padding: 0 0;">
    <span style="font-size:12px; display: block;">From</span>
    <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input id="from" name="from" type="text" value="<?php echo $backdate;?>" class="form-control input-sm" style="width:120px">
     </div>
    </div>
    <div class="form-group" style="padding: 0 10px;">
        <span style="font-size:12px; display: block;">To</span>
        <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input id="to" name="to" type="text" value="<?php echo $frontdate;?>" class="form-control input-sm" style="width:120px">
         </div>
    </div>
    <div class="form-group" style="padding: 0 0; margin-top:15px;">
        <div class="input-group">
           <?php echo $this->Form->submit('OK',array('class'=>'btn btn-success','style'=>"height: 2em;")); ?> 
        </div>
    </div>
  </div>
  <?php echo $this->Form->end() ?>
  </div>
            <div class="panel-body">
                <?= $this->Flash->render(); ?>
                <table id="basic-datatables-schedules" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="per5">ID</th>
                            <th class="per5">Shift From</th>
                            <th class="per5">Shift To</th>
                            <th class="per5">Gig</th>
                            <th class="per5">Shift Description</th>
                            <th class="per5">Gigstr</th>
                            <th class="per5">Check In</th>
                            <th class="per5">Check Out</th>
                            <th class="per5">Start</th>
                            <th class="per5">Stop</th>
                            <th class="per5">Comment</th>
                            <th class="per5">Attested</th>
                            <th class="per5">Delete</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php 
                        foreach ($schedules as $schedule):
                            ?>
                            <tr>

                                <td><?php echo $schedule->gigstr ?></td>
                                <td>
<?= $this->Form->text('from', ['class' => 'form-control required shiftfromdate', 'name'=> 'shift_from','value'=> date('Y-m-d H:i',strtotime($schedule->shift_from. " +2 hours")),'id' => 'from_'.$schedule->id,'required' => false, 'style'=> 'width:10em;','onkeydown'=>'return false;']) ?>
</td>
                        <td>

<?= $this->Form->text('to', ['class' => 'form-control required shiftfromdate', 'name'=> 'shift_to','value'=> date('Y-m-d H:i',strtotime($schedule->shift_to. " +2 hours")),'id' => 'fromtime_'.$schedule->id,'required' => false, 'style'=> 'width:10em;', 'onkeydown'=>'return false;']) ?>
</td>
                                <td>
                                    
                                    <select class="gig_id form-control" name="gig_id" id="<?php echo 'title_'.$schedule->id ?>" style='width:10em;'>
   <?php
                                foreach ($gigs as $gig)
                                {
                                   
                                     ?>   
                                      <option <?php if (($gig->id == $schedule['job']['id'])) { ?>selected="true" <?php }; ?>value="<?php echo $gig->id;?>"><?php echo $gig->title;?></option>
                                            <?php  
                                    }
                                
                        ?>
                                    </select>
</td>
<?php 
if(strlen($schedule->shift_description)> 6)
{
$description = substr(strip_tags($schedule->shift_description),0,6).'...';
}
else
{
    $description = $schedule->shift_description;
}
?>
    <td><a href="#my_modal" data-toggle="modal" data-description='<?php echo $schedule->shift_description;?>' data-name="shift_description" data-id="<?php echo $schedule->id;?>"><?php echo $description; ?></a>
</td>
                                <td>
<input id="<?php echo 'gigstr_'.$schedule->id ?>" class="form-control autocomplete" name="gigstr" value="<?php echo $schedule['user']['name']?>" style="width:10em;">
</td>
                                <td style="white-space: nowrap;">
                                    <?php
                                        if ($schedule->checkin == null) {
                                            $checkin = "";
                                        } else {
                                            $checkin = date('Y-m-d H:i', strtotime($schedule->checkin . " +2 hours"));
                                        }
                                        ?>
<?php echo $checkin;?>
</td>
                                <td style="white-space: nowrap;">
                                    <?php
                                        if ($schedule->checkout == null) {
                                            $checkout = "";
                                        } else {
                                            $checkout = date('Y-m-d H:i', strtotime($schedule->checkout. " +2 hours"));
                                        }
                                        ?>
<?php echo $checkout;?>
</td>
                                <td>
                                       <?php
                                        if ($schedule->start == null) {
                                            $schedulestart = "";
                                        } else {
                                            $schedulestart = date('Y-m-d H:i', strtotime($schedule->start. " +2 hours"));
                                        }
                                        ?>
<?= $this->Form->text('start', ['class' => 'form-control required shiftfromdate', 'name'=> 'start','value'=> $schedulestart,'id' => 'start_'.$schedule->id,'required' => false,'style'=> 'width:10em;','onkeydown'=>'return false;']) ?>
</td>
                                <td>
                                    <?php
                                        if ($schedule->stop == null) {
                                            $stop = "";
                                        } else {
                                            $stop = date('Y-m-d H:i', strtotime($schedule->stop. " +2 hours"));
                                        }
                                        ?>
<?= $this->Form->text('stop', ['class' => 'form-control required shiftfromdate', 'name'=> 'stop','value'=> $stop,'id' => 'stop_'.$schedule->id,'required' => false,'style'=> 'width:10em;','onkeydown'=>'return false;']) ?>
</td>
                                <td>
                                    <?php 
if(strlen($schedule->comment)> 6)
{
$comment = substr(strip_tags($schedule->comment),0,6).'...';
}
else
{
    $comment = $schedule->comment;
}
?>
<a href="#my_modal" data-toggle="modal" data-description='<?php echo $schedule->comment;?>' data-name="comment" data-id="<?php echo $schedule->id;?>"><?php echo $comment; ?></a>
                                </td>
                                <td>
<input type="checkbox" id="<?php echo 'attest_'.$schedule->id; ?> "name="attest" <?php if (( $schedule->attested == '1')) { ?>checked="checked" <?php }; ?> class="attestcheck">
</td>
                                <td><?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $schedule->id], ['confirm' => __('Are you sure you want to delete this shift?', $schedule->id)]) ?></td>
                            </tr>
                           
                        <?php 
                        endforeach; ?>
                            
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
</div></div>


<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-hidden="true">

                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel2">Shift Description</h4>
                                </div>
                <?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'savedescription', 'class'=> 'form-inline'));?>
                                <div class="modal-body">
                                 <?php echo $this->Form->textarea('summernote', ['class' => 'form-control', 'id' => 'summernote', 'required' => false]); ?>
                                <input type="hidden" name="scheduleid" value=""/>
                                <input type="hidden" name="schedulename" value=""/>
                                </div>
                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
<?php echo $this->Form->end() ?>
                            </div>
                        </div>

    <div class="modal fade" id="mySmallModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title" id="mySmallModalLabel">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                        est laborum.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
 