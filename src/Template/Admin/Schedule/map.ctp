<script>
/*
setTimeout(function(){
   window.location.reload(1);
}, 60000);
*/
</script>
<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">

</div>
<!-- Start .row -->       
<div class="row">

<?= $this->Html->script('https://maps.google.com/maps/api/js?key=AIzaSyBliNlxy25k799ESODTwkLmaEPWkp9_ic0'); ?>

<?php

     $map_options = array(
        'id' => 'map_canvas',
        'width' => '100%',
        'height' => '100vh',
        'style' => '',
        'zoom' => 5,
        'type' => 'ROADMAP',
        'custom' => null,
        'localize' => false,
        'latitude' => '59.3233892',
        'longitude' => '18.0778182',
        'marker' => false,
        'infoWindow' => false,
     );

    echo $this->GoogleMap->map($map_options);

    $count = 1;
    foreach($locations as $location) {

        $checkindate = new DateTime($location['checkin'], new DateTimeZone('UTC'));
        $checkindate->setTimezone(new DateTimeZone('Europe/Stockholm'));
        $checkin = $checkindate->format('Y-m-d H:i');

        date_default_timezone_set('Europe/Stockholm');
        $now = date('Y-m-d h:i:s') ;
        $checkingDate = $checkindate->format('Y-m-d H:i:s');

        $timeDiff = round((strtotime($now) - strtotime($checkingDate))/3600, 1);

        if($timeDiff <= 24) {

            $map_arr = array(
                'markerTitle' => $location['name'],
                'markerIcon' => 'img/checkin.png',
                'markerShadow' => '',
                'infoWindow' => true,
                'windowText' => '<div class="maptxt"><b>' . $checkin . '</b><p>Gig: ' . $location['title'] . '</p><p>Gigstr: ' . $location['name'] . '</p></div>',
                'draggableMarker' => false
            );

            echo $this->GoogleMap->addMarker("map_canvas", $count,
                array("latitude" => $location['checkin_lat'], "longitude" => $location['checkin_long']), $map_arr);

            $count ++;

        }
    }
?>
</div>
 