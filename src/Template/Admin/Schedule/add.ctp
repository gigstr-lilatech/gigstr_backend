<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Create Shift</h2>
        <span class="txt">This page allows user to create a shift</span>
    </div>

</div>
<!-- Start .row -->
<div class="row">
    <div class="col-lg-6 col-md-6">
        <!-- col-lg-6 start here -->

        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Shift Information</h4>
            </div>
            <div class="panel-body">
                <?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'add', 'class'=> 'form-horizontal add_shift','id' => 'form'));?>
                <?= $this->Flash->render(); ?>
<?php $companyid= isset($_POST['company'])?>

                <div class="form-group">
                    <label for="email" class="col-sm-12 control-label">Company</label>
                    <div class="col-sm-12">

                         <select class='form-control' name="company" id="company">

<option value=''>Please select a company</option>
   <?php
                      foreach ($companies as $company) { ?>
<option <?php if ((isset($_POST['company']))  && ($_POST['company'] == $company->id)) { ?>selected="true" <?php }; ?>value="<?php echo $company->id;?>"><?php echo $company->name;?></option>
<?php
                                }
                        ?>
                                    </select>
                    </div>
                </div>
<?php if(isset($_POST['gig']))
{ 
?>
<div class="form-group">
                    <label for="email" class="col-sm-12 control-label">Gig</label>
                    <div class="col-sm-12">

                         <select class='form-control' name="gig" id="gig">
                                <option value=''>Please select a company</option>
                    <?php   foreach ($gigs as $gig) { ?>
                            <option <?php if ((isset($_POST['gig']))  && ($_POST['gig'] == $gig->id)) { ?>selected="true" <?php }; ?>value="<?php echo $gig->id;?>"><?php echo $gig->title;?></option>

                    <?php    }?>
                                    </select>
                    <span class="errormsg" id="gigError" style="display: none;">Please select a Gig</span>
                    </div>
                </div>
<?php } ?>

                <div class="form-group">
                    <div class="col-sm-12" id="response">
                        <span>Gig</span>
                        <select name='gig' class='form-control' id='gigId'>
                        </select>
                    </div>
                </div>
                <div class="form-group">
<label for="email" class="col-sm-12 control-label">Gigstr (Optional)</label>
<div class="col-sm-12">
                    <input id="tags" class="form-control">
<input type="hidden" id="candidate" name="candidate" >
</div>
                </div>

                <input type="hidden" id="dataShift" name="dataShift"/>

                <div class="form-group">
                </div>
                
                <div class="form-group">

                    <div class="col-sm-6">
                       <label for="email">Shift Start Date</label>
        <div class="input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input id="from" name="from" type="text" class="form-control" aria-required="false" value="<?php echo isset($_POST['from']) ? $_POST['from'] : '' ?>" required>
            <br/>

        </div>
                </div>
                    <div class="col-md-6">
<label for="email">Shift Start Time</label>
                        <div class="input-group bootstrap-timepicker">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            <input id="from_time" name="from_time" type="text" class="form-control" value="<?php echo isset($_POST['from_time']) ? $_POST['from_time'] : '' ?>" required>
<span id="errormassagefromtime" style="display: none;">Please enter start time less than end time</span>                                                    
</div>
                    </div>
                </div>

                <div class="form-group">
                </div>
                
                <div class="form-group">

                    <div class="col-sm-6">
                                 <label for="email">Shift End Date</label>
    <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input id="to" name="to" type="text" class="form-control" value="<?php echo isset($_POST['to']) ? $_POST['to'] : '' ?>" required>
            <span id="errormassage" style="display: none;">Please enter shift start date</span>
            <span id="errormassageto" style="display: none;">End Date/time must be later than Start Date/time</span>
     </div>
                    </div>
            <div class="col-md-6">
                <label for="email">Shift End Time</label>
                <div class="input-group bootstrap-timepicker">
                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    <input id="to_time" name="to_time" type="text" class="form-control" value="<?php echo isset($_POST['to_time']) ? $_POST['to_time'] : '' ?>" required>
</div>
            </div>
        </div>

                <div class="form-group">
                    <label for="editor" class="col-sm-12 control-label">Shift Description</label>
                    <div class="col-sm-12">
                        <?php  echo $this->Form->textarea('description', ['class' => 'form-control', 'id' => 'editor', 'required' => false]); ?>

<!--                        <textarea name="description" class="form-control" id="editor"></textarea>-->
                    </div>
                </div>



                <?= $this->Form->hidden('created_by', ['value' => $user_id]); ?>
                <div class="form-group  row">
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">
                    <?php echo $this->Form->submit('ADD SHIFT',array('class'=>'btn btn-success pull-right test add_shift')); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!--<div class="alert alert-success" id="success-message" style="display:none;">Shift added successfully.</div>-->
                        <div  id="success-message"></div>
                        <p id="shift_count"></p>
                    </div>
                </div>
                <?php echo $this->Form->end() ?>

               
                <script>
                    var base_url = '<?php echo $base_url; ?>';
                </script>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Gig Template</h4>
            </div>
            <div class="panel-body">
                <input type="hidden" name="gig_template_id" id="gig_template_id">
                <p id="gig_template_msg">Please select a gig to start editing the Gig Template.</p>
                <div id="dragndrop_outer">
                    <div id="dragndrop"></div>
                    <button class="btn btn-success mr5 mb10" type="button" id="gig_template">Save Gig Template</button>
                </div>
                <div id="gig_err"></div>
            </div>
        </div>
    </div>
    <!-- End .panel -->
</div>
<script>
                    var base_url = '<?php echo $base_url; ?>';
        </script>
</div>
<!-- End .row -->