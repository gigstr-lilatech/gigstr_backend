<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Schedule Map</h2>
        <span class="txt">This page show all checking Schedule in map</span>
    </div>

</div>
<!-- Start .row -->
<div class="row">


<?= $this->Html->script('https://maps.googleapis.com/maps/api/js?key=AIzaSyBliNlxy25k799ESODTwkLmaEPWkp9_ic0'); ?>

<?php

$map_options = array(
    'id' => 'map_canvas',
    'width' => '100%',
    'height' => '100vh',
    'style' => '',
    'zoom' => 12,
    'type' => 'ROADMAP',
    'custom' => null,
    'localize' => false,
    'latitude' => $locations['checkin_lat'],
    'longitude' => $locations['checkin_long'],
    'marker' => false,
    'draggableMarker' => false
  );
?>
<?= $this->GoogleMap->map($map_options); ?>

<?php

    $checkindate = new DateTime($locations['checkin'], new DateTimeZone('UTC'));
    $checkindate->setTimezone(new DateTimeZone('Europe/Stockholm'));
    $checkin = $checkindate->format('Y-m-d H:i');
    //$checkin = date('Y-m-d H:i', strtotime($locations['checkin']));

    $checkoutdate = new DateTime($locations['checkout'], new DateTimeZone('UTC'));
    $checkoutdate->setTimezone(new DateTimeZone('Europe/Stockholm'));
    $checkout = $checkoutdate->format('Y-m-d H:i');
    //$checkout = date('Y-m-d H:i', strtotime($locations['checkout']) );

    $checkin_arr = array(
            'markerTitle' => $locations['name'],
            'markerIcon' => 'img/checkin.png',
            'markerShadow' => '',
            'infoWindow' => true,
            'windowText' => '<div class="maptxt"><b>' . $checkin . '</b><p>Gig: '. $locations['title'] .'</p><p>Gigstr: ' . $locations['name'] . '</p></div>',
            'draggableMarker' => false
          );

    echo $this->GoogleMap->addMarker("map_canvas", 1, array("latitude" => $locations['checkin_lat'], "longitude" => $locations['checkin_long']), $checkin_arr);

    $map_arr = array(
        'markerTitle' => $locations['name'],
        'markerIcon' => 'img/checkout.png',
        'markerShadow' => '',
        'infoWindow' => true,
        'windowText' => '<div class="maptxt"><b>' . $checkout . '</b><p>Gig: '. $locations['title'] .'</p><p>Gigstr: ' . $locations['name'] . '</p></div>',
        'draggableMarker' => false
      );

    echo $this->GoogleMap->addMarker("map_canvas", 2, array("latitude" => $locations['checkout_lat'], "longitude" => $locations['checkout_long']), $map_arr);

?>

 </div>