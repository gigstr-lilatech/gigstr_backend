<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Schedule</h2>
        <span class="txt">This page lists all shifts available in the database</span>
    </div>

</div>
<div class="pull-right" style="padding-bottom:10px;">
<?php
    if($page == 'search') {
    if (isset($_GET["unassigned"])) {
        $unassigine = $_GET["unassigned"];
    } else {
    $unassigine = '';
    }

    $url = 'company=' . $_GET["company"] . '&gig=' . $_GET["gig"] . '&gigstr=' . $_GET["gigstr"] . '&from=' . $_GET["from"] . '&to=' . $_GET["to"] . '&unassigned=' . $unassigine;
?>
<a class="btn btn-success create-gig-button" style="margin-right:5px;" href="/admin/schedule/export?<?=$url ?>">Download</a>

<?php } else { ?>

<?= $this->Html->link('Download', ['action' => 'export'],['class'=>'btn btn-success create-gig-button', 'style' => 'margin-right:5px;']) ?>
<?php } ?>


<?= $this->Html->link('Add Shift', ['action' => 'add'],['class'=>'btn btn-success create-gig-button']) ?>
</div>
<!-- Start .row -->       
<div class="row schedule-row">
    <!-- Start .row -->
    <div class="col-lg-12 wr-schedule-data-table">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
         <div class="panel-body" style="padding-top: 8px;padding-bottom: 0px;">
            <?php echo $this->Form->create('Schedule', array('type' => 'get', 'action' => 'search', 'class'=> 'form-inline'));?>
    <div class="form-group">
             <div class="form-group" style="padding: 0 0;">
      <span style="font-size:12px; display: block;">Company</span>
      <?php

                    $companyarray = array();
                    $companyarray["all"]= "All";
                      foreach ($companies as $company)
                      {
                          $companyarray[$company->id] = $company->name;
                      }

                          if(isset($_GET["company"])) {
                            echo $this->Form->select('company', $companyarray, array('id' => 'company','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px', 'value'=>$_GET["company"]));
                            }
                            else {
                            echo $this->Form->select('company', $companyarray, array('id' => 'company','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px'));
                            }


                        ?>
    </div>
    <div class="form-group" id="gigselect" style="padding: 0 10px;">
       <span style="font-size:12px; display: block;">Gig</span>
      <?php

        $searchgigarray = array();
        $searchgigarray["all"]= "All";
            foreach ($searchgigs as $searchgig)
            {
                $searchgigarray[$searchgig->id] = $searchgig->title;
            }
                       if(isset($_GET["gig"])) {
                            echo $this->Form->select('gig', $searchgigarray, array('id' => 'gig','escape' => false,'class'=> 'form-control input-sm','style'=>'width:160px', 'value'=>$_GET["gig"]));
                            }
                       else {
                       echo $this->Form->select('gig', $searchgigarray, array('id' => 'gig','escape' => false,'class'=> 'form-control input-sm','style'=>'width:160px'));
                       }
                        ?>
    </div>

    <div class="form-group" id="response" style="padding: 0 10px; display:none;">
    <input type="hidden" value="<?php echo isset($_POST["company"])?>">
    </div>

    <div class="form-group" id="gigselect" style="padding: 0 10px 0 0;">
            <span style="font-size:12px; display: block;">Gigstr</span>
            <div class="input-group">
            <?php if (isset($_GET["from"]) && !empty($_GET["gigstr"])) { ?>
                    <input id="gigstr" name="gigstr" type="text" class="form-control input-sm" style="width:160px" value="<?php echo $_GET["gigstr"]; ?>">
              <?php } else {?>
              <input id="gigstr" name="gigstr" type="text" class="form-control input-sm" style="width:160px">
              <?php }?>
             </div>
        </div>


    <?php
    if (isset($_GET["from"])) {
        $backdate = date('Y-m-d', strtotime($_GET["from"]));
    }
    elseif($this->request->session()->read('Config.from') != null && ($this->request->params['controller'] == "Schedule" && $this->request->params['action'] != "allschedule"))
    {
        $backdate = date('Y-m-d', strtotime($this->request->session()->read('Config.from')));
    }
    else
    {
        $backdate = date('Y-m-d', strtotime(date("Y-m-d")));
    }

    if (isset($_GET["to"])) {
        $frontdate = date('Y-m-d', strtotime($_GET["to"]));
    }
    elseif($this->request->session()->read('Config.to') != null && ($this->request->params['controller'] == "Schedule" && $this->request->params['action'] != "allschedule"))
    {
        $frontdate = date('Y-m-d', strtotime($this->request->session()->read('Config.to')));
    }
    else
    {
        $frontdate =date('Y-m-d', strtotime(date("Y-m-d") . " +30 days"));
    }

    ?>
    <div class="form-group" style="padding: 0 0;">
    <span style="font-size:12px; display: block;">From</span>
    <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input id="from" name="from" type="text" value="<?php echo $backdate;?>" class="form-control input-sm" style="width:120px">
     </div>
    </div>
    <div class="form-group" style="padding: 0 10px;">
        <span style="font-size:12px; display: block;">To</span>
        <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input id="to" name="to" type="text" value="<?php echo $frontdate;?>" class="form-control input-sm" style="width:120px">
         </div>
    </div>
    <div class="form-group">
        <span style="font-size:12px; display: block;">Unassigned</span>
        <div class="input-group">
        <?php if (isset($_GET["unassigned"])) { ?>
        <input id="unassigned" name="unassigned" type="checkbox" checked class="form-control input-sm">
        <?php } else { ?>
              <input id="unassigned" name="unassigned" type="checkbox" class="form-control input-sm">
              <?php } ?>
        </div>
    </div>

    <div class="form-group" style="padding: 0 0; margin:15px 0 0 12px;">
        <div class="input-group">
           <?php echo $this->Form->submit('OK',array('class'=>'btn btn-success','style'=>"height: 2em;")); ?> 
        </div>
    </div>
  </div>
  <?php echo $this->Form->end() ?>
  </div>
            <div class="panel-body schedule_panel">


                <ul class="pagination pagi">
                        <?php
                            echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                            echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));
                        ?>
                  </ul>


 <div class="alert alert-danger" id="error-message" style="display:none;"></div>
<div class="alert alert-success" id="datetextbox" style="display:none;">Date has been saved.</div>
<div class="alert alert-success" id="gigtextbox" style="display:none;">Gig has been saved.</div>
<div class="alert alert-success" id="candidatetextbox" style="display:none;">Gigster has been saved.</div>
<div class="alert alert-success" id="gigsterremovetextbox" style="display:none;">Selected shift(s) deleted.</div>
<div class="alert alert-success" id="descriptiontextbox" style="display:none;">Saved successfully.</div>
<div class="alert alert-success" id="error-message" style="display:none;">Error.</div>
                <?=  $this->Flash->render(); ?>
                <div  id="schedule_table">
                <table id="basic-datatables-schedules" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="per5">ID</th>
                            <th class="per5">Job Offer ID</th>
                            <th class="per5">Shift From</th>
                            <th class="per5">Shift To</th>
                            <th class="per5">Gig</th>
                            <th class="per5">Shift Description</th>
                            <th class="per5">Gigstr</th>
                            <th class="per5">Check In</th>
                            <th class="per5">Check Out</th>
                            <th class="per5">Start</th>
                            <th class="per5">Stop</th>
                            <th class="per5">Comment</th>
                            <th class="per5">Attested</th>
                            <th class="per5">Not work</th>
                            <th class="per5">
                            <a style="background-color: #d7d7d7;" class="btn btn-default create-gig-button" id="multi_delete" href="javascript:;">Delete</a>
                            </th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $cnt = 1;
                        foreach ($schedules as $schedule):
                            if ($cnt < 7) {
                                $trclass= "class='top_datepicker'";
                            }
                            else {
                                $trclass = "class='bottom_datepicker'";
                            }
                            ?>
                            <tr <?php echo $trclass; ?>>

                                <td id="<?php echo 'idgigstr_'.$schedule->id;?>">
                                <a style="text-decoration: underline;" href="<?php echo $this->request->webroot . 'admin/candidate/profile/' . $schedule->gigstr; ?>">
                                <?php echo $schedule->gigstr; ?>
                                </a>
                                </td>

                                <td class="p-relative">
                                    <?php  echo ($schedule->job_offer_id !=0 ? $schedule->job_offer_id : ''); ?>
                                </td>

                                <td class="p-relative">
                                    <?php 
                                    $shift_from = new DateTime($schedule->shift_from, new DateTimeZone('UTC'));
$shift_from->setTimezone(new DateTimeZone('Europe/Stockholm'));
?>
<?= $this->Form->text('from', ['class' => 'form-control required shiftfromdate', 'name'=> 'shift_from','value'=> $shift_from->format('Y-m-d H:i'),'id' => 'from_'.$schedule->id,'required' => false, 'style'=> 'width:10em;','onkeydown'=>'return false;']) ?>
</td>
<?php 
                                    $shift_to = new DateTime($schedule->shift_to, new DateTimeZone('UTC'));
$shift_to->setTimezone(new DateTimeZone('Europe/Stockholm'));
?>
                        <td class="p-relative">

<?= $this->Form->text('to', ['class' => 'form-control required shiftfromdate', 'name'=> 'shift_to','value'=> $shift_to->format('Y-m-d H:i'),'id' => 'fromtime_'.$schedule->id,'required' => false, 'style'=> 'width:10em;', 'onkeydown'=>'return false;']) ?>
</td>
                                <td>
                                    
                                    <select class="gig_id form-control" name="gig_id" id="<?php echo 'title_'.$schedule->id ?>" style='width:10em;'>
   <?php
                                foreach ($gigs as $gig)
                                {
                                   
                                     ?>   
                                      <option <?php if (($gig->id == $schedule['job']['id'])) { ?>selected="true" <?php }; ?>value="<?php echo $gig->id;?>"><?php echo $gig->title;?></option>
                                            <?php  
                                    }
                                
                        ?>
                                    </select>
</td>
<?php 
if(strlen($schedule->shift_description)> 6)
{
$description = substr(strip_tags($schedule->shift_description),0,6).'...';
}
else
{
    $description = "...";
}
?>
    <td ><a href="#my_modal" style="line-height: 34px;" id="<?php echo 'iddescription_'.$schedule->id;?>" data-toggle="modal" data-type='description' data-name="shift_description" data-id="<?php echo $schedule->id;?>" data-from="<?php echo $backdate; ?>" data-to="<?php echo $frontdate; ?>"><?php echo $description; ?></a>
</td>
                                <td>
<input id="<?php echo 'gigstr_'.$schedule->id ?>" class="form-control autocomplete" name="gigstr" value="<?php echo $schedule['user']['name']?>" style="width:10em;">
</td>
                                <td style="white-space: nowrap;">
                                    <?php
                                        if ($schedule->checkin == null) {
                                            $checkin = "";
                                        } else {
                                            $checkindate = new DateTime($schedule->checkin, new DateTimeZone('UTC'));
                                            $checkindate->setTimezone(new DateTimeZone('Europe/Stockholm'));
                                            $checkin = $checkindate->format('Y-m-d H:i');
                                        }
                                        ?>
<span style="line-height: 34px;"><a href="/admin/schedule/checkinmap?id=<?php echo $schedule->id; ?>"><?php echo $checkin;?></a></span>
</td>
                                <td style="white-space: nowrap;">
                                    <?php
                                        if ($schedule->checkout == null) {
                                            $checkout = "";
                                        } else {
                                            $checkoutdate = new DateTime($schedule->checkout, new DateTimeZone('UTC'));
                                            $checkoutdate->setTimezone(new DateTimeZone('Europe/Stockholm'));
                                            $checkout = $checkoutdate->format('Y-m-d H:i');
                                        }
                                        ?>
<span style="line-height: 34px;"><a href="/admin/schedule/checkinmap?id=<?php echo $schedule->id; ?>"><?php echo $checkout;?></a></span>
</td>
                                <td class="p-relative">
                                       <?php
                                        if ($schedule->start == null) {
                                            $schedulestart = "";
                                        } else {
                                            $schedulestartdate = new DateTime($schedule->start, new DateTimeZone('UTC'));
                                            $schedulestartdate->setTimezone(new DateTimeZone('Europe/Stockholm'));
                                            $schedulestart = $schedulestartdate->format('Y-m-d H:i');
                                        }
                                        ?>
<?= $this->Form->text('start', ['class' => 'form-control required shiftfromdate', 'name'=> 'start','value'=> $schedulestart,'id' => 'start_'.$schedule->id,'required' => false,'style'=> 'width:10em;','onkeydown'=>'return false;']) ?>
</td>
                                <td class="p-relative">
                                    <?php
                                        if ($schedule->stop == null) {
                                            $stop = "";
                                        } else {
                                            $stopdate = new DateTime($schedule->stop, new DateTimeZone('UTC'));
                                            $stopdate->setTimezone(new DateTimeZone('Europe/Stockholm'));
                                            $stop = $stopdate->format('Y-m-d H:i');
                                        }
                                        ?>
<?= $this->Form->text('stop', ['class' => 'form-control required shiftfromdate', 'name'=> 'stop','value'=> $stop,'id' => 'stop_'.$schedule->id,'required' => false,'style'=> 'width:10em;','onkeydown'=>'return false;']) ?>
</td>
                                <td>
                                    <?php 
if(strlen($schedule->comment)> 6)
{
$comment = substr(strip_tags($schedule->comment),0,6).'...';
}
else
{
    //$comment = $schedule->comment;
    $comment = "...";
}
?>
<a href="#my_modal" style="line-height: 34px;" id="<?php echo 'idcomment_'.$schedule->id;?>" data-toggle="modal" data-type='comment' data-name="comment" data-id="<?php echo $schedule->id;?>" data-from="<?php echo $backdate; ?>" data-to="<?php echo $frontdate; ?>"><?php echo $comment; ?></a>
                                </td>
                                <td>
                                    <input type="checkbox" id="<?php echo 'attest_'.$schedule->id; ?>" name="attest" <?php echo (($schedule->attested == '1') ? 'checked="checked"' : '');?> class="attestcheck" onclick="attestCheck(<?php echo $schedule->id;?>, 'attested');">
                                </td>

                                <td>
                                    <input type="checkbox" id="<?php echo 'not_work_'.$schedule->id; ?>" name="not_work" <?php echo (($schedule->not_work == 1) ? 'checked="checked"' : '');?> class="attestcheck" onclick="attestCheck(<?php echo $schedule->id;?>, 'not_work');">
                                </td>

                                <td>
                      <input type="checkbox" name="schedule_delete" class="" value="<?= $schedule->id ?>">
                                </td>
                            </tr>
                           
                        <?php
                        if($cnt == 31) {
                        $cnt = 0;
                        } else {
                            $cnt ++;
                        }

                        endforeach; ?>
                            
                    </tbody>
                </table>
</div>
                <ul class="pagination pagi">
                                        <?php
                                            echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                                            echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                                        ?>
                                  </ul>

            </div>
        </div>
        <!-- End .panel -->
    </div>
<script>
                    var base_url = '<?php echo $base_url; ?>';
        </script>
    <!-- col-lg-12 end here -->
</div>
</div></div>


<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-hidden="true">

                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title" id="descLabel"></h4>
                                </div>
                <?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'savedescription', 'class'=> 'form-inline'));?>
                                <div class="modal-body">
                                 <?php echo $this->Form->textarea('summernote', ['class' => 'form-control', 'id' => 'editor', 'required' => false]); ?>
                                <input type="hidden" id="scheduleid" name="scheduleid" value=""/>
                                <input type="hidden" id="schedulename" name="schedulename" value=""/>
                                </div>
                
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" id="btnadd" class="btn btn-primary">Save changes</button>
                                </div>
<?php echo $this->Form->end() ?>
                            </div>
                        </div>

   