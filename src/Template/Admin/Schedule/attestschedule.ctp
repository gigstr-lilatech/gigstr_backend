<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Attest</h2>
        <span class="txt">This page show all checking Schedule in map</span>
    </div>

</div>
<!-- Start .row -->
<!-- Start .row -->
<div class="row schedule-row">
    <!-- Start .row -->
    <div class="col-lg-12 wr-schedule-data-table">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">

     <div class="panel-body" style="padding-top: 8px;padding-bottom: 0px;">
        <?php echo $this->Form->create('Schedule', array('type' => 'get', 'action' => 'attestSearch', 'class'=> 'form-inline'));?>
        <div class="form-group">

            <div class="form-group" style="padding: 0 0;">
                 <span style="font-size:12px; display: block;">Country</span>
                 <?php
                     $countryArray["all"]= "All";
                     foreach ($countries as $country) {
                         $countryArray[$country['country_code']] = $country['country_name'];
                     }

                    if(isset($_GET["country"])) {
                        echo $this->Form->select('country', $countryArray, array('id' => 'country','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px', 'value'=>$_GET["country"]));
                    }
                    else {
                        echo $this->Form->select('country', $countryArray, array('id' => 'country','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px'));
                    } ?>
            </div>

            <div class="form-group" style="padding: 0 0 0 10px;">
                  <span style="font-size:12px; display: block;">Company</span>
                  <?php
                $companyArray["all"]= "All";
                foreach ($companies as $company) {
                      $companyArray[$company->id] = $company->name;
                  }
                  if(isset($_GET["company"])) {
                      echo $this->Form->select('company', $companyArray, array('id' => 'company','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px', 'value'=>$_GET["company"]));
                      }
                      else {
                      echo $this->Form->select('company', $companyArray, array('id' => 'company','escape' => false,'class'=> 'form-control input-sm', 'style'=>'width:160px'));
                  }
                ?>
            </div>

            <div class="form-group" id="gigselect" style="padding: 0 10px;">
                   <span style="font-size:12px; display: block;">Gig</span>
                <?php
                $searchGigArray["all"]= "All";
                foreach ($searchGigs as $searchgig) {
                    $searchGigArray[$searchgig->id] = $searchgig->title;
                }
                    if(isset($_GET["gig"])) {
                            echo $this->Form->select('gig', $searchGigArray, array('id' => 'gig','escape' => false,'class'=> 'form-control input-sm','style'=>'width:160px', 'value'=>$_GET["gig"]));
                    } else {
                       echo $this->Form->select('gig', $searchGigArray, array('id' => 'gig','escape' => false,'class'=> 'form-control input-sm','style'=>'width:160px'));
                    }
                ?>
            </div>
            <div class="form-group" id="response" style="padding: 0 10px; display:none;">
            <input type="hidden" value="<?php echo isset($_POST["company"])?>">
            </div>

            <div class="form-group" id="gigselect" style="padding: 0 10px 0 0;">
                <span style="font-size:12px; display: block;">Gigstr</span>
                <div class="input-group">
                <?php if (isset($_GET["gigstr"])) { ?>

                      <select name="gigstr"  class="form-control select2-minimum" id="gigstrList" style="width:160px">
                            <?php
                             foreach($userArr as $key => $user) {
                              echo '<option value="'. $key . '" '. ($_GET["gigstr"] ==  $key ? "selected" : "").'>'. $user .'</option>';
                             }
                            ?>
                        </select>

                  <?php }

                  else {?>
                  <select name="gigstr" class="form-control select2-minimum" id="gigstrList" style="width:160px">
                      <?php
                       foreach($userArr as $key => $user) {
                        echo '<option value="'. $key . '">'. $user .'</option>';
                       }
                      ?>
                  </select>

                       <?php
                      // echo $this->Form->select('gigstr', $userArr, array('id' => 'gigstrList','escape' => false,'class'=> 'form-control select2-minimum','style'=>'width:160px'));
?>

                  <?php }?>
                 </div>
            </div>

            <div class="form-group">
                <span style="font-size:12px; display: block;">Attested</span>
                <div class="input-group">
                <?php if (isset($_GET["attested"])) { ?>
                <input id="attested" name="attested" type="checkbox" checked class="form-control input-sm">
                <?php } else { ?>
                      <input id="attested" name="attested" type="checkbox" class="form-control input-sm">
                      <?php } ?>
                </div>
            </div>

            <div class="form-group" style="padding: 0 0; margin:15px 0 0 12px;">
                <div class="input-group">
                   <?php echo $this->Form->submit('OK',array('class'=>'btn btn-success','style'=>"height: 2em;")); ?>
                </div>
            </div>

        </div>
        <?php echo $this->Form->end() ?>
     </div>

    <div class="panel-body">

    <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));
            ?>
    </ul>
<div class="alert alert-success" id="gigsterremovetextbox" style="display:none;">Selected Shift Updated.</div>
    <div  id="attest_table">
        <table id="basic-datatables-attest" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="per5 text-center">Date</th>
                <th class="per5 text-center">Gig</th>
                <th class="per5 text-center">Gigstr</th>
                <th class="per5 text-center">Hours</th>
                <th class="per5 text-center">Shift Data</th>
                <th class="per5 text-center">Gigstr Rating</th>
                <th class="per5 text-center">Success Rating</th>
                <th class="per5 text-center"><a class="btn btn-success" id="multi_attest" href="javascript:;">Attest</a></th>
            </tr>
        </thead>

        <tbody>
        <?php  foreach ($schedules as $schedule) {
        //var_dump($schedule);die;
        ?>
            <tr>
                <td class="text-center">
                    <?php
                    $shift_from = new DateTime($schedule->shift_from, new DateTimeZone('UTC'));
                    $shift_from->setTimezone(new DateTimeZone('Europe/Stockholm'));
                    echo $shift_from->format('Y-m-d');
                    ?>
                </td>
                <td data-html="true" data-toggle="tooltip" data-placement="bottom" title="<?php echo str_replace('"', "'", $schedule->shift_description); ?>">
                    <?php echo $schedule->job->title; ?>
                </td>
                <td>
                    <a href="<?php echo $base_url . 'admin/candidate/profile/'. $schedule->gigstr; ?>">
                        <?php echo $schedule->user->name;?>
                    </a>
                </td>

                <!-- Cell Red -->
                <?php
                if(($schedule->shift_from != $schedule->start) || ($schedule->shift_to != $schedule->stop) ) {
                $notMatch = 'red';
                } else {
                    $notMatch = '';
                }

                $startSch = new DateTime($schedule->start, new DateTimeZone('UTC'));
                $startSch->setTimezone(new DateTimeZone('Europe/Stockholm'));
                $startTime = $startSch->format('H:i');
                $start = $startSch->format('Y-m-d H:i');

                $endSch = new DateTime($schedule->stop, new DateTimeZone('UTC'));
                $endSch->setTimezone(new DateTimeZone('Europe/Stockholm'));
                $endTime = $endSch->format('H:i');
                $stop = $endSch->format('Y-m-d H:i');
                ?>

                <td id="shiftHours-<?= $schedule->id; ?>"  class="<?php echo $notMatch;?> my_tooltip text-center">
                    <a class="tooltip_pop"   id="timeDiff-<?= $schedule->id; ?>" href="#shift_report" data-toggle="modal" data-from="<?= $schedule->from_popup; ?>" data-to="<?= $schedule->to_popup; ?>" data-checkin="<?= $schedule->checkin_popup; ?>" data-checkout="<?= $schedule->checkout_popup;?>" data-scheduleid="<?= $schedule->id; ?>" data-comment="<?= $schedule->comment; ?>" data-start="<?= $start; ?>" data-stop="<?= $stop; ?>">
                    <span data-html="true" data-toggle="tooltip" data-placement="bottom" title="<?php echo $startTime. ' - ' . $endTime;?><br /><?php echo $schedule->comment; ?>">
                <?php
                    echo $schedule->timeDiff;
                ?>
                </span>
                    </a>
                </td>

                <td id="data_shift_td_<?= $schedule->id;?>">
                    <?php if($schedule->firstDataField) { ?>
                    <a href="#data_shift" data-scheduleid="<?= $schedule->id;?>" data-toggle="modal">
                        <!--<span data-html="true" data-toggle="tooltip" data-placement="bottom" title="<?php /*echo $schedule->dataFieldHtml;  */?>">-->
                        <?php // echo $schedule->firstDataField;?>
                    </a>
                    <?php } ?>
                </td>

                <?php
                $gigstr_rating_count = explode(" ", $schedule->gigstr_rating);
                if(count($gigstr_rating_count) == 1) {
                    $gigstr_rating = $gigstr_rating_count[0];
                }
                else {
                     $gigstr_rating = 0;
                }
                ?>

                <?php
               //  $rateCount = explode(" ", $schedule->gigstr_rating);
                 if($gigstr_rating_count[0] >= 5 && count($gigstr_rating_count) > 1) {
                    $notMatch = 'red';
                 } else {
                    $notMatch = "";
                 }
                ?>
                <td class="attest_rate_td <?=$notMatch?> text-center">
                <?php

               if(count($gigstr_rating_count) == 1) { ?>
                    <a class="star_rate" href="#success_rate" data-toggle="modal" data-gigstr_name="<?=$schedule->user->name;?>" data-scheduleid="<?= $schedule->id;?>" data-gigstr_rating="<?= $gigstr_rating;?>" data-gigstr_rating_comment="<?= $schedule->gigstr_rating_comment;?>" data-internal_rating="<?=$schedule->internal_rating;?>" data-internal_rating_comment ="<?= $schedule->internal_rating_comment;?>">
                    <div data-html="true" data-toggle="tooltip" data-placement="bottom" title="<?= $schedule->gigstr_rating_comment;?>">
                    <?php
                    echo '<span>';
                    for($i = 0; $i < $gigstr_rating_count[0]; $i++) {
                        echo '&#9733';
                    }
                    echo '</span>';
                    for($i = 0; $i < (5 - $gigstr_rating_count[0]); $i++) {
                        echo '&#9733';
                    } ?>
                    </div>
                    </a>

               <?php } else {
                    echo $schedule->gigstr_rating;
                    } ?>
                </td>

                <td class="attest_rate_td text-center" id="attest_rate_<?= $schedule->id;?>">
                <?php
                if($schedule->internal_rating < 1) { ?>
                    <button type="button" id="success_rate-<?= $schedule->id;?>" data-target="#success_rate" data-toggle="modal" class="btn btn-warning" data-gigstr_name="<?=$schedule->user->name;?>" data-scheduleid="<?= $schedule->id;?>" data-gigstr_rating="<?=$gigstr_rating;?>" data-gigstr_rating_comment="<?=$schedule->gigstr_rating_comment;?>" data-internal_rating="<?=$schedule->internal_rating;?>" data-internal_rating_comment ="<?=$schedule->internal_rating_comment;?>">&#9733 &nbsp;Rate</button>
                <?php
                }
                else { ?>
                    <a class="star_rate" href="#success_rate" data-toggle="modal" data-gigstr_name="<?=$schedule->user->name;?>" data-scheduleid="<?= $schedule->id;?>" data-gigstr_rating="<?= $gigstr_rating;?>" data-gigstr_rating_comment="<?= $schedule->gigstr_rating_comment;?>" data-internal_rating="<?=$schedule->internal_rating;?>" data-internal_rating_comment ="<?= $schedule->internal_rating_comment;?>">
                    <div data-html="true" data-toggle="tooltip" data-placement="bottom" title="<?= $schedule->internal_rating_comment;?>">
                <?php
                    echo '<span>';
                    for($i =0; $i < $schedule->internal_rating; $i++) {
                        echo '&#9733';
                    }
                    echo '</span>';
                    for($i =0; $i < (5 - $schedule->internal_rating); $i++) {
                        echo '&#9733';
                    } ?>
                    </div>
                    </a>
                <?php } ?>
                </td>

                <td>
                    <input value="<?php echo $schedule->id; ?>" type="checkbox" id="<?php echo 'attest-'.$schedule->id; ?> "name="attest_check" <?php if (( $schedule->attested == '1')) { ?>checked="checked" <?php }; ?> class="attestcheck">

                <?php if($schedule->attestImages != 0) { ?>
                    <a class="attest_image" data-scheduleid="<?= $schedule->id;?>" data-toggle="modal" href="#attest_image"><i class="glyphicon glyphicon-paperclip"></i></a>
                <?php } ?>
                </td>

            </tr>

        <?php } ?>
        </tbody>
        </table>
    </div>

    <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));
            ?>
    </ul>
</div>
            </div>
        </div>
    </div>
<!--</div>-->
<script>
var base_url = '<?php echo $base_url; ?>';
</script>


<div class="modal fade bs-example-modal-lg" id="data_shift" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="imagesatt">Shift Data</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <div id="data_shift_body" class="">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="attest_image" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="imagesatt">Image Attachments</h4>
            </div>

            <div class="modal-body">
                <div class="panel-body">
                    <div id="carousel-schedule" class="" data-ride="carousel">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="success_rate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Success Rating</h4>
            </div>

            <div class="modal-body">
<?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'updateRating', 'class'=> ''));?>
            <input type="hidden" id="scheduleId" name="scheduleId" value=""/>
                <div class="row popup_bottom">
                    <div class="col-md-12">
                        <h4 id="rating_gigstr" class="text-center"></h4>
                     </div>
                     <div class="col-md-4 col-md-offset-4  text-center" id="gig_rate">
                        <input id="gigstr_rating" min=0 max=5 step=1 data-size="xs" data-rtl="false" data-showClear="false" data-showCaption="false" name="gigstr_rating" type="number" class="rating " disabled>
                     </div>
                    <div class="col-md-12">
                    <textarea name="popup_comment" id="gigstr_comment" class="form-control" disabled>
                    </textarea>
                    </div>
                </div>

                <div class="row popup_bottom">
                    <div class="col-md-12">
                    <h4 class="text-center">Give Your Rating!</h4>
                        <div class="col-md-10 col-md-offset-1  text-center">
                        <input required id="internal_rating" min=0 max=5 step=1 data-size="md" data-rtl="false" data-showClear="false" data-showCaption="false" name="internal_rating" type="number" class="rating">
                        </div>
                    </div>
                    <div class="col-md-12">
                    <textarea required name="popup_comment" id="internal_comment" class="form-control"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="alert alert-danger fade in text-center" id="rate_alert" style ="display:none">
                    Please give rating with comment
                </div>

                    <div class="col-md-4 col-md-offset-4  text-center">
                        <button id="popup_rate" class="btn btn-success btn-block" type="button">Update</button>
                    </div>
                </div>
<?php echo $this->Form->end() ?>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="shift_report" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Shift Report </h4>
            </div>

            <div class="modal-body">
            <?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'updateAttested', 'class'=> ''));?>
            <input type="hidden" id="scheduleId" name="scheduleId" value=""/>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4  text-center">
                        <h4>Scheduled <i class="fa fa-calendar" aria-hidden="true"></i></h4>
                    </div>
                    <div class="col-md-6">
                    <fieldset disabled>
                        <div class="form-group">
                          <input type="text"  class="form-control" value="" id="from_popup">
                        </div>
                    </fieldset>
                    </div>
                    <div class="col-md-6">
                    <fieldset disabled>
                        <div class="form-group">
                          <input type="text"  class="form-control" value="" id="to_popup">
                        </div>
                    </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4  text-center">
                        <h4>Check in/out <a href="" id="mapUrl"  target="_blank"><i class="fa fa-map-marker" aria-hidden="true"></i></a></h4>
                    </div>
                    <div class="col-md-6">
                        <fieldset disabled>
                            <div class="form-group">
                              <input type="text"  class="form-control" value="" id="checkin_popup">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset disabled>
                            <div class="form-group">
                              <input type="text"  class="form-control" value="" id="checkout_popup">
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4  text-center">
                        <h4>Reported <i class="fa fa-mobile" aria-hidden="true"></i></h4>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                              <input type="text" name="start_popup"  class="form-control popup_calender" id="start_popup">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <div class="form-group">
                              <input type="text" id="stop_popup" class="form-control popup_calender" id="stop_popup">
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row popup_bottom">
                    <div class="col-md-12">
                        <textarea name="popup_comment" id="popup_comment" class="form-control"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-md-offset-4  text-center">
                        <button id="popup_schedule" class="btn btn-success btn-block" type="button">Update</button>
                    </div>
                </div>
<?php echo $this->Form->end() ?>
            </div>

        </div>
    </div>
</div>