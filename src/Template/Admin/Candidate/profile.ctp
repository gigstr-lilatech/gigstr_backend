<!-- Start .page-content-inner -->
<?php
header('Content-type: text/html; charset=UTF-8');
?>
<style>
    .form-group .col-lg-12 {
        margin-bottom: 15px !important;
    }
    img.gig_img{
        max-width: 140px !important;
    }
</style>
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Candidates </h2>
        <span class="txt">This page displays candidates available in the database</span>
    </div>

</div>
<!-- Start .row -->
<div class="row">
    <div class="col-lg-6 col-md-6">
        <!-- col-lg-6 start here -->

        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Candidate Profile</h4>
            </div>
            <div class="panel-body">

                <?= $this->Flash->render(); ?>
                <div class="row profile-header">
                    <div class="col-md-4">
                        <?php
                        if ($candidate->image != "") {
                            if (preg_match('/facebook/', $candidate->image)) {
                                $prof_image = '<img src="'.$candidate->image.'" width="140" style="max-width: 140px;">';
                            } else {
                                $prof_image = $this->Html->image('user_images/' . $candidate->image, ['alt' => 'User Profile Image', 'width' => '160', 'class' => 'gig_img']);
                            }
                        } else {
                            if($user_data->fbid) {
                                $imageurl = "https://graph.facebook.com/".$user_data->fbid."/picture?type=normal";
                                $prof_image = '<img src="'.$imageurl.'" width="140" style="max-width: 140px;">';
                            }
                            else {
                                $prof_image = $this->Html->image('/img/avatars/profile_placeholder.png', ['alt' => 'User Profile Image', 'class' => 'gig_img']);
                            }
                        }
                        echo $prof_image;
                        ?>
                        </div>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <input class="inline-edit" id="candidate_name" type="text" name="candidate_name" value="<?= $user_data->name; ?>">

                        <div class="row">
                            <div class="col-lg-8">
                                <?php if($candidateRating['rateCount'] == 0) { ?>
                                <div class="no-pd-right">
                                    <input id="candidate_rating_dummy" type="number" name="candidate_rating" class="rating" min=0 max=5 step=1 data-size="xs" value="<?= $candidate->candidate_rating; ?>" disabled>
                                </div>

                                <?php } else { ?>

                                <div class="no-pd-right candidate_rate">
                                    <input id="candidate_rating_dummy" type="number" name="candidate_rating" class="rating" min=0 max=5 step=1 data-size="xs" value="<?= $candidateRating['rateAvg']; ?>" disabled>
                                    <div class="rate_limit"><?= $candidateRating['rateAvg']; ?> (<?= $candidateRating['rateCount']; ?> ratings)</div>
                                </div>
                                <?php } ?>
                                <?php if ($user_data->fbid != '') { ?>
                                    <small class="created_by">Facebook: <br/><a href="http://www.facebook.com/<?= $user_data->fbid; ?>" target="_blank">facebook.com/<?= $user_data->fbid; ?></a></small><br/>
                                <?php } ?>
                                <small class="updated">Last modified : <?= date('Y-m-d', strtotime
                                    ($user_data->modified)); ?></small>
                                <br />
                                <small class="updated">Looking for Gigs in : <?= $candidate->lng_country; ?></small>

                                <?php if(isset($referrers->name)) { ?>
                                    <br />
                                    <small class="updated">Referred by: <a href="<?php echo $base_url; ?>admin/candidate/profile/<?= $referrers->id; ?>"><?= $referrers->name; ?><a/></small>
                                <?php } ?>

                                <?= $this->Form->create("Candidate", ['class' => 'form-horizontal', 'id' => 'profile_form']) ?>
                            </div>

                            <div class="col-lg-4">
                                <p>Status</p>
                                <h4 class="setStatus">
                                <?php echo($candidate->is_verified == 1 ? "Verified" : "Not verified");?>
                                </h4>
                                <a href="javascript:;" data-id="<?=$candidate->id; ?>" id="verify" class="btn btn-success mr5 mb10" <?php echo ($candidate->is_verified == 1 ? 'disabled' : '');?>>VERIFY</a>
                            </div>
                        </div>

                        <div id="target"></div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<?= $this->Form->input('email', ['class' => 'form-control', 'id' => 'email', 'value' => $user_data->email]) ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<?= $this->Form->input('person_number', ['class' => 'form-control', 'id' => 'person_number', 'label' => 'Personal Number', 'value' => $candidate->person_number]) ?>
                        </div>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<?= $this->Form->input('phone_number', ['class' => 'form-control', 'id' => 'email', 'label' => 'Mobile Phone Number', 'value' => $candidate->phone_number]) ?>
                        </div>
                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="checkbox1">Gender</label>
                            <select name="gender" class="form-control" id="gender" label="">
                                <option value="">-Select gender-</option>
                                <option value="M" <?php if ($candidate->gender == 'M') echo 'selected'; ?>>Male</option>
                                <option value="F" <?php if ($candidate->gender == 'F') echo 'selected'; ?>>Female</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="input text">
                                <label for="languages">Languages</label>
                                <input value="<?php echo $candidate->language; ?>" id="languages" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="input text">
                                <label for="skills">Skills</label>
                                <div class="candidate_data">
                                <?php echo $candidate->skills; ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input text">
                                <label for="address">Address</label>
                                <div class="candidate_data">
                                    <?php echo $candidate->address; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="input text">
                                <label for="work">Work Experience</label>
                                <div class="candidate_data">
                        <?php echo $candidate->experiences; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="input text">
                                <label for="education">Education</label>
                                <div class="candidate_data">
                                    <?php echo $candidate->education; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="candidate_url col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input text">
                                <label for="company">Company</label>
                                <input value="<?php echo $candidate->company; ?>" id="company"
                                       class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="candidate_url col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input text">
                                <label for="facebook">Facebook</label>
                                <input value="<?php echo $candidate->facebook; ?>" id="facebook" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="candidate_url col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input text">
                                <label for="linkedin">Linkedin</label>
                                <input value="<?php echo $candidate->linkedin; ?>" id="facebook" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="candidate_url col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="input text">
                                <label for="website">Website</label>
                                <input value="<?php echo $candidate->website; ?>" id="facebook" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<?= $this->Form->input('bank_name', ['class' => 'form-control', 'id' => 'bank_name', 'value' => $candidate->bank_name]) ?>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<?= $this->Form->input('clearing_number', ['class' => 'form-control', 'id' => 'clearing_number', 'value' => $candidate->clearing_number]) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                        <div class="col-lg-12">
                            <div class="row">
                                <!-- Start .row -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <?= $this->Form->input('account', ['class' => 'form-control', 'id' => 'account_number', 'value' => $candidate->account, 'label' => 'Account Number']) ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <?php
                                    $dob = '';
                                    $year = '';
                                    $day = date('d');
                                    $month = date('m');
                                    if ($candidate->day != '') {
                                        if (trim($candidate->year) != '' && $candidate->year != NULL) {
                                            $dob = $candidate->year . '-' . $candidate->month . '-' . $candidate->day;
                                            $year = $candidate->year;
                                            $day = $candidate->day;
                                            $month = $candidate->month;
                                        } else {
                                            $dob = $candidate->month . '-' . $candidate->day;
                                            $day = $candidate->day;
                                            $month = $candidate->month;
                                            $year = 2000;
                                        }
                                    }
                                    $dob = $year . '-' . $month . '-' . $day;
                                    ?>
                                    <?= $this->Form->input('dob', ['class' => 'form-control', 'id' => 'basic-datepicker', 'value' => $dob, 'label' => 'Date of Birth']) ?>
                                </div>
                            </div>
                        </div>
                    </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="checkbox1">Car driver license</label><br/>
                            <div class="checkbox-custom checkbox-inline">
                                <input type="checkbox" value="1" id="checkbox5" name="car_driver_license" <?php if ($candidate->car_driver_license == '1') echo 'checked'; ?>>
                                <label for="checkbox5"></label>
                            </div>
                        </div>

                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Manual payment method</label><br/>
                            <div class="checkbox-custom checkbox-inline">
                                <?php echo $candidate->manual_payment; ?>
                                <input type="checkbox" value="1" id="manual_payment" name="manual_payment" <?php  echo (($candidate->manual_payment == '1') ? 'checked' : ''); ?>>
                                <label for="manual_payment"></label>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="verified">Verified profile</label><br/>
                            <div class="checkbox-custom checkbox-inline">
                                <input type="checkbox" value="1" id="verified" name="is_verified" <?php if ($candidate->is_verified == '1') echo 'checked'; ?>>
                                <label for="checkbox5"></label>
                            </div>
                        </div>

                        <!-- Start .row -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label for="verified_company">Verified company</label><br/>
                            <div class="checkbox-custom checkbox-inline">
                                <input type="checkbox" value="1" id="verified_company" name="verified_company" <?php if ($candidate->verified_company == '1') echo 'checked'; ?>>
                                <label for="checkbox5"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="input text">
                                <label for="salary">Salary Payout <?= $candidate->payoutCountry; ?></label>
                                <div class="candidate_data">
                                    <?php echo $candidate->payout; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="input text">
                                <label for="payment">Payment method</label>
                                <div class="candidate_data">
                                    <?php echo $candidate->card_brand . (!empty($candidate->card_last4) ? " **** **** **** " . $candidate->card_last4 : ""); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Start .row -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Pitch</label>
                            <?= $this->Form->textarea('pitch', ['class' => 'form-control', 'id' => 'pitch', 'label' => 'Pitch', 'value' => $candidate->pitch, 'disabled']) ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-12">
                    <div class="row">
                        <!-- Start .row -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label>Notes</label>
<?= $this->Form->textarea('notes', ['class' => 'form-control', 'id' => 'email', 'label' => 'Notes', 'value' => $candidate->notes]) ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group" id="delete_mg" style="display: none;">
                <div class="col-lg-12">
                    <div class="row">
                    <div class="alert alert-warning fade in">
                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                    <i class="glyphicon glyphicon-warning-sign alert-icon "></i>
                    User has shifts and can not be deleted.
                    </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-6" style="float: right;">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb25">
                            <input type="hidden" name="candidate_rating" id="candidate_rating" value="<?= $candidate->candidate_rating; ?>">
                            <input type="hidden" name="name" id="name" value="<?= $user_data->name; ?>">
<?= $this->Form->button('Update', ['type' => 'button', 'class' => 'btn btn-success ml10 pull-right', 'id' => 'update_profile']); ?>
                        </div>
                    </div>
                </div>
            </div>
    <?= $this->Form->end() ?>
            <a href="javascript:;" class="btn btn-danger btn-alt mr5 mb10" style="margin: 18px 0 0 15px;" id="delete_candidate" rel="<?php echo $candidate->user_id; ?>">Delete Candidate</a>
            <!--<a href="/admin/candidate/export?id=<?= $candidate->user_id; ?>" class="btn btn-danger btn-alt mr5 mb10" style="margin: 18px 0 0 15px;" id="export_candidate">Export Candidate</a> -->
                </div>

        </div>
            <!-- End .panel -->
         </div>
        <!-- col-lg-6 end here -->
        <div class="col-lg-6 col-md-6">
            <div class="panel panel-default">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title">Applications</h4>
                </div>
                <div class="panel-body">

                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="per40"><?= __('Ad'); ?></th>
                            <th class="per30"><?= __('Selection'); ?></th>
                            <th class="per30"><?= __('Date'); ?></th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach($activityLog as $log) { ?>
                        <tr>
                            <td><a href="<?= $this->Url->build(["controller" => "Gig","action" => "edit",$log['gig_id']]); ?>"> <?= $log['add']; ?></a></td>
                            <td><?= $log['selection']; ?></td>
                            <td><?= $log['date']; ?></td>
                        </tr>
                       <?php }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div></div></div>
    </div>
    <script>
        var base_url = '<?php echo $base_url; ?>';
        var user_id = '<?php echo $user_id; ?>';
        var candidate_id = '<?php echo $candidate->user_id; ?>'
        var user_name = '<?php echo $user_name; ?>';
        var rating = '<?php echo $candidate->candidate_rating; ?>';
        var dob_date = '<?php echo $year . '-' . $month . '-' . $day; ?>';
        var year = '<?php echo $year; ?>';
        var month = '<?php echo $month; ?>';
        var day = '<?php echo $day; ?>';
</script>
<!-- End .row -->