 <!-- Start .page-content-inner -->
                        <div id="page-header" class="clearfix">
                            <div class="page-header">
                                <h2>Candidates</h2>
                                <span class="txt">This page lists all candidates available in the database</span>
                            </div>
                            
                        </div>
                        <!-- Start .row -->       
<div class="row">
    <!-- Start .row -->
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">All Candidates</h4>
            </div>
            <div class="panel-body">
                <?= $this->Flash->render(); ?>

            <div class="row">
                <div class="col-md-2">
                    <input class="form-control input-sm" placeholder="Search" name="candidateSearch" id="candidateSearch">
                </div>
                <div class="col-md-10 ">
                    <span id="PreeValue" class="pagination_button">← Previous</span>
                    <span id="nextValue" class="pagination_button">Next →</span>
                <ul class="pagination pull-right">
                    <?php
                        echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                        echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                        echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));
                    ?>
                </ul>
                </div>
            </div>

                <table id="basic-datatables-candidatesx" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="per5"><?php echo $this->Paginator->sort('Candidate.user_id', 'Id');?></th>
                    <th class="per15"><?php echo $this->Paginator->sort('User.name', 'Name');?></th>
                    <th class="per15"><?php echo $this->Paginator->sort('User.email', 'Email');?></th>
                    <th class="per10"><?php echo $this->Paginator->sort('Candidate.phone_number', 'Phone');?></th>
                    <th class="per10"><?php echo $this->Paginator->sort('Candidate.country', 'Country');?></th>
                    <th class="per15"><?php echo $this->Paginator->sort('Candidate.city', 'City');?></th>
                    <th class="per5"><?php echo $this->Paginator->sort('Candidate.app_mode', 'App Mode');?></th>
                    <th class="per5"><?php echo $this->Paginator->sort('Candidate.app_mode_preference', 'App Pref.');?></th>
                    <th class="per5"><?php echo $this->Paginator->sort('Candidate.is_verified', 'Verified');?></th>
                    <th class="per5"><?php echo $this->Paginator->sort('Candidate.verified_company', 'Verified Co');?></th>
                    <th class="per5"><?php echo $this->Paginator->sort('Candidate.manual_payment', 'Manual Payment');?></th>
                    <th class="per15"><?php echo $this->Paginator->sort('Candidate.created', 'Created');?></th>
                </tr>
            </thead>

            <tbody id ="candidateTable">
    <?php
    foreach ($candidates as $candidate): ?>
                 <tr>
                 <td><?= $candidate['user_id'] ?></td>
                 <td><a href="<?=
                            $this->Url->build([
                                "controller" => "Candidate",
                                "action" => "profile",
                                $candidate['user_id']
                            ]);
        ?>">
                    <?= $candidate["user"]->name ?></a></td>
                 <td><?= $candidate['user']->email ?></td>
                 <td><?= $candidate->phone_number ?></td>
                 <td><?= $candidate->country_name ?></td>
                <td><?= $candidate->city ?></td>
                <td><?php   if($candidate->app_mode == 1)
                                echo 'Work';
                            elseif ($candidate->app_mode == 2)
                                echo 'Hire';?>
                </td>
                <td><?php
                    if($candidate->app_mode_preference == 1)
                        echo 'Work';
                    elseif ($candidate->app_mode_preference == 2)
                        echo 'Hire';?>
                </td>
                <td><?= $candidate->is_verified ?></td>
                <td><?= $candidate->verified_company ?></td>
                <td><?= $candidate->manual_payment ?></td>
                <td><?= date('Y-m-d', strtotime($candidate->created) ) ?></td>
          </tr>
    <?php
    endforeach;
    ?>
            </tbody>

        </table>

        <ul class="pagination">
            <?php
                echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));
            ?>
        </ul>
    </div>
                                </div>
                                <!-- End .panel -->
                            </div>
                            <!-- col-lg-12 end here -->
    <script>
        var base_url = '<?php echo $base_url; ?>';
    </script>
   </div>


        