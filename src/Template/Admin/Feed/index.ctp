<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Feed[]|\Cake\Collection\CollectionInterface $feed
  */
?>
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Gigstr Explore</h2>
        <span class="txt"></span>
    </div>
</div>

<div class="row">
    <!-- Start .row -->
    <div class="col-lg-6">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Component</h4>
            </div>
            <div class="panel-body">
                <div class="tabs mb20">

                    <ul id="feedTab" class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a data-toggle="tab" href="#ourWorld" aria-expanded="true">OUR WORLD</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#getHelp">COMPANY</a>
                        </li>
                    </ul>
                    <div id="feed_tab" class="tab-content">
                        <div id="ourWorld" class="tab-pane fade active in section" data-section="client">
                            <div class="row">
                                <div class="col-md-12">
                            <button id="addPanel" class="btn btn-success btn-round mr5 mb10 pull-right" type="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                </div>
                            </div>

                            <form id="ourWorldForm">
                            <ul id="ourWorldPanel" class="explorePanel">
                        <?php  foreach($ourWorld as $item) { ?>
                            <li>
                                <input class="feed_order" type="hidden" name="feed[<?= $item->feed_order; ?>]" value="<?= $item->id; ?>">
                                <div class="panel panel-default panelMove toggle panelRefresh panelClose">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-panelid="<?= $item->id; ?>" href="javascript:;" class="removePanel pull-right">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                       <p class="text-center"><?= $item->title; ?></p>
                                       <a href="javascript:;" id="<?= $item->id; ?>" class="feed_component">
                                           <img class="img-responsive center-block" src="/img/<?= $item->ui_type; ?>.png">
                                       </a>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                            </ul>
                            </form>

                        </div>
                        <div id="getHelp" class="tab-pane fade section" data-section="help-gig">
                            <div class="row">
                                <div class="col-md-12">
                                <button id="addPanelHelpGig" class="btn btn-success btn-round mr5 mb10 pull-right" type="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                    </div>
                            </div>
                            <form id="gigHelpForm">
                            <ul id="gigHelpPanel" class="explorePanel">
                                <?php  foreach($gigHelp as $item) { ?>
                                    <li>
                                        <input class="feed_order" type="hidden" name="feed[<?= $item->feed_order; ?>]" value="<?= $item->id; ?>">
                                        <div class="panel panel-default panelMove toggle panelRefresh panelClose">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-panelid="<?= $item->id; ?>" href="javascript:;" class="removePanel pull-right">
                                                        <i class="fa fa-times" aria-hidden="true"></i>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-body">
                                                <p class="text-center"><?= $item->title; ?></p>
                                                <a href="javascript:;" id="<?= $item->id; ?>" class="feed_component"><img class="img-responsive center-block" src="/img/<?= $item->ui_type; ?>.png">
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                                </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Edit</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                    <form enctype="multipart/form-data" class="form-horizontal" role="form" action="" method="post" id="feedForm">
                        <input type="hidden" value="" id="feedId" name="feedId">

                        <div id="fieldsOuter">

                            <div class="form-group" id="uiComponentOuter">
                                <label class="col-sm-12 control-label" for="uiComponent">Component</label>
                                <div class="col-sm-12">
                                    <select id="uiComponent"  name="uiComponent" class="form-control">
                                        <option value="">Select Component</option>
                                        <option value="button">Button</option>
                                        <option value="carousel">Carousel</option>
                                        <option value="link">Link</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="componentTitleOuter">
                                <label class="col-sm-12 control-label" for="componentTitle">Component Title</label>
                                <div class="col-sm-12">
                                    <input id="componentTitle" name="title" class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                            <div class="form-group" id="componentActionOuter">
                                <label class="col-sm-12 control-label" for="componentAction">Action</label>
                                <div class="col-sm-12">
                                    <select id="componentAction"  name="componentAction" class="form-control">
                                        <option value="">Select Action</option>
                                        <option value="web">Webview</option>
                                        <option value="youtube">Youtube</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" id="componentUrlOuter">
                            <label class="col-sm-12 control-label" for="componentUrl">URL</label>
                            <div class="col-sm-12">
                                <input id="componentUrl" name="componentUrl" class="form-control" type="text" placeholder="">
                            </div>
                        </div>

                            <div class="row">
                            <div class="col-lg-12">
                                <button type="button" id="feedButton" class="btn btn-success mr5 mb10 pull-right">Save</button>
                                <button id="add" class="btn btn-success btn-sm btn-round mr5 mb10 pull-right" type="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                <div id="feedErr"></div>
                            </div>
                        </div>

                            <ul id="sortable">

                            <li class="ui-state-default addNew sub_component" id="firstItem">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="javascript:;" class="remove pull-right">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6 panel-input">
                                                <input class="itemId_0" type="hidden" name="item[0][itemId]" value="">
                                                <input type="hidden" name="item[0][count]" value="0">
                                                <input class="form-control" name="item[0][imageTitle]" type="text" placeholder="Image title">
                                                <input class="form-control" name="item[0][desc]" type="text" placeholder="Description">
                                                <select class="form-control" name="item[0][linkType]">
                                                    <option value="">Select Action</option>
                                                    <option value="web">Web</option>
                                                    <option value="youtube">Youtube</option>
                                                </select>
                                                <input class="form-control" name="item[0][link]" type="text" placeholder="link">
                                                <input type="hidden" name="item[0][image]" id="feedImage_0">
                                                <input type="hidden" name="item[0][thumbnail]" id="feedImageThumbnail_0">
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="dropzone small-dropzone feedDropZone" data-count="0">
                                                    <div class="dz-message" data-dz-message>
                                                        <p>Drop image</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>

                        </ul>
                        </div>

                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = '<?php echo $base_url; ?>';
</script>