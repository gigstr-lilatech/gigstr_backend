<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Gigstr';
$controller = $this->request->params['controller'];
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
    <?= $this->Html->charset() ?>
        <!-- Mobile specific metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1 user-scalable=no">
        <!-- Force IE9 to render in normal mode -->
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="author" content="" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="application-name" content="" />
        <link rel="icon" href="https://backoffice.gigstr.com/img/favicon.png" type="image/png">
        <!-- Import google fonts - Heading first/ text second -->
        <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700' rel='stylesheet' type='text/css'>

        <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
        </title>
    
    <?= $this->Html->css('icons.css') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('plugins.css') ?>
    <?= $this->Html->css('main.css') ?>
    <?= $this->Html->css('custom.css') ?>

        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">
        
       
        <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
        <meta name="msapplication-TileColor" content="#3399cc" />

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

        <?= $this->Html->script('libs/jquery-2.1.1.min.js'); ?>
        <?= $this->Html->script('bootstrap/bootstrap.js'); ?>

    <?= $this->fetch('script') ?>


    <script type="text/javascript">
    // load the Branch SDK file
    (function(b,r,a,n,c,h,_,s,d,k){if(!b[n]||!b[n]._q){for(;s<_.length;)c(h,_[s++]);d=r.createElement(a);d.async=1;d.src="https://cdn.branch.io/branch-latest.min.js";k=r.getElementsByTagName(a)[0];k.parentNode.insertBefore(d,k);b[n]=h}})(window,document,"script","branch",function(b,r){b[r]=function(){b._q.push([r,arguments])}},{_q:[],_v:1},"addListener applyCode banner closeBanner creditHistory credits data deepview deepviewCta first getCode init link logout redeem referrals removeListener sendSMS setBranchViewData setIdentity track validateCode".split(" "), 0);
    branch.init('key_live_jbxl88cdgfC29C2FbvS8TpffEulXn15t');
    </script>


    </head>
    <body>

        <!-- .page-navbar -->
        <div id="header" class="page-navbar">
            <!-- .navbar-brand -->
            <a href="<?= $this->Url->build([
                                    "controller" => "Gig",
                                    "action" => "index"
                                ]); ?>" class="navbar-brand hidden-xs hidden-sm">
                <?= $this->Html->image('logotype_backoffice.jpg', ['alt' => 'Gigstr Logo','class'=>'logo hidden-sm hidden-xs']); ?>
                <?= $this->Html->image('logotype_backoffice.jpg', ['alt' => 'Gigstr Logo','class'=>'logo-sm hidden-lg hidden-md']); ?>
<!--                <img src="img/logo.png" class="logo hidden-xs" alt="Dynamic logo">
<img src="img/logosm.png" class="logo-sm hidden-lg hidden-md" alt="Dynamic logo">-->
            </a>
            <!-- / navbar-brand -->
            <!-- .no-collapse -->
            <div id="navbar-no-collapse" class="navbar-no-collapse">
                <!-- top left nav -->
                <ul class="nav navbar-nav">
<!--                    <li class="toggle-sidebar">
                        <a href="#">
                            <i class="fa fa-reorder"></i>
                            <span class="sr-only">Collapse sidebar</span>
                        </a>
                    </li>-->
<!--                    <li>
                        <a href="#" class="reset-layout tipB" title="Reset panel position for this page"><i class="fa fa-history"></i></a>
                    </li>-->
                </ul>
                <!-- / top left nav -->
                <!-- top right nav -->
                <ul class="nav navbar-nav navbar-right">

                    <li>
                        <a href="<?= $this->Url->build([
                                    "controller" => "User",
                                    "action" => "logout"
                                ]); ?>">
                            <i class="fa fa-power-off"></i>
                            <span class="sr-only">Logout</span>
                        </a>
                    </li>

                </ul>
                <!-- / top right nav -->
            </div>
            <!-- / collapse -->
        </div>
        <!-- / page-navbar -->
        <!-- #wrapper -->
        <div id="wrapper">
            <!-- .page-sidebar -->
            <aside id="sidebar" class="page-sidebar hidden-md hidden-sm hidden-xs">
                <!-- Start .sidebar-inner -->
                <div class="sidebar-inner">
                    <!-- Start .sidebar-scrollarea -->
                    <div class="sidebar-scrollarea">
                        <!--  .sidebar-panel -->
                        <div class="sidebar-panel">
                            <h5 class="sidebar-panel-title">Profile</h5>
                        </div>
                        <div class="user-info clearfix">
                            <?= $this->Html->image('avatars/profile_placeholder.png', ['alt' => 'User']); ?>
                            <span class="name">Admin User</span>

                        </div>
                        <div class="sidebar-panel">
                            <h5 class="sidebar-panel-title">Navigation</h5>
                        </div>
                        <!-- / .sidebar-panel -->
                        <!-- .side-nav -->
                        <?php

                        $action = $this->request->params['action'];
                        $request_action = $controller . '/' . $action;
                        ?>
                        <div class="side-nav">
                            <ul class="nav">
                                <!--                                <li><?= $this->Html->link(__('Gigs'), ['controller'=>'Gig','action' => 'index']) ?>
                                                                                                        <ul class="sub">
                                        <?php //foreach($menu_company AS $company_id=>$company_name){
                                            //echo '<li>'.$this->Html->link(__($company_name), ['controller'=>'Gig','action' => 'company' , $company_id]).'</li>';
                                        //}
                                        ?>
                                                                                                        </ul>
                                                                </li>-->
                                <li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Gig",
                                    "action" => "index"
                                ]); ?>" class='<?php if($controller == "Gig") echo "active"; ?>'>
<!--                                        <i class="l-arrows-right sideNav-arrow"></i>-->
                                        <i class="fa fa-search"></i>
                                        Ads
                                        <span class="badge pull-right">
                                        <?php
                                        echo $this->InboxCount->getPendingGigCount();
                                        ?>
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Candidate",
                                    "action" => "index"
                                ]); ?>" class='<?php if($controller == "Candidate") echo "active"; ?>'>
                                        <i class="l-arrows-right sideNav-arrow"></i>
                                        <i class="fa fa-user"></i>
                                        Candidates
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Company",
                                    "action" => "index"
                                ]); ?>" class='<?php if($controller == "Company") echo "active"; ?>'>
                                        <i class="l-arrows-right sideNav-arrow"></i>
                                        <i class="company-icon"></i>
                                        Companies
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Chat",
                                    "action" => "allchat"
                                ]); ?>" class='<?php if($controller == "Chat") echo "active"; ?>'>
                                        <i class="inbox-icon"></i>
                                        Inbox
                                        <span class="badge pull-right">
                                        <?php
                                        echo $this->InboxCount->getNotificationCount();
                                        ?>

                                        </span>
                                    </a>
                                </li>
                                
                                <!--<li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Schedule",
                                    "action" => "allschedule"
                                ]); ?>" class='<?php if($controller == "Schedule" && ($action == "index" || $action == "savedescription" || $action == "search" || $action == "allschedule" || $action == "delete")) echo "active"; ?>'>
                                        <i class="l-arrows-right sideNav-arrow"></i>
                                        <i class="fa fa-calendar"></i>
                                        Schedule
                                    </a>
                                </li> -->
                                
                                <li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Job",
                                    "action" => "index"
                                ]); ?>" class='<?php if($controller == "Job") echo "active"; ?>'>
                                        <i class="l-arrows-right sideNav-arrow"></i>
                                        <i class="l-software-layers2"></i>
                                        Gigs
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= $this->Url->build([
                                    "controller" => "Schedule",
                                    "action" => "map"
                                ]); ?>" class='' target="_blank">
                                        <i class="l-arrows-right sideNav-arrow"></i>
                                        <i class="fa fa-map-marker"></i>
                                        Map
                                    </a>
                                </li>

                                <li class="hasSub <?php if($controller == "Schedule" && ($action == "attest" || $action == "attestSearch")) echo "highlight-menu"; ?>">
                                    <a class="notExpand <?php if($controller == "Schedule" && ($action == "attest" || $action == "attestSearch")) echo "active-state"; ?>" href="#">
                                        <i class="l-arrows-right sideNav-arrow rotate90"></i>
                                        <i class="l-basic-webpage"></i>
                                        <span class="txt">Gigin</span>
                                    </a>
                                    <ul class="sub  <?php if($controller == "Schedule" && ($action == "attest" || $action == "attestSearch")) echo "show"; ?>">
                                        <li>
                                            <a href="<?= $this->Url->build([
                                            "controller" => "Schedule",
                                            "action" => "allschedule"
                                        ]); ?>" class='<?php if($controller == "Schedule" && ($action == "index" || $action == "savedescription" || $action == "search" || $action == "allschedule" || $action == "delete")) echo "active"; ?>'>
                                               <span class="txt">Schedule</span>
                                            </a>

                                        </li>
                                        <li>
                                            <a class="<?php if($controller == "Schedule" && ($action == "attest")) echo "active"; ?>"  href="<?= $this->Url->build(["controller" => "Schedule",
                                             "action" => "attest"]); ?>">
                                            <span class="txt">Attest</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="<?= $this->Url->build([
                                        "controller" => "Feed",
                                        "action" => "index"
                                    ]); ?>" class='<?php if($controller == "Feed") echo "active"; ?>'>
                                        <i class="l-arrows-right sideNav-arrow"></i>
                                        <i class="l-software-shape-polygon"></i>
                                        Explore
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <!-- / side-nav -->
                    </div>
                    <!-- End .sidebar-scrollarea -->
                </div>
                <!-- End .sidebar-inner -->
            </aside>
            <!-- / page-sidebar -->

            <div class="page-content sidebar-page clearfix">
                <!-- .page-content-wrapper -->
                <div class="page-content-wrapper">
                    <div class="page-content-inner">
                       <?= $this->fetch('content') ?>
                    </div>
                    <!-- End .page-content-inner -->
                </div>
                <!-- / page-content-wrapper -->
            </div>
        </div>
        <!-- / #wrapper -->
        <div id="footer" class="clearfix sidebar-page right-sidebar-page">
            <!-- Start #footer  -->
            <p class="pull-left">
                Copyrights &copy; <?= date('Y'); ?> Gigstr. All rights reserved.
            </p>
<!--            <p class="pull-right">
                <a href="#" class="mr5">Terms of use</a>
                |
                <a href="#" class="ml5 mr25">Privacy policy</a>
            </p>-->
        </div>
        <!-- End #footer  -->
        <!-- Back to top -->
        <div id="back-to-top"><a href="#">Back to Top</a>
        </div>

        <!-- Javascripts -->
        <!-- Load pace first -->
        <?= $this->Html->script('/plugins/core/pace/pace.min.js'); ?>
        <!--Important javascript libs(put in all pages)-->

        <!--
        [if lt IE 9]>
  <script type="text/javascript" src="js/libs/excanvas.min.js"></script>
  <script type="text/javascript" src="js/libs/respond.min.js"></script>
<![endif]-->
        
        <!-- Bootstrap plugins -->

        <!-- Core plugins ( not remove ) -->
        <?= $this->Html->script('libs/modernizr.custom.js'); ?>
        <!-- Handle responsive view functions -->
        <?= $this->Html->script('jRespond.min.js'); ?>
        <!-- Custom scroll for sidebars,tables and etc. -->
        <?= $this->Html->script('/plugins/core/slimscroll/jquery.slimscroll.min.js'); ?>
        <?= $this->Html->script('/plugins/core/slimscroll/jquery.slimscroll.horizontal.min.js'); ?>
        <!-- Remove click delay in touch -->
        <?php //$this->Html->script('/plugins/core/fastclick/fastclick.js'); ?>
        <!-- Increase jquery animation speed -->
        <?= $this->Html->script('/plugins/core/velocity/jquery.velocity.min.js'); ?>
        <!-- Quick search plugin (fast search for many widgets) -->
        <?= $this->Html->script('/plugins/core/quicksearch/jquery.quicksearch.js'); ?>
        <!-- Bootbox fast bootstrap modals -->
        <?= $this->Html->script('/plugins/ui/bootbox/bootbox.js'); ?> 
        <?= $this->Html->script('/plugins/charts/sparklines/jquery.sparkline.js'); ?>
        <?= $this->Html->script('/plugins/tables/editable-table/mindmup-editabletable.js'); ?>
        <?= $this->Html->script('/plugins/tables/editable-table/numeric-input-example.js'); ?>
        <?= $this->Html->script('/plugins/forms/validation/jquery.validate.js'); ?>

        <?= $this->Html->script('libs/jquery-ui-1.10.4.min.js'); ?>
        <?= $this->Html->script('main.js'); ?>
        <?= $this->Html->script('jquery.dynamic.js'); ?>

        <?php
        if($controller == "Chat" || $controller == "Job" || $controller == "Company" ||
            $controller == "User") {

            echo $this->Html->css('jquery.datetimepicker.css');
            echo $this->Html->css('bootstrap-datetimepicker.css');
            echo $this->Html->script('/plugins/tables/datatables/jquery.dataTables.js');
            echo $this->Html->script('/plugins/tables/datatables/dataTables.tableTools.js');
            echo $this->Html->script('/plugins/tables/datatables/dataTables.bootstrap.js');
            echo $this->Html->script('/plugins/tables/datatables/dataTables.responsive.js');
            echo $this->Html->script('/plugins/tables/datatables/jquery.jeditable.js');
            echo $this->Html->script('/plugins/tables/datatables/jquery.dataTables.editable.js');
            echo $this->Html->script('pages/tables-data.js');
            echo $this->Html->script('pages/tables-editable.js');
        }

        if($controller == "Feed") {
            echo $this->Html->script('jRespond.min.js');
            echo $this->Html->script('libs/jquery-ui.js');
            echo $this->Html->script('pages/feed.js');
            echo $this->Html->script('/plugins/forms/dropzone/dropzone.js');
            echo $this->Html->script('/plugins/charts/sparklines/jquery.sparkline.js');
            echo $this->Html->script('/plugins/ui/bootstrap-sweetalert/sweet-alert.js');
            echo $this->Html->script('/plugins/ui/notify/jquery.gritter.js');
        }

        if($controller == "Schedule" && ( $action == "add" || $action == "edit" )) {
            echo $this->Html->css('summernote.css');
            echo $this->Html->css('jquery-ui.css');
            echo $this->Html->script('libs/jquery-ui.js');
            echo $this->Html->script('/plugins/ui/notify/jquery.gritter.js');
            echo $this->Html->script('/plugins/forms/summernote/summernote.min.js');
            echo $this->Html->script('/plugins/forms/dropzone/dropzone.js');
            echo $this->Html->script('pages/jquery.confirm.js');
            echo $this->Html->script('pages/forms-advanced.js');
            echo $this->Html->script('/plugins/dragndrop/form-builder.min.js');
            echo $this->Html->script('/plugins/dragndrop/form-render.min.js');
            echo $this->Html->script('pages/scheduleadd.js');
        }

        if($controller == "Schedule" && ($action == "index" || $action == "savedescription" || $action == "search" || $action == "allschedule" || $action == "delete")) {

            echo $this->Html->css('summernote.css');
            echo $this->Html->script('libs/jquery-ui.js');
            echo $this->Html->css('jquery-ui.css');
            echo $this->Html->script('/plugins/forms/summernote/summernote.min.js');
            echo $this->Html->script('pages/jquery.confirm.js');
            echo $this->Html->script('pages/schedule.js');
        }

        if($controller == "Schedule" && ($action == "attest" || $action == "attestSearch")) {
            echo $this->Html->css('star-rating.css');
            echo $this->Html->script('/plugins/forms/fancyselect/fancySelect.js');
            echo $this->Html->script('/plugins/forms/select2/select2.js');
            echo $this->Html->script('/plugins/star-rating/star-rating.js');
            echo $this->Html->script('pages/attest.js');
        }

        if($controller == "Schedule") {
            echo $this->Html->css('jquery.datetimepicker.css');
            echo $this->Html->css('bootstrap-datetimepicker.css');
            echo $this->Html->script('/plugins/forms/bootstrap-datepicker/moment.js');
            echo $this->Html->script('/plugins/forms/bootstrap-datepicker/bootstrap-datepicker.js');
            echo $this->Html->script('/plugins/forms/bootstrap-timepicker/bootstrap-timepicker.js');
            echo $this->Html->script('/plugins/forms/bootstrap-datepicker/bootstrap-datetimepicker.js');
            echo $this->Html->script('/plugins/ui/bootstrap-sweetalert/sweet-alert.js');
            echo $this->Html->script('/plugins/ui/notify/jquery.gritter.js');

            echo $this->Html->css('/plugins/froalaEditor/css/froala_editor.min.css');
            echo $this->Html->css('/plugins/froalaEditor/css/froala_style.min.css');
            echo $this->Html->css('/plugins/froalaEditor/css/code_view.min.css');

            echo $this->Html->script('/plugins/froalaEditor/js/froala_editor.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/word_paste.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/code_view.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/lists.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/paragraph_format.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/code_beautifier.min.js');

        }

        if($controller == "Gig" && ( $action == "index" || $action == "add" || $action == "edit" )) {
            echo $this->Html->css('summernote.css');
            echo $this->Html->script('pages/gig.js');
            echo $this->Html->script('/plugins/forms/summernote/summernote.min.js');
            echo $this->Html->script('/plugins/forms/dropzone/dropzone.js');
            echo $this->Html->script('pages/forms-advanced.js');
            echo $this->Html->script('/plugins/ui/notify/jquery.gritter.js');

            echo $this->Html->css('/plugins/froalaEditor/css/froala_editor.min.css');
            echo $this->Html->css('/plugins/froalaEditor/css/froala_style.min.css');
            echo $this->Html->css('/plugins/froalaEditor/css/code_view.min.css');
            echo $this->Html->css('/plugins/froalaEditor/css/image.min.css');
            echo $this->Html->css('/plugins/froalaEditor/css/video.min.css');

            echo $this->Html->script('/plugins/froalaEditor/js/froala_editor.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/word_paste.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/code_view.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/lists.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/paragraph_format.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/code_beautifier.min.js');

            echo $this->Html->script('/plugins/froalaEditor/js/image.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/image_manager.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/link.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/url.min.js');
            echo $this->Html->script('/plugins/froalaEditor/js/video.min.js');
        }

        if($controller == "Candidate") {
            echo $this->Html->script('pages/candidate.js');
        }

       if($controller == "Candidate" && $action == "profile") {
           echo $this->Html->css('star-rating.css');
           echo $this->Html->script('/plugins/star-rating/star-rating.js');
        //   echo $this->Html->script('pages/chat.js');
           echo $this->Html->script('/plugins/forms/bootstrap-datepicker/bootstrap-datepicker.js');
           echo $this->Html->script('/plugins/forms/bootstrap-timepicker/bootstrap-timepicker.js');
           echo $this->Html->script('/plugins/ui/bootstrap-sweetalert/sweet-alert.js');
           echo $this->Html->script('/plugins/ui/notify/jquery.gritter.js');
       }
          
       if($controller == "Chat" && ($action == "allchat" || $action == "search")) {
           echo $this->Html->script('pages/chatinbox.js');
           echo $this->Html->script('/plugins/forms/bootstrap-datepicker/bootstrap-datepicker.js');
           echo $this->Html->script('/plugins/forms/bootstrap-timepicker/bootstrap-timepicker.js');
       }
          
       if($controller == "Job" && ( $action == "add" || $action == "index" )) {
           echo $this->Html->script('pages/job.js');
       }
        ?>
    </body>
</html>
