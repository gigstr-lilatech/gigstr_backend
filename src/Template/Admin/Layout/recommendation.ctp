<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Configure;

$cakeDescription = 'Gigstr';
$img = Configure::read('dev_base_url') .'img/gigstr_recommendation.jpg';
$url = Configure::read('dev_base_url') . 'admin/user/recommendations?id=' . $_GET['id']
?>

<!DOCTYPE html>
<html lang="en-GB" prefix="og: http://ogp.me/ns#">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="Please recommend me on Gigstr!">
    <meta property="og:description" content="Hi, can you write me a recommendation on Gigstr, the new way for students to work parallel to their studies?">
    <meta property="og:image" content="<?= $img; ?>">
    <meta property="og:image:secure_url" content="<?= $img; ?>" />
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@gigstruk" />
    <meta property="og:url" content="<?= $url; ?>">
    <link rel="icon" href="https://backoffice.gigstr.com/img/favicon.png" type="image/png">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://www.gigstr.com/xmlrpc.php">

    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/ico/apple-touch-icon-57-precomposed.png">

    <link rel="dns-prefetch" href="https://ajax.googleapis.com/">
    <link rel="dns-prefetch" href="https://fonts.googleapis.com/">
    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Gigstr » Feed" href="https://www.gigstr.com/feed/">

    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('recommendation_1.css') ?>
    <?= $this->Html->css('recommendation_2.css') ?>
    <?= $this->Html->css('recommendation_3.css') ?>
    <?= $this->Html->css('recommendation.css') ?>


</head>
<body data-rsssl="1" class="page-template page-template-page-templates page-template-page_front-page page-template-page-templatespage_front-page-php page page-id-3249 custom-background siteorigin-panels  user-registration-account user-registration-page group-blog sydney-child siteScrolled" style="">

<?= $this->fetch('content'); ?>



<!--
        [if lt IE 9]>
<script type="text/javascript" src="js/libs/excanvas.min.js"></script>
<!--<script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>-->
<script type="text/javascript" src="js/libs/respond.min.js"></script>
<![endif]-->

<?= $this->Html->script('libs/jquery-2.1.1.min.js'); ?>
<?= $this->Html->script('bootstrap/bootstrap.js'); ?>
<?= $this->Html->script('recommendation/recommendation.js'); ?>
<?= $this->Html->script('recommendation/jquery.validate.min.js'); ?>
<?= $this->Html->script('recommendation/recommendation_1.js'); ?>
<?= $this->Html->script('recommendation/styling-269.min.js'); ?>

<script type="text/javascript">
    /* <![CDATA[ */
    var panelsStyles = {"fullContainer":"body"};
    /* ]]> */
</script>
<script type="text/javascript">document.body.className = document.body.className.replace("siteorigin-panels-before-js","");</script>
</body>
</html>