<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Inbox</h2>
        <span class="txt">This page lists all messages from candidates</span>
    </div>
    <?php if ($autoreply == "1") { ?>
        <div id="warning" style="float: right;margin-top: 10px;padding-top: 35px;text-align: right;">
            <span class="alert alert-danger fade in" style="padding-left: 185px; padding-right: 185px;">Auto reply turned on</span>
        </div>
    <?php } ?>
    <div id="warning2" style="float: right;margin-top: 10px;padding-top: 35px;text-align: right; display:none;">
        <span class="alert alert-danger fade in"
              style="padding-left: 185px; padding-right: 185px;">Auto reply turned on</span>
    </div>
    <!-- Start .row -->
    <div class="row">
        <!-- Start .row -->
        <div class="col-lg-12">
            <!-- col-lg-12 start here -->
            <div class="panel panel-default toggle panelClose panelRefresh">
                <!-- Start .panel -->
                <div class="panel-body" style="padding-top: 8px;padding-bottom: 6px;">
                    <?php echo $this->Form->create('Schedule', array('type' => 'post', 'action' => 'search', 'class' => 'form-inline')); ?>

                    <?php
                    if (isset($_POST["chatfrom"])) {
                        $chatfrom = date('Y-m-d', strtotime($_POST["chatfrom"]));
                    } else {
                        $chatfrom = date('Y-m-d', strtotime(date("Y-m-d") . " -3 days"));
                    }

                    if (isset($_POST["chatto"])) {
                        $chatto = date('Y-m-d', strtotime($_POST["chatto"]));
                    } else {
                        $chatto = date('Y-m-d', strtotime(date("Y-m-d")));
                    }

                    $select_country = '';
                    if(isset($_POST["country"])) {
                        $select_country = $_POST["country"];
                    }


                    ?>
                    <div class="form-group" style="padding: 0 15px;">
                        <div class="form-group">
                            <span style="font-size:12px; display: block;">From</span>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input id="chatfrom" name="chatfrom" type="text" value="<?php echo $chatfrom; ?>"
                                       class="form-control input-sm" style="width:120px">
                            </div>
                        </div>

                        <div class="form-group" style="padding: 0 4px;">
                            <span style="font-size:12px; display: block;">To</span>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input id="chatto" name="chatto" type="text" value="<?php echo $chatto; ?>"
                                       class="form-control input-sm" style="width:120px">
                            </div>
                        </div>

                        <div class="form-group">
                            <span style="font-size:12px; display: block;">Country</span>
                            <select name="country" id="country" class="form-control" id="country" style="margin-right: 3px; height: 30px;">
                            <option value="all">All</option>
                            <?php foreach ($countries as $con): ?>
                                <option <?php echo($select_country == $con['code'] ? 'selected' : ''); ?> value="<?= $con['code']; ?>"><?= $con['name']; ?></option>
                            <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <span style="font-size:12px; display: block;">Unread</span>
                            <div class="input-group">
                                <input type="checkbox" <?php echo(isset($_POST["unread"]) ? 'checked' : ''); ?> name="unread" id="unread" class="form-control input-sm">
                            </div>
                        </div>

                        <div class="form-group" style="padding: 0 0; margin-top:15px;">
                            <div class="input-group">
                                <?php echo $this->Form->submit('OK', array('class' => 'btn btn-success input-sm', 'style' => "height: 2em;")); ?>
                            </div>

                        </div>
                    </div>
                    <?php echo $this->Form->end() ?>
                    <div class="input-group" style="float:right;margin-top:-24px;margin-right:23px">
                        <input type="checkbox" name="autoreply" id="autoreply"
                               <?php if (($autoreply == '1')) { ?>checked="checked" <?php }; ?>>Auto reply
                    </div>
                    <div class="panel-body chat_pagination">
                        <?= $this->Flash->render(); ?>
                        <table id="basic-datatables-chat" class="table table-striped table-bordered" cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th class="per15">Chat</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $count = 0;
                            foreach ($chatsnew as $chat):
                                 $schedulestartdate = new DateTime($chat['created'], new DateTimeZone('UTC'));
                                ?>

                                <input type="hidden" value="<?php $chat['chat_id'] ?>" id="chat_id" name="chat_id">
                                <tr>

                                    <?php //if ($chat['notification'] == '1') {  ?>

    <?php if(!empty($notification_chat_ids)&&in_array($chat["chat_id"],$notification_chat_ids)) {
                                    $unread = 'strong';

                                    } else { $unread = ''; }
                                    ?>
                                        <td>

                                                <div class="fullwidth custom">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                    <a href="<?= $this->Url->build([
                                                "controller" => "Candidate",
                                                "action" => "profile",
                                                $chat['id'], $chat['chat_id']
                                            ]);
                                            ?> " onclick="return readchat();">
                                                            <span class="chats-<?= $count ?> name <?= $unread ?>"><?= $chat['name'] ?></span>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-1">
                                                           <a href="javascript:;" class="pull-right text-right unreadChat" chat="<?=$chat['chat_id'] ?>" gig="<?=$chat['gig_id']?>" count="<?= $count ?>"  id="" ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> </a>                                                                                                         </div>

    <div class="col-md-8">
        <span class="chats-<?= $count ?> title <?= $unread ?>"><?= $chat['title'] ?></span>
        <span
            class="chats-<?= $count ?> message "><?php echo base64_decode($chat['message']); ?></span>
    </div>
                                                        <div class="col-md-1">
                                                                   <span class="chats-<?= $count ?> time <?= $unread ?> text-right">
        <?php if (date('Y-m-d') == date('Y-m-d', strtotime($chat['created']))) {
$schedulestartdate->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedulestart = $schedulestartdate->format('Y-m-d H:i');

            echo $schedulestartdate->format('G:i');
        } else {
             echo $schedulestartdate->format('Y-m-d');
        } ?>
                                                                   </span>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                </tr>
                            <?php
                            $count ++;
                            endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End .panel -->
            </div>
            <!-- col-lg-12 end here -->
        </div>
        <script>
            var base_url = '<?php echo $base_url; ?>';
        </script>
        <script type="text/javascript">
            function readchat() {
                var base_url = '<?php echo $base_url; ?>';
                var chat_id = $('#chat_id').val();
                $.ajax({
                    url: base_url + "admin/chat/readchat",
                    dataType: 'json',
                    data: {chat_id: chat_id},
                    async: false
                });
            }
        </script>


