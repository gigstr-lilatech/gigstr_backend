 <!-- Start .page-content-inner -->
                        <div id="page-header" class="clearfix">
                            <div class="page-header">
                                <h2>Inbox</h2>
                                <span class="txt">This page lists all masseges available in the database</span>
                            </div>
                            
                        </div>
                        <!-- Start .row -->       
<div class="row">
                            <!-- Start .row -->
                            <div class="col-lg-12">
                                <!-- col-lg-12 start here -->
                                <div class="panel panel-default toggle panelClose panelRefresh">
                                    <!-- Start .panel -->
                                    <div class="panel-heading">
                                        <h4 class="panel-title">All Masseges</h4>
                                    </div>
                                    <div class="panel-body">
                                        <?= $this->Flash->render(); ?>
                                        <table id="basic-datatables-chat" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th class="per15">Chat</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                foreach ($chats as $chat): ?>
                                                <tr>
                                                <td>
                                                        <?= $chat['name']."-".$chat['title']."  ".$chat['date']?>
                                                        <br>
                                                        <a href="<?=
                                                            $this->Url->build([
                                                                "controller" => "Candidate",
                                                                "action" => "profile",
                                                                $chat['id']
                                                            ]);
                                                            ?> "><?= $chat['message']?></a>
                                                </td>
                                                </tr>
                                            <?php  endforeach; ?>
                                            </tbody>                                       
                                        </table>
                                    </div>
                                </div>
                                <!-- End .panel -->
                            </div>
                            <!-- col-lg-12 end here -->
                        </div>


        