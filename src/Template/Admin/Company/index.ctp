<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Companies</h2>
        <span class="txt">This page lists all companies available in the database</span>
    </div>

</div>
<!-- Start .row -->       
<div class="row">
    <!-- Start .row -->
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">All Companies</h4>
            </div>
            <div class="panel-body">
                <?= $this->Flash->render(); ?>
                <table id="basic-datatables-companies" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="per5">
                                ID
                            </th>
                            <th class="per15">Name</th>
                            <th class="per15">Status</th>
                            <th class="per15">Last Modified</th>
                            <th class="per15">Delete</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        foreach ($company as $com):
                            ?>
                            <tr>
                                <td><?= $com->id; ?></td>
                                <td><?= $com->name ?></td>
                                <td><?= ($com->status == '1')? "Active": "Deactive"; ?></td>
                                <td><?= date('Y-m-d',strtotime($com->modified)); ?></td>
                                <td class="actions">
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $com->id], ['confirm' => __('Are you sure you want to delete this company? Company ('.$com->name.') WARNING: This will remove all Gigs created for this company', $com->id)]) ?>
                                </td>
                            </tr>
                        <?php
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
</div></div>


