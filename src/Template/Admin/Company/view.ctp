<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Company'), ['action' => 'edit', $company->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Company'), ['action' => 'delete', $company->id], ['confirm' => __('Are you sure you want to delete # {0}?', $company->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Company'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Company'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Gig'), ['controller' => 'Gig', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Gig'), ['controller' => 'Gig', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="company view large-9 medium-8 columns content">
    <h3><?= h($company->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($company->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($company->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Updated') ?></th>
            <td><?= h($company->updated) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($company->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $this->Number->format($company->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Created By') ?></th>
            <td><?= $this->Number->format($company->created_by) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Gig') ?></h4>
        <?php if (!empty($company->gig)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Company Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Gig Image') ?></th>
                <th><?= __('Sub Title1') ?></th>
                <th><?= __('Sub Title2') ?></th>
                <th><?= __('Status') ?></th>
                <th><?= __('Created By') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Updated') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($company->gig as $gig): ?>
            <tr>
                <td><?= h($gig->id) ?></td>
                <td><?= h($gig->company_id) ?></td>
                <td><?= h($gig->title) ?></td>
                <td><?= h($gig->gig_image) ?></td>
                <td><?= h($gig->sub_title1) ?></td>
                <td><?= h($gig->sub_title2) ?></td>
                <td><?= h($gig->status) ?></td>
                <td><?= h($gig->created_by) ?></td>
                <td><?= h($gig->description) ?></td>
                <td><?= h($gig->created) ?></td>
                <td><?= h($gig->updated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Gig', 'action' => 'view', $gig->id]) ?>

                    <?= $this->Html->link(__('Edit'), ['controller' => 'Gig', 'action' => 'edit', $gig->id]) ?>

                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Gig', 'action' => 'delete', $gig->id], ['confirm' => __('Are you sure you want to delete # {0}?', $gig->id)]) ?>

                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
    </div>
</div>
