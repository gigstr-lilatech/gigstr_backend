<?php if($response == 'success'): ?>     

<div class="panel-body">
                    <?= $this->Flash->render(); ?>
                    <?= $this->Form->create("User",['id'=>'password-form', 'class'=>'form-horizontal mt0', 'url' => ['controller' => 'User', 'action' => 'resetPassword', 'prefix' => 'admin']]) ?>
                        <div class="alert alert-danger" id="error-message" style="display:none;"></div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="input-group input-icon">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <?= $this->Form->password('password', ['class'=>'form-control','id'=>'password','placeholder'=>'New password']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="input-group input-icon">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <?= $this->Form->password('confirmpassword', ['class'=>'form-control','id'=>'confirmpassword','placeholder'=>'Confirm new password']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb0">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                               <?= $this->Form->hidden('email_reset',['value'=>$email]); ?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                                <?= $this->Form->button('Set new password', ['type' => 'submit','class'=>'btn btn-default pull-right']); ?>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                  
                </div>

<?php elseif($response == 'error'): ?>

                <div class="panel-body">
                    <?= $this->Flash->render(); ?>
                    <div class="alert alert-danger">
                        You are not authorized to access this page
                    </div>
                </div>
          
<?php elseif($response == 'done'): ?>

                <div class="panel-body">
                    <?= $this->Flash->render(); ?>
                    
                </div>
          
<?php endif; ?>