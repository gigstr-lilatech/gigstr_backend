<div class="preloader" style="opacity: 0; display: none;">
    <div class="spinner">
        <div class="pre-bounce1"></div>
        <div class="pre-bounce2"></div>
    </div>
</div>

<div id="page" class="hfeed site">
    <a class="skip-link screen-reader-text" href="https://www.gigstr.com/form-test/#content">Skip to content</a>


    <div class="header-clone" style="height: 88px;"></div>


    <header id="masthead" class="site-header fixed" role="banner">
        <div class="header-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-8 col-xs-12">
                        <a href="https://www.gigstr.com/" title="Gigstr"><img class="site-logo"
                                                                              src="/img/recommendation/gigstr-logo-dark.svg"
                                                                              alt="Gigstr"></a>
                    </div>
                    <div class="col-md-8 col-sm-4 col-xs-12">
                        <div class="btn-menu"></div>

                        <nav id="mainnav" class="mainnav" role="navigation">
                            <div class="menu-menueng-container"><ul id="menu-menueng" class="menu"><li id="menu-item-12175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12175"><a href="https://www.gigstr.com/post-a-job/">Hire staff</a></li>

                                    <li id="menu-item-12869" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12869"><a href="https://www.gigstr.com/hire-staff/">I’m a company</a></li>


                                </ul></div>					</nav><!-- #site-navigation -->
                    </div>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->


    <div class="sydney-hero-area">
        <div class="header-image">
            <div class="overlay"></div>			<img class="header-inner" src="https://www.gigstr.com/form-test/" width="1920" alt="Gigstr" title="Gigstr">
        </div>

    </div>


    <div id="content" class="page-wrap">
        <div class="container content-wrapper">
            <div class="row">
                <div id="primary" class="fp-content-area">
                    <main id="main" class="site-main" role="main">

                        <div class="entry-content">
                            <div id="pl-3249" class="panel-layout"><div id="pg-3249-0" class="panel-grid panel-has-style"><div class="siteorigin-panels-stretch panel-row-style panel-row-style-for-3249-0" style="padding: 100px 0px; background-position: 50% 0px; margin-left: -200px; margin-right: -200px; border-left: 0px; border-right: 0px;" data-stretch-type="full-stretched" data-overlay="true" data-overlay-color="#000000"><div id="pgc-3249-0-0" class="panel-grid-cell" style="padding-left: 0px; padding-right: 0px;"><div id="panel-3249-0-0-0" class="widget_text so-panel widget widget_custom_html panel-first-child panel-last-child" data-index="0"><div style="text-align: left;" id="recommendation-container" data-title-color="#1d202a" data-headings-color="#1d202a" class="widget_text panel-widget-style panel-widget-style-for-3249-0-0-0"><div class="textwidget custom-html-widget"><div class="recommendation-wrapper">

                                                            <div class="recommendation-col-1">
                                                                <img src="/img/recommendation/recommendation.jpg" alt="Students are
                                                                using the Gigstr app">
                                                            </div>

            <div class="recommendation-col-2">
                <div class="rec-col-2-wrapper">
                    <div class="rec-col-2-header">
                        <h3 style="color: rgb(29, 32, 42);">Hi, can you write me a recommendation?</h3>
                        <p>I have just started working via the Gigstr app, the new way for students to work parallel to their studies. Would you recommend me to other companies?</p>
                        <h3 class="rec-works" style="color: rgb(29, 32, 42);"><a href="https://www.gigstr.com/how-recommendations-work" style="color: rgb(29, 32, 42);">How recommendations work</a></h3>
                    </div>
                    <div class="recommendation-form">
                        <div role="form" class="wpcf7" id="wpcf7-f12999-p3249-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>

                            <form action="" id="recommendation_form" method="post" class="recommendation_form"
                                  novalidate="novalidate">

            <?php
            if(isset($_GET['id']) && !empty($_GET['id'])) {
                $gigstr = $_GET['id'];
                $userId = openssl_decrypt($gigstr,"AES-128-ECB","user_id");
            }
            else {
                $userId = "";
            }
            ?>
                                <input type="hidden" name="user_id" id="user_id" value="<?= $userId; ?>">

                                <p>    <p class="wpcf7-character-count down" id="rec_char"
                                         data-target-name="textarea-recommendation">280</p><br>
                                    <span class="wpcf7-form-control-wrap textarea-recommendation">
                                        <textarea name="recommendation" cols="40" rows="10" maxlength="280" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" id="recommendation" aria-required="true" aria-invalid="false" placeholder="I would recommend..."></textarea>
                                        <span role="alert" class="wpcf7-not-valid-tip reco_err hide_err">The field is required
                                            .</span>
                                    </span>
                                    <br>
                                    <span class="wpcf7-form-control-wrap text-name">
                                        <input required type="text" name="name"  size="40" class="wpcf7-form-control
                                        wpcf7-text wpcf7-validates-as-required" id="name" aria-required="true"
                                               aria-invalid="false" placeholder="Your name">
                                        <span role="alert" class="wpcf7-not-valid-tip name_error hide_err">The field is
                                            required
                                            .</span>
                                    </span>
                                    <span class="wpcf7-form-control-wrap text-company">
                                        <input required type="text" name="company"  size="40"
                                                class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" id="company" aria-required="true" aria-invalid="false" placeholder="Company">
                                        <span role="alert" class="wpcf7-not-valid-tip company_error hide_err">The field is
                                            required.</span>
                                    </span>
                                    <br>
                                    <button type="submit" value="SEND RECOMMENDATION" class="wpcf7-form-control
                                    wpcf7-submit" id="form-rec-submit">SEND RECOMMENDATION</button>
                                </p>
                                <div class="wpcf7-response-output wpcf7-display-none wpcf7-mail-sent-ok" id="from_submit"></div>
                            </form>

                        </div>
                    </div>
                    <p class="recommendation-about"><a href="https://www.gigstr.com/hire-staff/">About Gigstr</a></p>
                </div>
            </div>

                                                        </div></div></div></div></div></div></div></div>							</div><!-- .entry-content -->

                    </main><!-- #main -->
                </div><!-- #primary -->

            </div>
        </div>
    </div><!-- #content -->





    <div id="sidebar-footer" class="footer-widgets widget-area" role="complementary">
        <div class="container">
            <div class="sidebar-column col-md-4">
                <aside id="text-3" class="widget widget_text"><h3 class="widget-title">This is Gigstr</h3>			<div class="textwidget">Gigstr is the gig platform connecting students and companies! Companies post jobs for free, match with and hire young professionals. You can trial hire before deciding to make a permanent hire, or simply continue to hire via Gigstr. As a student you find jobs and act as your own boss, without all the admin.</div>
                </aside><aside id="custom_html-5" class="widget_text widget widget_custom_html"><div class="textwidget custom-html-widget"><style>
                            #custom_html-5 {
                                padding-top: 0!important;
                            }
                            img.footer-partner {
                                max-width: 100px;
                            }
                            p.footer-partner-text {
                                margin-bottom: 10px!important;
                            }
                        </style>
                        <p class="footer-partner-text">
                            In collaboration with
                        </p>
                        <a href="https://www.gigstr.com/gigstr-celebrates-living-wage-commitment/"><img
                                    class="footer-partner" src="/img/recommendation/living-wage-foundation-logo.png"
                                    alt="Living Wage
                                     logo"></a></div></aside>				</div>

            <div class="sidebar-column col-md-4">
                <aside id="custom_html-3" class="widget_text widget widget_custom_html"><h3 class="widget-title">Get in touch</h3><div class="textwidget custom-html-widget"><div class="a-footer">
                            <div class="a-footer-box">
                                <img src="/img/recommendation/home-icon.png" alt="home">
                            </div>
                            <div class="a-footer-box">
                                <p>
                                    1 St Katherine's Way | London E1W 1UN
                                </p>
                            </div>
                        </div>

                        <div class="a-footer">
                            <div class="a-footer-box">
                                <img src="/img/recommendation/phone-icon.png" alt="phone">
                            </div>
                            <div class="a-footer-box">
                                <p>
                                    <a href="tel:+442039113650">+442039113650</a>
                                </p>
                            </div>
                        </div>

                        <div class="a-footer">
                            <div class="a-footer-box">
                                <img src="/img/recommendation/mail-icon.png" alt="mail">
                            </div>
                            <div class="a-footer-box">
                                <p>
                                    <a href="mailto:support@gigstr.com">support@gigstr.com</a>
                                </p>
                            </div>
                        </div>
                        <div class="a-footer">
                            <div class="a-footer-box">
                                <img src="/img/recommendation/helpcenter1.png" alt="helpcenter icon">
                            </div>
                            <div class="a-footer-box">
                                <p>
                                    <a href="https://intercom.help/gigstr">
                                        Help center
                                    </a>
                                </p>
                            </div>
                        </div></div></aside>				</div>




        </div>
    </div>
    <a class="go-top"><i class="fa fa-angle-up"></i></a>

    <footer id="colophon" class="site-footer" role="contentinfo">
        <div class="site-info container">
            <a href="http://wordpress.org/">Proudly powered by WordPress</a>
            <span class="sep"> | </span>
            Theme: <a href="https://athemes.com/theme/sydney" rel="designer">Sydney</a> by aThemes.		</div><!-- .site-info -->
    </footer><!-- #colophon -->


</div><!-- #page -->
<script>
    var base_url = '<?php echo $base_url; ?>';
</script>