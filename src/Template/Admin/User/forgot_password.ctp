 <!-- Start login container -->
        <div class="container login-container">
            <div class="login-panel panel panel-default plain animated bounceIn">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title text-center">
                        <?= $this->Html->image('logo-dark.png', ['alt' => 'Gigstr Logo']); ?>
                    </h4>
                    <?= $this->Flash->render(); ?>
                </div>
                <div class="panel-body">
                    <?= $this->Flash->render(); ?>
                    <h5>Type your email address to receive the password reset instructions via email</h5>
                    <?= $this->Form->create("User",['id'=>'forgot-password-form']) ?>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="input-group input-icon">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <?= $this->Form->text('email', ['class'=>'form-control required','id'=>'email']) ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group mb0">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                                <?= $this->Form->button('Login', ['type' => 'submit','class'=>'btn btn-default pull-right']); ?>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
               
                </div>
            </div>
            <!-- End .panel -->
        </div>
        <!-- End login container -->