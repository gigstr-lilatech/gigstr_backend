<!-- Start login container -->
        <div class="container login-container">
            <div class="login-panel panel panel-default plain animated bounceIn">
                <!-- Start .panel -->
                <div class="panel-heading">
                    <h4 class="panel-title text-center">
                        <?= $this->Html->image('logotype_backoffice_login.png', ['alt' => 'Gigstr Logo']); ?>
                    </h4>
                    <?= $this->Flash->render(); ?>
                </div>
                <div class="panel-body">
                    
                    <?= $this->Form->create("User",['id'=>'login-form', 'class'=>'form-horizontal mt0']) ?>
                        <div class="alert alert-danger" id="error-message" style="display:none;"></div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="input-group input-icon">
                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                    <?= $this->Form->text('email', ['class'=>'form-control','id'=>'email','placeholder'=>'Email']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="input-group input-icon">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <?= $this->Form->password('password', ['class'=>'form-control','id'=>'password','placeholder'=>'Password']) ?>
                                </div>
                                <!--<span class="help-block text-right"><a href="#">Forgot password ?</a></span>--> 
                            </div>
                        </div>
                        <div class="form-group mb0">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
<!--                                <div class="checkbox-custom">
                                    <input type="checkbox" name="remember" id="remember" value="option">
                                    <label for="remember">Remember me ?</label>
                                </div>-->
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                                <?= $this->Form->button('Login', ['type' => 'submit','class'=>'btn btn-default pull-right']); ?>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
               
                </div>

            </div>
            <!-- End .panel -->
        </div>
        <!-- End login container -->