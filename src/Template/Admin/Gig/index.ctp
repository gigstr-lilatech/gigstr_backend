 <!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2><?= __('Ads'); ?></h2>
        <span class="txt"><?= __('This page lists all ads available in the database'); ?></span>
     </div>

</div>
<!-- Start .row -->
<div class="row">
    <!-- Start .row -->
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <?= $this->Flash->render(); ?>
        <div class="panel panel-default toggle panelClose panelRefresh">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title pull-left"><?= __('All Ads'); ?></h4>
                <!--<div class="pull-right" style="margin-top:6px;">
                     <?/*= $this->Html->link('Create Ad', ['action' => 'add'],['class'=>'btn btn-success create-gig-button']) */?>
                </div>-->
            </div>

            <div class="panel-body filter">
                <?php echo $this->Form->create('Gig', array('type' => 'get', 'url' => 'admin/gig', 'class'=> 'form-inline'));?>

                <div class="form-group gig_country">
                    <span style="font-size:12px; display: block;">Country</span>
                    <div class="input-group">
                        <select id="gig_country" name="country" class="form-control input-sm">
                            <option value="all">All</option>
                        <?php foreach($country as $code) { ?>
                            <option <?php echo (isset($_GET["country"]) && ($_GET["country"] == $code['country_code']) ? 'selected' : ''); ?> value="<?= $code['country_code']; ?>"><?= $code['country_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <span style="font-size:12px; display: block;">Status</span>
                    <div class="input-group">
                        <select id="status_type" name="status" class="form-control">
                            <option value="all">All</option>
                            <?php
                            foreach($status as $key => $state) { ?>
                                <option <?php echo (isset($_GET["status"]) && ($_GET["status"] == $key) && ($_GET["status"] != 'all') ? 'selected' : ''); ?> value="<?= $key ?>"><?= $state ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group" style="padding: 0 0; margin:15px 0 0 12px;">
                    <div class="input-group">
                        <?php echo $this->Form->submit('OK',array('class'=>'btn btn-success','style'=>"height: 2em;")); ?>
                    </div>
                </div>

                <?php echo $this->Form->end() ?>
            </div>



            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <input class="form-control input-sm gigSearch" placeholder="Search" name="addSearch" id="addSearch">
                    </div>
                    <div class="col-md-10 ">
                        <span id="PreeValue" class="pagination_button">← Previous</span>
                        <span id="nextValue" class="pagination_button">Next →</span>
                        <ul class="pagination pull-right">
                            <?php
                            echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                            echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));
                            ?>
                        </ul>
                    </div>
                </div>


              <table id="basic-datatables-gigsxx" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="per5"><?php echo $this->Paginator->sort('Gig.id', 'Id'); ?></th>
                        <th class="per15"><?php echo $this->Paginator->sort('Gig.title', 'Title');?></th>
                        <th class="per15"><?php echo $this->Paginator->sort('Gig.start_date', 'Start date ');?></th>
                        <th class="per15"><?php echo $this->Paginator->sort('Company.name', 'Company');?></th>
                        <th class="per10"><?php echo $this->Paginator->sort('Gig.country', 'Country');?></th>
                        <th class="per10"><?php echo $this->Paginator->sort('User.name', 'Created By');?></th>
                        <th class="per5"><?php echo $this->Paginator->sort('Gig.status', 'Status'); ?></th>
                        <th class="per15"><?php echo $this->Paginator->sort('Gig.created', 'Created'); ?></th>
                        <th class="per20"><?php echo $this->Paginator->sort('Gig.modified', 'Modified'); ?></th>
                        <th class="per10"><?= __('Candidates'); ?></th>
                    </tr>
                    </thead>
                                            
                    <tbody id="addTable">
                     <?php
                    foreach ($gigs as $gig) { ?>
                        <tr class="<?php if ($gig->notification == '1') {
                            echo 'bold_row'; } ?>">

                            <td><?= $gig['id'] ?></td>
                            <td><?= $this->Html->link(
                                    (!empty($gig->title) ? $gig->title : $gig->skill->skill),
                                    ['controller' => 'gig', 'action' => 'edit', $gig->id]
                                ); ?>
                            </td>
                            <td><?php echo ($gig->start_date) ? date('Y-m-d', strtotime($gig->start_date)) : 'ASAP';?></td>
                            <td><?= $gig['company']['name'] ?></td>
                            <td><?= $gig->countryName ?></td>
                            <td><?= $gig['user']['name'] ?></td>
                            <td><?= $gig['status'] ?></td>
                            <td><?= date('Y-m-d', strtotime($gig->created)); ?></td>

                            <td><?= date('Y-m-d', strtotime($gig->modified)); ?></td>
                            <td><?= count($gig['application']) ?></td>
                        </tr>
                    <?php
                    } ?>
                            </tbody>
                        </table>
            <ul class="pagination">
                <?php
                echo $this->Paginator->prev(__('← Previous'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('Next →'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a',  'ellipsis' => '' ));?>
            </ul>
                    </div>
                </div>
            <!-- End .panel -->
        </div>
        <!-- col-lg-12 end here -->
    <script>
        var base_url = '<?php echo $base_url; ?>';
    </script>
    </div>



        