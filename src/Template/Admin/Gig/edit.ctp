<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2><?= __('Ads'); ?></h2>
        <span class="txt"><?= __('This page displays information about Ads'); ?></span>
    </div>

</div>
<!-- Start .row -->
<div class="row">
    <div class="col-lg-6 col-md-6">
        <!-- col-lg-6 start here -->

        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Ad Information</h4>

            </div>

            <div class="panel-body">
                <div class="alert_area">
                    <?= $this->Flash->render(); ?>
                    <div class="alert alert-success" id="push_success" style="display:none;">
                        <p>Push message has been sent.</p>
                    </div>
                    <div class="alert alert-danger" id="push_error" style="display:none;">
                        <p>Unable to send push message. Please try again.</p>
                    </div>
                </div>

                <div class="img_area row">
                    <div class="col-md-8">
                    <?php
                                $url = $base_url.'img/gig_images/';?>
                    <?= $this->Html->image($url . $gig->gig_image, ['alt' => 'Gigstr Logo', 'width' => '100', 'class' => 'gig_img']); ?>
                    <h2 class="gig_title"><?php echo (!empty($gig->title) ? $gig->title : $gig['skill']->skill); ?></h2>
                        <small class="created_by"><?= __('Created by:'); ?> <?= $gig['user']['name']; ?></small>
                        <br/>
                        <small class="updated"><?= __('Last updated:'); ?> <?= date('Y-m-d', strtotime($gig->modified)); ?></small>
                    <?php echo $this->Form->postLink(__(''), ['action' => 'delete', $gig->id], ['class' => 'delete_item', 'confirm' => __('Are you sure you want to delete the gig "' . $gig->title . ' (Company : ' . $gig['company']['name'] . ' , No of Candidates : ' . count($gig['application']) . ')"?', $gig->id)]) ?>
                    </div>
                    <div class="col-md-4">
                        <p>Status</p>
                        <h4 class="setStatus">
                            <?php echo $gig->status; ?>
                        </h4>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="javascript:;" data-id="<?= $gig->id; ?>" id="activate_days" class="btn btn-success mr5 mb10" <?php echo ($gig->status == 'Published' ? 'disabled' : '');?>>Activate 14 Days</a>
                            </div>
                            <div class="col-md-12">
                                <a href="javascript:;" data-id="<?= $gig->id; ?>" id="declined_add" class="btn btn-danger mr5 mb10" <?php echo ($gig->status == 'Declined' ? 'disabled' : '');?>>Decline</a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="row link-outer">
                    <label for="" class="col-sm-2 control-label link-label">Deep Link</label>
                    <div class="col-sm-6">
                        <div class='deep_link'></div>
                    </div>
                    <div class="col-sm-3">
                        <button id='deep_link_btn' class="btn btn-success">Generate Link</button>
                    </div>
                 </div>

                <?= $this->Form->create($gig, ['class' => 'form-horizontal', 'role' => 'form', 'name' => 'gig', 'id' => 'gig_form']) ?>

                <div class="alert alert-danger" id="error-message" style="display:none;"></div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Title</label>
                    <div class="col-sm-12">

                        <?= $this->Form->text('title', ['class' => 'form-control ', 'id' => 'title', 'required' => false]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="company" class="company col-sm-2 control-label">Company</label>
                    <div class="col-sm-12">
                        <input id="company" class="form-control" disabled value="<?php echo $gig["company"]->name; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="website" class="col-sm-12 control-label">Website</label>
                    <div class="col-sm-12">
                        <input id="website" class="form-control" disabled value="<?php echo $gig->website; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="gig_address" class="col-sm-12 control-label">Gig Address</label>
                    <div class="col-sm-12">
                        <input id="gig_address" class="form-control" disabled
                               value="<?php echo $gig->address; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="skill" class="col-sm-12 control-label">Skill</label>
                    <div class="col-sm-12">
                        <input id="skill" class="form-control" disabled value="<?php echo $gig['skill']->skill; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="website" class="col-sm-12 control-label">Start Date</label>
                            <div class="col-sm-12">
                                <input id="website" class="form-control" disabled value="<?php echo (!empty($gig->start_date) ? date('Y-m-d', strtotime($gig->start_date)) : ""); ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="website" class="col-sm-12 control-label">End Date</label>
                            <div class="col-sm-12">
                                <input id="website" class="form-control" disabled value="<?php echo (!empty($gig->end_date) ? date('Y-m-d', strtotime($gig->end_date)) : ""); ?>">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="gigstrs_needed" class="col-sm-12 control-label">Gigstr Needed</label>
                    <div class="col-sm-12">
                    <select name="gigstrs_needed" class="form-control" id="gigstrs_needed">
                    <?php foreach ($gigsterNeed as $con): ?>
                        <option value="<?= $con; ?>" <?php echo(($con == $gig->gigstrs_needed) ? "selected" : "");?>  ><?= $con; ?></option>
                    <?php endforeach;?>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Gig Description</label>
                    <div class="col-sm-12">

                        <?php  echo $this->Form->textarea('description', ['class' => 'form-control', 'id' => 'editor', 'required' => false]); ?>
                    </div>
                </div>

                <?php /*= $this->Form->hidden('created_by', ['value' => $gig->created_by]); */?>
                <?= $this->Form->hidden('gig_image', ['value' => $gig->gig_image, 'id' => 'gig_image']); ?>
                <?php /*= $this->Form->hidden('status', ['value' => $gig->status]); */?>
                <?= $this->Form->hidden('gig_id', ['value' => $gig->id, 'id' => 'gig_id']); ?>
                <?= $this->Form->hidden('country', ['value' => $gig->country, 'id' => 'country']); ?>

                <div class="form-group gig_edit_buttons">
                    <div>
                        <?= $this->Form->button('UPDATE', ['type' => 'submit', 'class' => 'btn btn-success']); ?>
                    </div>
                </div>
                <?php
                $placeholder = "Push message will be sent to all Gigstrs in country.";
                $disabled = false;
                if ($gig->status != 'Published') {
                    $placeholder = "Push message is only available for active gigs";
                    $disabled = true;
                }
                ?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Push message</label>
                    <div class="col-sm-12">
                        <?php echo $this->Form->textarea('push_message', ['class' => 'form-control', 'id' => 'push_message', 'required' => false, 'placeholder' => $placeholder, 'disabled' => $disabled]); ?>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="checkbox">
                                    <label for="area_push">
                                        <input type="checkbox"  id="area_push" name="area_push"> Send push
                                        message only for <?= $gig->area;?> area
                                    </label>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <button class="btn btn-success pull-right" id="push_message_ajax" onclick="return false;" style="margin-top: 10px;" <?= ($gig->active == '2') ? 'disabled' : ''; ?>>SEND PUSH</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 0px;">
                    <label for="inputEmail3" class="col-sm-12 control-label">Edit Image</label>
                    <div class="col-sm-12">
                        <div id="image_upload_error" class="alert alert-danger" style="display: none;"><p></p></div>
                    </div>
                </div>
                <?= $this->Form->end() ?>

                <form id="my-awesome-dropzone" class="dropzone small-dropzone">
                    <div class="dz-message" data-dz-message>
                        <h3>Drop image or click to select</h3>
                        <span>Drop or select image here. Image should be JPEG, 800x600px and not exceed 250 KB</span>
                    </div>
                </form>

                <div id ="gig_activate">
                    <a href="javascript:;" class="btn btn-danger ml10 gig_activate" data-active="<?php echo  $gig->status; ?>">
                        <?php echo($gig->status == 'Published' ? 'DEACTIVATE' : 'ACTIVATE');?>
                    </a>
                </div>
                <script>
                    var base_url = '<?php echo $base_url; ?>';
                </script>
            </div>
        </div>
    </div>
    <!-- col-lg-6 end here -->
    <div class="col-lg-6 col-md-6">
        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= __('Candidates'); ?></h4>
            </div>
            <div class="panel-body">
            
                <table id="basic-datatables-applicants" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="per15"><?= __('Date'); ?></th>
                            <th class="per30"><?= __('Name'); ?></th>
                            <th class="per15"><?= __('Candidate'); ?></th>
                            <th class="per15"><?= __('Selection'); ?></th>
                            <th class="per15"><?= __(''); ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if (count($applicantsArr) > 0):
                            foreach ($applicantsArr as $application):
                                ?>
                                <tr>
                                    <td><?= $application['date']; ?></td>
                                    <td>
                                        <a data-html="true" data-toggle="tooltip" data-placement="bottom" title="<?= $application['pastSelect']; ?>" href="<?=  $this->Url->build([
                                            "controller" => "Candidate","action" => "profile",$application['user_id']]);
                                        ?>">
                                            <?= $application['name']; ?>
                                        </a>
                                    </td>
                                    <td><?php echo ($application['candidate'] == 1 ? 'Verified' : '<span class="setStatus">Not verified</span>');?></td>
                                    <td id="select_<?=$application["application_id"]; ?>"><?= $application['selection'];?></td>
                                    <td>
                                        <select data-selection="<?=$application["selection_id"];?>" data-application="<?=$application["application_id"];?>" data-gig="<?=$application["gig_id"]; ?>" data-user="<?=$application["user_id"];?>" class="form-control candidate_state">
                                            <option value="">Select</option>
                                            <option value="SHORTLIST">Shortlist</option>
                                            <option value="REJECTED">Rejected</option>
                                            <option value="INTERVIEW">Interview</option>
                                            <option value="BANNED">Banned</option>
                                        </select>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <script>
        var base_url = '<?php echo $base_url; ?>';
    </script>
</div>

<!--</div>
</div>-->