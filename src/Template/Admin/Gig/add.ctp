<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2>Create Ad</h2>
        <span class="txt">This page allows Admin to create ads</span>
    </div>

</div>
<!-- Start .row -->
<div class="row">
    <div class="col-lg-6 col-md-6">
        <!-- col-lg-6 start here -->

        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title">Ads Information</h4>
            </div>
            <div class="panel-body">
                <?= $this->Form->create($gig, ['class' => 'form-horizontal', 'role' => 'form', 'id' => 'create_gig']) ?>
                <?= $this->Flash->render(); ?>
                <div class="alert alert-danger" id="error-message" style="display:none;"></div>
                <div class="form-group">
                    <div class="alert alert-danger" id="company_error" style="display: none;"></div>
                    <label for="inputEmail3" class="col-sm-2 control-label">Company</label>
                    <div class="col-sm-5">
                        <select name="company_id" class="form-control" id="company_id" label="">
                            <option value="-1">Select company</option>
                            <?php foreach ($company as $comp): ?>
                                <option value="<?= $comp->id; ?>"><?= $comp->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <?= $this->Form->button('Add company', ['type' => 'button', 'class' => 'btn btn-success', 'id' => 'add_company', 'required' => false]); ?>
                        <div class="save_comp">
                            <input type="text" name="company_name" id="company_name" value="" class="form-control" placeholder="New company"/><br/>
                            <button class="btn btn-success pull-right" id="save_company_ajax" disabled onclick="return false;">SAVE</button>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Title</label>
                    <div class="col-sm-12">
                        <?= $this->Form->text('title', ['class' => 'form-control required', 'id' => 'title', 'required' => false]) ?>
                        <!--<span class="help-block color-red">This field is required</span>-->
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Subtitle 1</label>
                    <div class="col-sm-12">
                        <?= $this->Form->text('sub_title1', ['class' => 'form-control required ', 'id' => 'sub_title1', 'required' => false]) ?>
                        <!--<span class="help-block color-red">This field is required</span>-->
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Subtitle 2</label>
                    <div class="col-sm-12">
                        <?= $this->Form->text('sub_title2', ['class' => 'form-control required', 'id' => 'sub_title2', 'required' => false]) ?>
                        <!--<span class="help-block color-red">This field is required</span>-->
                    </div>
                </div>

                <div class="form-group">
                                    <label for="country" class="col-sm-12 control-label">Country</label>
                                    <div class="col-sm-12">
                                    <select name="country" class="form-control" id="country">
                                    <?php foreach ($country as $key => $con): ?>
                                        <option value="<?= $key; ?>"><?= $con; ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                    </div>
                                </div>

                <div class="form-group">
                    <label for="editor" class="col-sm-12 control-label">Ads Description</label>
                    <div class="col-sm-12">
                        <textarea name="description" class="form-control" id="editor"></textarea>
                        <?php
                       // $this->Froala->plugin();
                        //echo $this->Froala->editor('#froala', array('height' => '300px'));

                       // echo $this->Form->textarea('description', ['class' => 'form-control', 'id' => 'summernote_gig', 'required' => false]); ?>
                        <!--<span class="help-block color-red">This field is required</span>-->
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-12 control-label">Push message</label>
                    <div class="col-sm-12">
                        <?php /*echo $this->Form->textarea('push_message', ['class' => 'form-control', 'id' => 'push_message', 'required' => false, 'placeholder' => 'Push message will not be sent when text field is empty', 'value' => 'A new Ad has been added – Apply now']);*/ ?>
                        <?php echo $this->Form->textarea('push_message', ['class' => 'form-control', 'id' => 'push_message', 'required' => false, 'placeholder' => 'Push message will not be sent when text field is empty', 'value' => '']); ?>
                        <!--<span class="help-block color-red">This field is required</span>-->
                    </div>
                </div>

                <div class="form-group" style="margin-bottom: 0px;">
                    

                    <label for="inputEmail3" class="col-sm-12 control-label">Image</label>
                    <!--<span class="help-block color-red">This field is required</span>-->
                    <div class="col-sm-12">
                        <div id="image_upload_error" class="alert alert-danger" style="display: none;"><p></p></div>
                        <?= $this->Form->hidden('gig_image', ['value' => '', 'id' => 'gig_image']); ?>
                    </div>
                </div>
                <input type="hidden" name="scheduleid" value="<?php echo $user_id;?>"/>
                <?= $this->Form->hidden('created_by', ['value' => $user_id]); ?>
                <div class="form-group" style="bottom: 35px;
                     position: absolute;">
                    <div class="clearfix">
                        <div class="col-md-12">
                            <label for="checkbox5" style="font-weight: normal;"><?= __('Active'); ?></label>
                            <div class="checkbox-custom checkbox-inline active-checkbox">
                                <input type="checkbox" value="1" id="active" name="active" checked>
                                <label for="checkbox5"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" name="CREATE" class="btn btn-success pull-right save_gig">CREATE</button>
                <?php //echo $this->Form->button('CREATE', ['type' => 'submit', 'class' => 'btn btn-success pull-right save_gig']); ?>
                <?php echo $this->Form->end() ?>
                
                <form id="my-awesome-dropzone" class="dropzone small-dropzone" style="margin-bottom: 50px!important;">
                    
                    <div class="dz-message" data-dz-message>
                        <h3>Drop image or click to select</h3>
                        <span>Image should be JPEG and 800px by 600px and should not exceed 5 MB</span>
                    </div>
                </form>

                <div class="form-group"  style="display: block; height: 20px;">

                </div>
                <div class="form-group">

                </div>
                <script>
                    var base_url = '<?php echo $base_url; ?>';
                </script>
            </div>
        </div>
    </div>
    <!-- End .panel -->
</div>  

</div>
<!-- End .row -->