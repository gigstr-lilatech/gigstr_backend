<!-- Start .page-content-inner -->
<div id="page-header" class="clearfix">
    <div class="page-header">
        <h2><?= __('Ads'); ?></h2>
        <span class="txt"><?= __('This page displays ad information and all applicants'); ?></span>
    </div>

</div>
<!-- Start .row -->
<div class="row">
    <div class="col-lg-6 col-md-6">
        <!-- col-lg-6 start here -->

        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= __('Ads Information'); ?></h4>
            </div>
            <div class="panel-body">
                <?= $this->Flash->render(); ?>
                <?php $dir = 'gig_images/'; ?>
                <?= $this->Html->image($dir . $gig->gig_image, ['alt' => 'Gigstr Logo', 'width' => '100', 'class' => 'gig_img']); ?>
                <h2 class="gig_title"><?= $gig->title; ?></h2>
                <small class="created_by"><?= __('Created by:'); ?> <?= $gig->created_by; ?></small>
                <small class="updated"><?= __('Last updated:'); ?> <?= date('Y-m-d', strtotime($gig->modified)); ?></small>
                        <?php echo $this->Form->postLink(__(''), ['action' => 'delete', $gig->id], ['class'=>'delete_item','confirm' => __('Are you sure you want to delete the gig "'.$gig->title.' (Company : '.$gig['company']['name'].' , No of Candidates : '. count($gig['application']).')"?', $gig->id)]) ?>
                <br/><br/>

                <p><strong>Company</strong><br/>
                    <?= $gig['company']['name']; ?></p>

                <p><strong>Title</strong><br/>
                    <?= $gig->title; ?></p>
                <p><strong>Subtitle 1</strong><br/>
                    <?= $gig->sub_title1; ?></p>
                <p><strong>Subtitle 2</strong><br/>
                    <?= $gig->sub_title2; ?></p>

                <p><strong>Job Description</strong><br/>
                    <?= $gig->description; ?></p>

                <div class="text-right">
                    <?= $this->Form->postLink(__('DELETE'), ['action' => 'delete', $gig->id], ['confirm' => __('Are you sure you want to delete the gig "' . $gig->title . ' (Company : ' . $gig['company']['name'] . ' , No of Applications : ' . count($gig['application']) . ')"?', $gig->id), 'class' => 'btn btn-danger ml10 btn-delete']) ?>

                    <?php
                    if ($gig->active == '1')
                        echo $this->Form->postLink(__('DEACTIVATE'), ['action' => 'deactivate', $gig->id, 2], ['confirm' => __('Are you sure you want to delete the ad "' . $gig->title . ' (Company : ' . $gig['company']['name'] . ' , No of Applications : ' . count($gig['application']) . ')"?', $gig->id), 'class' => 'btn btn-danger ml10']);
                    else
                        echo $this->Form->postLink(__('ACTIVATE'), ['action' => 'deactivate', $gig->id, 1], ['confirm' => __('Are you sure you want to delete the ad "' . $gig->title . ' (Company : ' . $gig['company']['name'] . ' , No of Applications : ' . count($gig['application']) . ')"?', $gig->id), 'class' => 'btn btn-danger ml10'])
                        ?>

                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>  
    <!-- col-lg-6 end here -->
    <div class="col-lg-6 col-md-6">
        <div class="panel panel-default">
            <!-- Start .panel -->
            <div class="panel-heading">
                <h4 class="panel-title"><?= __('Applicants'); ?></h4>
            </div>
            <div class="panel-body">
                <table id="basic-datatables-applicants" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="per15"><?= __('Date'); ?></th>
                            <th class="per30"><?= __('Name'); ?></th>
                            <th class="per15"><?= __('Chat'); ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if (count($applications) > 0):
                            foreach ($applications as $application):
                                ?>
                                <tr class="<?php if ($application->notification == '1') echo 'bold_row'; ?>">
                                    <td><?= date('Y-m-d', strtotime($application->created)); ?></td>
                                    <td><?= $application['user']['name']; ?></td>
                                    <td><a href="<?=
                                        $this->Url->build([
                                            "controller" => "Candidate",
                                            "action" => "profile",
                                            $application->user_id,
                                            $application->chat_id
                                        ]);
                                        ?>"><i class="fa fa-wechat"></i></a></td>
                                </tr>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div></div></div>
</div>
<!-- End .row -->