<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GigTemplate Controller
 *
 * @property \App\Model\Table\GigTemplateTable $GigTemplate
 */
class GigTemplateController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Gigs']
        ];
        $this->set('gigTemplate', $this->paginate($this->GigTemplate));
       // $this->set('_serialize', ['gigTemplate']);
    }

    /**
     * View method
     *
     * @param string|null $id Gig Template id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gigTemplate = $this->GigTemplate->get($id, [
            'contain' => ['Gigs']
        ]);
        $this->set('gigTemplate', $gigTemplate);
        $this->set('_serialize', ['gigTemplate']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gigTemplate = $this->GigTemplate->newEntity();
        if ($this->request->is('post')) {
            $gigTemplate = $this->GigTemplate->patchEntity($gigTemplate, $this->request->data);
            if ($this->GigTemplate->save($gigTemplate)) {
                $this->Flash->success(__('The gig template has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gig template could not be saved. Please, try again.'));
            }
        }
        $gigs = $this->GigTemplate->Gigs->find('list', ['limit' => 200]);
        $this->set(compact('gigTemplate', 'gigs'));
        $this->set('_serialize', ['gigTemplate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gig Template id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gigTemplate = $this->GigTemplate->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gigTemplate = $this->GigTemplate->patchEntity($gigTemplate, $this->request->data);
            if ($this->GigTemplate->save($gigTemplate)) {
                $this->Flash->success(__('The gig template has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gig template could not be saved. Please, try again.'));
            }
        }
        $gigs = $this->GigTemplate->Gigs->find('list', ['limit' => 200]);
        $this->set(compact('gigTemplate', 'gigs'));
        $this->set('_serialize', ['gigTemplate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gig Template id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gigTemplate = $this->GigTemplate->get($id);
        if ($this->GigTemplate->delete($gigTemplate)) {
            $this->Flash->success(__('The gig template has been deleted.'));
        } else {
            $this->Flash->error(__('The gig template could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
