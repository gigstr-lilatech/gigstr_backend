<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Referal Controller
 *
 * @property \App\Model\Table\ReferalTable $Referal
 */
class ReferalController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('referal', $this->paginate($this->Referal));
        $this->set('_serialize', ['referal']);
    }

    /**
     * View method
     *
     * @param string|null $id Referal id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $referal = $this->Referal->get($id, [
            'contain' => []
        ]);
        $this->set('referal', $referal);
        $this->set('_serialize', ['referal']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        header('Content-type: application/json');
        $referal = $this->Referal->newEntity();
        $return = new \stdClass();
        $deviceID = $this->request->header('X-deviceID');

        if ($this->request->is('post')) {
            if($deviceID) {

                $isRef = $this->Referal
                    ->find()
                    ->select(['id', 'referrer_user_id'])
                    ->where(['device_id' => $deviceID])
                    ->toArray();

                $this->request->data['device_id'] = $deviceID;
                $userID = $this->request->data['userId'];
                $this->request->data['referrer_user_id'] = $userID;

                if(count($isRef) == 0) {
                    $referal = $this->Referal->patchEntity($referal, $this->request->data);
                    if ($this->Referal->save($referal)) {
                        $return->status = 0;

                    } else {
                        $return->status = 1;
                    }
                }
                else {
                    $return->status = 1;
                }
            }
            else {
                $return->status = 1;
            }
        }
        $this->set(compact('return'));
        $this->set('_serialize', ['return']);
        echo json_encode($return);
        die();
    }

    /**
     * Edit method
     *
     * @param string|null $id Referal id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $referal = $this->Referal->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $referal = $this->Referal->patchEntity($referal, $this->request->data);
            if ($this->Referal->save($referal)) {
                $this->Flash->success(__('The referal has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The referal could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('referal'));
        $this->set('_serialize', ['referal']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Referal id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $referal = $this->Referal->get($id);
        if ($this->Referal->delete($referal)) {
            $this->Flash->success(__('The referal has been deleted.'));
        } else {
            $this->Flash->error(__('The referal could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
