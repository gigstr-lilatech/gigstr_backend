<?php

namespace App\Controller;

//use Alaxos\View\Widget\Datetime;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use DateTime;

/**
 * Gig Controller
 *
 * @property \App\Model\Table\GigTable $Gig
 */
class ScheduleController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Stripe');
    }

    /**
     * Index method
     * @return void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Gig'],
            'conditions' => ['Gig.status' => '1', 'Gig.active' => '1']
        ];
        $this->set('schedule', $this->paginate($this->Schedule));
        $this->set('_serialize', ['schedule']);
    }

    /**
     * listAll method
     * Access method - gig/list_all
     * @return json encoded object array containing information about the gigs which are active in the system
     * @internal param null|string $user_id User id.
     */
    public function listAll() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $date = $this->request->data['date'];
            $page = $this->request->data['page'];
            $lastid = $this->request->data['last_id'];
            
            $token = $this->request->data['token'];
            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    $conn = ConnectionManager::get('default');
                    
                    if($page == 0)
                    {
                        $formatdate = date('Y-m-d', strtotime($date));
                        $formatdatenew = date('Y-m-d H:i:s',strtotime($formatdate. " -2 hours"));
                        $stmt = $conn->prepare('SELECT s.id,s.shift_from,s.shift_to,s.status, s.gigstr_rating, s.gigstr_rating_comment, s.internal_rating, s.internal_rating_comment, j.title, co.name from schedule s, job j, company co where co.id = j.company_id AND j.id = s.gig_id And s.status != 0 And s.gigstr = '.$user_id.' And (s.shift_from >"'.$formatdate.'") Order by shift_from ASC limit 30');

                        $stmt->execute();
                        $schedules = $stmt->fetchAll('assoc');

                        if(empty($schedules)) { //if no prest gigs get past
                            $stmt = $conn->prepare('SELECT s.id,s.shift_from,s.shift_to,s.status, s.gigstr_rating, s.gigstr_rating_comment, s.internal_rating, s.internal_rating_comment, j.title, co.name from schedule s, job j, company co where  co.id = j.company_id AND co.id = j.company_id AND j.id = s.gig_id And s.status != 0 And s.gigstr = '.$user_id.' Order by s.shift_from DESC limit 30');
                            $stmt->execute();
                            $schedules_arr = $stmt->fetchAll('assoc');

                            usort($schedules_arr, function($a, $b) {
                                $ad = new DateTime($a['shift_from']);
                                $bd = new DateTime($b['shift_from']);
                                if ($ad == $bd) {
                                    return 0;
                                }
                                return $ad < $bd ? -1 : 1;
                            });

                            $schedules = $schedules_arr;
                        }
                    }
                    //future schedules
                    elseif($page > 0)
                    {
                        $schedule = $this->Schedule->get($lastid);
                        $frontdate = date('Y-m-d H:i:s', strtotime($schedule->shift_from));
                        $stmt = $conn->prepare('SELECT s.id,s.shift_from,s.shift_to,s.status,  s.gigstr_rating, s.gigstr_rating_comment, s.internal_rating, s.internal_rating_comment, j.title, co.name from schedule s, job j, company co where co.id = j.company_id AND  j.id = s.gig_id And s.status != 0 And s.gigstr = '.$user_id.' And s.id !='.$lastid.' And s.shift_from >"'.$frontdate.'" Order by shift_from ASC limit 30');
                    
                        $stmt->execute();
                        $schedules = $stmt->fetchAll('assoc');
                    }
                    //past schedules
                    elseif($page < 0)
                    {
                        $schedule = $this->Schedule->get($lastid);
                        $backdate = date('Y-m-d H:i:s', strtotime($schedule->shift_from));
                        $stmt = $conn->prepare('SELECT s.id,s.shift_from,s.shift_to,s.status, s.gigstr_rating, s.gigstr_rating_comment, s.internal_rating, s.internal_rating_comment, j.title, co.name from schedule s, job j, company co where co.id = j.company_id AND  j.id = s.gig_id And s.status != 0 And s.gigstr = '.$user_id.' And s.id !='.$lastid.' And s.shift_from <"'.$backdate.'" Order by shift_from DESC limit 30');

                        $stmt->execute();
                        $schedules_arr = $stmt->fetchAll('assoc');

                        usort($schedules_arr, function($a, $b) {
                            $ad = new DateTime($a['shift_from']);
                            $bd = new DateTime($b['shift_from']);
                            if ($ad == $bd) {
                                return 0;
                            }
                            return $ad < $bd ? -1 : 1;
                        });

                        $schedules = $schedules_arr;
                    }

                    $data = array();
                    foreach($schedules as $i => $schedule)
                    {
                        $data[$i]['id']= $schedule['id'];
                        //$data[$i]['date']= gmdate('Y-m-d H:i:s', strtotime($schedule['shift_from']));
                        $data[$i]['date'] = $schedule['shift_from'];
                        $data[$i]['name']= $schedule['title'];
                        //$data[$i]['start_time']= gmdate('Y-m-d H:i:s', strtotime($schedule['shift_from']));
                        $data[$i]['start_time'] = $schedule['shift_from'];
                        //$data[$i]['end_time'] = gmdate('Y-m-d H:i:s', strtotime($schedule['shift_to']));
                        $data[$i]['end_time'] = $schedule['shift_to'];
                        $data[$i]['status']= (int)$schedule['status'];
                        $data[$i]['gigstr_rating']= (int)$schedule['gigstr_rating'];
                        $data[$i]['gigstr_rating_comment']= $schedule['gigstr_rating_comment'];
                        $data[$i]['internal_rating']= (int)$schedule['internal_rating'];
                        $data[$i]['internal_rating_comment']= $schedule['internal_rating_comment'];
                        $data[$i]['company']= ($schedule['name'] ? $schedule['name'] : "");
                    //    $data[$i]['internal_name'] = 'Admin';
                    }
                    $return = new \stdClass();
                    $return->status = 0;
                    $return->schedule = $data;
                
                    echo json_encode($return);
                    die();
                    
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
        }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }
    
    public function scheduleDetail() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $device_id=$this->request->data['device_type'];
            $token = $this->request->data['token'];

            $this->loadModel('User');
            $this->loadModel('Event');

            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    $scheduleid = $this->request->data['schedule_id'];  
                    $base_url = Configure::read('dev_base_url');
                    $conn = ConnectionManager::get('default');

                    if ($scheduleid){
                        $stmt = $conn->prepare('SELECT s.id,s.shift_description,s.shift_from,s.shift_to,s.status,j.title, s.gigstr_rating, s.gigstr_rating_comment, s.internal_rating, s.internal_rating_comment, s.comment, s.start, s.stop, s.attested, s.internal_user_id, s.gig_id, s.event_id, co.name from schedule s, job j, company co
                             where  co.id = j.company_id AND j.id = s.gig_id And s.id= '.$scheduleid.'');
                        $stmt->execute();
                        $schedule = $stmt->fetch('assoc');
                        $data = array();

                        /** update event table when view shift detail */
                        if($schedule['event_id'] != 0) {
                            $event = $this->Event->get($schedule['event_id']);
                            $event->is_read = 1;
                            $this->Event->save($event);
                        }

                        $data['id']= $schedule['id'];
                        if ($device_id == 2) {

                            if($schedule['shift_description'] == '<p><br></p>') {
                                $data['description'] = "";
                            } else {
                                $data['description'] = $this->replaceBr($schedule['shift_description']);
                            }
                            $data['description_link'] = $base_url . 'schedule/view_schedule/' . $scheduleid;
                        } else {
                            //If device type is Android then reply the message as convert links to weblinks
                            $msg = $this->makeClickableLinks($schedule['shift_description']);
                            $data['description'] =  $msg;
                        }

                        $data['date']= gmdate('Y-m-d H:i:s', strtotime($schedule['shift_from']));
                        $data['name']= $schedule['title'];
                        $data['start_time']= gmdate('Y-m-d H:i:s', strtotime($schedule['shift_from']));
                        $data['end_time']= gmdate('Y-m-d H:i:s', strtotime($schedule['shift_to']));
                        $data['status']= (int)$schedule['status'];
                        $data['comment']= ($schedule['comment'] ? $schedule['comment'] : "");
                        $data['gigstr_rating']= (int)$schedule['gigstr_rating'];
                        $data['gigstr_rating_comment']= $schedule['gigstr_rating_comment'];
                        $data['internal_rating']= (int)$schedule['internal_rating'];
                        $data['internal_rating_comment'] = $schedule['internal_rating_comment'];
                        $data['company'] = ($schedule['name'] ? $schedule['name'] : "");
                        $data['gigstr_start'] = gmdate('Y-m-d H:i:s', strtotime($schedule['start']));
                        $data['gigstr_stop'] = gmdate('Y-m-d H:i:s', strtotime($schedule['stop']));

                        $this->loadModel('User');
                        $name = $this->User->get($user_id);
                        $data['gigstr_name'] = $name->name;

                       // $this->loadModel('ScheduleDataField');
                        /*$fieldArr = $this->ScheduleDataField->find()
                            ->select(['ScheduleDataField.id'])
                            ->where(['schedule_id' => $scheduleid])
                            ->toArray();

                        if($fieldArr) {
                            $stmt = $conn->prepare('SELECT t.id, t.field_type, t.field_data, t.field_title, t.checkbox_value, t.field_name, t.required, s.data_field_value FROM gig_template_shift_data t
                        LEFT JOIN schedule_data_field s ON t.id = s.gig_template_shift_data_id
                        WHERE  s.schedule_id = '. $scheduleid .' AND t.is_delete = 0 ORDER BY t.field_order ASC, t.id ASC');
                        }
                        else {
                            $stmt = $conn->prepare('SELECT t.id, t.field_type, t.field_data, t.field_title, t.checkbox_value, t.field_name, t.required FROM gig_template_shift_data t
                        LEFT JOIN gig_template g ON t.gig_template_id = g.id
                        WHERE g.gig_id = ' . $schedule['gig_id'] . ' AND t.is_delete = 0 ORDER BY t.field_order ASC, t.id ASC');
                        }*/

                        $stmt = $conn->prepare('SELECT t.id, t.field_type, t.field_data, t.field_title, t.checkbox_value, t.field_name, t.required, s.data_field_value FROM gig_template_shift_data t
                        LEFT JOIN schedule_data_field s ON t.id = s.gig_template_shift_data_id
                        WHERE  s.schedule_id = '. $scheduleid .'  ORDER BY t.field_order ASC, t.id ASC');

                        $stmt->execute();
                        $gigTemplate = $stmt->fetchAll('assoc');
                        $data['gig_data'] = array();

                        if($gigTemplate) {
                            foreach ($gigTemplate as $template) {

                                switch($template['field_type']) {
                                    case 'checkbox-group' :
                                        $field = 'CHECKBOX';
                                        $checkFieldName = $template['field_name'];
                                        $template['required'] = "0";
                                        $fieldData = "";
                                        break;
                                    case 'title':
                                        $field = 'TITLE';
                                        if(!isset($template['data_field_value'])) {
                                            $template['data_field_value'] = "0";
                                        }
                                        $checkTitledName = $template['field_name'];
                                        $titleId = $template['id'];
                                        $fieldData = "";
                                        break;
                                    case 'number':
                                        $field = 'NUMBER';
                                        $fieldData = "";
                                        break;
                                    case 'textarea':
                                        $field = 'STRING';
                                        $fieldData = "";
                                        break;
                                    case 'select':
                                        $fieldData = unserialize($template['field_data']);
                                        $field = 'DROPDOWN';
                                        break;
                                    default:
                                        $field = 'STRING';
                                        $fieldData = "";
                                        break;
                                }

                                $gigField = array(
                                    'field_id' => $template['id'],
                                    'field_name' => $template['field_title'],
                                    'field_value' => (isset($template['data_field_value']) ? $template['data_field_value'] : ""),
                                    'field_type' => $field,
                                    'editable' => ($schedule['attested'] == 0 ? true : false),
                                    'field_data' => $fieldData,
                                    'required' => $template['required']
                                );

                                if($template['field_type'] == 'checkbox-group') {
                                    if($checkFieldName == $checkTitledName) {
                                        $gigField['field_parent'] = $titleId;
                                    }
                                }

                                array_push($data['gig_data'], $gigField);
                            }
                        }

                        $data['shift_data_editable'] = ($schedule['attested'] == 0 ? TRUE : FALSE);

                        $this->loadModel('ScheduleImage');
                        $imgCount = $this->ScheduleImage->findAllByScheduleId($scheduleid)
                                    ->count();
                        $data['schedule_image_count'] = $imgCount;

                        if($schedule['internal_user_id'] != 0) {
                            $adminName = $this->User->get($schedule['internal_user_id']);
                            if ($adminName) {
                                $data['internal_name'] = $adminName->name;
                            } else {
                                $data['internal_name'] = "Gigstr admin";
                            }
                        } else {
                            $data['internal_name'] = "Gigstr admin";
                        }

                        $this->loadModel('Candidate');
                        $candidate = $this->Candidate->find()
                            ->select(['image'])
                            ->where(['user_id' => $user_id])
                            ->first();

                        if ( $candidate->image != "") {
                            if (preg_match('/facebook/', $candidate->image)) {
                                $data['gigstr_image'] = $candidate->image;
                            } else {
                                $data['gigstr_image'] = Configure::read('dev_base_url') . 'img/user_images/' . $candidate->image;
                            }
                        } else {
                            if($name->fbid) {
                                $data['gigstr_image'] ="https://graph.facebook.com/".$name->fbid."/picture?type=normal";
                            }
                            else {
                                $data['gigstr_image'] = "";
                            }
                        }

                        $return = new \stdClass();
                        $return->status = 0;
                        $return->schedule = $data;
                        
                        if ($device_id == 2) {
                            echo json_encode($return);
                            die();
                        }
                        else
                        {
                            $text = json_encode($return);
                            $text = str_replace('&lt;',"<",$text);
                            $text = str_replace('&gt;',">",$text);
                            echo $text;
                            die();
                        }
                    }
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }
    
    public function checkin()
    {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $schedule_id = $this->request->data['schedule_id'];
            $token = $this->request->data['token'];
            $this->loadModel('User');

            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    
                    $data = array();
                    $data['checkin'] = date('Y-m-d H:i:s');
                    
                    $this->loadModel('Schedule');
                    $schedule = $this->Schedule->get($schedule_id);
                    
                    $schedule->checkin= $data['checkin'];
                    $schedule->status= 2;

                    if(isset($this->request->data['lat'])) {
                        $schedule->checkin_lat = $this->request->data['lat'];
                    } else {
                        $schedule->checkin_lat = "";
                    }

                    if(isset($this->request->data['long'])) {
                        $schedule->checkin_long= $this->request->data['long'];
                    } else {
                        $schedule->checkin_long = "";
                    }
                    
                    $this->Schedule->save($schedule);
                    
                    $return = new \stdClass();
                    $return->status = 0;
                    $return->checkin = $data;
                    
                    echo json_encode($return);
                    die();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
        }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }
    
    public function checkout() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $schedule_id = $this->request->data['schedule_id'];
            $comment= $this->request->data['comment'];
            $token = $this->request->data['token'];
            $device_type = $this->request->header('device_type');

            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    $data = array();
                    $data['checkout'] = date('Y-m-d H:i:s');
                    
                    $this->loadModel('Schedule');
                    $schedule = $this->Schedule->get($schedule_id);
                    
                    $schedule->checkout= $data['checkout'];
                    $schedule->start= $this->request->data['start'];
                    $schedule->stop= $this->request->data['stop'];
                    $schedule->comment= $comment;
                    $schedule->status= 3;

                    if(isset($this->request->data['lat'])) {
                        $schedule->checkout_lat = $this->request->data['lat'];
                    } else {
                        $schedule->checkout_lat = "";
                    }
                    if(isset($this->request->data['long'])) {
                        $schedule->checkout_long = $this->request->data['long'];
                    } else {
                        $schedule->checkout_long = "";
                    }
                    
                    $this->Schedule->save($schedule);

                    if(isset($this->request->data['gig_data'])) {

                        $conn = ConnectionManager::get('default');
                        $shiftData = $this->request->data['gig_data'];

                        if($device_type == 1) {
                            $shiftData = json_decode($shiftData,true);
                        }
                        foreach ($shiftData as $shift) {
                            if($shift['field_type'] == 'CHECKBOX' && $shift['field_value'] == 'UnvfEH9UVI') {
                                $data_field_value = "";
                            }
                            else if($shift['field_type'] == 'NUMBER') {
                                $data_field_value = str_replace(',', '.', $shift['field_value']);
                            }
                            else {
                                $data_field_value = str_replace('"', '\'', $shift['field_value']);
                            }
                            $stmt = $conn->prepare('UPDATE schedule_data_field SET data_field_value = "'.  $data_field_value .'"
                        WHERE gig_template_shift_data_id = '. $shift['field_id'] .' AND schedule_id = '. $schedule_id);
                            $stmt->execute();

                        }
                    }

                    $return = new \stdClass();
                    $return->status = 0;
                    $return->checkout = $data;
                    
                    echo json_encode($return);
                    die();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function writeToLog($message) {
        if ($fp = fopen(Configure::read('logfile'), 'at')) {
            fwrite($fp, date('c') . ' ' . $message . PHP_EOL);
            fclose($fp);
        }
    }

    public function updateShiftData() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->header('user_id');
            $token = $this->request->header('token');
            $device_type = $this->request->header('device_type');
            $conn = ConnectionManager::get('default');

            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    $shiftData = $this->request->data['gig_data'];
                    $scheduleId = $this->request->data['schedule_id'];
                   // $this->loadModel('ScheduleDataField');
                    if($device_type == 1) {
                        $shiftData = json_decode($shiftData,true);
                    }

                    foreach ($shiftData as $shift) {

                        if($shift['field_type'] == 'CHECKBOX' && $shift['field_value'] == 'UnvfEH9UVI') {
                            $data_field_value = "";
                        }
                        else if($shift['field_type'] == 'NUMBER') {
                            $data_field_value = str_replace(',', '.', $shift['field_value']);
                        }
                        else {
                            $data_field_value = str_replace('"', '\'', $shift['field_value']);
                        }

                        $stmt = $conn->prepare('UPDATE schedule_data_field SET data_field_value = "'.                               $data_field_value .'"
                        WHERE gig_template_shift_data_id = '. $shift['field_id'] .' AND schedule_id = '. $scheduleId);
                        $stmt->execute();
                    }

                    $return = new \stdClass();
                    $return->status = 0;
                    echo json_encode($return);
                    die();
                } else {
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function updateCheckout() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');

        if ($this->request->is('post')) {
            //$user_id = $this->request->data['user_id'];
            $schedule_id = $this->request->data['schedule_id'];
            $comment= $this->request->data['comment'];
            //$token = $this->request->data['token'];

            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    $data = array();

                    $schedule = $this->Schedule->get($schedule_id);
                    $schedule->comment= $comment;
                    $this->Schedule->save($schedule);

                    $return = new \stdClass();
                    $return->status = 0;
                    $return->checkout = $data;

                    echo json_encode($return);
                    die();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }
    
    public function replaceBr($text) {
        return str_replace('<br>', '</br>', $text);
    }
    
    public function makeClickableLinks($text) {
        $text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ginascheme://$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
        $text = str_replace('https', 'ginascheme', $text);
        $text = str_replace('http', 'ginascheme', $text);
        $text = str_replace('>ginascheme://', '>', $text);
        

        return($text);
    }
    
    public function viewSchedule($id = null) {
        $this->viewBuilder()->layout('blank');
        $schedule = $this->Schedule->get($id);
        
        $this->set('schedule', $schedule );
        $this->set('_serialize', ['data']);
    }

    public function rating() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');

        if ($this->request->is('post')) {
            $gigstr_rating = $this->request->data['gigstr_rating'];
            $schedule_id = $this->request->data['schedule_id'];
            $gigstr_rating_comment = $this->request->data['gigstr_rating_comment'];
           /* $user_id = $this->request->data['user_id'];
            $token = $this->request->data['token'];*/

            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {

                    $this->loadModel('Schedule');
                    $schedule = $this->Schedule->get($schedule_id);

                    $schedule->gigstr_rating = $gigstr_rating;
                    $schedule->gigstr_rating_comment = $gigstr_rating_comment;

                    if($this->Schedule->save($schedule)) {
                        $return = new \stdClass();
                        $return->status = 0;
                        echo json_encode($return);
                        die();
                    }
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }

        }

    }



    /**
     **********************************************************************************************
     * V2 end points
     * ********************************************************************************************
     */


    /**
     * did not work API
     */
    public function notWork() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');

        if ($this->User->isLoggedIn($token, $user_id)) {

            $scheduleId = $this->request->data['schedule_id'];
            $return = new \stdClass();

            $this->loadModel('Schedule');
            $this->loadModel('GigOrder');

            $schedule = $this->Schedule->get($scheduleId);
            $schedule->checkin = $schedule->shift_from;
            $schedule->checkout = $schedule->shift_from;
            $schedule->start = $schedule->shift_from;
            $schedule->stop = $schedule->shift_from;
            $schedule->not_work = 1;
            $schedule->status = 3;
            $savedSchedule = $this->Schedule->save($schedule);

            /** create zero order for did not work shift for only DYI shifts */
            if($savedSchedule->job_offer_id != 0) {
                $order = $this->GigOrder->newEntity();
                $order->schedule_id = $savedSchedule->id;
                $order->job_offer_id = $savedSchedule->job_offer_id;
                $order->hours = 0;
                $order->total = 0;
                $this->GigOrder->save($order);

                $last = $this->Schedule->find()
                    ->where(['status !=' => 3, 'job_offer_id' => $savedSchedule->job_offer_id])
                    ->first();

                if (!$last) {
                    $this->loadModel('JobOffer');
                    $jobOffer = $this->JobOffer->get($savedSchedule->job_offer_id);
                    $jobOffer->status = 'COMPLETED';
                    $jobOffer->status_date = date('Y-m-d H:i:s');
                    $this->JobOffer->save($jobOffer);
                }
            }

            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }


    /**
     * V2 Checkout
     */
    public function checkoutNew() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $device_type = $this->request->header('device_type');
        $this->loadModel('User');

        if ($this->User->isLoggedIn($token, $user_id)) {

            $schedule_id = $this->request->data['schedule_id'];
            $comment= $this->request->data['comment'];

            $this->loadModel('Schedule');
            $schedule = $this->Schedule->get($schedule_id);
            $start = $this->request->data['start'];
            $stop = $this->request->data['stop'];
            $checkoutDate = date('Y-m-d H:i:s');
            $return = new \stdClass();

            /** check backoffice */
           /* if($schedule->job_offer_id == 0) {
                $payment = true;
            } else {
                $payment = $this->doPayment($schedule, $start, $stop);
            }
            if ($payment) {*/

                $schedule->checkout = $checkoutDate;
                $schedule->start = $start;
                $schedule->stop = $stop;
                $schedule->comment = $comment;
                $schedule->status = 3;
                $schedule->checkout_lat = (isset($this->request->data['lat'])) ? $this->request->data['lat'] : "";
                $schedule->checkout_long = (isset($this->request->data['long'])) ? $this->request->data['long'] : "";
                $savedSchedule = $this->Schedule->save($schedule);

                if (isset($this->request->data['gig_data'])) {
                     //   $conn = ConnectionManager::get('default');
                        $shiftData = $this->request->data['gig_data'];

                    $user_device_type = $this->User->findById($user_id)->first();
                    if ($user_device_type->device_type == '1') {
                        $shiftData = json_decode($shiftData, true);
                    }
                    $this->loadModel('ScheduleDataField');

                    foreach ($shiftData as $shift) {
                            /** iPhone fix */
                            if ($shift['field_type'] == 'CHECKBOX' && $shift['field_value'] == 'UnvfEH9UVI') {
                                $data_field_value = "";
                            } else {
                                if ($shift['field_type'] == 'NUMBER') {
                                    $data_field_value = str_replace(',', '.', $shift['field_value']);
                                } else {
                                    $data_field_value = str_replace('"', '\'', $shift['field_value']);
                                }
                            }

                            $shiftDataObj = $this->ScheduleDataField->find()->where([
                                "gig_template_shift_data_id" => $shift['field_id'],
                                "schedule_id" => $schedule_id
                            ])->first();

                            $shiftDataObj->data_field_value = $data_field_value;
                            $this->ScheduleDataField->save($shiftDataObj);

                            /*$stmt = $conn->prepare('UPDATE schedule_data_field SET data_field_value = "' . $data_field_value . '"
                WHERE gig_template_shift_data_id = ' . $shift['field_id'] . ' AND schedule_id = ' . $schedule_id);
                            $stmt->execute();*/
                        }
                }

                /** if schedule created from backoffice,  payment is not happen */
                if($schedule->job_offer_id != 0) {
                    // make job offre complete
                    $last = $this->Schedule->find()
                        ->where(['status !=' => 3, 'job_offer_id' => $savedSchedule->job_offer_id])
                        ->first();

                    if (!$last) {
                        $this->loadModel('JobOffer');
                        $jobOffer = $this->JobOffer->get($savedSchedule->job_offer_id);
                        $jobOffer->status = 'COMPLETED';
                        $jobOffer->status_date = date('Y-m-d H:i:s');
                        $this->JobOffer->save($jobOffer);
                    }
                }
                $return->checkout = $checkoutDate;
                $return->status = 0;
           /* }
            else {
                $return->status = 107;
                $return->message = 'Error with payment';
            }*/
        }
        else {
            //If the provided token and user_id not match
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }


    /**
     * cron job for check in and checkout Reminder
     * run in every 5 min
     */
    public function cronScheduleReminder() {

        $this->loadModel('LogScheduleReminder');
        $this->loadModel('PushNotification');
        $this->loadModel('Event');
        $this->loadModel('User');
        $this->loadModel('JobOffer');

        //date_default_timezone_set('Europe/Stockholm');
        $now = date('Y-m-d H:i:s');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SET @time_stamp = "'. $now .'"; CALL usp_update_schedule_reminders(@time_stamp);');
        $stmt->execute();
        $stmt->closeCursor();

        $remenders = $this->LogScheduleReminder->find('all')
                    ->contain(['Schedule'])
                    ->where(['LogScheduleReminder.created' => $now, 'LogScheduleReminder.push_status' => 0]);

        foreach($remenders as $remender) {
            $companyOwner = $this->User->findById($remender->schedule->internal_user_id)->first();
            $sentPerson = strtok($companyOwner->name, " ");

            $userObj = $this->User->findById($remender->schedule->gigstr)->first();
            $token = $userObj->fcm_token;
            $key_value = json_encode(["schedule_id" => $remender->schedule_id]);
            $badgeCount = $this->JobOffer->badgeCount($remender->schedule->gigstr);
            $badges = $badgeCount->total;

            if($remender->type == 1) {
            $payload = 'Your job started 15 minutes ago, let '. $sentPerson .' know you are at your job by checking in.';
                $notification = 'Reminder: Check-in';
            }
            else {
                $payload = $sentPerson .' is waiting for your timesheet, please see your schedule to send it.';
                $notification = 'Reminder: Check-out from Gig';
            }

            $eventId = $this->Event->eventSave($payload, $remender->schedule->gigstr, 'GIGSTR', 'SCHEDULE_DETAIL', $key_value, 1);

            $scheduleObj = $this->Schedule->get($remender->schedule_id);
            $scheduleObj->event_id = $eventId;
            $this->Schedule->save($scheduleObj);

            if(!empty($token)) {
                $this->PushNotification->sendPushEvent($payload, $token, $remender->schedule->gigstr, 'GIGSTR', 'SCHEDULE_DETAIL', $notification, $badges, $key_value, "");

                $remenderObj = $this->LogScheduleReminder->get($remender->id);
                $remenderObj->push_status = 1;
                $this->LogScheduleReminder->save($remenderObj);
            }
        }

        die();
    }


    /**
     * DEPRECATE
     * @param $schedule
     * @param $start
     * @param $stop
     * @return bool
     */
    private function doPayment($schedule, $start, $stop) {

        $this->loadModel('Candidate');
        $this->loadModel('JobOffer');
        $this->loadModel('Schedule');
        $this->loadModel('Country');
        $this->loadModel('StripeLog');
        $this->loadModel('GigOrder');
        $this->loadModel('User');

        $jobOfferId = $schedule->job_offer_id;
        $companyUserId = $schedule->internal_user_id;
        $candidateObj = $this->Candidate->findByUserId($companyUserId)->first();
        $jobOfferObj = $this->JobOffer->findById($jobOfferId)
                            ->contain(['User', 'Pricing'])->first();
        $hourlyCost = $jobOfferObj->hourly_cost;

        $startTime = new DateTime($start);
        $stopTime = new DateTime($stop);
        $interval = $startTime->diff($stopTime);
        $intervalMin = $interval->format('%i');
        $intervalHrs = $interval->format('%h');
        $workTimeMin = ((int)$intervalHrs * 60) + (int)$intervalMin;

        $shiftStart =  new DateTime($schedule->shift_from);
        $shiftTo = new DateTime($schedule->shift_to);
        $shiftInterval = $shiftStart->diff($shiftTo);
        $shiftIntervalHrs = $shiftInterval->format('%h');
        $shiftIntervalMin = $shiftInterval->format('%i');
        $shiftTimeMin = ((int)$shiftIntervalHrs * 60) + (int)$shiftIntervalMin;

        /**  work time is more than the shift time, So get the Shift time else get start stop time */
        if($workTimeMin > $shiftTimeMin) {
            $workTime = (float)$shiftIntervalHrs + round($shiftIntervalMin / 60, 2);
        }
        else {
            $workTime = (float)$intervalHrs + round($intervalMin / 60, 2);
        }

        if($workTime > 0) {
            $amount = $hourlyCost * $workTime;
            $amount = round($amount, 2);
            $currency = $this->Country->findById($jobOfferObj->pricing->country_id)->first();
            $currency = strtolower($currency->currency);

            $companyUserObj = $this->User->get($jobOfferObj->created_by);

            $stripeId = $candidateObj->stripe_id;
            $requestData = array(
                "amount" => $amount * 100,
                "currency" => $currency,
                "description" => $jobOfferObj->user->name. " " . $workTime . " Hours",
                "receipt_email" => $companyUserObj->email
            );

            $chargeCard = $this->Stripe->charge($requestData, $stripeId);
            $this->StripeLog->createLog($stripeId, $companyUserId, json_encode($requestData), json_encode($chargeCard), 'charge');

            if($chargeCard["status"] == "success") {

                $order = $this->GigOrder->newEntity();
                $order->schedule_id = $schedule->id;
                $order->job_offer_id = $schedule->job_offer_id;
                $order->stripe_payment_id = $chargeCard["response"]["id"];
                $order->hourly_cost = $hourlyCost;
                $order->hours = $workTime;
                $order->total = $amount;
                $order->currency = $currency;
                $this->GigOrder->save($order);
                return true;
            }
            else {
                return false;
            }
        }
        else {
            $order = $this->GigOrder->newEntity();
            $order->schedule_id = $schedule->id;
            $order->job_offer_id = $schedule->job_offer_id;
            $order->hourly_cost = $hourlyCost;
            $order->hours = $workTime;
            $order->total = 0;
            $this->GigOrder->save($order);
            return true;
        }
    }
}
