<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ScheduleImage Controller
 *
 * @property \App\Model\Table\ScheduleImageTable $ScheduleImage
 */
class ScheduleImageController extends AppController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('SimpleImage');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Schedule', 'Gig', 'User']
        ];
        $this->set('scheduleImage', $this->paginate($this->ScheduleImage));
        $this->set('_serialize', ['scheduleImage']);
    }

    /**
     * View method
     *
     * @param string|null $id Schedule Image id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $scheduleImage = $this->ScheduleImage->get($id, [
            'contain' => ['Schedule', 'Gig', 'User']
        ]);
        $this->set('scheduleImage', $scheduleImage);
        $this->set('_serialize', ['scheduleImage']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        header('Content-type: application/json');
       // $userId = $this->request->header('user_id');
        $token = $this->request->header('token');
        $scheduleImage = $this->ScheduleImage->newEntity();

        if ($this->request->is('post')) {

            $scheduleId  = $this->request->data['schedule_id'];
            $userId  = $this->request->data['user_id'];

            $return = new \stdClass();

            $dir = WWW_ROOT . 'img' . DS . 'schedule_images';
            $f = explode('.', $_FILES['image_data']['name']);
            $ext = '.' . end($f);
            $filename = time(). "_" . rand(000000, 999999);
            $fileUpload = move_uploaded_file($_FILES['image_data']['tmp_name'], $dir . DS . $filename . $ext);

            if($fileUpload) {
                $fn = $filename . $ext;
                $this->SimpleImage->load($dir . DS . $fn);
                $this->SimpleImage->save($dir . DS . $fn);

                $this->loadModel('Schedule');

                $schedule = $this->Schedule->get($scheduleId);
                $gigId = $schedule->gig_id;

                $scheduleImage->schedule_id = $scheduleId;
                $scheduleImage->user_id = $userId;
                $scheduleImage->gig_id = $gigId;
                $scheduleImage->image = $fn;

                //$scheduleImage = $this->ScheduleImage->patchEntity($scheduleImage, $this->request->data);
                if ($this->ScheduleImage->save($scheduleImage)) {

                    $return->status = 0;
                    echo json_encode($return);
                    die();
                } else {
                    $return->status = 1;
                    echo json_encode($return);
                    die();
                }
            }
            else {
                $return->status = 1;
                echo json_encode($return);
                die();
            }

        }

    }

    /**
     * Edit method
     *
     * @param string|null $id Schedule Image id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $scheduleImage = $this->ScheduleImage->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $scheduleImage = $this->ScheduleImage->patchEntity($scheduleImage, $this->request->data);
            if ($this->ScheduleImage->save($scheduleImage)) {
                $this->Flash->success(__('The schedule image has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The schedule image could not be saved. Please, try again.'));
            }
        }
        $schedules = $this->ScheduleImage->Schedules->find('list', ['limit' => 200]);
        $gigs = $this->ScheduleImage->Gigs->find('list', ['limit' => 200]);
        $users = $this->ScheduleImage->Users->find('list', ['limit' => 200]);
        $this->set(compact('scheduleImage', 'schedules', 'gigs', 'users'));
        $this->set('_serialize', ['scheduleImage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Schedule Image id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $scheduleImage = $this->ScheduleImage->get($id);
        if ($this->ScheduleImage->delete($scheduleImage)) {
            $this->Flash->success(__('The schedule image has been deleted.'));
        } else {
            $this->Flash->error(__('The schedule image could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
