<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

/******* THIS CALSS IS NOT USE ANYMORE ***/
// This script should be run as a background process on the server. It checks
// every few seconds for new messages in the database table push_queue and 
// sends them to the Apple Push Notification Service.
//
// If a fatal error occurs (cannot establish a connection to the database or
// APNS), this script exits. 
// This script
// If this
// script isn't running, no push notifications will be delivered!
////////////////////////////////////////////////////////////////////////////////

class PushController extends AppController {

    var $fp = NULL;

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index() {
        //$this->start();
    }

    // This is the main loop for this script. It polls the database for new
    // messages, sends them to APNS, sleeps for a few seconds, and repeats this
    // forever (or until a fatal error occurs and the script exits).
    public function start() {

        //  while (true) { Removed server issue.
        $this->loadModel('PushQueue');
        //Get the total count of downliners under the parent

        /* DO NOT LIMIT PUSH QUE **/
        $query = $this->PushQueue->find()->where(function ($exp, $q) {
                    return $exp->isNull('modified_ios');
                });
        # WHERE (population) IS NULL
        $count = $query->count();
        $results = $query->toArray();

//            $stmt = $conn->prepare('SELECT * FROM push_queue WHERE modified_ios IS NULL LIMIT 20');
//            $stmt->execute();

        $messages = $query->toArray();
        if (!empty($messages)) {
            $conn = ConnectionManager::get('default');
            foreach ($messages as $message) {
                //Check the message type
                //If the message type is 1 then it is a gig notification. So fetch all active user with the device token and logged in users
                $params = json_decode($message['param']);
                $this->writeToLog('applePushInitializer 03: ' . ' param ' . $message['param']);

                if ($message['type'] == '1') {
                   /* $stmt = $conn->prepare('SELECT u.*, c.image as user_image FROM user u, candidate c WHERE u.status = 1 AND device_type = 2 AND device_token IS NOT NULL AND u.id = c.user_id');*/

                    $country = $message['gig_country'];
                    $stmt = $conn->prepare('SELECT u.*,c.image as user_image FROM user u LEFT JOIN candidate c ON  u.id = c.user_id WHERE u.status = 1 AND device_type = 2 AND device_token IS NOT NULL AND c.lng_country ="' .$country . '"');


                    $stmt->execute();
                    $users = $stmt->fetchAll('assoc');
                } else {
                    //If the message type is 2 then it is a chat notification. So fetch the users with the user id and logged in user
                    $stmt = $conn->prepare('SELECT u.*,c.image as user_image FROM user u, candidate c WHERE u.status = 1 AND device_type = 2 AND device_token IS NOT NULL AND u.id = ' . $params->user_id . ' AND u.id = c.user_id');
                    $stmt->execute();
                    $users = $stmt->fetchAll('assoc');
                }

                if (!empty($users)) {
                    foreach ($users AS $user) {
                        $this->writeToLog('applePushInitializer 04: ' . ' users ' . print_r($user['name'], true));

                        //Send the push notification to the APNS server
                        $textJson = $this->jsonEncode($message['payload']);
                        $textJson = $this->truncateUtf8($textJson, Configure::read('MAX_MESSAGE_LENGTH'));

                        $payload = "";
                        $user_image = Configure::read('dev_base_url') . 'img/profile_placeholder.png';
                        //Before returning the image check if the image is a Facebook image.
                        if ($user['user_image'] != NULL && $user['user_image'] != '') {
                            if (preg_match('/facebook/', $user['user_image'])) {
                                //If it is a facebook image then return the URL as it is
                                $user_image = $user['user_image'];
                            } else {
                                //If it is a custom image uploaded by the user then return with the complete path
                                $user_image = Configure::read('dev_base_url') . 'img/user_images/' . $user['user_image'];
                            }
                        }

                        if ($message['type'] == 1) {
                            // Create the payload body
                            $body['aps'] = array(
                                'alert' => $message['payload'],
                                'sound' => 'default',
                                'gig_id' => $params->gig_id,
                                'type' => '1',
                                'badge' => $user['push_count'] + 1
                            );

                            // Encode the payload as JSON
                            $payload = json_encode($body);
                        }
                        else {
                        //push for chat

                            $stmt = $conn->prepare('SELECT g.title  FROM application a
                                      LEFT JOIN gig g ON a.gig_id = g.id
                                      WHERE a.user_id = ' . $params->user_id . ' AND a.chat_id = ' .                                              $params->chat_id);
                            $stmt->execute();
                            $result = $stmt->fetch('assoc');

                            // Create the payload body
                            $body['aps'] = array(
                                'alert' => array(
                                    'title' => $result['title'],
                                    'body' => $message['payload'],
                                    'action-loc-key' => 'Go to Add'
                                ),
                                'sound' => 'default',
                                'chat_id' => $params->chat_id,
                                'user_image' => $user_image,
                                'type' => '2',
                                'badge' => $user['push_count'] + 1
                            );

                            // Encode the payload as JSON
                            $payload = json_encode($body);
                        }

                        if ($user['device_token'] != "" && $user['device_token'] != NULL) {
                            // echo $message['id'].'||'.$user['device_token'].'||'.$payload;
                            // die();
                            //$nw_push_count = $user['push_count']+1;
                            $stmt = $conn->prepare("SELECT push_count FROM user WHERE id = " . $user['id'] . "");
                            $stmt->execute();
                            $push_count_results = $stmt->fetch('assoc');
                            $push_count_new = $push_count_results['push_count'] + 1;

                            $stmt = $conn->prepare("UPDATE user SET push_count = " . $push_count_new . " WHERE id = " . $user['id'] . " AND token IS NOT NULL");
                            $stmt->execute();

                            if ($this->connectToAPNS()) {
                                $this->sendNotification($message['id'], $user['device_token'], $payload);
                                $this->writeToLog('\napplePushInitializer 05: ' . $message['id'] . ' \ndevice_token ' . $user['device_token'] . ' \npayload ' . print_r($payload, true));
                            } else {
                                $this->reconnectToAPNS();
                                $this->sendNotification($message['id'], $user['device_token'], $payload);
                                $this->writeToLog('\napplePushInitializer 06: ' . $message['id'] . ' \ndevice_token ' . $user['device_token'] . ' \npayload ' . print_r($payload, true));
                            }
                        }
                    }
                    //If message type is Gig creation Update only the ios column
                    if ($message['type'] == '1') {
                        $stmt = $conn->prepare('UPDATE push_queue SET modified_ios = NOW() WHERE id = ' . $message['id'] . '');
                        $stmt->execute();
                    } else {
                        //If message type is chat then update both columns
                        $stmt = $conn->prepare('UPDATE push_queue SET modified_and = NOW(),modified_ios = NOW() WHERE id = ' . $message['id'] . '');
                        $stmt->execute();
                    }
                    $user['push_count'] = 0;
                }
            }
        }

        unset($messages);
//            sleep(5);
//        }

        die();
    }


    // Opens an SSL/TLS connection to Apple's Push Notification Service (APNS).
    // Returns TRUE on success, FALSE on failure.
    public function connectToAPNS() {
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'private/pushcert.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', '');
        /*
          $this->fp = stream_socket_client(
          Configure::read('server'), $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
         */

        $this->fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$this->fp) {
            $this->writeToLog("Failed to connect: $err $errstr");
            return FALSE;
        }

        $this->writeToLog('Connection OK 1');
        return TRUE;
    }

    // Drops the connection to the APNS server.
    public function disconnectFromAPNS() {
        fclose($this->fp);
        $this->fp = NULL;
    }

    // Attempts to reconnect to Apple's Push Notification Service. Exits with
    // an error if the connection cannot be re-established after 3 attempts.
    public function reconnectToAPNS() {
        $this->disconnectFromAPNS();

        $attempt = 1;

        while (true) {
            $this->writeToLog('Reconnecting to ' . Configure::read('server') . ", attempt $attempt");

            if ($this->connectToAPNS())
                return;

            if ($attempt++ > 3)
                $this->fatalError('Could not reconnect after 3 attempts');

            sleep(60);
        }
    }

    // Sends a notification to the APNS server. Returns FALSE if the connection
    // appears to be broken, TRUE otherwise.
    public function sendNotification($messageId, $user_id, $payload) {

        if (!$this->fp) {
            $this->writeToLog('No connection to APNS');
            return FALSE;
        }

        // The simple format
        $msg = chr(0)                       // command (1 byte)
                . pack('n', 32)                // token length (2 bytes)
                . pack('H*', $user_id)     // device token (32 bytes)
                . pack('n', strlen($payload))  // payload length (2 bytes)
                . $payload;                    // the JSON payload

        $result = @fwrite($this->fp, $msg, strlen($msg));

        if (!$result) {
            $this->writeToLog('Message not delivered ' . $result);
            return FALSE;
        }

        $this->writeToLog('Message successfully delivered 1');
        return TRUE;
    }

    /** No need to log anymore */
    public function writeToLog($message) {
        /*if ($fp = fopen('/opt/webapp/gigstr_backend/webroot/' .Configure::read('logfile'), 'at')) {
            fwrite($fp, date('c') . ' ' . $message . PHP_EOL);
            fclose($fp);
        }*/
    }

    public function fatalError($message) {
        $this->writeToLog('Exiting with fatal error: ' . $message);
        exit;
    }

    /*
     * send function
     * 
     * @param $gig_id gig id
     * @param $push_message text in the push_message column
     */

    public function send() {

        $pushTable = TableRegistry::get('push_queue');
        $push = $pushTable->newEntity();

        $gig_id = $this->request->data['gig_id'];
        $message = $this->request->data['push_message'];
        $gig_country = $this->request->data['gig_country'];

        $textJson = $this->jsonEncode($message);
        $textJson = $this->truncateUtf8($textJson, Configure::read('MAX_MESSAGE_LENGTH'));

        $payload = $message;

        $push->param = '{"gig_id":"' . $gig_id . '"}';
        $push->type = '1';
        $push->payload = $payload;
        $push->created = date('Y-m-d H:i:s');
        $push->modified_ios = NULL;
        $push->modified_and = NULL;
        $push->gig_country = $gig_country;

        if ($pushTable->save($push))
            echo json_encode(array('status' => 0));
        else
            echo json_encode(array('status' => 404));
        die();
    }

    /*
     * pushAndroid function
     * Function to send push notifications for android
     */

    public function pushAndroid() {
        //while (true) {
        $pushStatus = '';
        $gcmRegIds = array();

//        $conn = ConnectionManager::get('default');
//        $stmt = $conn->prepare('SELECT * FROM push_queue WHERE modified_and IS NULL LIMIT 20');
//        $stmt->execute();

        $this->loadModel('PushQueue');
        //Get the total count of downliners under the parent

        /* DO NOT LIMIT PUSH QUE **/
        $query = $this->PushQueue->find()->where(function ($exp, $q) {
                    return $exp->isNull('modified_and');
                });

        # WHERE (population) IS NULL
        $count = $query->count();
        $results = $query->toArray();

//      $stmt = $conn->prepare('SELECT * FROM push_queue WHERE modified_ios IS NULL LIMIT 20');
//      $stmt->execute();
        $messages = $query->toArray();
        //$messages = $stmt->fetchAll('assoc');

        if (!empty($messages)) {
            $conn = ConnectionManager::get('default');
            foreach ($messages as $message) {
                //Check the message type
                //If the message type is 1 then it is a gig notification. So fetch all active user with the device token and logged in users
                $params = json_decode($message['param']);
                if ($message['type'] == '1') {
                   /* $stmt = $conn->prepare('SELECT u.*,c.image as user_image FROM user u,candidate c WHERE u.status = 1 AND device_type = 1 AND device_token IS NOT NULL AND u.id = c.user_id');*/

                    $country = $message['gig_country'];
                    $stmt = $conn->prepare('SELECT u.*,c.image as user_image FROM user u LEFT JOIN candidate c ON  u.id = c.user_id WHERE u.status = 1 AND device_type = 1 AND device_token IS NOT NULL AND c.lng_country="' . $country . '"');

                    $stmt->execute();
                    $users = $stmt->fetchAll('assoc');
                } else {
                    //If the message type is 2 then it is a chat notification. So fetch the users with the user id and logged in user
                    $stmt = $conn->prepare('SELECT u.*,c.image as user_image FROM user u, candidate c WHERE u.status = 1 AND device_type = 1 AND device_token IS NOT NULL AND u.id = ' . $params->user_id . ' AND u.id = c.user_id');
                    $stmt->execute();
                    $users = $stmt->fetchAll('assoc');
                }
                $user_image = "";
                if (!empty($users)) {
                    foreach ($users AS $user) {

                        array_push($gcmRegIds, $user['device_token']);

                        //Before returning the image check if the image is a Facebook image.
                        if ($user['user_image'] != NULL) {
                            if (preg_match('/facebook/', $user['user_image'])) {
                                //If it is a facebook image then return the URL as it is
                                $user_image = $user['user_image'];
                            } else {
                                //If it is a custom image uploaded by the user then return with the complete path
                                $user_image = Configure::read('dev_base_url') . 'img/user_images/' . $user['user_image'];
                            }
                        }
                    }
                    $pushMessage = $message['payload'];

                    if (isset($gcmRegIds) && isset($pushMessage)) {

                        $regIdChunk = array_chunk($gcmRegIds, 1000);
                        foreach ($regIdChunk as $RegId) {

                            $stmt = $conn->prepare("UPDATE user SET push_count = push_count+1 WHERE id = " . $user['id'] . "");
                            $stmt->execute();


                            if ($message['type'] == '1') {
                                $stmt = $conn->prepare('SELECT *  FROM gig WHERE id = ' . $params->gig_id);
                                $stmt->execute();
                                $result = $stmt->fetch('assoc');
                                $gig = new \stdClass();

                                $gig->id = $result['id'];
                                $gig->title = $result['title'];
                                $gig->sub_title1 = $result['sub_title1'];
                                $gig->sub_title2 = $result['sub_title2'];
                                $gig->gig_image = Configure::read('dev_base_url') . 'img/gig_images/thumblist-' . $result['gig_image'];
                                $gig->id = $result['id'];
                                $gig->applied = 0;

                                $this->loadModel('Application');
                                $applications = $this->Application->find('all')->where(['gig_id' => $result['id']]);

                                foreach ($applications AS $app) {
                                    if ($app->user_id == $user['id']) {
                                        $gig->applied = '1';
                                    }
                                }

                                $message_array = array('id' => $params->gig_id, 'message' => $pushMessage, 'type' => "1", 'job' => json_encode($gig), 'badge' => $user['push_count'] + 1);
                            }
                            else {
                                //push msg for chat
                                $return = new \stdClass();
                                $return->chat_id = $params->chat_id;
                                $userId = $params->user_id;

                                $stmt = $conn->prepare('SELECT g.title  FROM application a
                                      LEFT JOIN gig g ON a.gig_id = g.id
                                      WHERE a.user_id = ' . $params->user_id . ' AND a.chat_id = ' .                                              $params->chat_id);
                                $stmt->execute();
                                $result = $stmt->fetch('assoc');
                                $pushMessage = $result['title'] . ': ' . $pushMessage;

                                $message_array = array('id' => $params->chat_id, 'user_image' => $user_image, 'message' => $pushMessage, 'type' => "2", 'chat' => json_encode($return), 'title' => $result['title'], 'badge' => $user['push_count'] + 1);
                            }
                            $pushStatus = $this->sendPushNotification($RegId, $message_array);
                            $this->writeToLog('androidPushInitializer 06: ' . print_r($RegId, true) . print_r($message_array, true) . ' status ' . $pushStatus);

                            if ($message['type'] == '1') {
                                $stmt = $conn->prepare('UPDATE push_queue SET modified_and = NOW() WHERE id = ' . $message['id'] . '');
                                $stmt->execute();
                            } else {
                                $stmt = $conn->prepare('UPDATE push_queue SET modified_and = NOW(),modified_ios = NOW() WHERE id = ' . $message['id'] . '');
                                $stmt->execute();
                            }
                            $user['push_count'] = 0;
                        }
                    }
                }
            }
        }

        die();
    }

    public function applePushInitializer() {
        $this->loadModel('PushQueue');
        $lastSentMessage = $this->PushQueue->find()->where(['modified_ios IS NOT NULL'])->order(['id' => 'desc'])->first();
        $recordsWithNotNull = $this->PushQueue->find()->where(['modified_ios IS NULL'])->count();
        $recordsInTable = $this->PushQueue->find()->count();
        $query = $this->PushQueue->find()->where(['modified_ios IS NULL']);
        $messages = $query->all();
        $this->writeToLog(" applePushInitializer 01: " . date('Y-m-d H:i:s'));

        if (!empty($lastSentMessage)) {
            //    $lastMessageSentTime = strtotime($lastSentMessage->modified_ios); // Removed: we dont need to wait 5 min for user to read the message.
            //    if (time() > ($lastMessageSentTime + (60 * 5))) {

            if (!empty($messages)) {
                try {
                //    $this->start();
                } catch (Exception $ex) {
                    
                }
            }
            //   }
        } elseif ($recordsWithNotNull === $recordsInTable) {
            $query = $this->PushQueue->find()->where(['modified_ios IS NULL']);
            $messages = $query->all();
            if (!empty($messages)) {
                try {
                    $this->writeToLog("\n applePushInitializer 02: " . print_r($recordsInTable, true));
                  //  $this->start();
                } catch (Exception $ex) {
                    
                }
            }
        } else {
            die(); // No action from the cron
        }
        die(); // No action from the cron
    }

    public function androidPushInitializer() {
        $this->loadModel('PushQueue');
        $this->writeToLog("\n --------------------- androidPushInitializer 01: " . date('Y-m-d H:i:s'));
        $recordsWithNull = $this->PushQueue->find()->where(['modified_and IS NULL'])->count();
        if ($recordsWithNull > 0) {
            try {
             //   $this->pushAndroid();
            } catch (Exception $ex) {
                
            }
        } else {
            return false;
        }
    }

}
