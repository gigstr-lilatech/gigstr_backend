<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    use \Crud\Controller\ControllerTrait;

    public $components = [
        'RequestHandler',
        'Crud.Crud' => [
            'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Add',
                'Crud.Edit',
                'Crud.Delete'
            ],
            'listeners' => [
                'Crud.Api',
                'Crud.ApiPagination',
                'Crud.ApiQueryLog'
            ]
        ]
    ];
    public $public_access = array();

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');


        $this->public_access = array('User/login', 'User/add', 'User/resetPassword', 'Pages/display', 'Gig/viewGig', 'Schedule/viewSchedule','Push/index', 'Push/pushAndroid', 'Push/getUnsentMessages', 'Push/testPush', 'Gig/listAll', 'Gig/listGig', 'Push/applePushInitializer', 'Push/androidPushInitializer', 'Schedule/cronScheduleReminder');
        $this->admin_public_access = array('User/verifyPassword', 'User/forgotPassword', 'User/resetPassword', 'User/recommendations', 'User/recommendationSubmit');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event) {
        if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function beforeFilter(Event $event) {

        $this->loadModel('Company');
        $companies = $this->Company->find('all');


        $company_list = array();
        foreach ($companies as $company) {
            $company_list[$company['id']] = $company['name'];
        }
        $this->set('menu_company', $company_list);

        $controller = $this->request->params['controller'];
        $action = $this->request->params['action'];
        $request_action = $controller . '/' . $action;

        if ((isset($this->request->params['prefix']) && ($this->request->params['prefix'] == "admin")) || ($controller == "pages")) {
            // Security::setHash('md5');

            if (in_array($request_action, $this->admin_public_access)) {
                
            } else {
                $this->loadComponent('Auth', [
                    'authenticate' => [
                        'Form' => [
                            'fields' => [
                                'username' => 'email',
                                'password' => 'password'
                            ]
                        ]
                    ],
                    'loginAction' => [
                        'controller' => 'user',
                        'action' => 'login',
                        'admin' => false
                    ],
                    'loginRedirect' => [
                        'controller' => 'gig',
                        'action' => 'index'
                    ],
                    'logoutRedirect' => [
                        'controller' => 'user',
                        'action' => 'login'
                    ],
                    'unauthorizedRedirect' => [
                        'controller' => 'user',
                        'action' => 'login',
                        'prefix' => 'admin'
                    ]
                ]);
                $this->Auth->config('authenticate', [
                    'Basic' => ['userModel' => 'User'],
                    'Form' => ['userModel' => 'User']
                ]);
                $this->Auth->allow('login');
                $this->Auth->autoRedirect = false;
            }
        } else {
            //header('Content-type: application/json');
            if ($this->request->is('post') || $this->request->is('get') || $this->request->is('put')) {
                // ************ Commented: Token validation is inside only private service calls.
                //if (in_array($request_action, $this->public_access)) {
                    //Allow users to access these funtions without login
                //} else {
                    //echo $this->request->header('token');
                    //$this->loadModel('User');
//
//                    if ($this->request->header('token') != NULL && $this->request->header('user_id') != NULL) {
//                        if ($this->User->isLoggedIn($this->request->header('token'), $this->request->header('user_id'))) {
//                            //If the user is logged in and authorized to access this page
//                        } else {
//                            //If the provided token and user_id not match
//                            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
//                            echo json_encode($return);
//                            die();
//                        }
//                    } else {
//                        //If the token or the user id not set return error
//                        $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
//                        echo json_encode($return);
//                        die();
//                    }
                //}
                // ************
            } else {
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Unauthorized access');
                echo json_encode($return);
                die();
            }
        }
    }

    // Creates the JSON payload for the push notification message. The "alert"
    // text has the following format: "sender_name: message_text". Recipients
    // can obtain the name of the sender by parsing the alert text up to the
    // first colon followed by a space.
//    function makePayload($text, $type = 1) {
//       
//        // Convert and truncate the message text
//        $textJson = $this->jsonEncode($text);
//        $textJson = truncateUtf8($textJson, self::MAX_MESSAGE_LENGTH);
//
//        // Combine everything into a JSON string
//        $payload = '{"aps":{"' . $textJson . '","sound":"default"}}';
//        return $payload;
//    }
    // We don't use PHP's built-in json_encode() function because it converts
    // UTF-8 characters to \uxxxx. That eats up 6 characters in the payload for
    // no good reason, as JSON already supports UTF-8 just fine.
    public function jsonEncode($text) {
        static $from = array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"');
        static $to = array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"');
        return str_replace($from, $to, $text);
    }

    public function truncateUtf8($string, $maxLength) {
        $origString = $string;
        $origLength = $maxLength;

        while (strlen($string) > $origLength) {
            $string = mb_substr($origString, 0, $maxLength, 'utf-8');
            $maxLength--;
        }

        return $string;
    }

    public function sendPushNotification($registration_ids, $message_array) {

        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message_array,
        );
//        echo '<pre>';
//        print_r($fields);
//        echo '</pre>';
//        exit();
        /// define('GOOGLE_API_KEY', Configure::read('google_api_key'));

        $headers = array(
            'Authorization:key=' . Configure::read('google_api_key'),
            'Content-Type: application/json'
        );
        //echo json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === false)
            die('Curl failed ' . curl_error());

        curl_close($ch);
        return $result;
    }

    public function redirectPush($url) {
        if (!headers_sent()) {
            header('Location: ' . $url);
            exit;
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $url . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
            echo '</noscript>';
            exit;
        }
    }

    public function pushAndroid() {
        //while (true) {
        $pushStatus = '';
        $gcmRegIds = array();

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT * FROM push_queue WHERE modified IS NULL LIMIT 20');
        $stmt->execute();

        $messages = $stmt->fetchAll('assoc');

        if (!empty($messages)) {
            foreach ($messages as $message) {
                //Check the message type
                //If the message type is 1 then it is a gig notification. So fetch all active user with the device token and logged in users

                if ($message['type'] == '1') {
                    $stmt = $conn->prepare('SELECT * FROM user WHERE status = 1 AND device_type = 1 AND token IS NOT NULL AND device_token IS NOT NULL');
                    $stmt->execute();
                    $users = $stmt->fetchAll('assoc');
                } else {
                    //If the message type is 2 then it is a chat notification. So fetch the users with the user id and logged in user
                    $stmt = $conn->prepare('SELECT * FROM user WHERE status = 1 AND device_type = 1 AND token IS NOT NULL AND device_token IS NOT NULL AND id = ' . $message['param'] . '');
                    $stmt->execute();
                    $users = $stmt->fetchAll('assoc');
                }
                if (!empty($users)) {

                    foreach ($users AS $user) {
                        array_push($gcmRegIds, $user['device_token']);
                    }
                    $pushMessage = $message['payload'];

                    if (isset($gcmRegIds) && isset($pushMessage)) {

                        $regIdChunk = array_chunk($gcmRegIds, 1000);
                        foreach ($regIdChunk as $RegId) {

                            if ($message['type'] == '1')
                                $message_array = array('id' => $message['param'], 'message' => $pushMessage, 'type' => '1');
                            else
                                $message_array = array('id' => $message['param'], 'message' => $pushMessage, 'type' => '2');

                            //$message_text = json_encode($message_array);

                            $pushStatus = $this->sendPushNotification($RegId, $message_array);
                        }
                    }

                    $stmt = $conn->prepare('UPDATE push_queue SET modified = NOW() WHERE id = ' . $message['id'] . '');
                    $stmt->execute();
                }
            }
        }


        //$url = Configure::read('dev_base_url') . Configure::read('android_push_url');
        //}
        //die();
    }

}
