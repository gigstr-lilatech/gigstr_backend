<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Psy\Util\Json;

/**
 * User Controller
 *
 * @property \App\Model\Table\UserTable $User
 */
class UserController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('SimpleImage');
        $this->loadComponent('Stripe');
    }

    /*
     * Api related functions
     */

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        header('Content-type: application/json');

        $country = $this->request->header('X-Country');
        $device_id = $this->request->header('X-deviceID');

        $referrer_id = "";
        if($device_id) {
            $conn = ConnectionManager::get('default');
            $stmt = $conn->prepare('SELECT referrer_user_id FROM referal
                            WHERE device_id = "' . $device_id . '"');
            $stmt->execute();
            $referrer = $stmt->fetch('assoc');
            if($referrer) {
                $referrer_id = $referrer['referrer_user_id'];
            }
        }

        $return = new \stdClass();

        if ($this->request->is('post')) {
            $hasher = new DefaultPasswordHasher();
            $data = $this->request->data;

            $userTable = TableRegistry::get('User');
            $user = $userTable->newEntity();

            $data['accountType'] = (isset($this->request->data['accountType'])) ? $this->request->data['accountType'] : 0;

            if ($data['accountType'] == 0) {
                //If the user trying to login via email
                $user->email = $email = $this->request->data['email'];
                $user->name = $this->request->data['name'];
                $user->password = MD5($this->request->data['password']);
            } else {
                //If the user trying to login via FB
                $user->name = $this->request->data['name'];
                $user->fbid = $fbid = $this->request->data['fbid'];
                $user->image = $fbid = $this->request->data['image'];
                $user->email = $this->request->data['email'];
                $user->password = MD5(time());
            }
            $user->device_token = $this->request->data['device_id'];
            $user->device_type = $this->request->data['device_type'];
            $user->referrer_id = $referrer_id;
            if($device_id) {
                $user->udid = $device_id;
            }

            $user->user_type = 1;
            $return->status = -1;

            if ($data['accountType'] == 0) {
                //Check if the email already exist
                if ($this->User->exists(['email' => $email])) {
                    $return->status = 101;
                    $return->message = "User already exists";
                }
            } else {
                
            }

            if ($return->status > 0) {
                //Already exist. Do not do anything.
            } else {
                //Else insert the user and return the information
                if ($userTable->save($user)) {
                    $this->loadModel('Candidate');

                    $return->status = 0;
                    $return->user_id = $user->id;
                    $return->name = $user->name;
                    $return->image = $this->getUserImage($user->id);
                    $return->token = $user->token;

                    $candidateTable = TableRegistry::get('Candidate');
                    $candidate = $candidateTable->newEntity();
                    $candidate->user_id = $user->id;
                    $candidate->country = 'SE';
                    if($country) {
                        $candidate->lng_country = $country;
                    }
                    else {
                        $candidate->lng_country = 'SE';
                    }

                    if ($data['accountType'] == 1) {
                        $candidate->day = $this->request->data['day'];
                        $candidate->month = $this->request->data['month'];
                        $candidate->year = $this->request->data['year'];
                        $candidate->gender = $this->request->data['gender'];
                    }

                    $newCandidate = $candidateTable->save($candidate);
                }
                //If couldn't save the data return the message
                else {
                    $return->status = 104;
                    $return->message = "Unable to save user. Try again.";
                }
            }
            $this->set('return', $return);
            $this->set('_serialize', 'return');
            echo json_encode($return);
            die();
        } else {
            $return->status = 500;
            $this->set('return', $return);
            $this->set('_serialize', 'return');
            echo json_encode($return);
            die();
        }
    }

    /**
     * login method
     * @return json encoded response containing user_id, name and login token
     * @internal param int $email Email address of the user
     * @internal param int $password
     *
     */
    public function login() {
        header('Content-type: application/json');
        $userTable = TableRegistry::get('User');
        $save = array();
        $return = new \stdClass();
        $country = $this->request->header('X-Country');
        $device_id = $this->request->header('X-deviceID');

        if ($this->request->is('post')) {
            $hasher = new DefaultPasswordHasher();
            
            $data['accountType'] = (isset($this->request->data['accountType'])) ? $this->request->data['accountType'] : 0;

            //If this is an Email login
            if ($data['accountType'] == '0') {
                $post['email'] = $this->request->data['email'];
                $post['password'] = $this->request->data['password'];

                if (trim($this->request->data['device_id']) != '') {
                    $user['device_token'] = $this->request->data['device_id'];
                }
                $user['device_type'] = $this->request->data['device_type'];
                $data['hash'] = MD5($post['password']);

                $query = $this->User->find('all', [
                    'conditions' => ['User.email' => $post['email'], 'User.password' => $data['hash'], 'User.status' => '1']
                ]);
                $count = $query->count();
                $userDetails = $query->first();

                //If email already exist in the database
                if ($count > 0) {
                    $user_id = $userDetails->id;
                    $token = $hasher->hash(sha1(Text::uuid()));

                    $query = $this->User->query();
                    $query->update()
                            ->set(['token' => $token, 'device_token' => $user['device_token'], 'device_type' => $user['device_type']])
                            ->where(['id' => $user_id])
                            ->execute();

                    $return->status = 0;
                    $return->user_id = $user_id;
                    $return->name = $userDetails->name;
                    $return->image = $this->getUserImage($user_id);
                    $return->token = $token;
                } else {
                    //No user found with the prvidd email
                    $return->status = 105;
                    $return->message = "Verify Login credentials";
                }
            }
            //If it is a FB login
            else {
                $this->loadModel('Candidate');
                $post['fbid'] = $this->request->data['fbid'];
                $post['name'] = $this->request->data['name'];

                if($post['fbid'])
                {
                    $post['image'] = "https://graph.facebook.com/".$post['fbid']."/picture?type=large";
                }
                else
                {
                    $post['image'] = $this->request->data['image'];
                }

                $post['day'] = $this->request->data['day'];
                $post['month'] = $this->request->data['month'];
                $post['year'] = $this->request->data['year'];

                $post['gender'] = $this->request->data['gender'];
                $post['email'] = $this->request->data['email'];

                $query = $this->User->find('all', [
                    'conditions' => ['User.fbid' => $post['fbid']]
                ]);

                $count = $query->count();
                $userDetails = $query->first();

                //If the user exist in the database
                if ($count > 0) {

                    $user_id = $userDetails->id;
                    $token = $hasher->hash(sha1(Text::uuid()));

                    $device_token = $userDetails->device_token;
                    $device_type = $userDetails->device_type;

                    //If the device id is provided then update the device id
                    if (trim($this->request->data['device_id']) != '') {
                        $device_token = $this->request->data['device_id'];
                        $device_type = $this->request->data['device_type'];
                    }
                    //$this->writeToLog('new' . ' param ' . $this->request->data['device_id']."||".$post['fbid']);
                    $query = $this->User->query();
                    $query->update()
                            ->set(['token' => $token, 'device_token' => $device_token, 'device_type' => $device_type, 'status' => '1'])
                            ->where(['id' => $user_id])
                            ->execute();

                    $return->status = 0;
                    $return->user_id = $user_id;
                    $return->name = $userDetails->name;
                    $return->image = $this->getUserImage($user_id);
                    $return->token = $token;
                }
                else {
                    //If User not exist with the given information then insert a new entry in the database
                    $user = $userTable->newEntity();
                    $user->name = $post['name'];
                    $user->fbid = $post['fbid'];

                    if($this->request->data['device_type'] == '1') {

                        $referrer_id = "";
                        if($device_id) {
                            $conn = ConnectionManager::get('default');
                            $stmt = $conn->prepare('SELECT referrer_user_id FROM referal
                            WHERE device_id = "' . $device_id . '"');
                            $stmt->execute();
                            $referrer = $stmt->fetch('assoc');
                            if($referrer) {
                                $referrer_id = $referrer['referrer_user_id'];
                            }

                            $user->udid = $device_id;
                            $user->referrer_id = $referrer_id;
                        }
                    }

                    $user->password = $hasher->hash(time());
                    $user->device_token = $this->request->data['device_id'];
                    $user->device_type = $this->request->data['device_type'];
                    $user->email = ($this->request->data['email'] != '') ? $this->request->data['email'] : NULL;
                    $userTable->save($user);

                    //Insert a new entry in the candidate table
                    $candidateTable = TableRegistry::get('Candidate');
                    $candidate = $candidateTable->newEntity();

                    $candidate->user_id = $user->id;
                    $candidate->country = 'SE';
                    $candidate->lng_country = $country;
                    $candidate->image = $post['image'];
                    $candidate->day = $post['day'];
                    $candidate->month = $post['month'];
                    $candidate->year = $post['year'];
                    $candidate->gender = $post['gender'];

                    $candidateTable->save($candidate);

                    $return->status = 0;
                    $return->user_id = $user->id;
                    $return->name = $post['name'];
                    $return->image = $this->getUserImage($user->id);
                    $return->token = $user->token;
                }
            }
        } else {
            $return->status = 500;
            $return->message = "Unauthorized login";
        }
        $this->set('return', $return);
        $this->set('_serialize', 'return');
        echo json_encode($return);
        die();
    }

    private function getUserImage($userId) {

        $this->loadModel('Candidate');
        $image = $this->Candidate->findByUserId($userId)->first();

        if ($image->image != "") {
            if (preg_match('/facebook/', $image->image)) {
                $imageUrl = $image->image;
            } else {
                $imageUrl = Configure::read('dev_base_url') . 'img/user_images/' . $image->image;
            }
        }
        else {
            $imageUrl = Configure::read('dev_base_url') . 'img/user_images/user_placeholder.jpg';
        }
        return $imageUrl;
    }

    /**
     * logout method
     * @return json encoded object containing status 0 if logged out (Cleared the token for the user)
     * @internal param int $user_id User id
     *
     */
    public function logout() {
        header('Content-type: application/json');

        $return = new \stdClass();
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $token = $this->request->data['token'];

            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    //If the user is logged in and authorized to access this page

                    $query = $this->User->query();
                    //Set the login token to NULL
                    $query->update()
                            ->set(['token' => NULL, 'fcm_token' => ''])
                            ->where(['id' => $user_id])
                            ->execute();

                    $return->status = 0;
                } else {
                    $return->status = 500;
                    $return->message = "Unauthorized access";
                }
                $this->set('return', $return);
                $this->set('_serialize', 'return');
                echo json_encode($return);
                die();
            } else {
                //If the provided token and user_id not match
                $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                echo json_encode($return);
                die();
            }
        } else {
            //If the token or the user id not set return error
            $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
            echo json_encode($return);
            die();
        }
    }

    public function resetPassword() {

        $return = new \stdClass();
        if ($this->request->is('post')) {
            header('Content-type: application/json');
            $email = $this->request->data['email'];
            //$email = 'dkchamara@gmail.com';
//            echo $email;
//            die();
            //Check if the email already exist
            $query = $this->User
                    ->find('all')
                    ->where(['User.email' => trim($email)]);
            //pr($query->toArray());die;
            $count = $query->count();

            if ($count > 0) {
                $hasher = new DefaultPasswordHasher();
                $resetEmail = TableRegistry::get('Resetemail');
                $userDetails = $query->first();
                $reset = $resetEmail->newEntity();

                //$reset->user_id = $userDetails->id;
                $reset->email = $userDetails->email;
                $reset->token = $hasher->hash(time());
                $reset->status = 0;

                $resetEmail->save($reset);
                //echo json_encode($reset);
                //exit();
                $base_url = Configure::read('dev_base_url');

                $email = new Email('default');
                $email->viewVars(['base_url' => $base_url, 'token' => $reset->token]);
                $email->template('resetpassword', 'default')
                        ->emailFormat('html')
                        ->from([Configure::read('from_email') => Configure::read('from_site')])
                        ->to($this->request->data['email'])
                        ->subject(Configure::read('reset_password_subject'))
                        ->send();

                //Send email with password reset code
                $return->status = 0;
            } else {
                $return->status = 106;
                $return->message = "Unknown email. Please try again.";
            }
        }
        $this->set('return', $return);
        $this->set('_serialize', 'return');
        echo json_encode($return);
        die();
    }

    public function getPersonal() {
        header('Content-type: application/json');
        $this->loadModel('Candidate');
        if ($this->request->is('post')) {
            $return = new \stdClass();
            $user_id = $this->request->data['user_id'];
            $token = $this->request->data['token'];

            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    //If the user is logged in and authorized to access this page

                    if(isset($this->request->data['applicant_id'])) {
                        $user_id = $this->request->data['applicant_id'];
                        $gigId = $this->request->data['gig_id'];

                        $this->loadModel('Application');
                        $applicant = $this->Application->findByUserIdAndGigId($user_id, $gigId)->first();
                        $applicantObj = $this->Application->get($applicant->id);
                        $applicantObj->status = '1';
                        $this->Application->save($applicantObj);
                    }

                    $conn = ConnectionManager::get('default');
                    $query = $conn->newQuery();
                    $query->select('user_id, phone_number,gender,day,month,year,u.email,person_number,image,u.name, u.fbid,address,address_co, city,country, zip_code, pitch, car_driver_license, candidate_rating')
                            ->from('candidate')
                            ->join(
                                    ['u' => [
                                            'table' => 'user',
                                            'type' => 'INNER',
                                            'conditions' => 'u.id = candidate.user_id',
                                ]])
                            ->where(['user_id' => $user_id]);
                    $results = new \stdClass();
                    $this->loadModel('Schedule');

                    if (count($query) > 0) {
                        foreach ($query as $row) {
                            $results->name = $row['name'];
                            $results->email = ($row['email'] != "") ? $row['email'] : "";
                            $results->mobile = ($row['phone_number'] != "") ? $row['phone_number'] : "";
                            $results->gender = ($row['gender'] != NULL) ? $row['gender'] : "";
                            $results->day = ($row['day'] != NULL) ? $row['day'] : "";
                            $results->month = ($row['month'] != NULL) ? $row['month'] : "";
                            $results->year = ($row['year'] != NULL) ? $row['year'] : "";
                            $results->person_number = ($row['person_number'] != "") ? $row['person_number'] : "";

                            $rating = $this->Schedule->find()
                                ->select('internal_rating')
                                ->where(['gigstr' => $row['user_id'], 'internal_rating !=' => 0])
                                ->toArray();
                            $rates = 0;
                            foreach($rating as $rate) {
                                $rates +=  $rate->internal_rating;
                            }
                            $total = count($rating);

                            if($total > 0) {
                                $avg = round($rates / $total, 1);
                                $results->candidate_rating = $avg;
                                $results->candidate_rating_count = $total;

                            } else {
                                $results->candidate_rating = $row['candidate_rating'];
                                $results->candidate_rating_count = 0;
                            }

                         //   $results->candidate_rating = $row['candidate_rating'];

                            if ($row['image'] != "") {
                                if (preg_match('/facebook/', $row['image'])) {
                                    $results->image = $row['image'];
                                } else {
                                    $results->image = Configure::read('dev_base_url') . 'img/user_images/' . $row['image'];
                                }
                            } else {
                                if($row['fbid'])
                                {
                                    $results->image ="https://graph.facebook.com/".$row['fbid']."/picture?type=large";
                                }
                                else
                                {
                                    $results->image = "";
                                }
                            }

                            //$results->image = ($row['image'] != "") ? 'img/user_images/' . $row['image'] : "";
                            $results->address = ($row['address'] != "") ? $row['address'] : "";
                            $results->address_co = ($row['address_co'] != "") ? $row['address_co'] : "";
                            $results->city = ($row['city'] != "") ? $row['city'] : "";
                            $results->country = ($row['country'] != "") ? $row['country'] : "";
                            $results->zip_code = ($row['zip_code'] != "") ? $row['zip_code'] : "";

                            $results->pitch = ($row['pitch'] != "") ? $row['pitch'] : "";
                            $results->car_driver_license = $row['car_driver_license'];
                        }
                        $return->status = 0;
                        $return->data = $results;
                    } else {
                        $return->status = 404;
                        $return->data = "User not found";
                    }


                    echo json_encode($return);
                    die();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function updatePersonal() {
        Configure::write('cache', 'disable');

        header('Content-type: application/json');
        
        $return = new \stdClass();
        
        $this->loadModel('Candidate');
        if ($this->request->is('post')) {

            $mobile = $this->request->data['mobile'];
            $gender = $this->request->data['gender'];
            $day = $this->request->data['day'];
            $month = $this->request->data['month'];
            $year = $this->request->data['year'];
            $user_id = $this->request->data['user_id'];
            $email = $this->request->data['email'];
            $name = $this->request->data['name'];
            $person_number = $this->request->data['person_number'];

            $address = $this->request->data['address'];
            $image = $this->request->data['image'];
            $address_co = $this->request->data['address_co'];
            $city = $this->request->data['city'];
            $country = $this->request->data['country'];
            $zip_code = $this->request->data['zip_code'];

            $pitch = $this->request->data['pitch'];
            $car_driver_license = $this->request->data['car_driver_license'];

            //$image = $this->request->data['image'];

            $query = $this->Candidate->query();
            $query->update()
                    ->set(['phone_number' => $mobile, 'gender' => $gender, 'day' => $day, 'month' => $month, 'year' => $year, 'person_number' => $person_number,
                        'address' => $address, 'address_co' => $address_co, 'city' => $city, 'country' => $country, 'zip_code' => $zip_code,
                        'pitch' => $pitch, 'car_driver_license' => $car_driver_license, 'modified' => date('Y-m-d H:i:s')])
                    ->where(['user_id' => $user_id])
                    ->execute();

            if($email != null || $email != "")
            {
                $this->loadModel('User');
                $user = $this->User->get($user_id);
            
                if($user->email == $email)
                {
                    $query = $this->User->query();
                    $query->update()
                        ->set(['name' => $name])
                        ->where(['id' => $user_id])
                        ->execute();
                }
                else
                {
                    $conn = ConnectionManager::get('default');
                    $stmt = $conn->prepare('SELECT * FROM user where email = "'.$email.'"');

                    $stmt->execute();
                    $existemails = $stmt->fetchAll('assoc');

                    if(count($existemails) > 0)
                    {
                        $return = array('status' => 405, 'status_message' => 'error', 'message' => 'This email already exist. Please log in using the specified email or enter another email.');
                        echo json_encode($return);
                        die();
                    }
                    else
                    {
                        $query = $this->User->query();
                        $query->update()
                            ->set(['name' => $name,'email' => $email])
                            ->where(['id' => $user_id])
                            ->execute();
                    }
                }
            }
            else 
            {
                $query = $this->User->query();
                        $query->update()
                            ->set(['name' => $name,'email' => $email])
                            ->where(['id' => $user_id])
                            ->execute();
            }

            $return->status = 0;
            echo json_encode($return);
            exit();
        }
    }

    public function getBank() {
        header('Content-type: application/json');
        $this->loadModel('Candidate');
        if ($this->request->is('post')) {
            $return = new \stdClass();
            $user_id = $this->request->data['user_id'];

            $token = $this->request->data['token'];
            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    //If the user is logged in and authorized to access this page

                    $conn = ConnectionManager::get('default');
                    $query = $conn->newQuery();
                    $query->select('bank_name,clearing_number,account')
                            ->from('candidate')
                            ->where(['user_id' => $user_id]);
                    $results = new \stdClass();
                    foreach ($query as $row) {
                        $results->bank_name = ($row['bank_name'] != "") ? $row['bank_name'] : "";
                        $results->clearing_number = ($row['clearing_number'] != "") ? $row['clearing_number'] : "";
                        $results->account = ($row['account'] != "") ? $row['account'] : "";
                    }
                    $return->status = 0;
                    $return->data = $results;

                    echo json_encode($return);
                    die();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function updateBank() {
        header('Content-type: application/json');
        $this->loadModel('Candidate');
        if ($this->request->is('post')) {
            $bank_name = $this->request->data['bank_name'];
            $clearing_number = $this->request->data['clearing_number'];
            $account = $this->request->data['account'];
            $user_id = $this->request->data['user_id'];

            $token = $this->request->data['token'];
            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    //If the user is logged in and authorized to access this page

                    $query = $this->Candidate->query();
                    $query->update()
                            ->set(['bank_name' => $bank_name, 'clearing_number' => $clearing_number, 'account' => $account])
                            ->where(['user_id' => $user_id])
                            ->execute();

                    $return = new \stdClass();
                    $return->status = 0;
                    echo json_encode($return);
                    exit();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function updateDeviceToken() {
        header('Content-type: application/json');
        $this->loadModel('Candidate');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];

            $token = $this->request->data['token'];
            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    //If the user is logged in and authorized to access this page

                    $device_token = $this->request->data['device_token'];

                    $query = $this->User->query();
                    $query->update()
                            ->set(['device_token' => $device_token])
                            ->where(['id' => $user_id])
                            ->execute();

                    $return = new \stdClass();
                    $return->status = 0;
                    echo json_encode($return);
                    exit();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function uploadImage() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $user_id = $this->request->data['user_id'];
            //If the user is logged in and authorized to access this page
            //$exif_value = $this->request->data['image_exif'];
            /* Path to Images folder */
            $dir = WWW_ROOT . 'img' . DS . 'user_images';
            /* Explode the name and ext */
            $f = explode('.', $_FILES['image_data']['name']);
            $ext = '.' . end($f);
            /* Generate a Name in my case i use ID  & slug */
            $filename = time();

            /* Save image in the thumbnail folders and replace if exist */
            move_uploaded_file($_FILES['image_data']['tmp_name'], $dir . DS . $filename . $ext);

            $fn = $filename . $ext;
            //// $this->writeToLog($exif_value . '---' . $user_id);
            $this->SimpleImage->load($dir . DS . $fn);
            $this->SimpleImage->resize(200, 200);
            $this->SimpleImage->save($dir . DS . 'thumbnail-' . $fn);
            //$this->image_fix_orientation($dir . DS . $fn, $exif_value);

            $this->loadModel('Candidate');


            $query = $this->Candidate->query();
            $query->update()
                    ->set(['image' => $fn, 'image_thumbnail' => 'thumbnail-' . $fn])
                    ->where(['user_id' => $user_id])
                    ->execute();

            $return = new \stdClass();
            $return->status = 0;
            $return->image =  Configure::read('dev_base_url') . 'img/user_images/' . $fn;
            echo json_encode($return);
            exit();
        }
    }

    public function uploadImageAndroid() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $user_id = $this->request->data['user_id'];
            $token = $this->request->data['token'];
            $this->loadModel('User');
            if ($token != NULL && $user_id != NULL) {
                if ($this->User->isLoggedIn($token, $user_id)) {
                    //If the user is logged in and authorized to access this page


                    $exif_value = $this->request->data['image_exif'];
                    /* Path to Images folder */
                    $dir = WWW_ROOT . 'img' . DS . 'user_images';
                    /* Explode the name and ext */
                    $f = explode('.', $_FILES['image_data']['name']);
                    $ext = '.' . end($f);
                    /* Generate a Name in my case i use ID  & slug */
                    $filename = time();

                    /* Save image in the thumbnail folders and replace if exist */
                    move_uploaded_file($_FILES['image_data']['tmp_name'], $dir . DS . $filename . $ext);

                    $fn = $filename . $ext;
                    //// $this->writeToLog($exif_value . '---' . $user_id);
                    $this->SimpleImage->load($dir . DS . $fn);
                    $this->SimpleImage->resizeToHeight(200);
                    $this->SimpleImage->save($dir . DS . 'thumbnail-' . $fn);

                    $this->image_fix_orientation($dir . DS . $fn, $exif_value);

                    $this->loadModel('Candidate');

                    $query = $this->Candidate->query();
                    $query->update()
                            ->set(['image' => $fn, 'image_thumbnail' => 'thumbnail-' . $fn])
                            ->where(['user_id' => $user_id])
                            ->execute();

                    $return = new \stdClass();
                    $return->status = 0;
                    $return->image =  Configure::read('dev_base_url') . 'img/user_images/' . $fn;
                    echo json_encode($return);
                    exit();
                } else {
                    //If the provided token and user_id not match
                    $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
                    echo json_encode($return);
                    die();
                }
            } else {
                //If the token or the user id not set return error
                $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
                echo json_encode($return);
                die();
            }
        }
    }

    public function image_fix_orientation($filename, $exif_value) {
        //$exif = exif_read_data($filename);

        $exif = $exif_value;
        if (!empty($exif)) {
            $image = imagecreatefromjpeg($filename);
            // $this->writeToLog($exif);
            switch ($exif) {
                case 3:
                    $image = imagerotate($image, 180, 0);
                    break;

                case 6:
                    $image = imagerotate($image, -90, 0);
                    break;

                case 8:
                    $image = imagerotate($image, 90, 0);
                    break;
            }

            imagejpeg($image, $filename, 90);
        }
    }

    public function writeToLog($message) {
        if ($fp = fopen(Configure::read('logfile'), 'at')) {
            fwrite($fp, date('c') . ' ' . $message . PHP_EOL);
            fclose($fp);
        }
    }

    public function updatePushCount() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $device_token = $this->request->data['device_token'];

            $push_count = 0;
            $query = $this->User->query();
            $query->update()
                    ->set(['push_count' => 0])
                    ->where(['id' => $user_id])
                    ->execute();

            $return = new \stdClass();
            $return->status = 0;
            echo json_encode($return);
            exit();
        }
    }

    /**
     * get country list
     */
    public function country() {
        header('Content-type: application/json');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare(
            'SELECT country_code AS code, alternate_country AS name FROM `country` WHERE user_country = 1'
        );
        $stmt->execute();

        $countries = $stmt->fetchAll('assoc');
        $return = new \stdClass();

        if($countries) {
            $return->status = 0;
        }
        else {
            $return->status = 1;
        }

        $return->countries = $countries;
        $this->set('return', $return);
        $this->set('_serialize', ['return']);
        echo json_encode($return);
        die();

    }

    /**
     * update X-Country
     */
    public function updateCountry() {
        header('Content-type: application/json');
        $country = $this->request->header('X-Country');
        $conn = ConnectionManager::get('default');

        if ($this->request->is('post')) {
            $user = $this->request->data['user_id'];

            $stmt = $conn->prepare(
                "UPDATE candidate SET lng_country = '". $country . "' WHERE user_id = " . $user
            );

            $return = new \stdClass();
            if($stmt->execute()) {
                $return->status = 0;
            }
            else {
                $return->status = 1;
            }
            $this->set('return', $return);
            $this->set('_serialize', ['return']);
            echo json_encode($return);
            die();
        }
    }



    /**
     ***************************************************************************************************
     * V2 end points
     * *************************************************************************************************
     */


    /**
     * Add method
     * Register new user
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function newAdd() {
        header('Content-type: application/json');

        $country = $this->request->header('X-Country');
        $device_id = $this->request->header('X-deviceID');
        $return = new \stdClass();

        $referrer_id = "";
        if($device_id) {
            $ref = $this->User->checkReferrer($device_id);
            if(!empty($ref)) {
                $referrer_id = $ref;
            }
        }

        if ($this->request->is('post')) {

            $hasher = new DefaultPasswordHasher();

            $data = $this->request->data;
            $user = $this->User->newEntity();
            $data['accountType'] = (isset($this->request->data['accountType'])) ? $this->request->data['accountType'] : 0;

            if ($data['accountType'] == 0) {
                //If the user trying to login via email
                $user->email = $email = $this->request->data['email'];
                $user->name = $this->request->data['name'];
                $user->password = MD5($this->request->data['password']);
            } else {
                //If the user trying to login via FB
                $user->name = $this->request->data['name'];
                $user->fbid = $fbid = $this->request->data['fbid'];
                $user->image = $fbid = $this->request->data['image'];
                $user->email = $this->request->data['email'];
                $user->password = MD5(time());
            }
            $user->device_token = $this->request->data['device_id'];
            $user->device_type = $this->request->data['device_type'];
            $user->referrer_id = $referrer_id;
            if($device_id) {
                $user->udid = $device_id;
            }

            $user->user_type = 1;
            $return->status = -1;

            if ($data['accountType'] == 0) {
                //Check if the email already exist
                if ($this->User->exists(['email' => $email])) {
                    $return->status = 101;
                    $return->message = "User already exists";
                }
            }

            if ($return->status > 0) {
                //Already exist. Do not do anything.
            }
            else {
                //Else insert the user and return the information
                if ($this->User->save($user)) {
                    $this->loadModel('Candidate');

                    $return->status = 0;
                    $return->user_id = $user->id;
                    $return->name = $user->name;
                    $return->city = "";
                    $return->token = $user->token;

                    $candidate = $this->Candidate->newEntity();
                    $candidate->user_id = $user->id;
                    $candidate->country = $country;
                    $candidate->lng_country = $country;
                    if ($data['accountType'] == 1) {
                        $candidate->day = $this->request->data['day'];
                        $candidate->month = $this->request->data['month'];
                        $candidate->year = $this->request->data['year'];
                        $candidate->gender = $this->request->data['gender'];
                    }

                    $newCandidate = $this->Candidate->save($candidate);

                    //$this->sendMail($user->id);

                    if(isset( $this->request->data['work'])) {
                        $this->loadModel('CandidateExperiences');
                        $experiences = $this->request->data['work'];
                        foreach($experiences as $experience) {
                            $newJob = $this->CandidateExperiences->newEntity();
                            $newJob->candidate_id = $newCandidate->id;
                            $newJob->employer = $experience['employer']['name'];
                            if(isset($experience['position']['name'])) {
                                $newJob->position = $experience['position']['name'];
                            }
                            $this->CandidateExperiences->save($newJob);
                        }
                    }

                    if(isset( $this->request->data['education'])) {
                        $this->loadModel('CandidateEducation');
                        $education = $this->request->data['education'];
                        foreach($education as $edu) {
                            $newEdu = $this->CandidateEducation->newEntity();
                            $newEdu->candidate_id = $newCandidate->id;
                            $newEdu->school = $edu['school']['name'];
                            $newEdu->type = $edu['type'];
                            $this->CandidateEducation->save($newEdu);
                        }
                    }

                    $wizard = $this->Candidate->get($newCandidate->id);

                    $checkWizard = $this->User->checkWizard($wizard, true);
                    $return->signup = true;
                    $return->wizard_fire = $checkWizard['wizard_enable'];
                    $return->missing_data = $checkWizard['wizardArr'];

                    $return->profile = $this->User->profileObj($user->id);
                }
                //If couldn't save the data return the message
                else {
                    $return->status = 104;
                    $return->message = "Unable to save user. Try again.";
                }
            }
            $this->set('return', $return);
            $this->set('_serialize', 'return');
            echo json_encode($return);
            die();
        } else {
            $return->status = 500;
            $this->set('return', $return);
            $this->set('_serialize', 'return');
            echo json_encode($return);
            die();
        }
    }


    /** login v2 endpoint*/
    public function newLogin() {
        header('Content-type: application/json');
        $return = new \stdClass();
        $country = $this->request->header('X-Country');
        $device_id = $this->request->header('X-deviceID');
        $this->loadModel('Candidate');

        if ($this->request->is('post')) {
            $hasher = new DefaultPasswordHasher();
            $return->is_user_create = false;

            $data['accountType'] = (isset($this->request->data['accountType'])) ? $this->request->data['accountType'] : 0;

            //If this is an Email login
            if ($data['accountType'] == '0') {
                $post['email'] = $this->request->data['email'];
                $post['password'] = $this->request->data['password'];

                if (trim($this->request->data['device_id']) != '') {
                    $user['device_token'] = $this->request->data['device_id'];
                }
                $user['device_type'] = $this->request->data['device_type'];
                $data['hash'] = MD5($post['password']);

                $query = $this->User->find('all', [
                    'conditions' => ['User.email' => $post['email'], 'User.password' => $data['hash'], 'User.status' => '1']]);
                $count = $query->count();
                $userDetails = $query->first();

                //If email already exist in the database
                if ($count > 0) {
                    $user_id = $userDetails->id;
                    $token = $hasher->hash(sha1(Text::uuid()));

                    $query = $this->User->query();
                    $query->update()
                        ->set(['token' => $token, 'device_token' => $user['device_token'], 'device_type' => $user['device_type']])
                        ->where(['id' => $user_id])
                        ->execute();

                    $return->status = 0;
                    $return->user_id = $user_id;
                    $return->name = $userDetails->name;
                    $return->image = $this->getUserImage($user_id);
                    $return->token = $token;
                    //$return->wizard_fire = false;
                    $return->signup = false;

                    $newCandidate = $this->Candidate->findByUserId($user_id)->first();
                    $return->city = $newCandidate->city ? $newCandidate->city : "";

                    $checkWizard = $this->User->checkWizard($newCandidate, true);
                    $wiz = array_values($checkWizard['wizardArr']);
                    $return->wizard_fire = $checkWizard['wizard_enable'];
                    $return->missing_data = $wiz;
                    $return->profile = $this->User->profileObj($user_id);

                } else {
                    //No user found with the prvidd email
                    $return->status = 105;
                    $return->message = "Verify Login credentials";
                }
            }

            //If it is a FB login
            else {

                $post['fbid'] = $this->request->data['fbid'];
                $post['name'] = $this->request->data['name'];

                if($this->request->data['device_type'] == '1') { //android
                    $post['image'] = "https://graph.facebook.com/".$post['fbid']."/picture?type=large";
                }
                else {
                    $post['image'] = $this->request->data['image'];
                }

                $query = $this->User->find('all', [
                    'conditions' => ['User.fbid' => $post['fbid']]
                ]);
                $count = $query->count();
                $userDetails = $query->first();

                //If the user exist in the database
                if ($count > 0) {

                    $user_id = $userDetails->id;
                    $token = $hasher->hash(sha1(Text::uuid()));
                    $device_token = $userDetails->device_token;
                    $device_type = $userDetails->device_type;

                    //If the device id is provided then update the device id
                    if (trim($this->request->data['device_id']) != '') {
                        $device_token = $this->request->data['device_id'];
                        $device_type = $this->request->data['device_type'];
                    }
                    $query = $this->User->query();
                    $query->update()
                        ->set(['token' => $token, 'device_token' => $device_token, 'device_type' => $device_type, 'status' => '1'])
                        ->where(['id' => $user_id])
                        ->execute();

                    $return->status = 0;
                    $return->user_id = $user_id;
                    $return->name = $userDetails->name;
                    $return->image = $this->getUserImage($user_id);
                    $return->token = $token;
                    $return->wizard_fire = false;
                    $return->signup = false;

                    $newCandidate = $this->Candidate->findByUserId($user_id)->first();
                    $return->city = $newCandidate->city ? $newCandidate->city : "";

                    if(isset( $this->request->data['work'])) {
                        $experiences = $this->request->data['work'];
                        $this->saveExperiences($device_type, $experiences, $newCandidate->id);
                    }

                    if(isset( $this->request->data['education'])) {
                        $education = $this->request->data['education'];
                        $this->saveEducation($device_type, $education, $newCandidate->id);
                    }

                    if(isset($this->request->data['languages'])) {
                        $language = $this->request->data['languages'];
                        $this->saveLanguages($device_type, $language, $newCandidate->id);
                    }
                    $candidate = $this->Candidate->get($newCandidate->id);

                    if(isset($this->request->data['website'])) {
                        if(empty($newCandidate->website)) {
                            $website = trim($this->request->data['website']);
                            $webArr = explode("\n", $website);
                            $candidate->website = $webArr[0];
                        }
                    }
                    if(isset($this->request->data['fbLink']) && empty($newCandidate->facebook)) {
                        $candidate->facebook = $this->request->data['fbLink'];
                    }
                    $this->Candidate->save($candidate);

                }
                else {
                    //If User not exist with the given information then insert a new entry in the database
                    $user = $this->User->newEntity();
                    $user->name = $post['name'];
                    $user->fbid = $post['fbid'];

                    if($this->request->data['device_type'] == '1') {
                        $referrer_id = "";
                        if($device_id) {
                            $conn = ConnectionManager::get('default');
                            $stmt = $conn->prepare('SELECT referrer_user_id FROM referal
                            WHERE device_id = "' . $device_id . '"');
                            $stmt->execute();
                            $referrer = $stmt->fetch('assoc');
                            if($referrer) {
                                $referrer_id = $referrer['referrer_user_id'];
                            }
                            $user->udid = $device_id;
                            $user->referrer_id = $referrer_id;
                        }
                    }

                    $user->password = $hasher->hash(time());
                    $user->device_token = $this->request->data['device_id'];
                    $device_type = $this->request->data['device_type'];
                    $user->device_type = $device_type;
                    $user->email = ($this->request->data['email'] != '') ? $this->request->data['email'] : NULL;
                    $newUser = $this->User->save($user);

                    //Insert a new entry in the candidate table
                    $candidate = $this->Candidate->newEntity();
                    $candidate->user_id = $newUser->id;
                    $candidate->country = '';
                    $candidate->lng_country = $country;
                    $candidate->image = $post['image'];
                    $candidate->day = $this->request->data['day'];
                    $candidate->month = $this->request->data['month'];
                    $candidate->year = $this->request->data['year'];
                    $candidate->gender = $this->request->data['gender'];
                    $candidate->facebook = $this->request->data['fbLink'];
                    if(isset($this->request->data['mobile'])) {
                        $candidate->phone_number = $this->request->data['mobile'];
                    }
                    if(isset($this->request->data['website'])) {
                        $website = trim($this->request->data['website']);
                        $webArr = explode("\n", $website);
                        $candidate->website = $webArr[0];
                    }

                    $newCandidate = $this->Candidate->save($candidate);

                    if(isset( $this->request->data['work'])) {
                        $experiences = $this->request->data['work'];
                        $this->saveExperiences($device_type, $experiences, $newCandidate->id);
                    }

                    if(isset( $this->request->data['education'])) {
                        $education = $this->request->data['education'];
                        $this->saveEducation($device_type, $education, $newCandidate->id);
                    }

                    if(isset($this->request->data['languages'])) {
                        $language = $this->request->data['languages'];
                        $this->saveLanguages($device_type, $language, $newCandidate->id);
                    }

                    $return->status = 0;
                    $return->user_id = $user->id;
                    $return->name = $post['name'];
                    $return->image = $this->getUserImage($user->id);
                    $return->city = "";
                    $return->token = $user->token;
                    $return->signup = true;
                    $return->is_user_create = true;

                    $checkWizard = $this->User->checkWizard($newCandidate, true);
                    $wiz = array_values($checkWizard['wizardArr']);
                    $return->wizard_fire = $checkWizard['wizard_enable'];
                    $return->missing_data = $wiz;
                    $return->profile = $this->User->profileObj($user->id);
                }
            }
        } else {
            $return->status = 500;
            $return->message = "Unauthorized login";
        }
        $this->set('return', $return);
        $this->set('_serialize', 'return');
        echo json_encode($return);
        die();
    }


    /** getPersonal V2 endpoint */
    public function getNewPersonal() {
        header('Content-type: application/json');
        $this->loadModel('Candidate');
        if ($this->request->is('post')) {
            $return = new \stdClass();
            $user_id = $this->request->header('user_id');
            $token = $this->request->header('token');
            $this->loadModel('User');

            if ($this->User->isLoggedIn($token, $user_id)) {

                //get other users data [ view profile]
                $view_contact = false;
                if(isset($this->request->data['user_id'])) {
                    $user = $this->request->data['user_id'];


                    if($user == $user_id) {
                        $type = "ME";
                    }
                    else {
                        $type = "USER";
                        $view_contact = $this->User->viewedUser($user, $user_id);
                    }

                    if(isset($this->request->data['gig_id']))  {
                        $gig = $this->request->data['gig_id'];

                        $this->loadModel('Application');
                        $this->loadModel('Event');
                        $apply = $this->Application->findByUserIdAndGigId($user, $gig)->first();
                        if($apply) {
                            $applyObj = $this->Application->get($apply->id);
                            $applyObj->status = '1';
                            $updateApplication = $this->Application->save($applyObj);

                            /** update event as read */
                            if($updateApplication->event_id != 0) {
                                $event = $this->Event->get($updateApplication->event_id);
                                $event->is_read = 1;
                                $this->Event->save($event);
                            }
                        }
                    }
                }
                else {
                    $user = $user_id;
                    $type = "ME";
                    $view_contact = true;
                }

                $this->loadModel('Schedule');
                $this->loadModel('Country');
                $this->loadModel('Company');
                $this->loadModel('Recommendation');
                $results = new \stdClass();

                $userObj = $this->Candidate->findByUserId($user)
                    ->contain(['User'])->first();

                if($userObj) {
                    $results->user_id = $userObj->user->id;
                    $results->candidate_id = $userObj->id;
                    $results->name = $userObj->user->name;
                    $results->email = $userObj->user->email ? $userObj->user->email : "";
                    $results->mobile = $userObj->phone_number ? $userObj->phone_number : "";
                    $results->gender = $userObj->gender ? $userObj->gender : "";
                    $results->day = $userObj->day ? $userObj->day : "";
                    $results->month = $userObj->month ? $userObj->month : "";
                    $results->year = $userObj->year ?$userObj->year : "";
                    $results->person_number = $userObj->person_number ? $userObj->person_number : "";

                    $ratings = $this->User->getCandidateRating($user, $userObj->candidate_rating);
                    $results->candidate_rating = $ratings->candidate_rating;
                    $results->candidate_rating_count = $ratings->candidate_rating_count;

                    $results->image = $this->User->getUserImage($userObj->image);

                    $results->country = "";
                    if($userObj->country) {
                        $countryObj = $this->Country->findByCountryCode($userObj->country)->first();
                        $results->country = $countryObj->country_name;
                    }

                    $results->address_co = $userObj->address_co ? $userObj->address_co : "";
                    $results->city = $userObj->city ? $userObj->city : "";
                    $results->zip_code = $userObj->zip_code ? $userObj->zip_code : "";
                    $results->pitch = $userObj->pitch ? $userObj->pitch : "";
                    $results->car_driver_license = $userObj->car_driver_license;
                    $results->bio = $userObj->bio;

                    $addressFix = (!empty($userObj->address_co) ? $userObj->address_co . ", " : "") . (!empty
                        ($userObj->zip_code) ? $userObj->zip_code . ", " : "") . (!empty($userObj->city) ?
                            $userObj->city . ", " . $results->country : "");
                    $results->address = $addressFix;

                    $results->skills = $this->User->getSkillList($userObj->id);
                    $results->language = $this->User->getLanguagesList($userObj->id);
                    $results->education = $this->User->getEducation($userObj->id);
                    $results->experiences = $this->User->getExperiences($userObj->id);

                    $results->website = $userObj->website;
                    $results->facebook = $userObj->facebook;
                    $results->linkedin = $userObj->linkedin;
                    $results->gigin = "Since " . date("F Y", strtotime($userObj->created));
                    $results->with_gigstr =  date("M y", strtotime($userObj->created));
                    $results->verified = $userObj->is_verified;
                    $results->gig_completed = $this->Schedule->find('all')->where(['gigstr' => $user, 'status' => 3])->count();

                    $company = $this->User->getCompanyAddress($userObj->company_id);
                    $results->company = $company->company;
                    $results->company_address = $company->company_address;
                    $results->company_website = $company->website;

                    $recommendations = $this->User->getRecommendations($user, $type);
                    $results->recommendation_link = $recommendations['link'];
                    $results->recommendations = $recommendations['recommendations'];
                    $results->recommendations_count = $recommendations['count'];
                    $results->app_mode_preference = $userObj->app_mode_preference;
                    $results->viewed_user = $view_contact;

                    if(!empty($userObj->profile_link)) {
                        $results->profile_link = $userObj->profile_link;
                    } else {
                        $results->profile_link = $this->User->getProfileBranchUrl($user_id);
                    }

                    if($type == "USER") {
                        $chatId = $this->User->getChatGroup($user_id, $user);
                        $results->chat_id = $chatId;
                    }

                    $return->status = 0;
                    $return->profile = $results;
                }
                else {
                    $return->status = 404;
                    $return->data = "User not found";
                }
            }
            else {
                $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
            }
            echo json_encode($return);
            die();
        }
    }


    /**  updatePersonal V2 endpoint */
    public function editProfileList() {

        header('Content-type: application/json');
        $this->request->allowMethod(['post']);

        $this->loadModel('Skills');
        $return = new \stdClass();
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');

        $this->loadModel('CandidateExperiences');
        $this->loadModel('CandidateEducation');
        $this->loadModel('CandidateSkills');
        $this->loadModel('CandidateLanguages');
        $this->loadModel('Candidate');

        if ($this->User->isLoggedIn($token, $user_id)) {

            $candidate = $this->Candidate->findByUserId($user_id)->first();
            $candidateObj = $this->Candidate->get($candidate->id);
            $userObj = $this->User->get($user_id);

            if(isset($this->request->data['mobile'])) {
                $candidateObj->phone_number = $this->request->data['mobile'];
                $return->phone_number = $this->request->data['mobile'];
            }
            if(isset($this->request->data['email'])) {
                $userObj->email = $this->request->data['email'];
                $return->email = $this->request->data['email'];
            }
            if(isset($this->request->data['website'])) {
                $candidateObj->website = $this->request->data['website'];
                $return->website = $this->request->data['website'];
            }
            if(isset($this->request->data['facebook'])) {
                $candidateObj->facebook = $this->request->data['facebook'];
                $return->facebook = $this->request->data['facebook'];
            }
            if(isset($this->request->data['linkedin'])) {
                $candidateObj->linkedin = $this->request->data['linkedin'];
                $return->linkedin = $this->request->data['linkedin'];
            }
            if(isset($this->request->data['address_co'])) {
                $candidateObj->address_co = $this->request->data['address_co'];
                $return->address_co = $this->request->data['address_co'];
            }
            if(isset($this->request->data['city'])) {
                $candidateObj->city = $this->request->data['city'];
                $return->city = $this->request->data['city'];
            }
            if(isset($this->request->data['area'])) {
                $candidateObj->place_id_google = $this->request->data['place_id_google'];
                $return->place_id_google = $this->request->data['place_id_google'];

                $candidateObj->latitude = $this->request->data['latitude'];
                $return->latitude = $this->request->data['latitude'];

                $candidateObj->longitude = $this->request->data['longitude'];
                $return->longitude = $this->request->data['longitude'];

                $candidateObj->area_type = $this->request->data['area_type'];
                $return->area_type = $this->request->data['area_type'];

                if ($this->request->data['country_code'] == 'GB') {
                    $englishArea = $this->request->data['area'];
                }
                else {
                    $this->loadModel('Gig');
                    $englishArea = $this->Gig->translateAddress($this->request->data['area'],
                        $this->request->data['area_type'], $this->request->data['place_id_google']);
                }
                $candidateObj->area = $englishArea;
                $return->area = $englishArea;
            }
            if(isset($this->request->data['country'])) {
                $candidateObj->country_name = $this->request->data['country'];
                $return->country = $this->request->data['country'];
            }
            if(isset($this->request->data['country_code'])) {
                $candidateObj->country = $this->request->data['country_code'];
                $return->country_code = $this->request->data['country_code'];
            }
            if(isset($this->request->data['zip_code'])) {
                $candidateObj->zip_code = $this->request->data['zip_code'];
                $return->zip_code = $this->request->data['zip_code'];
            }
            if(isset($this->request->data['address'])) {
                $candidateObj->address = $this->request->data['address'];
                $return->address = ($return->address_co ? $return->address_co . ", " : "") . $return->city . ", " . ($return->zip_code ? $return->zip_code . ", " : "") . $return->country;
            }
            if(isset($this->request->data['pitch'])) {
                $candidateObj->pitch = $this->request->data['pitch'];
                $return->pitch = $this->request->data['pitch'];
            }
            if(isset($this->request->data['bio'])) {
                $candidateObj->bio = $this->request->data['bio'];
                $return->bio = $this->request->data['bio'];
            }
            if(isset($this->request->data['name'])) {
                $userObj->name = $this->request->data['name'];
                $return->name = $this->request->data['name'];
            }

            if(isset($this->request->data['company']) && !empty($this->request->data['company'])) {
                $this->loadModel('Company');
                $this->loadModel('Address');
                $this->loadModel('Gig');

                $company = trim($this->request->data['company']);
                $website = $this->request->data['company_website'];
                $company_address = $this->request->data['company_address'];
                $companyObj = $this->Company->findByName($company)->first();

                if (!empty($companyObj)) {
                    $addressId = $this->Gig->getAddress($company_address);
                    $companyCurrent = $this->Company->get($companyObj->id);
                    $companyCurrent->address_id = $addressId;
                    $companyCurrent->website = $website;
                    $this->Company->save($companyCurrent);
                    $company_id = $companyObj->id;
                }
                else {
                    $addressId = $this->Gig->getAddress($company_address);
                    $newCompany = $this->Company->newEntity();
                    $newCompany->name = $company;
                    $newCompany->address_id = $addressId;
                    $newCompany->website = $website;
                    $newCompany->created_by = $user_id;
                    $theCompany = $this->Company->save($newCompany);
                    $company_id = $theCompany->id;
                }

                $candidateObj->company_id = $company_id;
                $return->company = $company;
                $return->company_website = $website;
                $return->company_address = $company_address;
            }

            $this->Candidate->save($candidateObj);
            $this->User->save($userObj);

            if(isset($this->request->data['skills'])) {

                $skillList = $this->CandidateSkills->findByCandidateId($candidate->id)->toArray();

                if(!empty($skillList)) {
                    foreach($skillList as $list) {
                        $newSkill = $this->CandidateSkills->get($list->id);
                        $this->CandidateSkills->delete($newSkill);
                    }
                }

                $skills = $this->request->data['skills'];
                foreach($skills as $skill) {
                    if($skill['selected'] == true) {
                        $newSkill = $this->CandidateSkills->newEntity();
                        $newSkill->candidate_id = $candidate->id;
                        $newSkill->skills_id = $skill['skill_id'];
                        $this->CandidateSkills->save($newSkill);
                    }
                }
                $updateSkills = $this->User->getSkillList($candidate->id);
                $return->skills = $updateSkills;
            }

            if(isset($this->request->data['language'])) {
                $languages = $this->request->data['language'];
                foreach($languages as $language) {
                    if($language['id'] != 0) {
                        $newLang = $this->CandidateLanguages->get($language['id']);
                        $this->CandidateLanguages->delete($newLang);
                    }
                    if($language['selected'] == true) {
                        $newLang = $this->CandidateLanguages->newEntity();
                        $newLang->candidate_id = $candidate->id;
                        $newLang->languages_id = $language['language_id'];
                        $this->CandidateLanguages->save($newLang);
                    }
                }

                $updateLanguages = $this->User->getLanguagesList($candidate->id);
                $return->language = $updateLanguages;
            }

            if(isset($this->request->data['education'])) {
                $education = $this->request->data['education'];
                $this->CandidateEducation->deleteAll(['candidate_id' => $candidate->id ]);
                foreach($education as $edu) {
                    $eduObj = $this->CandidateEducation->newEntity();
                    $eduObj->candidate_id = $candidate->id;
                    $eduObj->school = $edu['title'];
                    $eduObj->type = $edu['type'];
                    $eduObj->subject = $edu['subject'];
                    $this->CandidateEducation->save($eduObj);
                }
                $education = $this->CandidateEducation->find()
                    ->select(['id', 'title' => 'school', 'type', 'subject'])
                    ->where(['candidate_id' => $candidate->id]);
                $return->education = $education;
            }

            if(isset($this->request->data['experiences'])) {
                $experience = $this->request->data['experiences'];
                $this->CandidateExperiences->deleteAll(['candidate_id' => $candidate->id ]);
                foreach($experience as $exp) {
                    $expObj = $this->CandidateExperiences->newEntity();
                    $expObj->candidate_id = $candidate->id;
                    $expObj->employer = $exp['title'];
                    $expObj->position = $exp['type'];

                    if(!empty($exp['start_date'])) {
                        $expObj->start_date = $exp['start_date'];
                    }
                    if(!empty($exp['end_date'])) {
                        if($exp['end_date'] == 'Present') {
                            $expObj->present = 1;
                        }
                        else {
                            $expObj->end_date = $exp['end_date'];
                        }
                    }

                    $this->CandidateExperiences->save($expObj);
                }
                $experiences = $this->CandidateExperiences->find()
                    ->select(['id', 'title' => 'employer', 'type' => 'position', 'start_date', 'end_date', 'present'])
                    ->where(['candidate_id' => $candidate->id]);

                foreach($experiences as $exp) {
                    if($exp->start_date) {
                        $exp->start_date = $exp->start_date->format("Y-m-d");
                    }
                    else {
                        $exp->start_date = "";
                    }

                    if($exp->present == 1) {
                        $exp->end_date = "Present";
                    }
                    else {
                        if ($exp->end_date) {
                            $exp->end_date = $exp->end_date->format("Y-m-d");
                        } else {
                            $exp->end_date = "";
                        }
                    }
                }
                $return->experiences = $experiences;
            }

            $recommendations = $this->User->getRecommendations($user_id, "ME");
            $return->recommendation_link = $recommendations['link'];
            $return->recommendations = $recommendations['recommendations'];
            $return->recommendations_count = $recommendations['count'];
            $return->app_mode_preference = $candidate->app_mode_preference;

            if(!empty($candidate->profile_link)) {
                $return->profile_link = $candidate->profile_link;
            } else {
                $return->profile_link = $this->User->getProfileBranchUrl($user_id);
            }

            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }


    /**
     * set app mode as Company or Gigster
     * 1=work, 2=hire
     */
    public function appMode() {

        header('Content-type: application/json');
        $this->request->allowMethod(['post']);
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $return = new \stdClass();

        if ($this->User->isLoggedIn($token, $user_id)) {
            $this->loadModel('Candidate');
            $this->loadModel('AppModeLog');

            $place = $this->request->data['place'];
            $app_mode = $this->request->data['app_mode'];

            $userObj = $this->Candidate->findByUserId($user_id)->first();
            $candidateObj = $this->Candidate->get($userObj->id);

            if ($place == 'wizzard') {
                $candidateObj->app_mode_preference = $app_mode;

                if($userObj->mail_send == 0) {
                    $this->sendMail($user_id, $app_mode);
                    $candidateObj->mail_send = 1;
                }

            } else {
                $candidateObj->app_mode = $app_mode;
            }
            $this->Candidate->save($candidateObj);

            $appModeLog = $this->AppModeLog->newEntity();
            $appModeLog->place = $place;
            $appModeLog->user_id = $user_id;
            $appModeLog->mode = $app_mode;
            $this->AppModeLog->save($appModeLog);

            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die;
    }

    /**
     *  generate ephemeral Keys for the iOS
     */
    public function ephemeralKeys() {

        $this->loadModel('StripeLog');
        $this->loadModel('Candidate');
        $user_id = $this->request->header('user_id');
        $candidateObj = $this->Candidate->findByUserId($user_id)
                                    ->contain(['User'])->first();

        if($candidateObj->stripe_id) {
            $customerId = $candidateObj->stripe_id;
        }
        else {
            $cardTokenArr = array("description" => $candidateObj->user->name,
                "email" => $candidateObj->user->email);
            $customerArr = $this->Stripe->createCustomer($cardTokenArr);
            $customerId = (isset($customerArr["response"]["id"]) ? $customerArr["response"]["id"] : "");
            $this->StripeLog->createLog($customerId, $user_id, json_encode($cardTokenArr), json_encode($customerArr), 'createCustomer');

            $candidate = $this->Candidate->get($candidateObj->id);
            $candidate->stripe_id = $customerId;
            $this->Candidate->save($candidate);
        }

        $api_version = Configure::read('stripe_api_version');
        $ephemeralKey = $this->Stripe->ephemeralKeys($api_version, $customerId);
        $this->StripeLog->createLog($customerId, $user_id, json_encode($customerId), $ephemeralKey, 'ephemeralKeys');

        echo  $ephemeralKey;
        die();
    }

    /**
     * save firebase token for push
     */
    public function fcmToken() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {

            $fcmToken = $this->request->data['fcm_token'];
            $userObj = $this->User->get($user_id);
            $userObj->fcm_token = $fcmToken;
            if($this->User->save($userObj)) {
                $return->status = 0;
            }
            else {
                $return->status = 1;
            }
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }

    /**
     * Use for manual password change.
     */
    public function testPasswordChange() {
        $password = "123456";
        $hasher = new DefaultPasswordHasher();
        $newPaaword = $hasher->hash($password);

        echo $newPaaword;
        die();
    }

    /**
     * cron job for user activation reminder
     * run in evey 4 hours
     */
    public function cronUserActivationReminder() {
        $this->loadModel('LogUserActivationReminder');
        $this->loadModel('PushNotification');
        $this->loadModel('Event');
        $this->loadModel('User');
        $this->loadModel('JobOffer');

        $now = date('Y-m-d H:i:s');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SET @time_stamp = "'. $now .'"; CALL usp_update_user_activation_reminders(@time_stamp);');
        $stmt->execute();
        $stmt->closeCursor();

        $remenders = $this->LogUserActivationReminder->find('all')
            ->contain(['User'])
            ->where(['LogUserActivationReminder.created' => $now, 'LogUserActivationReminder.push_status' => 0]);

        foreach ($remenders as $remender) {
            $firstName = strtok($remender->user->name, " ");
            switch ($remender->type) {
                case 1:
                    $payload = 'Hi '. $firstName .', Your next venture is just a click away - apply to your first gig today.';
                    $notification = 'Not applied to job in 2 days';
                    $redirect = 'GIG_LIST';
                    break;
                case 2:
                    $payload = 'Hi '. $firstName .', long time no gig? We miss you. ';
                    $notification = 'Not applied to job in 10 days';
                    $redirect = 'GIG_LIST';
                    break;
                case 3:
                    $payload = 'Hi '. $firstName .', check out our gigs and apply to your first gig today.';
                    $notification = 'Not applied to job in 20 days';
                    $redirect = 'GIG_LIST';
                    break;
                case 4:
                    $payload = 'Hi '. $firstName .', post your first ad for free at Gigstr today.';
                    $notification = 'Posted job in 2 days';
                    $redirect = 'MY_GIGS';
                    break;
                case 5:
                    $payload = 'Hi '. $firstName .', long time no gig? We miss you.';
                    $notification = 'Posted job in 10 days';
                    $redirect = 'MY_GIGS';
                    break;
                case 6:
                    $payload = 'Hi '. $firstName .', log in to the Gigstr app to post ads for free!';
                    $notification = 'Posted job in 20 days';
                    $redirect = 'MY_GIGS';
                    break;
                default :
                    break;
            }

            if($remender->app_mode_preference == 1) {
                $mode = 'GIGSTR';
            } else {
                $mode = 'COMPANY';
            }

            $key_value = json_encode([]);
            $badgeCount = $this->JobOffer->badgeCount($remender->user_id);
            $badges = $badgeCount->total;

            $this->Event->eventSave($payload, $remender->user_id, $mode, $redirect, $key_value, 0);

            if(!empty($remender->user->fcm_token)) {
                $this->PushNotification->sendPushEvent($payload, $remender->user->fcm_token, $remender->user_id, $mode, $redirect, $notification, $badges, $key_value, "");

                $remenderObj = $this->LogUserActivationReminder->get($remender->id);
                $remenderObj->push_status = 1;
                $this->LogUserActivationReminder->save($remenderObj);
            }
        }

        die();
    }

    /**
     *Remove candidate from list of applicants (
     */
    public function removeApplicant() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {

            //$companyUser = $this->User->get($user_id);
            $applicant = $this->request->data['applicant'];
            $gig = $this->request->data['gig'];

            $this->loadModel('Event');
            $this->loadModel('JobOffer');
            $this->loadModel('User');
            $this->loadModel('Gig');
            $this->loadModel('PushNotification');
            $this->loadModel('GigCandidate');

            $applicantObj = $this->GigCandidate->find('all')
                ->where(['gig_id' => $gig, 'user_id' => $applicant])->first();
            $gigApplicant = $this->GigCandidate->get($applicantObj->id);
            $gigApplicant->selection = 'REJECTED';
            $gigApplicant->selection_date = date('Y-m-d H:i:s');
            $this->GigCandidate->save($gigApplicant);

            $userObj = $this->User->get($applicant);
            $fcm_token = $userObj->fcm_token;

            $gigObj = $this->Gig->findById($gig)
                ->contain(['Skills'])->first();
            $title = (!empty($gigObj->title) ? $gigObj->title : $gigObj->skill->skill);
            $payload = 'Sorry, your application for '. $title .' was not accepted this time. Either the job has already been filled or your profile did not quite match the company\'s requirements. Don\'t worry! Keep an eye out for more gigs and we\'ll get you out gigging soon.';
            $key_value = json_encode([]);

            $this->Event->eventSave($payload, $applicant, 'GIGSTR', 'GIG_LIST', $key_value, 0);

            if (!empty($fcm_token)) {
                $badgeCount = $this->JobOffer->badgeCount($applicant);
                $this->PushNotification->sendPushEvent($payload, $fcm_token, $applicant, 'GIGSTR', 'GIG_LIST',
                    'Declined', $badgeCount->total, $key_value);
            }

            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }

    /**
     *Keep track of when a Gigstr has been viewed by use
     */
    public function gigstrSeen() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {
            $gigstr = $this->request->data['gigstr'];
            $this->loadModel('BrowseGigstrsSeen');

            $seenObj = $this->BrowseGigstrsSeen->find('all')->where(['user_id' => $user_id, 'gigstr' => $gigstr])->first();
            if($seenObj) {
                $this->BrowseGigstrsSeen->delete($seenObj);
            }

            $seen = $this->BrowseGigstrsSeen->newEntity();
            $seen->user_id = $user_id;
            $seen->gigstr = $gigstr;
            $this->BrowseGigstrsSeen->save($seen);

            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }

    /**
     *Browse gigstrs search
     */
    public function search() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {

            $skills = $this->request->data['skills'];
            $city = $this->request->data['city'];
            $offset = $this->request->data['page'];
            $round = $this->request->data['round'];
            $this->loadModel('Candidate');
            $this->loadModel('CandidateSkills');
            $this->loadModel('BrowseGigstrsSeen');

            $limit = 15;

            if($skills['id'] == 0) {

                $candidateObj = $this->Candidate->find('all')
                    ->select(['user_id', 'direct_hit' => 'IF(Candidate.city= "'. $city['title'] .'", 1,0)'])
                    ->contain(['User'])
                    ->where(['Candidate.area LIKE' => $city['area'], 'User.is_delete' => 0, 'Candidate.app_mode_preference' => 1, 'Candidate.is_verified' => 1])
                    ->order(['direct_hit' => 'DESC', 'Candidate.created' => 'DESC'])
                    ->limit($limit)
                    ->page($offset);

                $total = $candidateObj->count();
                $candidateObjArr =  $candidateObj->toArray();

            }
            else {
                $candidateSkillsObj = $this->CandidateSkills->find('all')
                    ->select(['user_id' => 'Candidate.user_id', 'direct_hit' => 'IF(Candidate.city= "'. $city['title']
                        .'", 1,0)'])
                    ->contain(['Candidate',
                        'Candidate.User' => function ($q) {
                            return $q->where(['User.is_delete' => 0]);
                        }])
                    ->where(['CandidateSkills.skills_id' => $skills['id'], 'Candidate.area LIKE' => $city['area'], 'Candidate.app_mode_preference' => 1, 'Candidate.is_verified' => 1])
                    ->order(['direct_hit' => 'DESC', 'Candidate.created' => 'DESC'])
                    ->limit($limit)
                    ->page($offset);

                $total = $candidateSkillsObj->count();

                if($total < 15) {

                    $candidateObj = $this->Candidate->find('all')
                        ->select(['user_id', 'direct_hit' => 'IF(Candidate.city= "'. $city['title'] .'", 1,0)'])
                        ->contain(['User'])
                        ->where(['Candidate.area LIKE' => $city['area'], 'User.is_delete' => 0, 'Candidate.app_mode_preference' => 1, 'Candidate.is_verified' => 1])
                        ->order(['direct_hit' => 'DESC', 'Candidate.created' => 'DESC'])
                        ->limit($limit)
                        ->page($offset);

                    $total = $candidateObj->count();
                    $candidateObjArr =  array_unique(array_merge($candidateSkillsObj->toArray(), $candidateObj->toArray()));
                }
                else {
                    $candidateObjArr = $candidateSkillsObj->toArray();
                }
            }

            if($round == 1) {
                shuffle($candidateObjArr);
            }
            else {
                $this->loadModel('BrowseGigstrsSeen');
                $seenArr = $this->BrowseGigstrsSeen->findByUserId($user_id)->order(['created' => 'ASC'])->toArray();
                if($seenArr) {
                    foreach ($candidateObjArr as $idx => $candidate) {
                        foreach ($seenArr as $seen) {
                            if($candidate['user_id'] == $seen['gigstr']) {
                                $temp = $candidateObjArr[$idx];
                                unset($candidateObjArr[$idx]);
                                array_push($candidateObjArr, $temp);
                            }
                        }
                    }

                }
            }

            //$total = $candidateObj->count();
            $pageCount = ceil($total/$limit);
            if($pageCount > $offset) {
                $next = $offset + 1;
            } else  {
                $next = 1;
                $round = 1;
            }

            $userArr = [];
            foreach ($candidateObjArr as $candidate) {
                if($candidate['user_id'] != $user_id) {
                    $userObj = $this->User->profileObj($candidate['user_id'], $user_id);
                    array_push($userArr, $userObj);
                }
            }

            $return->round = $round;
            $return->next = $next;
            $return->status = 0;
            $return->profiles = $userArr;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }

    /**
     * Gigster serch city and skills list
     */
    public function browseDetails() {

        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $country = $this->request->header('X-Country');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {

            $this->loadModel('City');
            $this->loadModel('Skills');
            $this->loadModel('GigstrSearchLog');

            $skillList = $this->Skills->find('all')->where(['active' => 1, 'skill !=' => 'Other'])
                ->order(['skill' => 'ASC']);

            $cityList = $this->City->find('all')->where(['active' => 1])->order(['city_name' => 'ASC']);
            $lastSearch = $this->GigstrSearchLog->findByUserId($user_id)->last();

            $skillArr = [];
            $firstArr['id'] = 0;
            $firstArr['title'] = 'All Categories';
            $firstArr['selected'] = (($lastSearch && $lastSearch->skill == 0) ? true : false);
            array_push($skillArr, $firstArr);

            foreach ($skillList as $skill) {
                $temp = [];
                $temp['id'] = $skill->id;
                $temp['title'] = $skill->skill;
                $temp['selected'] = ($lastSearch->skill == $skill->id ? true : false);
                array_push($skillArr, $temp);
            }

            $cityArr = [];
            foreach ($cityList as $city) {
                $temp = [];
                $temp['id'] = $city->id;
                $temp['title'] = $city->city_name;
                $temp['area'] = $city->area;
                $temp['country_code'] = $city->country_code;
                $temp['selected'] = ($lastSearch->city_id == $city->id ? true : false);
                array_push($cityArr, $temp);
            }

            $return->status = 0;
            $return->skills = $skillArr;
            $return->city = $cityArr;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }

    /**
     * create new chat
     */
    public function setChat() {

        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $language = $this->request->header('X-Language');
        $return = new \stdClass();
        $return->status = 0;

        if($this->User->isLoggedIn($token, $user_id)) {

            $this->loadModel('Candidate');
            $companyUser = $this->Candidate->findByUserId($user_id)->contain(['User'])->first();

            if($companyUser->company_id == 0) {
                $return->wizard_enable = true;

                $return->wizardArr[] = array(
                    'wizard_id' => "w_company_name",
                    'wizard_key' => "",
                    'wizard_data' => ""
                );
                $return->profile = $this->User->profileObj($user_id);
            }
            else {

                $return->wizard_enable = false;
                $gigstr = $this->request->data['gigstr'];
                $chatId = $this->User->getChatGroup($user_id, $gigstr);
                $userObj = $this->User->findById($gigstr)->first();
                $return->name = $userObj->name;
                $return->user_image = $this->getUserImage($gigstr);

                $gigstrName = strtok($userObj->name, " ");
                $compUserName = strtok($companyUser->user->name, " ");
                $this->loadModel('Company');
                $compObj = $this->Company->findById($companyUser->company_id)->first();

                switch($language) {
                    case 'sv' :
                        $message = "Hej ". $gigstrName .", Jag skulle vilja kontakta dig gällande ett gig. Är du intresserad? Vänligen, ". $compUserName ." ". $compObj->name;
                        break;
                    case 'da' :
                        $message = "Hej ". $gigstrName .", Jeg vil gerne kontakte dig om et job. Er du interesseret? Med venlig hilsen ". $compUserName ." ". $compObj->name;
                        break;
                    case 'nb' :
                        $message = "Hei ". $gigstrName .", jeg vil gjerne kontakte deg angående et gig. Er du interessert? Med vennlig hilsen, ". $compUserName ." ". $compObj->name;
                        break;
                    default :
                        $message = "Hi ". $gigstrName .", I would like to contact you about a gig. Are you interested? Best regards ". $compUserName ." ". $compObj->name;
                        break;
                }

                if ($chatId < 0) {
                    $this->loadModel('Chat');
                    $this->loadModel('ChatGroup');

                    $return->chat = $this->Chat->setChatGroup($gigstr, $user_id);
                    $return->message = $message;
                } else {
                    $return->chat = $chatId;
                    $return->message = "";
                }
            }
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();

    }

    /**
     *
     */
    public function recommendationVisibility() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {
            $this->loadModel('Recommendation');

            $recommendation_id = $this->request->data['recommendation_id'];
            $recommendationObj = $this->Recommendation->get($recommendation_id);

            if($recommendationObj->visible == 0) {
                $recommendationObj->visible = 1;
            } else {
                $recommendationObj->visible = 0;
            }
            $this->Recommendation->save($recommendationObj);
            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }

    /**
     *
     */
    public function setUsersTopics() {

        $this->loadModel('Candidate');
        $this->loadModel('TopicSubscription');

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare(
            'SELECT 
                Candidate.id AS `candidate_id`, 
                Candidate.user_id AS `user_id`, 
                Candidate.modified AS `modified`, 
                Candidate.topic_update AS `topic_update`, 
                Candidate.app_mode AS `app_mode`, 
                Candidate.app_mode_preference AS `app_mode_preference`, 
                Candidate.country AS `country`, 
                Candidate.area AS `area`, 
                User.fcm_token AS `fcm_token`, 
                User.is_delete AS `is_delete` 
            FROM candidate Candidate 
                INNER JOIN user User ON User.id = (Candidate.user_id) 
            WHERE 
                (
                    User.is_delete = 0 
                    AND User.fcm_token != ""
                    AND (
                        Candidate.topic_update = "0000-00-00 00:00:00" 
                        OR Candidate.topic_update < Candidate.modified
                    )
                ) 
            ORDER BY Candidate.id DESC  
            LIMIT 40'
        );
        $stmt->execute();
        $users = $stmt->fetchAll('assoc');

        foreach ($users as $user) {
            $temp = [];
            $temp['user_id'] = $user["user_id"];
            $temp['fcm_token'] = $user["fcm_token"];

            if($user["app_mode_preference"] == "2") {
                   $mode = 'COMPANY';
            }
            else {
               $mode = 'GIGSTR';
            }

            if(!empty($user["country"])) {
                $temp['topic'][0] = $mode . '_' . $user["country"];
            }
            if(!empty($user["area"])) {
                $eng_Topic = $this->User->convertSwedishLetters($user["area"]);
                $temp['topic'][1] = $mode . '_' . strtoupper(str_replace(' ','', $eng_Topic));
            }

            $localSubscriptions = $this->TopicSubscription->findByUserId($user["user_id"]);

            if($localSubscriptions->count() == 0) {
                $this->userSubscribeToTopic($user["user_id"], $user["fcm_token"], $temp['topic']);
            }
            else {
                $this->userUnsubscribeToTopic($user["user_id"], $user["fcm_token"], $temp['topic'], $localSubscriptions);

            }

            $candidateObj = $this->Candidate->get($user["candidate_id"]);
            $candidateObj->topic_update = date('Y-m-d H:i:s');
            $this->Candidate->save($candidateObj);
        }

        die();
    }

    /**
     * @param $user_id
     * @param $fcm
     * @param $topics
     * @param string $place
     * @return bool
     */
    private function userSubscribeToTopic($user_id, $fcm, $topics) {
        $this->loadModel('TopicSubscription');

        foreach ($topics as $key => $topic) {
            $subscribe = $this->User->subscribeToTopic($fcm, $topic);

            $subsObj = $this->TopicSubscription->newEntity();
            $subsObj->user_id = $user_id;
            $subsObj->topic = $topic;
            $subsObj->user_fcm = $fcm;
            $subsObj->google_responce = json_encode($subscribe);
            $this->TopicSubscription->save($subsObj);
        }
        return true;
    }

    /**
     * @param $user_id
     * @param $fcm
     * @param $topics
     * @param $localSubscriptions
     * @return bool
     */
    private function userUnsubscribeToTopic($user_id, $fcm, $topics, $localSubscriptions) {

        $this->loadModel('TopicSubscription');

        foreach ($localSubscriptions as $topic) {
            $this->User->unsubscribeToTopic($fcm, $topic->topic);

            $this->TopicSubscription->deleteAll(['user_id' => $user_id, 'topic' => $topic->topic]);

        }

        $subscribe = $this->userSubscribeToTopic($user_id, $fcm, $topics);
        return $subscribe;
    }

    /**
     * Send welcome mail
     * @param $userId
     * @param $app_mode
     * @return bool
     */
    private function sendMail($userId, $app_mode) {

        $userObj = $this->User->findById($userId)->contain(['Candidate'])->first();
        $emailAddress = $userObj->email;
        $name = strtok($userObj->name, " ");
        $base_url = Configure::read('dev_base_url');
        $appMode = $userObj->candidate->app_mode;

        if($app_mode == 1) {
            $template = 'welcomeCompany';
        }else {
            $template = 'welcome';
        }

        $email = new Email('default');
        $email->template($template, 'default')
            ->emailFormat('html')
            ->viewVars(['base_url' => $base_url, 'name' => $name])
            ->from([Configure::read('from_email') => Configure::read('from_site')])
            ->to($emailAddress)
            ->subject('Welcome to Gigstr')
            ->send();

        return true;
    }


    /** save fb work experience data
     * @param $device_type
     * @param $experiences
     * @param $candidateId
     * @return bool
     */
    private function saveExperiences($device_type, $experiences, $candidateId) {
        $this->loadModel('CandidateExperiences');
        if($device_type == 1) {
            $experiences = json_decode($experiences,true);
        }
        $experiencesObj = $this->CandidateExperiences
            ->findByCandidateId($candidateId)
            ->first();

        if(!$experiencesObj) {
            foreach ($experiences as $experience) {
                $newJob = $this->CandidateExperiences->newEntity();
                $newJob->candidate_id = $candidateId;
                $newJob->employer = $experience['employer']['name'];
                if (isset($experience['position']['name'])) {
                    $newJob->position = $experience['position']['name'];
                }
                if (isset($experience['start_date'])) {
                    $newJob->start_date = $experience['start_date'];
                }

                if (isset($experience['end_date'])) {
                    $newJob->end_date = $experience['end_date'];
                }
                else {
                    $newJob->present = 1;
                }

                $this->CandidateExperiences->save($newJob);
            }
        }

        return true;
    }

    /**
     * @param $device_type
     * @param $education
     * @param $candidateId
     * @return bool
     */
    private function saveEducation($device_type, $education, $candidateId) {

        $this->loadModel('CandidateEducation');
        if($device_type == '1') {
            $education = json_decode($education,true);
        }
        $educationObj = $this->CandidateEducation
            ->findByCandidateId($candidateId)
            ->first();
        if(!$educationObj) {
            foreach ($education as $edu) {
                $newEdu = $this->CandidateEducation->newEntity();
                $newEdu->candidate_id = $candidateId;
                $newEdu->school = $edu['school']['name'];
                $newEdu->type = $edu['type'];
                $newEdu->subject = "";
                $this->CandidateEducation->save($newEdu);
            }
        }
        return true;
    }

    /**
     * @param $device_type
     * @param $language
     * @param $candidateId
     */
    private function saveLanguages($device_type, $language, $candidateId) {
        $this->loadModel('CandidateLanguages');
        $this->loadModel('Languages');

        if($device_type == 1) {
            $language = json_decode($language,true);
        }

        $languageObj = $this->CandidateLanguages
            ->findByCandidateId($candidateId)
            ->first();

        if(!$languageObj) {
            foreach ($language as $lang) {
                $userLang = trim($lang['name']);
                $clearLang = str_replace('-', " ", str_replace(',', " ", $userLang));
                $langArr = explode(" ", $clearLang);
                $fbLang = $this->Languages->findByLanguage($langArr[0])->first();

                if ($fbLang) {
                    $newLang = $this->CandidateLanguages->newEntity();
                    $newLang->candidate_id = $candidateId;
                    $newLang->languages_id = $fbLang->id;
                    $this->CandidateLanguages->save($newLang);
                }
            }
        }
    }


}
