<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Feed Controller
 *
 * @property \App\Model\Table\FeedTable $Feed
 *
 * @method \App\Model\Entity\Feed[] paginate($object = null, array $settings = [])
 */
class FeedController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {
        header('Content-type: application/json');

        if ($this->request->is('post')) {

            $this->loadModel('FeedItem');
            $section =  $this->request->data['section'];
            $feedObj = new \stdClass();
            $base_url =  Configure::read('dev_base_url');

            $feeds = $this->Feed->find()
                ->where(['is_delete' => 0, 'section' => $section])
                ->order(['feed_order' => 'DESC'])
                ->all();

            foreach($feeds as $value) {
                $data = new \stdClass();
                $data->id = $value->id;
                $data->section = $value->section;
                $data->component = $value->ui_type;
                $data->title = $value->title;

                if($value->ui_type == 'carousel') {
                    $feedItems = $this->FeedItem->find()
                        ->where(['feed_id' => $value->id, 'is_delete' => 0])
                        ->order(['item_order' => 'DESC'])->all();

                    foreach($feedItems as $item) {
                        $items = new \stdClass();
                        $items->id = $item->id;
                        $items->title = $item->title;
                        $items->description = $item->description;
                        $items->action = $item->link_type;
                        $items->image = $base_url . 'img/feed_images/' . $item->image;
                        if($item->link_type == 'youtube') {
                            parse_str( parse_url($item->url, PHP_URL_QUERY ), $youtube);
                            $items->url = $youtube['v'];
                        }
                        else {
                            $items->url = $item->url;
                        }
                        $data->items[] = $items;
                    }
                }
                else {
                    $data->action = $value->url_action;
                    if($value->url_action == 'youtube') {
                        parse_str( parse_url( $value->url, PHP_URL_QUERY ), $youtube);
                        $data->url = $youtube['v'];
                    }
                    else {
                        $data->url = $value->url;
                    }
                }
                $feedObj->feed[] = $data;
            }

            $this->set('_serialize', ['feedObj']);
            echo json_encode($feedObj);
            die();
        }
    }

    /**
     * View method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $feed = $this->Feed->get($id, [
            'contain' => []
        ]);

        $this->set('feed', $feed);
        $this->set('_serialize', ['feed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $feed = $this->Feed->newEntity();
        if ($this->request->is('post')) {
            $feed = $this->Feed->patchEntity($feed, $this->request->getData());
            if ($this->Feed->save($feed)) {
                $this->Flash->success(__('The feed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feed could not be saved. Please, try again.'));
        }
        $this->set(compact('feed'));
        $this->set('_serialize', ['feed']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $feed = $this->Feed->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $feed = $this->Feed->patchEntity($feed, $this->request->getData());
            if ($this->Feed->save($feed)) {
                $this->Flash->success(__('The feed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feed could not be saved. Please, try again.'));
        }
        $this->set(compact('feed'));
        $this->set('_serialize', ['feed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $feed = $this->Feed->get($id);
        if ($this->Feed->delete($feed)) {
            $this->Flash->success(__('The feed has been deleted.'));
        } else {
            $this->Flash->error(__('The feed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

}
