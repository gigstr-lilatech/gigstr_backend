<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\User;
use Cake\Core\App;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use DateTime;
use DateTimeZone;
use Exception;


/**
 * Gig Controller
 *
 * @property \App\Model\Table\GigTable $Gig
 */
class GigController extends AppController {

    private $welcome = 'Oops! Your Gigstr app seems to be out of date. Simply update to the new app in your app store today to apply for gigs and to get hired. Simple as that.';
     //   'Hello and welcome to Gigstr. Thank you for applying to this gig. We have now received your application and will provide you with more info if we select you for the gig. Best regards //Team Gigstr';

    private $welcome_sv = 'Hej och välkommen till Gigstr. Vad kul att du sökt detta gig! Vi har nu tagit emot din ansökan och återkommer om du blivit utvald till gigget. Allt gott //Team Gigstr';

    private $welcome_nb = 'Hei og velkommen til Gigstr. Takk for at du har søkt på dette gigget. Vi har nå fått din søknad og om du blir valgt ut til dette gigget vil du motta mer informasjon. Med venlig hilsen //Team Gigstr';

    private $welcome_da = 'Hej og velkommen til Gigstr. Tak fordi du har søgt dette gig. Vi har modtaget din ansøgning og vil kontakte dig med nærmere info hvis du bliver udvalgt. Med venlig hilsen // Team Gigstr';

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Stripe');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Company'],
            'conditions' => ['Gig.status' => '1', 'Gig.active' => '1']
        ];
        $this->set('gig', $this->paginate($this->Gig));
        $this->set('_serialize', ['gig']);
    }

    /**
     * listAll method
     * Access method - gig/list_all
     * @return json encoded object array containing information about the gigs which are active in the system
     * @internal param null|string $user_id User id.
     */
    public function listAll() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $country = $this->request->header('X-Country');

            if ($this->request->data['user_id'])
                $user_id = $this->request->data['user_id'];
            else
                $user_id = 0;

            if($country) {
                $this->paginate = [
                    'contain' => ['Company', 'User', 'Application'],
                    'conditions' => ['Gig.status' => '1', 'Gig.active' => '1', 'Gig.country' => $country, 'Gig.is_diy' => 0],
                    'order' => [
                        'modified' => 'DESC'
                    ],
                    //'conditions' => ['Application.user_id' => $user_id],
                    'page' => 1,
                    'limit' => 1000,
                    //'maxLimit' => 100,
                    'fields' => [
                        'id',
                        'title',
                        'sub_title1',
                        'sub_title2',
                        'gig_image',
                        'modified'
                    ],
                    'sortWhitelist' => [
                        'id',
                        'title',
                        'sub_title1',
                        'sub_title2',
                        'gig_image',
                        'modified'
                    ]
                ];
            }
            else {
                $this->paginate = [
                    'contain' => ['Company', 'User', 'Application'],
                    'conditions' => ['Gig.status' => '1', 'Gig.active' => '1', 'Gig.is_diy' => 0],
                    'order' => [
                        'modified' => 'DESC'
                    ],
                    //'conditions' => ['Application.user_id' => $user_id],
                    'page' => 1,
                    'limit' => 1000,
                    //'maxLimit' => 100,
                    'fields' => [
                        'id',
                        'title',
                        'sub_title1',
                        'sub_title2',
                        'gig_image',
                        'modified'
                    ],
                    'sortWhitelist' => [
                        'id',
                        'title',
                        'sub_title1',
                        'sub_title2',
                        'gig_image',
                        'modified'
                    ]
                ];
            }

            $this->set('data', $this->paginate($this->Gig));
            $results = $this->paginate($this->Gig);
            $data = new \stdClass();
            foreach ($results AS $gig) {
                $sub_data = new \stdClass();
                $sub_data->id = $gig->id;
                $sub_data->title = $gig->title;
                $sub_data->sub_title1 = $gig->sub_title1;
                $sub_data->sub_title2 = $gig->sub_title2;
                $sub_data->gig_image = Configure::read('dev_base_url') . 'img/gig_images/thumblist-' . $gig->gig_image;
                $sub_data->applied = 0;
                $sub_data->modified = $gig->modified;

                if ($user_id > 0) {
                    foreach ($gig['application'] AS $app) {
                        if ($app['user_id'] == $user_id) {
                            $sub_data->applied = '1';
                        }
                    }
                }

                $data->jobs[] = $sub_data;
            }

            $return = new \stdClass();
            $return->status = 0;
            if(is_null($data->jobs)) {
                $data->jobs = [];
            }
            $return->jobs = $data->jobs;
            $this->set('return', $return);
            $this->set('_serialize', ['return', 'data']);
            echo json_encode($return);
            die();
        }
    }


    /**
     * listGig method
     * Access method - gig/list_gig
     * @return json encoded object array containing information about the gig for the id
     * @internal param null|string $gig_id Gig id.
     * @internal param null|string $user_id User id.
     */

    public function listGig() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $gig_id = $this->request->data['gig_id'];

            if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
                $user_id = $this->request->data['user_id'];
            } else {
                $user_id = 0;
            }

            $checkGig = true;
            try {
                $gig = $this->Gig->get($gig_id);
            }
            catch(Exception $e) {
                $checkGig = false;
            }

            $chat_id = 0;
            if ($checkGig) {
                $this->loadModel('Candidate');
                $gig->gig_image = Configure::read('dev_base_url') . 'img/gig_images/thumblist-' . $gig->gig_image;
                $gig->applied = '0';
                $gig->description = $gig->description;
                $base_url = Configure::read('dev_base_url');

                $gig->description_link = $base_url . 'gig/view_gig/' . $gig_id;

                $this->loadModel('Application');
                $applications = $this->Application->find('all')->where(['gig_id' => $gig_id]);

                //Get the user image to pass for the chat screen
                $conn = ConnectionManager::get('default');
                $stmt_user_image = $conn->prepare(
                        'SELECT
                          image FROM candidate WHERE user_id = ' . $user_id . ''
                );

                $stmt_user_image->execute();
                $user_image = $stmt_user_image->fetch('assoc');

                //$user_image_default = Configure::read('dev_base_url') . 'img/profile_placeholder.png';
                $user_image_default = "";
                //Before returning the image check if the image is a Facebook image.
                if ($user_image['image'] != NULL && $user_image['image'] != '') {
                    if (preg_match('/facebook/', $user_image['image'])) {
                        //If it is a facebook image then return the URL as it is
                        $gig->user_image = $user_image['image'];
                    } else {
                        //If it is a custom image uploaded by the user then return with the complete path
                        $gig->user_image = Configure::read('dev_base_url') . 'img/user_images/' . $user_image['image'];
                    }
                } else {
                    $gig->user_image = $user_image_default;
                }
                if ($user_id > 0) {
                    foreach ($applications AS $app) {
                        if ($app->user_id == $user_id) {
                            $gig->applied = '1';
                            $chat_id = $app->chat_id;
                        }
                    }
                }

                $this->set('data', $gig);
                $return = new \stdClass();
                $return->status = 0;
                $return->job = $gig;
                $return->chat_id = $chat_id;
                $this->set('return', $return);
                $this->set('_serialize', ['return', 'data']);
            } else {
                $return = array('status' => 201);
                $return->message = "Incorrect job id";
            }
            echo json_encode($return);
            die();
        }
    }


    /**
     * Apply method
     * Access method - gig/apply
     * @return json encoded object array containing status, chat_id related to the application
     * @internal param null|string $gig_id Gig id.
     * @internal param null|string $user_id User id.
     */
    public function apply() {
        header('Content-type: application/json');
        $this->loadModel('Application');
        $this->loadModel('Gig');
        $this->loadModel('Chat');
        $this->loadModel('ChatGroup');
        $this->loadModel('Message');
        $this->loadModel('UnreadMessage');
        
        $user_id = $this->request->data['user_id'];
        $token = $this->request->data['token'];
        $language = $this->request->header('X-Language');
        
        if ($token != NULL && $user_id != NULL) {
            $this->loadModel('User');
            if ($this->User->isLoggedIn($token, $user_id)) {
            if ($this->request->is('post')) {
                $return = new \stdClass();
                $applicant = $this->Application->newEntity();

                $applicant->gig_id = $this->request->data['gig_id'];
                $user_id = $applicant->user_id = $this->request->data['user_id'];
                $applicant->status = 1;

                //Insert entry to chat table
                $chatEntry = $this->Chat->newEntity();
                $chatEntry->created_by = 1;
                if ($this->Application->save($applicant)) {

                    $application_id = $applicant->id;

                    $this->Chat->save($chatEntry);
                    $chat_id = $chatEntry->id;
                    $query = $this->Application->query();
                    $query->update()
                            ->set(['chat_id' => $chat_id])
                            ->where(['id' => $applicant->id])
                            ->execute();

                    //Insert an entry to the Chat group table with the user and admin ids
                    $chatgroup = $this->ChatGroup->newEntity();
                    $chatgroup->chat_id = $chat_id;
                    $chatgroup->user_id = $user_id;
                    $chatgroup->status = 1;
                    $this->ChatGroup->save($chatgroup);

                    $chatgroup2 = $this->ChatGroup->newEntity();
                    $chatgroup2->chat_id = $chat_id;
                    $chatgroup2->user_id = Configure::read('admin_id');
                    $chatgroup->status = 1;
                    $this->ChatGroup->save($chatgroup2);

                    //Insert entry to the messages table
                    /*if(!empty($language))
                        if($language == 'sv' || $language == 'da' || $language == 'nb') {
                            $msg_config = 'job_application_message_' . $language;
                        }
                        else {
                            $msg_config = 'job_application_message';
                        }
                    else
                        $msg_config = 'job_application_message';*/

                    /*if(!empty($language)) {
                        switch($language) {
                            case 'sv':
                                $msg_config = $this->welcome_sv;
                                break;
                            case 'da':
                                $msg_config = $this->welcome_da;
                                break;
                            case 'nb':
                                $msg_config = $this->welcome_nb;
                                break;
                            default:
                                $msg_config = $this->welcome;
                                break;
                        }
                    } else {
                        $msg_config = $this->welcome;
                    }*/

                    $message = $this->Message->newEntity();
                    $message->message = base64_encode('Oops! Your Gigstr app seems to be out of date. Simply update to the new app in your app store today to apply for gigs and to get hired. Simple as that.');
                    $message->created_by = Configure::read('admin_id');
                    $message->chat_id = $chat_id;
                    $this->Message->save($message);
                    $message_id = $message->id;

                    //Insert entry to the Unread Messages table
                    $unread_message = $this->UnreadMessage->newEntity();
                    $unread_message->message_id = $message_id;
                    $unread_message->user_id = $user_id;
                    $this->UnreadMessage->save($unread_message);

                    //Update the chat id to the application table
                    $query = $this->Application->query();
                    $query->update()
                            ->set(['chat_id' => $chat_id])
                            ->where(['id' => $application_id])
                            ->execute();

                    //Update the Gig table with the modified time
                    //This was removed as per the client request on 18th March 2016
    //                $query = $this->Gig->query();
    //                $query->update()
    //                        ->set(['modified' => date('Y-m-d H:m:s')])
    //                        ->where(['id' => $applicant->gig_id])
    //                        ->execute();

                    //Insert entry to the admin notification table to notify the admin about the new message
                    $conn = ConnectionManager::get('default');
                    $stmt = $conn->prepare(
                            "INSERT INTO admin_notification (gig_id,chat_id,type) VALUES ($applicant->gig_id,$chat_id,1)"
                    );
                    $stmt->execute();

                    $return->status = 0;
                    $return->chat_id = $chat_id;
                    $this->set('return', $return);
                    $this->set('_serialize', $return);
                } else {
                    $return->status = 202;
                    $return->message = "Unable to apply now. Please try again.";
                    $this->set('return', $return);
                    $this->set('_serialize', $return);
                }
                echo json_encode($return);
                die();
                }
            }
            else
            {
               $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
               echo json_encode($return);
               die();
            }
        }
        else
        {
            $return = array('status' => 401, 'status_message' => 'error', 'message' => 'Parameter mismatch');
            echo json_encode($return);
            die();
        }
    }

    /**
     * listAllPagination method
     * Access method - gig/list_all_pagination
     * Not used in anywhere. This was written as the request from Hasitha
     * @return json encoded object array with pagination containing information about the gigs which are active in the system
     * @internal param null|string $user_id User id.
     */
    public function listAllPagination() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $current_page = ($this->request->data['page'] != "") ? $this->request->data['page'] : 1;
            $this->paginate = ['page' => $page,
                'limit' => Configure::read('gigs_list_per_page'),
                'maxLimit' => 100,
                'fields' => [
                    'id', 'title', 'sub_title1', 'sub_title2', 'gig_image'
                ],
                'sortWhitelist' => [
                    'id', 'title', 'sub_title1', 'sub_title2', 'gig_image'
            ]];
            $this->set('data', $this->paginate($this->Gig));
            $data = $this->paginate($this->Gig);
            $return = new \stdClass();
            $return->status = 0;
            $return->jobs = $data;
            $return->page = $current_page;
            $return->total_pages = (int) ($this->Gig->find('all')->count() / Configure::read('gigs_list_per_page'));
            $this->set('return', $return);
            $this->set('_serialize', ['return', 'data']);
            echo json_encode($return);
            die();
        }
    }

    public function viewGig($id = null) {
        $this->viewBuilder()->layout('blank');
        $gig = $this->Gig->get($id);
        $this->set('gig', $gig);
        $this->set('_serialize', ['data']);
    }



    /**
     **********************************************************************************************
     * V2 end points
     * ********************************************************************************************
     */


    /** Gig detail view - listGig V2 endpoint */
    public function listGigNew() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $gig_id = $this->request->data['gig_id'];
            if (isset($this->request->data['user_id']) && !empty($this->request->data['user_id'])) {
                $user_id = $this->request->data['user_id'];
            } else {
                $user_id = 0;
            }

            $this->loadModel('Candidate');
            $this->loadModel('Application');
            $base_url = Configure::read('dev_base_url');
            $gig = new \stdClass();

            $chat_id = 0;
            $gig->description_link = $base_url . 'gig/view_gig/' . $gig_id;
            $applications = $this->Application->find('all')->where(['gig_id' => $gig_id]);

            $gig->applied = '0';
            if ($user_id > 0) {
                foreach ($applications AS $app) {
                    if ($app->user_id == $user_id) {
                        $gig->applied = '1';
                        $chat_id = $app->chat_id;
                    }
                }
            }

            $candidate = $this->Candidate->findByUserId($user_id)->first();

            //Before returning the image check if the image is a Facebook image.
            if ($candidate->image != NULL && $candidate->image != '') {
                if (preg_match('/facebook/', $candidate->image)) {
                    //If it is a facebook image then return the URL as it is
                    $gig->user_image = $candidate->image;
                } else {
                    //If it is a custom image uploaded by the user then return with the complete path
                    $gig->user_image = Configure::read('dev_base_url') . 'img/user_images/' . $candidate->image;
                }
            } else {
                $gig->user_image = "";
            }

            $gigData = $this->Gig->get($gig_id, [
                'contain' => ['Company', 'Address', 'User', 'Skills']
            ]);

            if($gigData->is_diy == 0) {
                $title = $gigData->title;
                $image = $gigData->gig_image;
            }
            else {
                $this->loadModel('SkillImages');
                $skill = $this->SkillImages->findBySkillsId($gigData->skills_id)
                    ->contain(['Images'])
                    ->first();
                $title = $gigData->skill->skill;
                $image = $skill->image->name;
            }

           /* $this->loadModel('City');
            if($gigData->city_id != 0) {
                $cityName = $this->City->findById($gigData->city_id)->first();
                $address = $cityName->city_name;
            }
            else {
               $address =  $this->makeAddress($gigData->addres);
            }*/

            $gig->id = $gigData->id;
            $gig->title = (!empty($gigData->title) ? $gigData->title : $title);
            $gig->gig_image = Configure::read('dev_base_url') . 'img/gig_images/' . (!empty($gigData->gig_image) ? $gigData->gig_image : $image);
            $gig->address = $this->makeAddress($gigData->addres);
            $gig->company = $gigData->company->name;
            $gig->vacancies = $gigData->gigstrs_needed;
            $gig->schedule = (is_null($gigData->start_date) ? 'ASAP - ' : $gigData->start_date->format('d M') . ' - ') . (is_null($gigData->end_date) ? 'ongoing' : $gigData->end_date->format('d M'));
            $gig->contact = strtok($gigData->user->name, " ");
            $gig->description = $gigData->description;
            $gig->active = $gigData->active;
            $gig->company_website =  $gigData->company->website;
            $gig->status = $this->Gig->getGigStatus($gigData->status);
            $gig->created_by = $gigData->created_by;
            $gig->ad_type = $gigData->ad_type;

            $this->set('data', $gig);
            $return = new \stdClass();
            $return->status = 0;
            $return->job = $gig;
            $return->chat_id = $chat_id;
            $this->set('return', $return);
            $this->set('_serialize', ['return', 'data']);
            echo json_encode($return);
            die();
        }
    }


    /** add DIY gig V2 */
    public function addNewGig() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $user_id = $this->request->header('user_id');
            $token = $this->request->header('token');
            $this->loadModel('User');

            if ($this->User->isLoggedIn($token, $user_id)) {

                $this->loadModel('Company');
                $this->loadModel('Address');

                $company = trim($this->request->data['company']);
                $website = $this->request->data['website'];
                $company_address = $this->request->data['company_address'];
                $gig_address = $this->request->data['gig_address'];
                $skills_id = $this->request->data['skill'];

                $query = $this->Company->find('all', [
                    'conditions' => ['name' => $company]
                ]);
                $row = $query->first();

                if (!empty($row)) {
                    $addressId = $this->Gig->getAddress($company_address);
                    $company = $this->Company->get($row->id);
                    $company->address_id = $addressId;
                    $company->website = $website;
                    $this->Company->save($company);
                    $company_id = $row->id;
                }
                else {
                    $addressId = $this->Gig->getAddress($company_address);
                    $newCompany = $this->Company->newEntity();
                    $newCompany->name = $company;
                    $newCompany->address_id = $addressId;
                    $newCompany->website = $website;
                    $newCompany->created_by = $user_id;
                    $theCompany = $this->Company->save($newCompany);
                    $company_id = $theCompany->id;
                }

                $gig = $this->Gig->newEntity();
                $gig->company_id = $company_id;
                $gig->start_date = ($this->request->data['start_date'] == "" ? '0000-00-00 00:00:00' : $this->request->data['start_date']);
                $gig->end_date = ($this->request->data['end_date'] == "" ? '0000-00-00 00:00:00' : $this->request->data['end_date']);
                $gig->is_diy = 1;
                $addressId = $this->Gig->getAddress($gig_address);
                $gig->address_id = $addressId;
                $gig->gigstrs_needed = $this->request->data['gigstrs_needed'];
                $gig->description = nl2br($this->request->data['description']);
                $gig->skills_id = $skills_id;
                $gig->status = "0";
                $gig->created_by = $user_id;
                $gig->country = $gig_address['country_code'];
                $gig->website = $website;

                if(isset($this->request->data['ad_type'])) {
                    $gig->ad_type = $this->request->data['ad_type'];
                }

                if($newGig = $this->Gig->save($gig)) {
                    $this->loadModel('SkillImages');

                    $company = $this->Company->findById($company_id)->first();
                    $skill = $this->SkillImages->findBySkillsId($skills_id)
                        ->contain(['Images', 'Skills'])
                        ->first();

                    $return = array(
                        'status' => 0,
                        'gig_id' => $newGig->id,
                        'start_date' => $newGig->start_date,
                        'title' => $skill->skill->skill,
                        'gig_image' => Configure::read('dev_base_url') . 'img/gig_images/' . $skill->image->name,
                        'company' => $company->name,
                        'applied' => '0'
                    );
                } else {
                    $return = array('status' => 1);
                }
            } else {
                $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
            }

            echo json_encode($return);
            die();
        }
    }


    /** add DIY gig V2 DRAFT */
    public function draftGig() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $user_id = $this->request->header('user_id');
            $token = $this->request->header('token');
            $this->loadModel('User');

            if ($this->User->isLoggedIn($token, $user_id)) {

                $this->loadModel('Company');
                $this->loadModel('Address');
                $this->loadModel('Candidate');

                $company = trim($this->request->data['company']);
                $website = $this->request->data['website'];
                $company_address = $this->request->data['company_address'];
                $gig_address = $this->request->data['gig_address'];
                $skills_id = $this->request->data['skill'];

                $query = $this->Company->find('all', [
                    'conditions' => ['name' => $company]
                ]);
                $row = $query->first();

                if (!empty($row)) {
                    $addressId = $this->Gig->getAddress($company_address);
                    $company = $this->Company->get($row->id);
                    $company->address_id = $addressId;
                    $company->website = $website;
                    $this->Company->save($company);
                    $company_id = $row->id;
                }
                else {
                    $addressId = $this->Gig->getAddress($company_address);
                    $newCompany = $this->Company->newEntity();
                    $newCompany->name = $company;
                    $newCompany->address_id = $addressId;
                    $newCompany->website = $website;
                    $newCompany->created_by = $user_id;
                    $theCompany = $this->Company->save($newCompany);
                    $company_id = $theCompany->id;
                }

                if(isset($this->request->data['id'])) {
                    $gigId = $this->request->data['id'];
                    $gig = $this->Gig->get($gigId);
                }
                else {
                    $gig = $this->Gig->newEntity();
                }

                $addressId = $this->Gig->getAddress($gig_address);
                $cityObj = $this->request->data['city_id'];
                $this->loadModel('City');
                //$cityView = $this->City->findById($cityObj['id'])->first();

                $gig->company_id = $company_id;
                $gig->start_date = ($this->request->data['start_date'] == "" ? '0000-00-00 00:00:00' : $this->request->data['start_date']);
                $gig->end_date = ($this->request->data['end_date'] == "" ? '0000-00-00 00:00:00' : $this->request->data['end_date']);
                $gig->is_diy = 1;
                $gig->city_id = $cityObj['id'];
                $gig->address_id = $addressId;
                $gig->gigstrs_needed = $this->request->data['gigstrs_needed'];
                $gig->description = nl2br($this->request->data['description']);
                $gig->skills_id = $skills_id;
                $gig->title = $this->request->data['title'];
                $gig->status = "6";
                $gig->created_by = $user_id;
                $gig->country = $gig_address['country_code']; //$cityView->country_code;
                $gig->website = $website;

                if(isset($this->request->data['ad_type'])) {
                    $gig->ad_type = $this->request->data['ad_type'];
                }

                if($newGig = $this->Gig->save($gig)) {
                    $this->loadModel('SkillImages');

                    $company = $this->Company->findById($company_id)->first();
                    $skill = $this->SkillImages->findBySkillsId($skills_id)
                        ->contain(['Images', 'Skills'])
                        ->first();

                    if(!empty($newGig->title)) {
                        $title = $newGig->title;
                    }
                    else {
                        $title = $skill->skill->skill;
                    }

                    $return = array(
                        'status' => 0,
                        'gig_id' => $newGig->id,
                        'start_date' => $newGig->start_date,
                        'title' => $title,
                        'gig_image' => Configure::read('dev_base_url') . 'img/gig_images/' . $skill->image->name,
                        'company' => $company->name,
                        'applied' => '0',
                        'gig_status' => 'Draft'
                    );
                } else {
                    $return = array('status' => 1);
                }
            } else {
                $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
            }

            echo json_encode($return);
            die();
        }
    }

    /**
     * save gig to pending or Published
     */
    public function saveGig() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');
        $this->loadModel('Candidate');
        $return = new \stdClass();

        if ($this->User->isLoggedIn($token, $user_id)) {

            $candidateObj = $this->Candidate->findByUserId($user_id)->first();

            $gigId = $this->request->data['id'];
            $gig = $this->Gig->get($gigId, [
                'contain' => ['User', 'Skills']
            ]);
            $gig->status = "0";
            $publish = 'Pending';

            if($candidateObj->verified_company == 1) {
                $date = date('Y-m-d h:i:s');
                $expireDate = strtotime($date);
                $expire = strtotime("+14 day", $expireDate);
                $gig->status = '1';
                $gig->activate_date = $date;
                $gig->expire_date = date('Y-m-d h:i:s', $expire);
                $publish = 'Published';
            }

            $this->Gig->save($gig);

            if(empty($gig->title)) {
                $title = $gig->skill->skill;
            } else {
                $title = $gig->title;
            }

            $email = new Email('default');
            $email->from(['noreply@gigstr.com' => Configure::read('from_site')])
                ->to('support@gigstr.com')
                //->to('anton.lundberg@codebuilders.se')
                ->subject('New ad is created')
                ->send($title .' by ' . $gig->user->name . ' is ' . $publish);

            $return->status = 0;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }


    /** Gig list view. load all active gigs for select country */
    public function listAllNew() {
        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $country = $this->request->header('X-Country');

            $gigsArr = $this->Gig->find('all')
                ->contain(['Company', 'Application', 'Address', 'Skills'])
                ->where(['Gig.status' => '1', 'Gig.country' => $country, 'Gig.is_diy' => 1])
                ->order(['Gig.modified' => 'DESC']);

            $data = new \stdClass();
            $this->loadModel('SkillImages');

            foreach ($gigsArr AS $gig) {
                $sub_data = new \stdClass();

                if($gig->is_diy == 0) {
                    $title = $gig->title;
                    $image = $gig->gig_image;
                    $sub_title1 = $gig->sub_title1;
                    $sub_title2 = $gig->sub_title2;
                }
                else {
                    $skill = $this->SkillImages->findBySkillsId($gig->skills_id)
                        ->contain(['Images'])
                        ->first();

                    /*if($gig->city_id =! 0) {
                        $this->loadModel('City');
                        $cityObj = $this->City->findById($gig->city_id)->first();
                        $city = $cityObj->city_name;
                    } else {
                        $city = $gig->addres->city;
                    }*/

                    $title = $gig->skill->skill;
                    $image = $skill->image->name;
                    $sub_title1 = $gig->company->name;
                    $sub_title2 = $gig->addres->city . ' - ' . 'Start: ' . (is_null($gig->start_date) ? ' ASAP' : $gig->start_date->format('d M'));
                }

                $sub_data->id = $gig->id;
                $sub_data->title = (!empty($gig->title) ? $gig->title : $title) ;
                $sub_data->sub_title1 = $sub_title1;
                $sub_data->sub_title2 = $sub_title2;
                $sub_data->gig_image = Configure::read('dev_base_url') . 'img/gig_images/' . (!empty($gig->gig_image) ? $gig->gig_image : $image);
                $sub_data->applied = 0;
                $sub_data->modified = $gig->modified;

                if (isset($this->request->data['user_id'])) {
                    foreach ($gig['application'] AS $app) {
                        if ($app['user_id'] == $this->request->data['user_id']) {
                            $sub_data->applied = '1';
                        }
                    }
                }
                $data->jobs[] = $sub_data;
            }

            $return = new \stdClass();
            $return->status = 0;
            if(is_null($data->jobs)) {
                $data->jobs = [];
            }
            $return->jobs = $data->jobs;
            $this->set('return', $return);
            $this->set('_serialize', ['return', 'data']);
            echo json_encode($return);
            die();
        }
    }


    /** get skill list */
    public function gigSkills() {
        header('Content-type: application/json');
        $this->loadModel('Skills');
        $skills = $this->Skills->find('all')
                    ->select(['id', 'name' => 'skill'])
                    ->where(['active' => 1])
                    ->order(['name' => 'ASC'])
                    ->toArray();

        foreach($skills as $key => $val) {
            if($val['name'] == 'Other') {
                $item = $skills[$key];
                unset($skills[$key]);
                array_push($skills, $item);
                break;
            }
        }

        $skillArr = [];
        foreach ($skills as $skill) {
            $temp = [];
            $temp['name'] = $skill['name'];
            $temp['id'] = $skill['id'];

            array_push($skillArr, $temp);
        }

        $return['skills'] = $skillArr;
        $return['status'] = 0;

        echo json_encode($return);
        die();
    }


    /** gig list created by user [Company]*/
    public function myGigs() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');

        if ($this->User->isLoggedIn($token, $user_id)) {

            $this->loadModel('Application');
            $this->loadModel('SkillImages');

            $return['myGigs'] = [];

            if ($this->request->is('post')) {

                $gigs = $this->Gig->find('all')
                        ->contain(['Company', 'Skills'])
                        ->where(['Gig.created_by' => $user_id,
                            'OR' => [['Gig.status' => '0'], ['Gig.status' => '1'], ['Gig.status' => '2'] , ['Gig.status' => '3'], ['Gig.status' => '5']]
                            ])
                        ->order(['Gig.created' => 'DESC']);

                foreach($gigs as $key => $gig) {

                    $new_applicants = 0;
                    $status = $this->Gig->getGigStatus($gig->status, true);
                    if(!empty($gig->gig_image)) {
                        $image = $gig->gig_image;
                    }
                    else {
                        $skill = $this->SkillImages->findBySkillsId($gig->skills_id)
                            ->contain(['Images'])
                            ->first();
                        $image = $skill->image->name;
                    }

                    $temp = [];
                    $applicants = $this->Application->find()
                        ->join([
                            'alias' => 'g',
                            'table' => 'gig_candidate',
                            'type' => 'LEFT',
                            'conditions' => 'g.user_id = Application.user_id'
                        ])
                        ->where(['Application.gig_id' => $gig->id,
                            'g.selection' => 'SHORTLIST', 'g.gig_id' => $gig->id]);

                    foreach($applicants as $applicant) {
                       if($applicant->status == '0') {
                           $new_applicants ++;
                       }
                    }

                    $temp['gig_id'] = $gig->id;
                    $temp['gig_title'] = (!empty($gig->title) ? $gig->title : $gig->skill->skill);
                    $temp['gig_image'] = Configure::read('dev_base_url') . 'img/gig_images/' . $image;
                    $temp['company'] = $gig->company->name;
                    $temp['created_date'] = $gig->created->format('d M Y');
                    $temp['status'] = $status;
                    $temp['applicant_count'] = $applicants->count();
                    $temp['new_applicant_count'] = $new_applicants;
                    array_push($return['myGigs'], $temp);
                }
                $return['status'] = 0;
                echo json_encode($return);
                die;
            }
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
            echo json_encode($return);
            die();
        }
    }


    /** gig applicant list
     * Company mode
     */
    public function applicants() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');

        if ($this->User->isLoggedIn($token, $user_id)) {
            $this->loadModel('Application');
            $this->loadModel('Candidate');
            $this->loadModel('Chat');
            $this->loadModel('ChatGroup');
            $gigiId = $this->request->data['gigId'];

            $applicants = $this->Application->find()
                ->select(['Application.status', 'u.id', 'c.city', 'u.name', 'c.id', 'c.image', 'c.image_thumbnail', 'Application.created'])
                ->join([
                    'u' => [
                        'table' => 'user',
                        'type' => 'LEFT',
                        'conditions' => 'Application.user_id = u.id',
                    ],
                    'c' => [
                        'table' => 'candidate',
                        'type' => 'LEFT',
                        'conditions' => 'c.user_id = u.id'
                    ],
                    'g' => [
                        'table' => 'gig_candidate',
                        'type' => 'LEFT',
                        'conditions' => 'g.user_id = Application.user_id'
                    ]
                ])
                ->where(['Application.gig_id' => $gigiId, 'g.selection' => 'SHORTLIST', 'g.gig_id' => $gigiId]);

            $return['applicants'] = [];
            foreach ($applicants as $key => $apply) {
                $temp = [];
                $imageUrl = $this->getUserImage($apply->c['image']);

                $chatObj = $this->getChatGroup($user_id, (int)$apply->u['id']);

                if($chatObj != 0) {
                    $chat_id = $chatObj;
                } else {
                    $chatEntry = $this->Chat->newEntity();
                    $chatEntry->created_by = $user_id;
                    $this->Chat->save($chatEntry);
                    $chat_id = $chatEntry->id;

                    //Insert an entry to the Chat group table
                    $chatgroup = $this->ChatGroup->newEntity();
                    $chatgroup->chat_id = $chat_id;
                    $chatgroup->user_id = $user_id;
                    $chatgroup->status = 1;
                    $this->ChatGroup->save($chatgroup);

                    $chatgroup2 = $this->ChatGroup->newEntity();
                    $chatgroup2->chat_id = $chat_id;
                    $chatgroup2->user_id = (int)$apply->u['id'];
                    $chatgroup2->status = 1;
                    $this->ChatGroup->save($chatgroup2);
                }

                $temp['user_id'] = (int)$apply->u['id'];
                $temp['name'] = $apply->u['name'];
                $temp['image'] = (empty($apply->c['image_thumbnail']) ? $imageUrl :  Configure::read('dev_base_url') . 'img/user_images/' . $apply->c['image_thumbnail']);
                $temp['apply_date'] = $apply->created->format('d M Y');
                $temp['profile_viewed'] = ($apply->status == '0' ? 'New applicant' : 'Viewed');
                $temp['applicant_status'] = "";
                $temp['city'] = (is_null($apply->c['city']) ? "" : $apply->c['city']);
                $temp['chat_id'] = $chat_id;
                array_push($return['applicants'], $temp);
            }
            $return['status'] = 0;
        }
        else {
            $return = $this->errorMsg();
        }
        echo json_encode($return);
        die;
    }


    /**
     * @param $userId
     * @param $applicantId
     * @return int
     */
    private function getChatGroup($userId, $applicantId) {

        $this->loadModel('ChatGroup');
        $chatObj = $this->ChatGroup->find('all')->where(['user_id' => $userId, 'status' => 1]);

        $chatId = 0;

        if($chatObj) {
            foreach ($chatObj as $chat) {
                $chat = $this->ChatGroup->find('all')->where(['chat_id' => $chat->chat_id, 'user_id' => $applicantId, 'status' => 1])->first();

                if ($chat) {
                    $chatId = $chat->chat_id;
                }
            }
        }

        return $chatId;
    }


    /** apply a gig request
     * Gigstr Mode
     */
    public function applyNew() {
        header('Content-type: application/json');
        $this->loadModel('Application');
        $this->loadModel('Gig');
        $this->loadModel('Chat');
        $this->loadModel('User');
        $user_id = $this->request->data['user_id'];
        $token = $this->request->data['token'];

        if ($this->User->isLoggedIn($token, $user_id)) {
            if ($this->request->is('post')) {
                $return = new \stdClass();

                $this->loadModel('Candidate');
                $newCandidate = $this->Candidate->findByUserId($user_id)->first();
                $user = $this->User->get($user_id);
                $checkWizard = $this->User->checkWizard($newCandidate);
                $wiz = array_values($checkWizard['wizardArr']);

                $return->signup = false;
                //check wizard is available, fire it
                if($checkWizard['wizard_enable'] == true) {
                    $return->wizard_fire = $checkWizard['wizard_enable'];
                    $return->missing_data = $wiz;
                    $return->profile = $this->User->profileObj($user_id);
                    $return->status = 0;
                }
                else { // no wizard
                    $return->wizard_fire = false;
                    $applicant = $this->Application->newEntity();
                    $applicant->gig_id = $this->request->data['gig_id'];
                    $applicant->user_id = $user_id;
                    $applicant->status = 0;

                    //Insert entry to chat table
                    if ($this->Application->save($applicant)) {
                        $return->status = 0;
                        $return->chat_id = 0;
                        $this->set('return', $return);
                        $this->set('_serialize', $return);
                    } else {
                        $return->status = 202;
                        $return->message = "Unable to apply now. Please try again.";
                        $this->set('return', $return);
                        $this->set('_serialize', $return);
                    }
                }
                echo json_encode($return);
                die();
            }
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }


    /** deactivate gig from Company */
    public function deactivate() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');
        $return = new \stdClass();

        if ($this->User->isLoggedIn($token, $user_id)) {

            $gigiId = $this->request->data['gig_id'];
            $gigObj = $this->Gig->get($gigiId);
            $gigObj->status = '2';
            $this->Gig->save($gigObj);

            $return->status = 0;
        }
        else {
            $return = $this->errorMsg();
        }
        echo json_encode($return);
        die;
    }


    /**
     * search function
     * use for the list gig
     */
    public function search() {
        header('Content-type: application/json');
        $country = $this->request->header('X-Country');

        $this->loadModel('Address');
        $this->loadModel('SearchLog');
        $this->loadModel('SkillImages');
        $data = new \stdClass();

        $country_code = $this->request->data['country_code'];
        $city = $this->request->data['city'];
        $area = $this->request->data['area'];
        $areaType = $this->request->data['areatype'];
        $placeIdGoogle = $this->request->data['place_id_google'];
        $userId = $this->request->data['user_id'];
        $filterStartDate = $this->request->data['filter_start_date'];

        if(empty($filterStartDate) && empty($area)) { /** gig list */
            $gigsArr = $this->Gig->find('all')
                ->contain(['Company', 'Application', 'Address', 'Skills'])
                ->where(['Gig.status' => '1', 'Gig.country' => $country, 'Gig.is_diy' => 1])
                ->order(['Gig.modified' => 'DESC']);
        }
        /** Search list */
        else {
            $searchLog = $this->SearchLog->newEntity();
            $searchLog->user_id = $userId;

            $filterArr = array('Gig.status' => '1', 'Gig.is_diy' => 1, 'Gig.country' => $country);
            $selectArr = ['Gig.id', 'Gig.title', 'Gig.sub_title1', 'Gig.sub_title2', 'Gig.gig_image', 'Gig.modified',
                'Gig.skills_id', 'Gig.start_date', 'Gig.is_diy', 'Gig.created_by', 'Company.name', 'Address.city', 'Address.id', 'Skills.skill'];
            $orderArr = ['Gig.modified' => 'DESC'];

            if(!empty($area)) {
                $filterArr['Address.area LIKE'] = $area;
                $selectArr['direct_hit'] = 'IF(Address.city= "'. $city .'", 1,0)';
                $orderArr = ['direct_hit' => 'DESC', 'Gig.modified' => 'DESC'];

                $searchLog->city = $city;
                $searchLog->area = $area;
                $searchLog->areatype = $areaType;
                $searchLog->place_id_google = $placeIdGoogle;
                $searchLog->country_code = $country_code;
            }

            if(!empty($filterStartDate)) {
                $onlyDate = date($filterStartDate);
                $onlyDate = date('Y-m-d', strtotime($onlyDate));
                $newDate = $onlyDate . ' 00:00:00';

                $filterArr['OR'] = [['Gig.start_date >=' => $newDate],['Gig.start_date' => '0000-00-00 00:00:00']];
                $searchLog->from_date = $newDate;
            }

            if(count($filterArr) == 3) {
                $gigsArr = new \stdClass();
            }
            else {
                $gigsArr = $this->Gig->find('all')
                    ->select($selectArr)
                    ->contain(['Company', 'Application', 'Address', 'Skills'])
                    ->where($filterArr)
                    ->order($orderArr)
                    ->limit(40);
            }
            $this->SearchLog->save($searchLog);
        }

        $data->jobs = [];

        foreach ($gigsArr AS $gig) {
            $sub_data = new \stdClass();

            if ($gig->is_diy == 0) {
                $title = $gig->title;
                $image = $gig->gig_image;
                $sub_title1 = $gig->sub_title1;
                $sub_title2 = $gig->sub_title2;
            } else {
                $skill = $this->SkillImages->findBySkillsId($gig->skills_id)
                    ->contain(['Images'])
                    ->first();

                $title = $gig->skill->skill;
                $image = $skill->image->name;
                $sub_title1 = $gig->company->name;
                $sub_title2 = $gig->addres->city . ' - ' . 'Start: ' . (is_null($gig->start_date) ? ' ASAP' : $gig->start_date->format('d M'));
            }

            $sub_data->id = $gig->id;
            $sub_data->title = (!empty($gig->title) ? $gig->title : $title);
            $sub_data->sub_title1 = $sub_title1;
            $sub_data->sub_title2 = $sub_title2;
            $sub_data->gig_image = Configure::read('dev_base_url') . 'img/gig_images/' . (!empty($gig->gig_image) ? $gig->gig_image : $image);
            $sub_data->applied = 0;
            $sub_data->modified = $gig->modified;
            $sub_data->created_by = $gig->created_by;

            if (isset($this->request->data['user_id'])) {
                foreach ($gig['application'] AS $app) {
                    if ($app['user_id'] == $this->request->data['user_id']) {
                        $sub_data->applied = '1';
                    }
                }
            }
            $data->jobs[] = $sub_data;
        }

        $return = new \stdClass();
        $return->status = 0;
        /*if(is_null($data->jobs)) {

        }*/
        $return->jobs = $data->jobs;
        $this->set('return', $return);
        $this->set('_serialize', ['return', 'data']);
        echo json_encode($return);
        die();
    }


    /**
     *
     */
    public function pushMonitor() {

        $this->loadModel('PushNotification');

        $time = date("Y-m-d h:i:s", time() - 300);

        $pusObj = $this->PushNotification->find('all')
            ->where(['response !=' => '1', 'created >' => $time])
            ->order(['created' => 'DESC'])
            ->limit(10);

        var_dump($pusObj);die;

        $return = array();
        foreach($pusObj as $push) {
            if($push->response != '1') {
                $temp = [];
                $temp['id'] = $push->id;
                $temp['user_id'] = $push->user_id;
                $temp['name'] = $push->name;
                $temp['response'] = $push->response;

                array_push($return, $temp);
            }
        }

        var_dump($return);die;

        if(!empty($return)) {
            echo json_decode($return);
            die();
        }
        else {

        }
    }


    /**
     * Cron for Push notification Ad Expired
     * Expire the Gig
     */
    public function cronExpireGig() {

        $this->loadModel('JobOffer');
        $this->loadModel('Event');
        $this->loadModel('PushNotification');

        $now = date('Y-m-d h:i:s');
        $expireObj = $this->Gig->find()
            ->contain(['Skills', 'User'])
            ->where(['Gig.is_diy' => 1, 'Gig.status' => '1', 'Gig.active !=' => '3', 'Gig.expire_date <' => $now]);

        foreach($expireObj as $expire) {

            $gigObj = $this->Gig->get($expire->id);
            $gigObj->status = '3';
            if($this->Gig->save($gigObj)) {

                $title = (!empty($expire->title) ? $expire->title : $expire->skill->skill);
                $payload = 'Your ad for '. $title .' has expired, post a new ad if you still are in need of people.';
                $key_value = json_encode([]);

                if (!empty($expire->user->fcm_token)) {
                    $badgeCount = $this->JobOffer->badgeCount($expire->user->id);
                    $badges = $badgeCount->total;

                    $this->Event->eventSave($payload, $expire->user->id, 'COMPANY', 'MY_GIGS', $key_value, 0);

                    $this->PushNotification->sendPushEvent($payload, $expire->user->fcm_token, $expire->user->id, 'COMPANY', 'MY_GIGS', 'Ad Expired', $badges, $key_value, "");

                }
            }
        }

        die();
    }


    /**
     * Reminder: Pending Job Offer
     * Job tomorrow
     * Run every 4 hour
     */
    public function cronJobOfferReminder() {

        $this->loadModel('LogJobOfferReminder');
        $this->loadModel('PushNotification');
        $this->loadModel('Event');
        $this->loadModel('User');
        $this->loadModel('JobOffer');
        $this->loadModel('Gig');

        $now = date('Y-m-d H:i:s');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SET @time_stamp = "'. $now .'"; CALL usp_update_job_offer_reminders(@time_stamp);');
        $stmt->execute();
        $stmt->closeCursor();

        $remenders = $this->LogJobOfferReminder->find('all')
            ->contain(['JobOffer'])
            ->where(['LogJobOfferReminder.created' => $now, 'LogJobOfferReminder.push_status' => 0]);

        foreach($remenders as $remender) {

            $creater = $this->User->findById($remender->job_offer->created_by)->first();
            $firstName = strtok($creater->name, " ");
            $skill = $this->Gig->findById($remender->job_offer->gig_id)->contain(['Skills'])->first();
            $skillName = (!empty($skill->title) ? $skill->title : $skill->skill->skill);

            if($remender->type == 1) {
                $payload = 'You have a new job with '. $firstName .' as '. $skillName .' coming up. Please check your schedule for details.';
                $notification = 'Job tomorrow';
                $redirect = 'SCHEDULE_DETAIL';
                $action = 0;
                $key_value = json_encode(["schedule_id" => $remender->schedule_id]);
            }
            else {
                $payload = $firstName . ' is waiting for an answer, please check your pending job offer.';
                $notification = 'Reminder: Pending Job Offer';
                $redirect = 'JOB_OFFER_DETAIL';
                $action = 1;
                $key_value = $key_value = json_encode([
                    "jobOffer_id" => $remender->job_offer_id,
                    "gig_id" => $remender->job_offer->gig_id
                ]);
            }

            $pushUser = $this->User->findById($remender->job_offer->user_id)->first();
            $badgeCount = $this->JobOffer->badgeCount($remender->job_offer->user_id);
            $badges = $badgeCount->total;

            $eventId = $this->Event->eventSave($payload, $remender->job_offer->user_id, 'GIGSTR', $redirect, $key_value, $action);
            if($action == 1) {
                $jobOfferUpdate = $this->JobOffer->get($remender->job_offer_id);
                $jobOfferUpdate->event_id = $eventId;
                $this->JobOffer->save($jobOfferUpdate);
            }

            if(!empty($pushUser->fcm_token)) {
                $this->PushNotification->sendPushEvent($payload, $pushUser->fcm_token, $remender->job_offer->user_id, 'GIGSTR', $redirect, $notification, $badges, $key_value, "");

                $remenderObj = $this->LogJobOfferReminder->get($remender->id);
                $remenderObj->push_status = 1;
                $this->LogJobOfferReminder->save($remenderObj);
            }
        }
        die();

    }


    /** COPY
     * @param $imageUrl
     * @return string
     */
    private function getUserImage($imageUrl) {

        if ($imageUrl == null || empty($imageUrl)) {
            $image = Configure::read('dev_base_url') . 'img/user_images/user_placeholder.jpg';
        }
        else {
            if (preg_match('/facebook/', $imageUrl)) {
                $image = $imageUrl;
            } else {
                $image = Configure::read('dev_base_url') . 'img/user_images/' . $imageUrl;
            }
        }
        return $image;
    }

    /**
     * error response
     * @param string $message
     * @return array
     */
    private function errorMsg($message = 'Restricted access') {

        return array('status' => 400, 'status_message' => 'error', 'message' => $message);
    }

    /** COPY
     * @param $addressObj
     * @return string
     * create address string
     */
    private function makeAddress($addressObj) {

        $address = ($addressObj->street ? $addressObj->street . ', ': '') . ($addressObj->city ? $addressObj->city . ', ': '') . ($addressObj->zip ? $addressObj->zip . ', ': '') . ($addressObj->country_name ? $addressObj->country_name : '');

        return $address;
    }

}
