<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Applicant Controller
 *
 * @property \App\Model\Table\ApplicantTable $Applicant
 */
class ApplicantController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['User']
        ];
        $this->set('applicant', $this->paginate($this->Applicant));
        $this->set('_serialize', ['applicant']);
    }

    /**
     * View method
     *
     * @param string|null $id Applicant id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $applicant = $this->Applicant->get($id, [
            'contain' => ['User']
        ]);
        $this->set('applicant', $applicant);
        $this->set('_serialize', ['applicant']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $applicant = $this->Applicant->newEntity();
        if ($this->request->is('post')) {
            $applicant = $this->Applicant->patchEntity($applicant, $this->request->data);
            if ($this->Applicant->save($applicant)) {
                $this->Flash->success(__('The applicant has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The applicant could not be saved. Please, try again.'));
            }
        }
        $users = $this->Applicant->User->find('list', ['limit' => 200]);
        $gigs = $this->Applicant->Gig->find('list', ['limit' => 200]);
        $this->set(compact('applicant', 'users', 'gigs'));
        $this->set('_serialize', ['applicant']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Applicant id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $applicant = $this->Applicant->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $applicant = $this->Applicant->patchEntity($applicant, $this->request->data);
            if ($this->Applicant->save($applicant)) {
                $this->Flash->success(__('The applicant has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The applicant could not be saved. Please, try again.'));
            }
        }
        $users = $this->Applicant->User->find('list', ['limit' => 200]);
        $gigs = $this->Applicant->Gig->find('list', ['limit' => 200]);
        $this->set(compact('applicant', 'users', 'gigs'));
        $this->set('_serialize', ['applicant']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Applicant id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $applicant = $this->Applicant->get($id);
        if ($this->Applicant->delete($applicant)) {
            $this->Flash->success(__('The applicant has been deleted.'));
        } else {
            $this->Flash->error(__('The applicant could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
