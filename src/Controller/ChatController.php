<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Cake\Mailer\Email;

/**
 * Chat Controller
 *
 * @property \App\Model\Table\ChatTable $Chat
 */
class ChatController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('chat', $this->paginate($this->Chat));
        $this->set('_serialize', ['chat']);
    }

    /**
     * listAll method [Chat inbox]
     *
     * @param int $user_id User id.
     * @param int $device_type 1 for Android and 2 for iOs. (This is to respond with the proper format of the chat message.
     * There is a requirement that weblinks to be displayed as links in the chat. iOS by default converts the links to clickable links. But Android doesn't
     * provice a facility like that. So for Android devices need to convert the links as weblinks from server side and respond. Based on the device type
     * the response varies)
     * @return json encoded response of the chat threads related to the current user
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function listAll() {

        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {

            $this->loadModel('Message');
            $this->loadModel('ChatGroup');
            $this->loadModel('User');
            $app_mode = $this->request->data['app_mode'];
            $conn = ConnectionManager::get('default');

            $stmt = $conn->prepare('SELECT  cg.chat_id, (SELECT user_id FROM chat_group WHERE chat_id = cg.chat_id 
                AND user_id != '. $user_id .' ) AS chat_user, c.modified
                FROM chat_group cg
                INNER JOIN chat c ON cg.chat_id = c.id
                WHERE cg.user_id = '. $user_id .' AND cg.status = 1 
                ORDER BY c.modified DESC');
            $stmt->execute();
            $chats = $stmt->fetchAll('assoc');
            $inboxArr = [];

            foreach ($chats as $inbox) {
                $checkMsg = $this->Message->findByChatId($inbox['chat_id'])->last();
                if($checkMsg) {
                    $userObj = $this->User->findById($inbox['chat_user'])->first();
                    $temp = [];
                    $temp['chat_id'] = $inbox['chat_id'];
                    $temp['name'] = $userObj->name;
                    $temp['user_id'] = $inbox['chat_user'];
                    $temp['image'] = $this->getUserImage($inbox['chat_user']);
                    $temp['modified'] = $inbox['modified'];
                    $temp['unread_message_max'] = ($checkMsg ? $checkMsg->message : '');
                    $temp['unread_messages_count'] = $this->unreadMsgCount($user_id, $inbox['chat_id']);

                    array_push($inboxArr, $temp);
                }
            }
            $return->status = 0;
            $return->chat = $inboxArr;
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }

    /**
     * send method [Chat]
     *
     * @return void encoded array containing the status, message_id and created date time
     */
    public function send() {

        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');

        $return = new \stdClass();
        $this->loadModel('User');
        $this->loadModel('Message');
        $this->loadModel('ChatGroup');
        $this->loadModel('Candidate');
        $this->loadModel('UnreadMessage');
        $this->loadModel('PushNotification');

        if ($this->User->isLoggedIn($token, $user_id)) {

            $message = $this->request->data['message'];
            $chat_id = $this->request->data['chat_id'];

            $messageEntity = $this->Message->newEntity();
            $messageEntity->message = $message;
            $messageEntity->created_by = $user_id;
            $messageEntity->chat_id = $chat_id;
            $this->Message->save($messageEntity);

            //Get other user in the chat grop
            $chat_group_members = $this->ChatGroup->find('all')
                ->where(['chat_id' => $chat_id, 'user_id <> ' => $user_id]);

            $sendUser = $this->Candidate->findByUserId($user_id)->contain(['User'])->first();

            //Insert entries in the unread message table for other users in the chat group
            $unreadMessageEntity = $this->UnreadMessage->newEntity();
            foreach ($chat_group_members AS $chat_group) {

                $groupObj = $this->ChatGroup->find('all')->where(['chat_id' => $chat_id, 'user_id' => $chat_group->user_id])
                    ->first();
                if($groupObj->status == 1) {

                    $unreadMessageEntity->message_id = $messageEntity->id;
                    $unreadMessageEntity->user_id = $chat_group->user_id;
                    $unreadMessageEntity->chat_id = $chat_id;
                    $this->UnreadMessage->save($unreadMessageEntity);

                    $userObj = $this->Candidate->findByUserId($chat_group->user_id)->contain(['User'])->first();
                    $mode = 'GIGSTR';
                    if ($userObj->app_mode == 2)
                        $mode = 'COMPANY';

                    $key_value = json_encode([
                        'chat_id' => $chat_id,
                        'image' => $this->getUserImage($sendUser->user_id),
                        'name' => $sendUser->user->name,
                        'user_id' => $sendUser->user_id
                    ]);

                    if (!empty($userObj->user->fcm_token)) {
                        $this->PushNotification->sendPushEvent(base64_decode($message), $userObj->user->fcm_token,
                            $chat_group->user_id,
                            $mode, 'CHAT_INBOX', 'New chat message', 0, $key_value);
                    }
                }
            }

            //Update the chat table with the latest modified time
            $chatUpdate = $this->Chat->get($chat_id);
            $chatUpdate->modified = date('Y-m-d H:i:s');
            $this->Chat->save($chatUpdate);

            $return->status = 0;
            $return->message_id = $messageEntity->id;
            $return->created = $messageEntity->created;
            $return->user_image = $this->getUserImage($user_id);

        } else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
         die();
    }

    /**
     * [Load all chats in chat]
     * listChat method
     * Access info = chat/list_all
     * list the messages related to a chat thread
     *
     * @return void encoded object containing status and messages of the chat
     */
    public function listChat() {

        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');
        $this->loadModel('Message');
        $this->loadModel('ChatGroup');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {

            $chat_id = $this->request->data['chat_id'];

            $groupObj = $this->ChatGroup->findByChatId($chat_id)->first();

        //    if($groupObj->status == 1) {

                $device_type = $this->request->data['device_type'];
                $messageAll = $this->Message->find('all')
                    ->where(['chat_id' => $chat_id]);

                $chatsArr = [];
                foreach ($messageAll as $result) {
                    $temp = [];
                    $temp['id'] = $result->id;
                    if ($device_type == '2') {
                        $temp['message'] = $this->replaceBr($result->message);
                    } else {
                        $msg = $this->makeClickableLinks(base64_decode($result->message));
                        $temp['message'] = base64_encode($msg);
                    }

                    $imgurl = $this->getUserImage($result->created_by);

                    $temp['user_image'] = $imgurl;
                    $temp['created_by'] = $result->created_by;
                    $temp['chat_id'] = $result->chat_id;
                    $temp['created'] = date('Y-m-d H:i:s', strtotime($result->created));
                    $temp['modified'] = date('Y-m-d H:i:s', strtotime($result->modified));

                    array_push($chatsArr, $temp);
                }

                $userObj = $this->ChatGroup->find('all')
                    ->contain(['User'])
                    ->where(['chat_id' => $chat_id, 'user_id <> ' => $user_id])->last();

                $return->status = 0;
                $return->messages = $chatsArr;
                $return->name = $userObj->user->name;

                $conn = ConnectionManager::get('default');
                $stmt = $conn->prepare('DELETE um FROM unread_message um INNER JOIN message m ON m.id = um.message_id WHERE m.chat_id = ' . $chat_id . ' AND um.user_id = ' . $user_id . ';');
                $stmt->execute();
            /*}
            else {
                $return->status = 101;
            }*/
        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }
        echo json_encode($return);
        die();
    }


    /**
     * block user from chat
     */
    public function blockUser() {
        header('Content-type: application/json');
        $user_id = $this->request->header('user_id');
        $token = $this->request->header('token');
        $this->loadModel('User');
        $return = new \stdClass();

        if($this->User->isLoggedIn($token, $user_id)) {
            $return->status = 0;

            $this->loadModel('ChatGroup');
            $chat_id = $this->request->data['chat_id'];
            $gigstr = $this->request->data['gigstr'];

            $companyUser = $this->User->get($user_id);
            $gigstrUser = $this->User->get($gigstr);

            /** company status 0 */
            $chatGroupObj = $this->ChatGroup->find('all')->where(['chat_id' => $chat_id, 'user_id' => $user_id])
                ->first();
            $updateObj = $this->ChatGroup->get($chatGroupObj->id);
            $updateObj->status = 0;
            $this->ChatGroup->save($updateObj);

            /** gigter status 0 */
            $chatGroupObjGig = $this->ChatGroup->find('all')->where(['chat_id' => $chat_id, 'user_id' => $gigstr])
                ->first();
            $updateObjGig = $this->ChatGroup->get($chatGroupObjGig->id);
            $updateObjGig->status = 0;
            $this->ChatGroup->save($updateObjGig);

             $email = new Email('default');
             $email->from(['noreply@gigstr.com' => Configure::read('from_site')])
              ->to('blocked-user@gigstr.com')
              //->to('anton.lundberg@codebuilders.se')
              ->subject('Blocked')
              ->send($companyUser->name .' has blocked User ' . $gigstrUser->name);

        }
        else {
            $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
        }

        echo json_encode($return);
        die();
    }


    /**
     * getUnread method
     * Get the unread messages of a chat.
     * Access info - chat/get_unread
     *
     * @param int $user_id User id
     * @param int $chat_id Chat Id
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function getUnread()
    {
        header('Content-type: application/json');
        if ($this->request->is('post')) {

            $user_id = $this->request->header('user_id');
            $token = $this->request->header('token');

            $this->loadModel('User');
            if ($this->User->isLoggedIn($token, $user_id)) {
                //If the user is logged in and authorized to access this page

                $this->loadModel('ChatGroup');
                $chat_id = $this->request->data['chat_id'];
                $device_type = $this->request->data['device_type'];

                $chatGroup = $this->ChatGroup->find()
                    ->select(['user_id'])
                    ->where(['chat_id' => $chat_id, 'user_id <>' =>
                        $user_id])->last();

                $conn = ConnectionManager::get('default');
                $stmt = $conn->prepare(
                    'SELECT m.* '
                    . 'FROM `message` m '
                    . 'INNER JOIN unread_message um ON um.message_id = m.id '
                    . 'WHERE m.`chat_id` = ' . $chat_id . ' AND um.`user_id` = ' . $user_id . ' '
                );
                $stmt->execute();

                $rows = $stmt->fetchAll('assoc');
                $data = new \stdClass();

                foreach ($rows as $row) {
                    $sub_data = new \stdClass();
                    $sub_data->id = $row['id'];

                    if ($device_type == '2')
                        //If the device is iOs return the message as it is
                        $sub_data->message = $row['message'];
                    else
                        //If the device is Android return the message as changing the links to weblinks
                        $sub_data->message = $this->makeClickableLinks($row['message']);

                    $sub_data->created_by = $row['created_by'];
                    $sub_data->created = $row['created'];
                    $sub_data->user_image = $this->getUserImage($chatGroup->user_id);

                    $data->data[] = $sub_data;
                }

                //Prepare the return object
                $return = new \stdClass();
                $return->status = 0;
                $return->messages = ($data ? $data->data : []);

                $conn = ConnectionManager::get('default');

                //Delete unread messages related this chat and user
                $stmt = $conn->prepare(
                    'DELETE um FROM unread_message um INNER JOIN message m ON m.id = um.message_id WHERE m.chat_id = ' . $chat_id . ' AND um.user_id = ' . $user_id . ';'
                );

                $stmt->execute();

            } else {
                //If the provided token and user_id not match
                $return = array('status' => 400, 'status_message' => 'error', 'message' => 'Restricted access');
            }
            echo json_encode($return);
            die();
        }
    }

    /**
     * makeClickableLinks method
     * Function to convert weblinks to clickable links
     *
     * @param int $text text to convert
     * @return text with clickable links
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function makeClickableLinks($text)
    {
        //return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href=\'$1" target="_blank">$1</a>', $s);
        //$reg_exUrl = "/(http|https|ftp|ftps|www.)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        $text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ginascheme://$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
        $text = str_replace('http', 'ginascheme', $text);
        $text = str_replace('>ginascheme://', '>', $text);
        return ($text);
    }

    public function replaceBr($text)
    {
        return str_replace('<br>', '</br>', $text);
    }

    /**
     * @param $userId
     * @return string
     */
    private function getUserImage($userId) {

        $this->loadModel('Candidate');
        $imageUrl = "";
        $image = $this->Candidate->findByUserId($userId)->first();

        if ($image->image != "") {
            if (preg_match('/facebook/', $image->image)) {
                $imageUrl = $image->image;
            } else {
                $imageUrl = Configure::read('dev_base_url') . 'img/user_images/' . $image->image_thumbnail;
            }
        }
        else {
            $imageUrl = Configure::read('dev_base_url') . 'img/user_images/user_placeholder.jpg';
        }
        return $imageUrl;
    }


    /**
     * Get the total unread messages count of an user.
     * @param $user_id
     * @param $chat_id
     * @return int
     */
    private function unreadMsgCount($user_id, $chat_id) {

        $conn = ConnectionManager::get('default');

        $stmt = $conn->prepare(
            'SELECT COUNT(DISTINCT unread_message.id) AS unread_messages_count
                        FROM
                          unread_message,
                          message
                        WHERE
                            unread_message.message_id = message.id AND 
                            unread_message.user_id = ' . $user_id .' AND message.chat_id = ' . $chat_id
        );

        $stmt->execute();
        $row = $stmt->fetch('assoc');

        return (int)$row['unread_messages_count'];
    }

}
