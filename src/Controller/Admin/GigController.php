<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
//use Kerox\Push\Adapter\Fcm;
//use Kerox\Push\Push;

/**
 * Gig Controller
 *
 * @property \App\Model\Table\GigTable $Gig
 */
class GigController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('SimpleImage');
    }

    /**
     * Index method
     *
     * @return list of gigs which are with status 1
     */
    public function index() {
     //   $this->Gig->recursive = 2;
        $gigCountry = $this->request->query('country');
        $gigStatus = $this->request->query('status');
        $searchFilter = array('Gig.status !=' => '4');

        if(isset($gigCountry) && $gigCountry != 'all') {
            $searchFilter['Gig.country'] =  $gigCountry;
        }

        if(isset($gigStatus) && $gigStatus != 'all') {
            $searchFilter['Gig.status'] = $gigStatus;
        }

        $this->paginate = [
            'fields' => ['id', 'title', 'sub_title1', 'sub_title2', 'start_date', 'created', 'modified', 'country', 'Company.name', 'User.name', 'active', 'status', 'address_id', 'Skills.skill'],
            'contain' => ['Company', 'User', 'Application', 'Address', 'Skills'],
            'conditions' => $searchFilter,
            'sortWhitelist'=>['Gig.id', 'Gig.title', 'Gig.sub_title1', 'Gig.sub_title2', 'Gig.start_date', 'Gig.created', 'Gig.modified', 'Gig.country', 'Gig.active', 'Company.name', 'User.name'],
            'order' => ['Gig.id' => 'desc'],
            'limit' => 30
        ];

        $gigs = $this->paginate($this->Gig);
        $this->loadModel('AdminNotification');
        $this->loadModel('Country');
        $this->loadModel('Address');

        foreach ($gigs AS $gig) {
            $gig->notification = 0;
            $query = $this->AdminNotification->find('all')->where(['gig_id' => $gig->id]);
            $notifications = $query->count();
            if ($notifications > 0) {
                $gig->notification = 1;
            }

            if($gig->address_id != 1) {
                $country = $this->Address->findById($gig->address_id)->first();
                $gig->countryName = $country->country_name;
            }
            else {
                $gig->countryName = "";
                if(!empty($gig->country)) {
                    $country = $this->Country->findByCountryCode($gig->country)->first();
                    $gig->countryName = $country->country_name;
                }
            }

            $gig->status = $this->Gig->getGigStatus($gig->status, true);
        }

        $base_url = Configure::read('dev_base_url');
        $country = $this->Country->find()
            ->select(['country_code', 'country_name'])
            ->where(['user_country' => 1])
            ->toArray();

        $status = array('2' => 'Deactivated', '3' => 'Expired', '0' => 'Pending', '1' => 'Published', '5' => 'Declined');

        $this->set(compact('gigs', 'base_url', 'country', 'status'));
        $this->set('_serialize', ['gigs']);
    }

    /**
     * View method
     *
     * @param string|null $id Gig id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $gig = $this->Gig->get($id, [
            'contain' => ['Company', 'Application', 'User']
        ]);
        $this->set('gig', $gig);

        $this->loadModel('Application');

        $query = $this->Application->find('all', [
                    'conditions' => ['Application.gig_id' => $id],
                    'order' => [
                        'Application.modified' => 'desc'
                    ]
                ])->contain(['User']);

        $results = $query->all();
        $data = $results->toArray();

        $this->loadModel('AdminNotification');

        foreach ($data AS $application) {
            $application->notification = 0;
            $query = $this->AdminNotification->find('all')->where(['chat_id' => $application->chat_id]);
            $notifications = $query->count();
            if ($notifications > 0) {
                $application->notification = 1;
            }
        }

        $this->set('gig', $gig);
        $this->set('applications', $data);
        $this->set('_serialize', ['data']);

    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $this->loadModel('Company');
        $gig = $this->Gig->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['status'] = 1;
            if (!isset($this->request->data['active'])) {
                $this->request->data['active'] = 0;
            }
            if($this->request->data['push_message'] == "")
            {
                $this->request->data['push_message'] = "";
                $gig->push_message = "";
                $gig['push_message'] ="";
                
            }
                $gig = $this->Gig->patchEntity($gig, $this->request->data);
                
            
            if ($this->Gig->save($gig)) {
                //Check if the Gig is active then insert a push notification
                
                if ($this->request->data['active'] == '1' && trim($this->request->data['push_message'])!="") { // If Gig is ACTIVE and Push message available
                    $pushTable = TableRegistry::get('push_queue');
                    $push = $pushTable->newEntity();
                    
                    $message = $this->request->data['push_message'];

                    $textJson = $this->jsonEncode($message);
                    $textJson = $this->truncateUtf8($textJson, Configure::read('MAX_MESSAGE_LENGTH'));

                    //$payload = '{"aps":{"alert":"' . $textJson . '","sound":"default","type":"1","gig_id":"'.$gig->id.'"}}';
                    $payload = $message;
                    //$push->param = json_encode(array('gig_id'=>$gig->id));
                    $push->param = '{"gig_id":"'.$gig->id.'"}';
                    $push->type = '1';
                    $push->payload = $payload;
                    $push->created = date('Y-m-d H:i:s');
                    $push->modified_ios = NULL;
                    $push->modified_and = NULL;
                    $push->gig_country = $gig->country;
                    
                    $pushTable->save($push);
                    
                    //$this->pushAndroid();
                 }

                $this->Flash->success(__('The gig has been saved.'));
                return $this->redirect(['action' => 'edit', $gig->id]);
            } else {
                $this->Flash->success(__('The gig could not be saved. Please, try again.'));
            }
        }
        $companies = $this->Company->find('all', [
                    'order' => [
                        'name' => 'ASC'
                    ]
                ])->where(['status' => 1]);
        $query = $this->Company->find('all', [
            'conditions' => ['Company.status' => 1],
            'order' => [
                'Company.name' => 'asc'
            ]
        ]);

        $this->loadModel('Country');
        $countries = $this->Country->find('all', [
           'conditions' => ['Country.user_country' => 1]
        ]);
        $country = [];
        foreach($countries as $count) {
            $country[$count->country_code] = $count->country_name;
        }

        $results = $query->all();
        $company = $results->toArray();
        $user_id = $_SESSION['Auth']['User']['id'];
        $base_url = Configure::read('dev_base_url');
        $this->set(compact('gig', 'company', 'user_id', 'base_url', 'country'));
        $this->set('_serialize', ['gig', 'company', 'companies', 'base_url', 'country']);

    }

    /**
     * Edit method
     *
     * @param string|null $id Gig id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $gig = $this->Gig->get($id, [
            'contain' => ['Company', 'Application', 'User', 'Address', 'Skills']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $gig = $this->Gig->patchEntity($gig, $this->request->data);
            if ($this->Gig->save($gig)) {
                $this->Flash->success(__('The gig has been updated.'));
                return $this->redirect(['action' => 'edit', $id]);
            } else {
                $this->Flash->success(__('The gig could not be updated. Please, try again.'));
            }
        }

        $this->loadModel('Skills');
        $this->loadModel('Application');
        $this->loadModel('GigCandidate');
        $this->loadModel('SkillImages');

        $skill = $this->SkillImages->findBySkillsId($gig->skill->id)
            ->contain(['Images'])->first();

        if(empty($gig->gig_image)) {
            $gig->gig_image = $skill->image->name;
        }

        $gig->status = $this->Gig->getGigStatus($gig->status, true);
        $gig->address = (!empty($gig->addres->street) ? $gig->addres->street . ', ' : '') . (!empty($gig->addres->zip) ? $gig->addres->zip . ', ' : '') . $gig->addres->city . ', ' . $gig->addres->country_name;
    //    $gig->country_code = $gig->addres->country_code;
        $gig->area = $gig->addres->area;

        if(empty($gig->title)) {
            $gig->title = $gig->skill->skill;
        }

        $applicants = $this->Application->find('all')
                            ->select(['id', 'created', 'Gig.id', 'User.name', 'User.id', 'c.is_verified'])
                            ->contain(['User', 'Gig'])
                            ->join([
                                'table' => 'candidate',
                                'alias' => 'c',
                                'type' => 'INNER',
                                'conditions' => 'c.user_id = Application.user_id'])
                            ->where(['gig_id' => $id])->toArray();

        $applicantsArr = [];
        foreach($applicants as $applicant) {
            $temp['application_id'] = $applicant->id;
            $temp['date'] = $applicant->created->format('Y-m-d');
            $temp['name'] = $applicant->user->name;
            $temp['user_id'] = $applicant->user->id;
            $temp['gig_id'] = $applicant->gig->id;
            $temp['candidate'] = $applicant->c['is_verified'];

            $selection = $this->GigCandidate->findByUserIdAndGigId($applicant->user->id, $applicant->gig->id)                                            ->first();
            $temp['selection'] = ($selection ? ucwords(strtolower($selection->selection)) : "");
            $temp['selection_id'] = ($selection ? $selection->id : "");

            $pastSelect = "";
            $pastSelectObj = $this->GigCandidate->findByUserId($applicant->user->id)
                ->contain(['Gig'])
                ->order(['GigCandidate.created' => 'DESC'])
                ->limit(10);
            if($pastSelectObj) {
                foreach($pastSelectObj as $past) {
                    if(empty($past->gig->title)) {
                        $skill = $this->Skills->findById($past->gig->skills_id)->first();
                        $title = $skill->skill;
                    } else {
                        $title = $past->gig->title;
                    }
                    $pastSelect .= ucwords(strtolower($past->selection)) . ' - ' . $title . ' - ' . $past->selection_date->format('Y-m-d') . ' </br >';
                }
            }
            $temp['pastSelect'] = $pastSelect;
            array_push($applicantsArr, $temp);
        }

        $this->set('gig', $gig);
        $this->set('applicantsArr', $applicantsArr);
        $this->set('_serialize', ['data']);

        $gigsterNeed = array('1','2','3','4','5','5+');
        $base_url = Configure::read('dev_base_url');

        $this->set(compact('gig', 'base_url', 'gigsterNeed'));
        $this->set('_serialize', ['gig', 'gigsterNeed']);

    }

    /**
     * Delete method
     *
     * @param string|null $id Gig id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $articlesTable = TableRegistry::get('Gig');
        $article = $articlesTable->get($id); // Return article with id 12

        $article->status = '4';
        $articlesTable->save($article);
        $this->Flash->success(__('Gig has been deleted.'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * listAll method
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */

    public function listAll() {
        if ($this->request->is('post')) {
            $this->paginate = ['page' => 1,
                'limit' => 10000,
                'fields' => [
                    'id', 'title', 'description', 'sub_title1', 'sub_title2', 'gig_image'
                ],
                'sortWhitelist' => [
                    'id', 'title', 'description', 'sub_title1', 'sub_title2', 'gig_image'
            ]];
            $this->set('data', $this->paginate($this->Gig));
            $return = new \stdClass();
            $return->status = 0;
            $this->set('return', $return);
            $this->set('_serialize', ['return', 'data']);
        }
    }
    
    /**
     * listGig Method
     * 
     */
    public function listGig() {
        if ($this->request->is('post')) {
            $gig_id = $this->request->data['gig_id'];
            $gig = $this->Gig->get($gig_id);
            if ($gig) {
                $this->set('data', $gig);
                $return = new \stdClass();
                $return->status = 0;
                $this->set('return', $return);
                $this->set('_serialize', ['return', 'data']);
            } else {
                $return = array('status' => 201);
            }
        }
    }

    public function apply() {
        $this->loadModel('Applicant');
        if ($this->request->is('post')) {
            $return = new \stdClass();
            $applicant = $this->Applicant->newEntity();

            $applicant->gig_id = $this->request->data['gig_id'];
            $applicant->user_id = $this->request->data['user_id'];
            $applicant->status = 1;

            if ($this->Applicant->save($applicant)) {
                $return->status = 0;
                $this->set('return', $return);
                $this->set('_serialize', $return);
            } else {
                $return->status = 202;
                $this->set('return', $return);
                $this->set('_serialize', $return);
            }
        }
    }

    public function deactivate($id = null, $status = null) {
        $this->request->allowMethod(['post', 'delete']);

        $gigTable = TableRegistry::get('Gig');
        $gig = $gigTable->get($id);
        $gig->active = $status;
        $gigTable->save($gig);

        $this->Flash->success(__('Gig has been updated.'));
        return $this->redirect(['action' => 'edit', $id]);
    }

    /** activate deactivate button */
    public function activate_update() {
        $gigData = $this->request->data['data'];
       // $desc = $this->request->data['desc'];
        $active = $this->request->data['active'];
        parse_str($gigData, $gigArr);

        $gigTable = TableRegistry::get('Gig');
        $gig = $gigTable->get($gigArr["gig_id"]);
        $gig->title = $gigArr["title"];
        $gig->description = $gigArr["description"];
        $gig->status = ($active == '1' ? '2' : '1');
        $gig->active = ($active == '1' ? '2' : '3');
        $date =  date('Y-m-d H:i:s');
        if($active == '2') {
            $gig->activate_date = $date;
        }
        $gigTable->save($gig);

        echo "1";
        die();
    }

    /** Active for 14 days and Decline */
    public function activeDays() {
        $this->loadModel('Event');

        $active = $this->request->data['active'];
        $gigId = $this->request->data['gigId'];
        $date = date('Y-m-d h:i:s');
        $expireDate = strtotime($date);
        $expire = strtotime("+14 day", $expireDate);
        //$expire = strtotime("+5 minutes", $expireDate);

        $gig = $this->Gig->get($gigId);
        $gig->status = $active;
        $gig->activate_date = $date;

        if($active == '1') {
            $gig->expire_date = date('Y-m-d h:i:s', $expire);
        }

        if($newGig = $this->Gig->save($gig)) {

            $gigObj = $this->Gig->findById($newGig->id)
                    ->contain(['User', 'Skills'])
                    ->first();
            $addTitle = (!empty($gigObj->title) ? $gigObj->title : $gigObj->skill->skill);
            $token = $gigObj->user->fcm_token;

            if($active == '5') {
                $payload = 'Your ad for '. $addTitle .' has not been approved, please contact us for more information at support@gigstr.com or try to post a new.';
                $notification = 'Declined ad';
            }
            else {
                $payload = 'Your ad for '. $addTitle .' has been approved and will be up for 14 days.';
                $notification = 'Published ad 14 days';
            }

            $key_value = json_encode([]);

            if(!empty($token)) {
                $this->loadModel('JobOffer');
                $this->loadModel('PushNotification');

                $eventId = $this->Event->eventSave($payload, $gigObj->user->id, 'COMPANY', 'MY_GIGS', $key_value, 0);
                $badgeCount = $this->JobOffer->badgeCount($gigObj->user->id);

                $this->PushNotification->sendPushEvent($payload, $token, $gigObj->user->id, 'COMPANY', 'MY_GIGS', $notification, $badgeCount->total, $key_value);

               /* $updateGig = $this->Gig->get($newGig->id);
                $updateGig->event_id = $eventId;
                $this->Gig->save($updateGig);*/
            }

        }
        echo "1";
        die();
    }

    public function upload() {
        if (!empty($_FILES)) {

            /* Path to Images folder */
            $dir = WWW_ROOT . 'img' . DS . 'gig_images';
            /* Explode the name and ext */
            $f = explode('.', $_FILES['file']['name']);
            $ext = '.' . end($f);
            /* Generate a Name in my case i use ID  & slug */
            $filename = time();

            /* Save image in the thumbnail folders and replace if exist */
            move_uploaded_file($_FILES['file']['tmp_name'], $dir . DS . $filename . $ext);

            $fn = $filename . $ext;

            $this->SimpleImage->load($dir . DS . $fn);

            // $this->SimpleImage->resize(1024,600);
            // $this->SimpleImage->save($dir . DS . $fn);

            $this->SimpleImage->resize(800, 600);
            $this->SimpleImage->save($dir . DS . 'thumblist-' . $fn);

            $this->SimpleImage->resize(200, 150);
            $this->SimpleImage->save($dir . DS . 'thumbadmin-' . $fn);

            $success_message = array('success' => 200,
                'filename' => $fn,
                'thumb-list' => 'thumblist-' . $fn,
                'thumb-admin' => 'thumbadmin-' . $fn);

            echo json_encode($success_message);
            die();
        }
    }

    public function link_test() {

        $notificationcount= 0;
        $this->set('notificationcount', $notificationcount);
    }

    public function link() {
        $gig_id = $this->request->data['gig_id'];

        $conn = ConnectionManager::get('default');

        $stmt = $conn->prepare('SELECT link FROM gig WHERE id = '. $gig_id);
        $stmt->execute();
        $links = $stmt->fetchAll('assoc');

        $return = new \stdClass();
        $return->user = $this->Auth->user('id');
        if(!empty($links[0]['link'])) {
            $return->link = $links[0]['link'];
        }
        else {
            $return->link = "NOLINK";
        }
        $this->set(compact('return'));
        $this->set('_serialize', ['return']);
        echo json_encode($return);
        die();
    }

    public function link_save() {
        $conn = ConnectionManager::get('default');
        $link = $this->request->data['link'];
        $gig_id = $this->request->data['gig_id'];
        $stmt = $conn->prepare('UPDATE gig SET link = "'. $link  .'" WHERE id = '. $gig_id);
        $stmt->execute();

    }

    public function ajaxSearch() {
        $keyWord = $this->request->data['search_keyword'];
        $this->Gig->recursive = 2;
        $this->paginate = [
            'fields' => ['id', 'title', 'sub_title1', 'sub_title2', 'created', 'modified', 'country', 'Company.name', 'User.name', 'active', 'status', 'address_id', 'Skills.skill'],
            'contain' => ['Company', 'User', 'Application', 'Skills', 'Address'],
            'conditions' => ['Gig.status !=' => '4', 'OR' => [
                'Gig.title like' => "%{$keyWord}%", 'Gig.sub_title2 like' => "%{$keyWord}%", 'Company.name like' => "%{$keyWord}%", 'Gig.country like' => "%{$keyWord}%", 'Skills.skill like' => "%{$keyWord}%"
                ]
            ],
            'order' => [
                'modified' => 'desc'
            ],
            'limit' => 10000
        ];

        $gigs = $this->paginate($this->Gig);
        $this->loadModel('AdminNotification');
        $this->loadModel('Country');
        $this->loadModel('Address');

        foreach ($gigs AS $gig) {
            $gig->notification = 0;
            $query = $this->AdminNotification->find('all')->where(['gig_id' => $gig->id]);
            $notifications = $query->count();
            if ($notifications > 0) {
                $gig->notification = 1;
            }

            if($gig->address_id != 1) {
                $country = $this->Address->findById($gig->address_id)->first();
                $gig->countryName = $country->country_name;
            }
            else {
                $gig->countryName = "";
                if(!empty($gig->country)) {
                    $country = $this->Country->findByCountryCode($gig->country)->first();
                    $gig->countryName = $country->country_name;
                }
            }

            $gig->status = $this->Gig->getGigStatus($gig->status, true);
        }

        echo json_encode($gigs);
        die;
    }

    /**
     * candidate shortlist function
     */
    public function selectCandidate() {
        $userId = $this->request->data['userId'];
        $gigId = $this->request->data['gigId'];
        $select = $this->request->data['select'];

        $this->loadModel('GigCandidate');
        $this->loadModel('Application');
        $date =  date('Y-m-d H:i:s');

        $selection = $this->GigCandidate->newEntity();
        $selection->gig_id = $gigId;
        $selection->selection = $select;
        $selection->user_id = $userId;
        $selection->selection_date = $date;

        $selectObj = $this->GigCandidate->findByGigIdAndUserId($gigId, $userId)->first();
        if($selectObj) {
            $selection = $this->GigCandidate->get($selectObj->id);
            $selection->selection = $select;
            $selection->selection_date = $date;
        }
        else {
            $selection = $this->GigCandidate->newEntity();
            $selection->gig_id = $gigId;
            $selection->selection = $select;
            $selection->user_id = $userId;
            $selection->selection_date = $date;
        }

        // Save to push table
        if($newSelection = $this->GigCandidate->save($selection)) {

            $gigObj = $this->Gig->findById($newSelection->gig_id)
                ->contain(['Skills', 'User'])->first();

            $title = (!empty($gigObj->title) ? $gigObj->title : $gigObj->skill->skill);
            $name = strtok($gigObj->user->name, " ");

            $this->loadModel('User');
            $userObj = $this->User->get($newSelection->user_id);
            $token = $userObj->fcm_token;
            $gigstrName = strtok($userObj->name, " ");

            $this->loadModel('PushNotification');
            $this->loadModel('JobOffer');
            $this->loadModel('Event');

            $redirect = "";
            $payload = "";
            $notification = "";
            switch($newSelection->selection) {
                case 'SHORTLIST' :
                    $payload = 'Yay, we sent your application to the job as '. $title . ' to '. $name .'. Make sure your profile is up to date.';
                    $redirect = 'GIG_DETAIL';
                    $notification = 'Shortlist';
                    break;
                case 'INTERVIEW' :
                    $payload = 'Congrats, you applied to your very first job - we will soon send you an email invitation to our introduction. Make sure your profile is up to date.';
                    $redirect = 'PROFILE';
                    $notification = 'Introduction';
                    break;
                case 'REJECTED' :
                    $redirect = 'GIG_LIST';
                    $notification = 'Declined';
                    $payload = 'Sorry, your application for '. $title .' was not accepted this time. Either the job has already been filled or your profile did not quite match the company\'s requirements. Don\'t worry! Keep an eye out for more gigs and we\'ll get you out gigging soon.';
                    break;
                case 'BANNED' :
                    $payload = 'Sorry, your application for '. $title .' did unfortunately not make it all the way. Either the job is already filled or your profile did not match the company\'s requirements. Please check out our other gigs.';
                    $redirect = 'GIG_LIST';
                    $notification = 'Banned';
                    break;
                default :
                    break;
            }

            if($newSelection->selection == 'SHORTLIST') {
                $key_value = json_encode([
                    "id" => $newSelection->gig_id,
                    "title" => $title,
                    "sub_title1" => "",
                    "sub_title2" => "",
                    "gig_image" => "",
                    "applied" => "1"
                ]);
            }
            else {
                $key_value = json_encode([]);
            }

            if($newSelection->selection != 'INTERVIEW') {
                $this->Event->eventSave($payload, $userObj->id, 'GIGSTR', $redirect, $key_value, 0);

                if (!empty($token)) {
                    $badgeCount = $this->JobOffer->badgeCount($userObj->id);
                    $this->PushNotification->sendPushEvent($payload, $token, $userObj->id, 'GIGSTR', $redirect,
                        $notification, $badgeCount->total, $key_value);
                }
            }

            /**
             * shortlist company push
             */
            $employerGig = $this->Gig->findById($newSelection->gig_id)->contain(['User', 'Skills'])->first();
            $employerToken = $employerGig->user->fcm_token;
            $skill = (!empty($employerGig->title) ? $employerGig->title : $employerGig->skill->skill);
            $compKeyValue = json_encode([]);
            $payloadComp = 'Yay, '. $gigstrName. ' applied to your ad for '. $skill;

            if($newSelection->selection == 'SHORTLIST') {
                $event_id = $this->Event->eventSave($payloadComp, $employerGig->user->id, 'COMPANY', 'MY_GIGS', $compKeyValue, 1);
                $applicationObj = $this->Application->findByUserIdAndGigId($userId, $gigId)->first();
                $application = $this->Application->get($applicationObj->id);
                $application->event_id = $event_id;
                $this->Application->save($application);
            }

            if(!empty($employerToken) && ($newSelection->selection == 'SHORTLIST')) {
                $badgeCount = $this->JobOffer->badgeCount($employerGig->user->id);
                $this->PushNotification->sendPushEvent($payloadComp, $employerToken, $employerGig->user->id, 'COMPANY', 'MY_GIGS', 'New Applicant', $badgeCount->total, $compKeyValue);
            }
        }
        echo $newSelection->id;
        die();
    }

    /**
     * send push in New Add from Backoffice
     */
    public function jobPush() {
        $pushMessage = $this->request->data['push_message'];
        $gigId = $this->request->data['gig_id'];
        $country = $this->request->data['gig_country'];
        $gigObj = $this->Gig->findById($gigId)->contain(['Skills', 'Address'])->first();
        $title = (!empty($gigObj->title) ? $gigObj->title : $gigObj->skill->skill);
        $push_area = $this->request->data['push_area'];

        $key_value = json_encode([
            "id" => $gigId,
            "title" => $title,
            "sub_title1" => "",
            "sub_title2" => "",
            "gig_image" => "",
            "applied" => "0"
        ]);

        $this->loadModel('PushNotification');
        $this->loadModel('Event');
        $this->loadModel('User');

        $pushMessage = $title . ': ' . $pushMessage;

        if($push_area == 1) {
            $area = $this->User->convertSwedishLetters($gigObj->addres->area);
            $topic = 'GIGSTR_' . strtoupper(str_replace(' ', '', $area));
        } else {
            $topic = 'GIGSTR_' . $country;
        }

        $pushSend = $this->PushNotification->sendPushEvent($pushMessage, '', 0, 'GIGSTR', 'GIG_DETAIL', 'Pushing Ad from Backoffice', 0, $key_value, $topic);

        $this->Event->eventSave($pushMessage, 0, 'GIGSTR', 'GIG_DETAIL', $key_value, 0, $country);

        if($pushSend) {
            echo 0;
        } else {
            echo 1;
        }
        die();
    }

}
