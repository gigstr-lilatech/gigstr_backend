<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Datasource\ConnectionManager;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Core\Configure;

/**
 * User Controller
 *
 * @property \App\Model\Table\UserTable $User
 */
class UserController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->viewBuilder()->layout('default');
    }

    public function login() {
        if ($this->request->is('post')) {
            $results = $this->User->find('all')->where(['email' => $this->request->data['email'], 'user_type' => '0']);
            $count = $results->count();
            if ($count > 0) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->success(__('Invalid username or password, try again'));
            } else {
                $this->Flash->success(__('Invalid username or password, try again'));
            }
        }
        $this->viewBuilder()->layout('login');
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

    public function resetPassword() {

        if ($this->request->is('post')) {
            $password = MD5($this->request->data['password']);
            $email = $this->request->data['email_reset'];
            $query = $this->User->query();
            $query->update()
                    ->set(['password' => $password])
                    ->where(['email' => $email])
                    ->execute();
            $this->set('response', 'done');
            $this->Flash->success(__('Password resetted successfully.'));
            
        } 
        
        if ($this->request->query['token'] != '') {
            $reset_token = $this->request->query['token'];

            $this->loadModel('Resetemail');
            $query = $this->Resetemail->find('all', [
                'conditions' => ['Resetemail.token' => $reset_token]
            ]);
            $count = $query->count();
            //echo $reset_token;
            //echo 'sdfsdf'.$count;die();
            if ($count > 0) {
                $token_details = $query->first();
                
               if ($token_details->status == '0') {
                   
                    $query = $this->Resetemail->query();
                    $query->update()
                            ->set(['status' => '1'])
                            ->where(['id' => $token_details->id])
                            ->execute();
                    $this->set('response', 'success');
                    $this->set('email', $token_details->email);
                } else {
                    $this->set('response', 'error');
                   // $this->Flash->success(__('This link has been used already.'));
                }
            }
        } 
        $this->viewBuilder()->layout('resetpassword');
    }

    public function forgotPassword() {


        if ($this->request->is('post')) {
            $return = new \stdClass();
            if ($this->request->is('post')) {
                $email = $this->request->data['email'];
                $query = $this->User->find('all', [
                    'conditions' => ['User.email' => $email]
                ]);
                $count = $query->count();

                if ($count > 0) {
                    $hasher = new DefaultPasswordHasher();
                    $resetEmail = TableRegistry::get('Resetemail');
                    $userDetails = $query->first();
                    $reset = $resetEmail->newEntity();

                    $reset->user_id = $userDetails->id;
                    $reset->email = $userDetails->email;
                    $reset->token = $hasher->hash(time());

                    $resetEmail->save($reset);

                    $email = new Email('default');
                    $email->template('resetpassword', 'default')
                            ->emailFormat('html')
                            ->from([Configure::read('from_email') => Configure::read('from_site')])
                            ->to($this->request->data['email'])
                            ->subject(Configure::read('reset_password_subject'))
                            ->send();
                    $this->Flash->success(__('Check your email for password reset instructions'));
                } else {
                    $this->Flash->success(__('Unable to find a user with the given email address. Please check and try again.'));
                }
            }
        }
        $this->viewBuilder()->layout('login');
    }

    /**
     * delete USER
     */
    public function deleteUser() {

        $conn = ConnectionManager::get('default');
        $user_id = $this->request->data['user'];

        $this->loadModel('Schedule');
        $shedules = $this->Schedule->findByGigstr($user_id)->first();

        if(!$shedules) {
            $stmt = $conn->prepare('UPDATE user u LEFT JOIN candidate c ON u.id = c.user_id 
        LEFT JOIN candidate_experiences ce ON c.id = ce.candidate_id 
        LEFT JOIN payout_data pd ON u.id = pd.user_id 
        SET u.name = "USER REMOVED", u.email = "USER REMOVED", u.password = "", u.fbid = NULL, u.fbtoken = NULL, u.device_token = NULL, u.udid = NULL, u.fcm_token = "", c.phone_number = "", c.address = "", c.address_co = "", c.zip_code = "", c.city = "", c.place_id_google = "", c.country = "", c.country_name = "", c.bank_name = "", c.account = "", c.notes = "", c.pitch = "", c.clearing_number = "", c.image = "", c.image_thumbnail = "", c.website = "", c.facebook = "", c.linkedin = "", c.stripe_id = "", c.card_token = "", c.card_brand = "", c.card_last4 = "", ce.employer = "", ce.position = "", ce.start_date = "0000-00-00", ce.end_date = "0000-00-00", pd.value = "", pd.payout_id = 0 WHERE u.id = ' . $user_id);
            $stmt->execute();

            $userObj = $this->User->get($user_id);
            $userObj->is_delete = 1;
            $this->User->save($userObj);

            echo "0";
            die;
        }
        else {
            echo "1";
            die;
        }
    }
    
    
    public function readImage(){
        $dir = WWW_ROOT . 'img' . DS . 'gig_images';
        $file_name = 'preview41.jpg';
        
        echo file_get_contents($dir. DS .$file_name);
        die();
    }

    /**
     * recommendations html page
     */
    public function recommendations() {
        $this->viewBuilder()->layout('recommendation');

        $base_url = Configure::read('dev_base_url');

        $this->set(compact('base_url'));
        $this->set('_serialize', ['base_url']);
    }

    public function recommendationSubmit(){

        if ($this->request->is('post')) {

            $user_id = $this->request->data['user_id'];
            $name = $this->request->data['name'];
            $company = $this->request->data['company'];
            $recommend = $this->request->data['recommendation'];
            $this->loadModel('Recommendation');

            $recommendation = $this->Recommendation->newEntity();
            $recommendation->user_id = $user_id;
            $recommendation->name = $name;
            $recommendation->company = $company;
            $recommendation->recommendation = $recommend;
            $recommendation->email = "";

            if($this->Recommendation->save($recommendation)) {
                echo '1';
            }
            else {
                echo '0';
            }
        }
        die();
    }
}
