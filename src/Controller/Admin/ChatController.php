<?php

namespace App\Controller\Admin;


use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use DateTime;
use DateTimeZone;

/**
 * Chat Controller
 *
 * @property \App\Model\Table\ChatTable $Chat
 */
class ChatController extends AppController {

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['User', 'Application', 'Gig', 'UnreadMessage', 'Message'],
            'limit' => 100
        ];
        $chats = $this->paginate($this->Chat);
        
        $this->set('chats', $chats);
        $this->set('_serialize', ['chats']);
    }

    /**
     * /admin/chat/allchat
     * list
     */

    public function allchat() {

        $this->loadModel('AdminNotification');
        $this->loadModel('AutoReply');
        $this->loadModel('Message');

        $base_url = Configure::read('dev_base_url');
        $autoreply = "0";
        $chatto = date('Y-m-d H:i:s', strtotime(date("Y-m-d")." +1 days"));
        $chatfrom =date('Y-m-d H:i:s', strtotime(date("Y-m-d"). " -3 days"));

        $conn = ConnectionManager::get('default');

        $stmt = $conn->prepare('SELECT
                    m.chat_id AS chat_id,
                    m.message AS message,
                    m.created AS created,
                    g.title AS title,
                    u.id AS id,
                    u.name AS name,
                    g.id AS gig_id
                  FROM message m
                  NATURAL JOIN (
                    SELECT   chat_id, MAX(id) AS id
                    FROM     message
                    GROUP BY chat_id
                  ) t
                  RIGHT JOIN chat c ON c.id = t.chat_id
                  INNER JOIN application a ON a.chat_id = m.chat_id
                  INNER JOIN gig g ON g.id = a.gig_id
                  INNER JOIN user u ON u.id = a.user_id
                  WHERE (g.status = 1) AND
                  (m.created >"'.$chatfrom.'" AND m.created <"'.$chatto.'")
                  ORDER BY m.created DESC');

        $stmt->execute();
        $chatsnew = $stmt->fetchAll('assoc');

        /**
        $this->loadModel('Message');
        $dataChat = $this->Message->find()
            ->hydrate(false)
            ->select(['m.chat_id', 'm.message', 'm.created', 'u.id', 'u.name', 'g.id', 'g.title', ])
            ->from(['(SELECT chat_id, MAX(id) AS id FROM message GROUP BY chat_id) last_message'])
            ->join([
                'm' => [
                    'table'      => 'message',
                    'type'       => 'INNER',
                    'conditions' => 'm.id = last_message.id',
                    ],
                'c' => [
                    'table'      => 'chat',
                    'type'       => 'INNER',
                    'conditions' => 'c.id = last_message.chat_id',
                    ],
                'a' => [
                    'table' => 'application',
                    'type' => 'INNER',
                    'conditions' => 'a.chat_id = m.chat_id',
                    ],
                'g' => [
                    'table' => 'gig',
                    'type' => 'INNER',
                    'conditions' => 'g.id = a.gig_id',
                    ],
                'u'  => [
                    'table' => 'user',
                    'type' => 'INNER',
                    'conditions' => 'u.id = a.user_id',
                    ]
            ])
            ->where(['g.status' => 1,
                'm.created > ' => $chatfrom,
                'm.created < ' => $chatto])
            ->order(['m.created' => 'DESC']);

        $this->loadComponent('Paginator');
        $chatsnew = $this->Paginator->paginate($dataChat, ['limit' => 30]);*/


        $notifications = $this->AdminNotification->find('list', [
            'conditions' => array('type' => 2),
            'keyField' => 'id',
            'valueField' => 'chat_id'
        ]);

        $notification_chat_ids = $notifications->toArray();

        $results = $this->AutoReply->find('list', [
            'conditions' => array('user_id' => Configure::read('admin_id')),
            'keyField' => 'id',
            'valueField' => 'auto_reply'
        ]);

        $auto_reply = $results->toArray();

        if(!empty($auto_reply)) {
            $autoreply = reset($auto_reply);
        }

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare(
            'SELECT country_code AS code, country_name AS name FROM `country` WHERE user_country = 1'
        );
        $stmt->execute();
        $countries = $stmt->fetchAll('assoc');

        $this->set(compact('chatsnew','autoreply', 'base_url', 'notification_chat_ids', 'countries'));
        $this->set('_serialize', ['chatsnew','autoreply','base_url','notification_chat_ids', 'countries']);
    }

    public function search()
    {
        $this->viewBuilder()->template('allchat');
        $conn = ConnectionManager::get('default');
        $chatto = date('Y-m-d', strtotime($this->request->data['chatto']." +1 days"));
        $chatfrom =date('Y-m-d', strtotime($this->request->data['chatfrom']." -1 days"));
        $base_url = Configure::read('dev_base_url');
        $this->loadModel('AutoReply');
        $autoreply = "0";
        $country = $this->request->data['country'];

        //Get all chat threads related to this user
        $stmt = $conn->prepare('SELECT
                    m.chat_id AS chat_id,
                    m.message AS message,
                    m.created AS created,
                    g.title AS title,
                    u.id AS id,
                    u.name AS name,
                    g.id AS gig_id
                  FROM message m NATURAL JOIN (
                    SELECT   chat_id, MAX(id) AS id
                    FROM     message
                    GROUP BY chat_id
                  ) t RIGHT JOIN chat c ON c.id = t.chat_id
                  INNER JOIN application a ON a.chat_id = m.chat_id
                  INNER JOIN gig g ON g.id = a.gig_id
                  INNER JOIN user u ON u.id = a.user_id
                  WHERE (g.status = 1) AND
                  (m.created >"'.$chatfrom.'" AND m.created <"'.$chatto.'")' .
            ($country != 'all' ? ' AND g.country = "'. $country .'"' : '')
                  .' ORDER BY m.created DESC');

        $stmt->execute();
        $chatsnew = $stmt->fetchAll('assoc');


        $this->loadModel('AdminNotification');
        $notifications = $this->AdminNotification->find('list', [
            'conditions' => array('type' => 2),
            'keyField' => 'id',
            'valueField' => 'chat_id'
        ]);
        $notification_chat_ids = $notifications->toArray();

        if(isset($this->request->data['unread'])) {
            foreach ($chatsnew as $key => $chat) {
                if (!in_array($chat["chat_id"], $notification_chat_ids)) {
                    unset($chatsnew[$key]);
                }
            }
        }

        $results = $this->AutoReply->find('list', [
            'conditions' => array('user_id' => Configure::read('admin_id')),
            'keyField' => 'id',
            'valueField' => 'auto_reply'
        ]);

        $auto_reply = $results->toArray();

        if(!empty($auto_reply)) {
            $autoreply = reset($auto_reply);
        }

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare(
            'SELECT country_code AS code, country_name AS name FROM `country` WHERE user_country = 1'
        );
        $stmt->execute();
        $countries = $stmt->fetchAll('assoc');

        $this->set(compact('chatsnew','autoreply', 'base_url', 'notification_chat_ids', 'countries'));
        $this->set('_serialize', ['chatsnew','autoreply','base_url','notification_chat_ids', 'countries']);

    }

    public function readchat()
    {
        $chat_id = $this->request->data['chat_id'];
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare(
                            'DELETE FROM admin_notification WHERE chat_id = ' . $chat_id . ' AND type = 2'
                    );
        $stmt->execute();
    }

    public function unread() {

        if ($this->request->is('post')) {
            $gig_id = $this->request->data['gig'];
            $chat_id = $this->request->data['chat'];
            $conn = ConnectionManager::get('default');

            $stmt = $conn->prepare(
                'SELECT id FROM admin_notification WHERE chat_id = ' . $chat_id . ' AND type = 2 AND gig_id=' . $gig_id
            );
            $stmt->execute();
            $chats = $stmt->fetchAll('assoc');

            if(empty($chats)) {
                $qry = $conn->prepare(
                    'INSERT INTO admin_notification (gig_id, chat_id, type) VALUE ('. $gig_id .', '. $chat_id .',"2")'
                );
                $qry->execute();
                echo '1';
              die();
            }
            else {
                $qry = $conn->prepare(
                    'DELETE FROM admin_notification WHERE gig_id = '. $gig_id .' AND chat_id = '. $chat_id .'                       AND type = "2"');
                $qry->execute();
                echo '0';
                die();
            }
        }
    }
    

    /**
     * View method
     *
     * @param string|null $id Chat id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $chat = $this->Chat->get($id, [
            'contain' => ['Application', 'Message']
        ]);
        $this->set('chat', $chat);
        $this->set('_serialize', ['chat']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $chat = $this->Chat->newEntity();
        if ($this->request->is('post')) {
            $chat = $this->Chat->patchEntity($chat, $this->request->data);
            if ($this->Chat->save($chat)) {
                $this->Flash->success(__('The chat has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('chat'));
        $this->set('_serialize', ['chat']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Chat id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $chat = $this->Chat->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chat = $this->Chat->patchEntity($chat, $this->request->data);
            if ($this->Chat->save($chat)) {
                $this->Flash->success(__('The chat has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The chat could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('chat'));
        $this->set('_serialize', ['chat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Chat id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $chat = $this->Chat->get($id);
        if ($this->Chat->delete($chat)) {
            $this->Flash->success(__('The chat has been deleted.'));
        } else {
            $this->Flash->error(__('The chat could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function listAll() {

        header('Content-type: application/json');
        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $conn = ConnectionManager::get('default');

            $stmt = $conn->prepare(
                    'SELECT '
                    . 'g.id AS gig_id, g.gig_image,g.title,g.sub_title1,c.modified, a.chat_id AS chat_id,a.id as application_id '
                    . 'FROM `application` a '
                    . 'INNER JOIN gig g ON a.gig_id = g.id '
                    . 'INNER JOIN chat c ON a.chat_id = c.id '
                    . 'WHERE `user_id` = ' . $user_id
            );

            $stmt->execute();

            // Read all rows.
            $rows = $stmt->fetchAll('assoc');

            $data = array();
            $i = 0;
            $this->loadModel('Message');
            foreach ($rows as $row) {
                // Do something with the row.
                $data[$i]['gig_id'] = $row['gig_id'];
                $data[$i]['image'] = $row['gig_image'];
                $data[$i]['title'] = $row['title'];
                $data[$i]['sub_title1'] = $row['sub_title1'];
                $data[$i]['modified'] = $row['modified'];
                $data[$i]['chat_id'] = $row['chat_id'];
                $data[$i]['application_id'] = $row['application_id'];

                $stmt = $conn->prepare(
                        'SELECT 
                            COUNT(unread_message.id) AS unread_messages,
                            MAX(unread_message.message_id) AS max_id 
                        FROM
                            unread_message,
                            message,
                            application
                        WHERE 
                            unread_message.message_id = message.id AND 
                            message.chat_id = application.chat_id AND 
                            unread_message.user_id = ' . $user_id . ' AND 
                            message.chat_id = ' . $row['chat_id'] . ';'
                );

                $stmt->execute();
                $row = $stmt->fetch('assoc');
                $data[$i]['unread_message'] = ($row['unread_messages'] > 0) ? $row['unread_messages'] : 0;
                $max_message_id = $row['max_id'];

                $message = $this->Message->find('all')->where(['id' => $max_message_id]);
                foreach ($message AS $row) {
                    $data[$i]['unread_message_max'] = base64_decode($row->message);
                }
                $i++;
            }
            $stmt = $conn->prepare(
                    'SELECT
                          COUNT(DISTINCT unread_message.id) AS unread_messages_count
                        FROM
                          unread_message,
                          message,
                          application
                        WHERE
                            unread_message.message_id = message.id AND 
                            message.chat_id = application.chat_id AND unread_message.user_id = ' . $user_id . ' '
                    . 'GROUP BY application.id'
            );

            $stmt->execute();
            $row = $stmt->fetch('assoc');
            $return = new \stdClass();
            $return->status = 0;
            $return->chat = $data;
            $return->total_unread = ($row['unread_messages_count'] > 0) ? $row['unread_messages_count'] : 0;

            echo json_encode($return);
            die();
        }
    }

    public function send() {
        //header('Content-type: application/json');
        if ($this->request->is('post')) {
            $message = $this->request->data['message'];
            $created_by = $this->request->data['user_id'];
            $chat_id = $this->request->data['chat_id'];

            //Insert the message
            $this->loadModel('Message');
            $messageEntity = $this->Message->newEntity();

            $messageEntity->message = base64_encode($message);
            $messageEntity->created_by = $created_by;
            $messageEntity->chat_id = $chat_id;

            $this->Message->save($messageEntity);

            //Get users in the chat
            $this->loadModel('ChatGroup');
            $chat_group_members = $this->ChatGroup->find('all')
                    ->where(['chat_id' => $chat_id])
                    ->where(['user_id <> ' => $created_by]);


            $this->loadModel('UnreadMessage');
            $unreadMessageEntity = $this->UnreadMessage->newEntity();
            foreach ($chat_group_members AS $chat_group) {
                $unreadMessageEntity->message_id = $messageEntity->id;
                $unreadMessageEntity->user_id = $chat_group['user_id'];
                $this->UnreadMessage->save($unreadMessageEntity);

                $pushTable = TableRegistry::get('PushQueue');
                $push = $pushTable->newEntity();

               // $message = "You have a new chat message from Gigstr.";

                $textJson = $this->jsonEncode($message);
                $textJson = $this->truncateUtf8($textJson, Configure::read('MAX_MESSAGE_LENGTH'));

                //$payload = '{"aps":{"alert":"' . $textJson . '","sound":"default","type":"2","chat_id":"' . $chat_id . '"}}';
                $payload = $message;

                //$push->param = json_encode(array('chat_id'=>$chat_id,'user_id'=>$chat_group['user_id']));
                $push->param = '{"chat_id":"' . $chat_id . '", "user_id":"' . $chat_group['user_id'] . '"}';
                $push->type = '2';
                $push->payload = $payload;
                $push->created = date('Y-m-d H:i:s');
                $push->modified_ios = NULL;
                $push->modified_and = NULL;

                $pushTable->save($push);
            }

            //Update the chat table with the latest modified time
            $query = $this->Chat->query();
            $query->update()
                    ->set(['modified' => date('Y-m-d H:i:s')])
                    ->where(['id' => $chat_id])
                    ->execute();

            $this->loadModel('Application');
            $query2 = $this->Application->query();
            $query2->update()
                    ->set(['modified' => date('Y-m-d H:i:s')])
                    ->where(['chat_id' => $chat_id])
                    ->execute();

            $connection = ConnectionManager::get('default');
            $connection->delete('admin_notification', ['chat_id' => $chat_id]);

            $return = new \stdClass();
            $return->status = 0;
            $return->message_id = $messageEntity->id;

            $chatstartdate = new DateTime($messageEntity->created, new DateTimeZone('UTC'));
            $chatstartdate->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $return->created = $chatstartdate->format('Y-m-d H:i');
                //date('Y-m-d H:i', strtotime($messageEntity->created));

            echo json_encode($return);
            die();
        }
    }

    public function listChat() {
        if ($this->request->is('post')) {
            $this->loadModel('Message');
            $chat_id = $this->request->data['chat_id'];
            $user_id = $this->request->data['user_id'];
            $candidate_id = $this->request->data['candidate_id'];

            $conn = ConnectionManager::get('default');
            $stmt = $conn->prepare(
                    'SELECT m.*, u.name '
                    . 'FROM `message` m '
                    . 'INNER JOIN user u ON m.`created_by` = u.`id` '
                    . 'WHERE m.`chat_id` = ' . $chat_id
            );

            $stmt->execute();

            // Read all rows.
            $results = $stmt->fetchAll('assoc');

            $html = '';
            foreach ($results AS $result) {

                $chatStartDate = new DateTime($result['created'], new DateTimeZone('UTC'));
                $chatStartDate->setTimezone(new DateTimeZone('Europe/Stockholm'));

                if ($result['created_by'] == $candidate_id)
                    $html .= '<span class="right_msg">' . base64_decode($this->makeClickableLinks($result['message'])) . '</span><br/><span class="chat_time_right">' . $chatStartDate->format('Y-m-d H:i') . ', ' . $result['name'] . '</span><br/>';
                else
                    $html .= '<span class="left_msg">' . base64_decode($this->makeClickableLinks($result['message'])) . '</span><br/><span class="chat_time_left">' . $chatStartDate->format('Y-m-d H:i') . ', ' . $result['name'] . '</span><br/>';
            }
            $stmt = $conn->prepare(
                    'DELETE um FROM unread_message um INNER JOIN message m ON m.id = um.message_id WHERE m.chat_id = ' . $chat_id . ' AND um.user_id = ' . $user_id . ';'
            );

            $conn->delete('admin_notification', ['chat_id' => $chat_id, 'type' => '2']);

            $stmt->execute();
            echo $html;
            die();
        }
    }

    public function getUnread() {

        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $chat_id = $this->request->data['chat_id'];
            $candidate_id = $this->request->data['candidate_id'];
            $conn = ConnectionManager::get('default');

            $stmt = $conn->prepare(
                    'SELECT m.*, u.name, um.id AS unread_id '
                    . 'FROM `message` m '
                    . 'INNER JOIN unread_message um ON um.message_id = m.id '
                    . 'INNER JOIN user u ON m.created_by = u.id '
                    . 'WHERE m.`chat_id` = ' . $chat_id . ' AND um.`user_id` = ' . $user_id . ' '
            );

            $stmt->execute();

            // Read all rows.
            $results = $stmt->fetchAll('assoc');
            $html = '';
            foreach ($results AS $result) {
                if ($result['created_by'] == $candidate_id)
                    $html .= '<span class="right_msg">' . $this->makeClickableLinks(base64_decode($result['message'])) . '</span><br/><span class="chat_time_right">' . date('Y-m-d', strtotime($result['created'])) . ', ' . $result['name'] . '</span><br/>';
                else
                    $html .= '<span class="left_msg">' . $this->makeClickableLinks(base64_decode($result['message'])) . '</span><br/><span class="chat_time_left">' . date('Y-m-d', strtotime($result['created'])) . ', ' . $result['name'] . '</span><br/>';
            }
            $stmt = $conn->prepare(
                    'DELETE um FROM unread_message um INNER JOIN message m ON m.id = um.message_id WHERE m.chat_id = ' . $chat_id . ' AND um.user_id = ' . $user_id . ';'
            );

            $stmt->execute();

            echo $html;
            die();
        }
    }

    public function makeClickableLinks($text) {
        //return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href=\'$1" target="_blank">$1</a>', $s);
        //$reg_exUrl = "/(http|https|ftp|ftps|www.)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        $text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ginascheme://$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
        $text = str_replace('http', 'ginascheme', $text);
        $text = str_replace('>ginascheme://', '>', $text);
        return($text);
    }

    public function fixChat() {
        echo 'rr';
        die();
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT * FROM message'
        );

        $stmt->execute();

        // Read all rows.
        $results = $stmt->fetchAll('assoc');
        foreach ($results AS $result) {

            if (base64_encode(base64_decode($result['message'])) != $result['message']) {
                $message = base64_encode($result['message']);
                $stmt = $conn->prepare('UPDATE message SET message = "' . $message . '" WHERE id = ' . $result['id']);
                //echo $message;
                $stmt->execute();
            }
        }
        die();
    }

    public function writeToLog($message) {
        if ($fp = fopen(Configure::read('logfile'), 'at')) {
            fwrite($fp, date('c') . ' ' . $message . PHP_EOL);
            fclose($fp);
        }
    }
    
    public function autoreply() {

        $this->loadModel('AutoReply');
        $autoEntity = $this->AutoReply->newEntity();
        
        $id=Configure::read('admin_id');
        
        $this->loadModel('AutoReply');
        $results = $this->AutoReply->find('all')->where(['user_id' => $id]);
        $result = $results->first();

        if($this->request->data['checked'] == 'true')
        {
            if($result->id > 0)
            {
                $autoreply = $this->AutoReply->get($result->id);
                $autoreply->auto_reply = 1;
                $this->AutoReply->save($autoreply);
            }
            else
            {
                $autoEntity->user_id = $id;
                $autoEntity->auto_reply = 1;
                $this->AutoReply->save($autoEntity);
            }
            
        }
        else
        {
            $autoreply = $this->AutoReply->get($result->id);
            $autoreply->auto_reply = 0;
            $this->AutoReply->save($autoreply);
        }
        
        die();
    }
}
