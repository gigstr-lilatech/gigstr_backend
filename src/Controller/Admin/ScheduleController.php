<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use DateTime;
use DateTimeZone;

/**
 * Company Controller
 *
 * @property \App\Model\Table\CompanyTable $Company
 */
class ScheduleController extends AppController {

    /**
     * Index method
     *
     * @return void
     */
    public $helpers = array('GoogleMap');

    public function inbox()
    {
        $this->paginate = [
            'conditions' => ['Schedule.status !=' => '0'],
            'contain' => ['Job','User'],
            'order' => [
                    'shift_from' => 'ASC'
            ],
            'limit' => 1000000
        ];
        $schedules = $this->paginate($this->Schedule);
        
        $this->set(compact('schedules'));
        $this->set('_serialize', ['schedules']);
    }

    /**
     * Export CSV
     */
    public function export() {
        $conn = ConnectionManager::get('default');

        if(isset($this->request->query['from'])) {
            $company = $this->request->query['company'];
            $gig_id = $this->request->query['gig'];
            $schedulefrom = date('Y-m-d', strtotime($this->request->query['from']));
            $scheduleto = date('Y-m-d', strtotime($this->request->query['to']." +1 days"));

            $gigstr = $this->request->query['gigstr'];
            $unassigned =   $this->request->query['unassigned'];
            $gig_arr = explode(' ', $gigstr);

            $qry = 'SELECT s.id, j.title, s.gig_id, s.gigstr, s.shift_from, s.shift_to, s.checkin, s.checkout, s.start, s.stop, s.comment, s.attested, s.job_offer_id
                FROM schedule s
                INNER JOIN job j ON j.id = s.gig_id
                WHERE s.status != 0 AND s.shift_from > "' . $schedulefrom .'" AND s.shift_from < "' . $scheduleto . '"';
            if($company != 'all' && $gig_id == 'all') {
                $qry .= ' AND j.company_id = ' . $company;
            }
            else if($company == 'all' && $gig_id != 'all') {
                $qry .= ' AND s.gig_id = ' . $gig_id;
            }
            else if($company != 'all' && $gig_id != 'all') {
                $qry .= ' AND j.company_id = ' . $company . ' AND s.gig_id = ' . $gig_id;
            }

            if(!empty($gigstr)) {
                $qry .= ' AND s.gigstr = ' . $gig_arr[0];
            }

            if(isset($unassigned) && !empty($unassigned)) {
                $qry .= ' AND s.gigstr IS NULL ';
            }
            $qry .= ' ORDER BY s.shift_from DESC';

            $stmt = $conn->prepare($qry);
            $stmt->execute();
            $schedules = $stmt->fetchAll('assoc');
        }
        else { //Get allschedule
            $stmt = $conn->prepare('SELECT s.id, s.gig_id, j.title, s.gigstr,s.shift_from,s.shift_to,s.checkin,s.checkout,s.start,s.stop,s.comment,s.attested, s.job_offer_id FROM schedule s, job j WHERE j.id = s.gig_id AND s.status != 0 ORDER BY s.shift_from DESC');
            $stmt->execute();
            $schedules = $stmt->fetchAll('assoc');
        }

        $this->loadModel('User');
        $this->loadModel('Candidate');
        $this->loadModel('Country');
        $this->loadModel('Job');
        $this->loadModel('JobOffer');

        $shiftData = array();
        $gigTitleData =  array();

        $formatschdules = array();
        $shiftDataTitle = array();
        $i = 0;
        foreach ($schedules as $schedule) {
            
            $shift_from = new \DateTime($schedule['shift_from'], new \DateTimeZone('UTC'));
            $shift_from->setTimezone(new \DateTimeZone('Europe/Stockholm'));
            $formatschdules[$i][0] = $shift_from->format('Y-m-d H:i');
            
            $shift_to = new \DateTime($schedule['shift_to'], new \DateTimeZone('UTC'));
            $shift_to->setTimezone(new \DateTimeZone('Europe/Stockholm'));
            $formatschdules[$i][1] = $shift_to->format('Y-m-d H:i');

            $totalSchedule = $this->timeDiff($shift_from, $shift_to);
            $formatschdules[$i][2] = $totalSchedule;

            $name = "";
            $country = "";

            if($schedule['gigstr']) {
                $candidate = $this->Candidate->findByUserId($schedule['gigstr'])->contain(['User'])->first();
                $name = ($candidate ? $candidate->user->name : "");
                if(isset($candidate->country)) {
                    $countryObj = $this->Country->findByCountryCode($candidate->country)->first();
                    $country = ($countryObj ? $countryObj->country_name : "");
                }
            }

            $gigObj = $this->Job->findById($schedule['gig_id'])->contain(['Company'])->first();
            $company = ($gigObj ? $gigObj->company->name : "");

            $formatschdules[$i][3] = $country;
            $formatschdules[$i][4] = $company;
            $formatschdules[$i][5] = $schedule['title'];
            $formatschdules[$i][6] = $schedule['gigstr'];
            $formatschdules[$i][7] = $name;

            if($schedule['checkin'] != null) {
                $checkin = new \DateTime($schedule['checkin'], new \DateTimeZone('UTC'));
                $checkin->setTimezone(new \DateTimeZone('Europe/Stockholm'));
                $formatschdules[$i][8] = $checkin->format('Y-m-d H:i');
            }
            else {
                $formatschdules[$i][8] = "";
            }
            
            if($schedule['checkout'] != null) {
                $checkout = new \DateTime($schedule['checkout'], new \DateTimeZone('UTC'));
                $checkout->setTimezone(new \DateTimeZone('Europe/Stockholm'));
                $formatschdules[$i][9] = $checkout->format('Y-m-d H:i');

                $totalCheckout = $this->timeDiff($checkin, $checkout);
                $formatschdules[$i][10] = $totalCheckout;
            }
            else {
                $formatschdules[$i][9] = "";
                $formatschdules[$i][10] = "";
            }
            
            if($schedule['start'] != null) {
                $start = new \DateTime($schedule['start'], new \DateTimeZone('UTC'));
                $start->setTimezone(new \DateTimeZone('Europe/Stockholm'));
                $formatschdules[$i][11] = $start->format('Y-m-d H:i');
            }
            else {
                $formatschdules[$i][11] = "";
            }
            
            if($schedule['stop'] != null) {
                $stop = new \DateTime($schedule['stop'], new \DateTimeZone('UTC'));
                $stop->setTimezone(new \DateTimeZone('Europe/Stockholm'));
                $formatschdules[$i][12] = $stop->format('Y-m-d H:i');

                $totalWork = $this->timeDiff($start, $stop);
                $formatschdules[$i][13] = $totalWork;
            }
            else {
                $formatschdules[$i][12] = "";
                $formatschdules[$i][13] = "";
            }

            if($schedule['job_offer_id'] != 0) {
                $jobOffer = $this->JobOffer->findById($schedule['job_offer_id'])->first();
                $candidate = $this->Candidate->findByUserId($jobOffer->created_by)->first();
                $formatschdules[$i][14] = $jobOffer->hourly_rate;
                $formatschdules[$i][15] = ($jobOffer->hourly_rate * $totalSchedule);
                $formatschdules[$i][16] = $jobOffer->hourly_cost;
                $formatschdules[$i][17] = ($jobOffer->hourly_cost * $totalSchedule);
                $manualpayment = ($candidate->manual_payment == 1 ? 'Yes' : 'No');

                if($jobOffer->coupon_code_id != 0) {
                    $this->loadModel('CouponCode');
                    $coupon  = $this->CouponCode->findById($jobOffer->coupon_code_id)->first();
                    $promo_code = $coupon->code;
                }
                else {
                    $promo_code = "";
                }
            }
            else {
                $formatschdules[$i][14] = "";
                $formatschdules[$i][15] = "";
                $formatschdules[$i][16] = "";
                $formatschdules[$i][17] = "";
                $manualpayment = "";
                $promo_code = "";
            }

            $replacelinebreak = str_replace(array("\r\n", "\n\r", "\n", "\r"), ' ', $schedule['comment']);
            $formatschdules[$i][18] = strip_tags($replacelinebreak);

            $formatschdules[$i][19] = $manualpayment;

            if($schedule['attested'] == "1") {
                $formatschdules[$i][20] = "Yes";
            }
            else {
                $formatschdules[$i][20] = "No";
            }

            $formatschdules[$i][21] = $promo_code;

            if(isset($this->request->query['gig']) && $this->request->query['gig'] != 'all') {

                $gigTitle = $conn->prepare('SELECT t.id, s.data_field_value, t.field_name, s.schedule_id, t.field_title, t.field_type, s.gig_template_shift_data_id, t.checkbox_parent, t.field_data
                          FROM schedule_data_field s
                          LEFT JOIN gig_template_shift_data t ON s.gig_template_shift_data_id = t.id
                          WHERE s.schedule_id = ' . $schedule['id'] . ' AND t.field_type != "checkbox-group"  ORDER BY t.created ASC');

                $gigTitle->execute();
                $gigTitleData = $gigTitle->fetchAll('assoc');
                array_push($shiftDataTitle, $gigTitleData);
            }
            $i++;
        }

        if (! function_exists('array_column')) {
            function array_column(array $input, $columnKey, $indexKey = null) {
                $array = array();
                foreach ($input as $value) {
                    if ( !array_key_exists($columnKey, $value)) {
                        trigger_error("Key \"$columnKey\" does not exist in array");
                        return false;
                    }
                    if (is_null($indexKey)) {
                        $array[] = $value[$columnKey];
                    }
                    else {
                        if ( !array_key_exists($indexKey, $value)) {
                            trigger_error("Key \"$indexKey\" does not exist in array");
                            return false;
                        }
                        if ( ! is_scalar($value[$indexKey])) {
                            trigger_error("Key \"$indexKey\" does not contain scalar value");
                            return false;
                        }
                        $array[$value[$indexKey]] = $value[$columnKey];
                    }
                }
                return $array;
            }
        }

        $heders = array('Shift From','Shift To','Hours scheduled','Country','Company','Gig', 'ID','Gigstr','Checkin','Checkout','Total hrs by Gigstr check in','Start','Stop','Total hrs','Hourly salary','Total salary','Hourly client cost','Total client cost','Comment','Manual payment','Attested', 'Promo code');

        $name = array();
        $count = 0;
        $titleArr = array();

        foreach($shiftDataTitle as $title) {
            foreach($title as $head) {
               array_push($titleArr, $head['field_title']);
                $count ++;
                array_push($name, $head['field_name']);
            }
        }
        $fieldNames = array_unique($name);
        $fieldTitle = array_unique($titleArr);

        foreach($shiftDataTitle as $key => $dataShiftArr) {
            foreach($dataShiftArr as $dataArr) {
            $j = 21;
                foreach ($fieldNames as $nameArr) {
                    if ($nameArr == $dataArr['field_name']) {
                        if ($dataArr['field_type'] == 'title') {
                            $checked = $this->getSelectCheckboxValue($dataArr['id'], $dataArr['schedule_id']);
                            $formatschdules[$key][$j] = $checked;
                        }
                        elseif ($dataArr['field_type'] == 'checkbox-group') {
                        }
                        elseif ($dataArr['field_type'] == 'select') {
                            $selectData = unserialize($dataArr['field_data']);
                            foreach ($selectData as $select) {
                                if ($select['value'] == $dataArr['data_field_value']) {
                                    $formatschdules[$key][$j] = $select['label'];
                                }
                            }
                        } else {
                            $formatschdules[$key][$j] = $dataArr['data_field_value'];
                        }
                    }
                    $j++;
                }
            }
        }

        $colCount = 20 + (count($fieldNames));
        $newData = array();

        foreach($formatschdules as $key => $val) {
            for ($i = 0; $i <= $colCount; $i++) {
                if (!isset($val[$i])) {
                    $val[$i] = "";
                }
            }
            ksort($val);
            array_push($newData, $val);
        }

        foreach ($fieldTitle as $shifts) {
            array_push($heders, $shifts);
        }

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=Schedules.csv');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM

        $fp = fopen('php://output', 'w');
        fputcsv($fp, $heders);
        
        foreach ($newData as $formatschdule) {
            fputcsv($fp, $formatschdule);
        }
        die();
    }

    /**
     * @param $start
     * @param $end
     * @return string
     */
    private function timeDiff($start, $end) {

        $interval = $start->diff($end);
        $min =  $interval->format('%i');
        $mins = round($min / 60 * 100);
        $timeDiff = $interval->format('%h') . '.' . ($mins < 10 ? '0'.$mins : $mins);

        return $timeDiff;
    }

    /**
     * @param $checkboxParent
     * @param $scheduleId
     * @return string
     */
    private function getSelectCheckboxValue($checkboxParent, $scheduleId) {

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT t.field_title
                          FROM gig_template_shift_data t
                          LEFT JOIN schedule_data_field s  ON t.id = s.gig_template_shift_data_id
                          WHERE t.checkbox_parent = ' . $checkboxParent . ' AND t.is_delete = 0 AND s.data_field_value = "1" AND s.schedule_id = '. $scheduleId .' ORDER BY t.created ASC');
        $stmt->execute();
        $shiftData = $stmt->fetchAll('assoc');
        $shiftCount = count($shiftData);

        $selected = "";
        foreach($shiftData as $key => $shift) {
            $selected .= $shift["field_title"] . ($shiftCount == ($key +1) ? '' : ', ');
        }

        return $selected;
    }

    /**
     *
     */
    public function allschedule() {
        
        $backdate = date('Y-m-d', strtotime(date("Y-m-d")));
        $frontdate =date('Y-m-d', strtotime(date("Y-m-d") . " +30 days"));
            
        $this->paginate = [
            'conditions' => ['Schedule.shift_from >=' =>$backdate, 'Schedule.shift_from <=' =>$frontdate, 'Schedule.status !=' => '0'],
            'contain' => ['Job','User'],
            'order' => [
                    'shift_from' => 'ASC'
            ],
            'limit' => 30
        ];
        $schedules = $this->paginate($this->Schedule);

        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
                    'order' => [
                        'name' => 'ASC'
                    ]
                ])->where(['status' => 1]);

        $this->loadModel('Job');
        $gigs = $this->Job->find('all',['order' => [
            'title' => 'ASC']]);

        if (isset($_GET["company"])) {
            $companyid = $_GET["company"];
            $searchgigs = $this->Job->find('all',['order' => [
                'title' => 'ASC']])->where(['company_id' => $companyid]);
        }
        else {
            $searchgigs = $this->Job->find('all',['order' => [
                'title' => 'ASC']]);
        }

        $base_url = Configure::read('dev_base_url');
        $page = 'schedule';

        $this->set(compact('schedules', 'companies','gigs', 'searchgigs','base_url', 'page'));
        $this->set('_serialize', ['schedules', 'companies','gigs', 'searchgigs', 'base_url', 'page']);

    }


    public function search() {
        $this->viewBuilder()->template('allschedule');

        $scheduleto = date('Y-m-d', strtotime($this->request->query['to']." +1 days"));
        $schedulefrom =date('Y-m-d', strtotime($this->request->query['from']));

        if($this->request->query['company'] != 'all' && $this->request->query['gig'] == 'all') {
            $searchfilter = array(
                'Schedule.shift_from > ' => $schedulefrom,
                'Schedule.shift_from < ' => $scheduleto,
                'Job.company_id' =>$this->request->query['company'],
                'Schedule.status !=' =>0,
                );

        }
        else if($this->request->query['company'] == 'all' && $this->request->query['gig'] != 'all') {
            $searchfilter = array(
                'Schedule.shift_from > ' => $schedulefrom,
                'Schedule.shift_from < ' => $scheduleto,
                'Schedule.gig_id' =>$this->request->query['gig'],
                'Schedule.status !=' =>0,
                );

        }
        else if($this->request->query['company'] != 'all' && $this->request->query['gig'] != 'all') {
            $searchfilter = array(
                'Job.company_id' =>$this->request->query['company'],
                'Schedule.gig_id' =>$this->request->query['gig'],
                'Schedule.shift_from > ' => $schedulefrom,
                'Schedule.shift_from < ' => $scheduleto,
                'Schedule.status !=' =>0,
                );
        }
        else {
            $searchfilter = array(
                'Schedule.shift_from > ' => $schedulefrom,
                'Schedule.shift_from < ' => $scheduleto,
                'Schedule.status !=' =>0,
                );
        }

        if(!empty($this->request->query['gigstr'])) {
            $gig_arr = explode(' ', $this->request->query['gigstr']);
            $searchfilter['Schedule.gigstr = '] = $gig_arr[0];
        }

        if(isset($this->request->query['unassigned'])) {
            $searchfilter['Schedule.gigstr IS'] =   NULL;
        }

        $this->paginate = [
            'conditions' => $searchfilter,
            'contain' => ['Job','User'],
            'order' => [
                'shift_from' => 'ASC'
            ],
            'limit' => 30
        ];

        $schedules = $this->paginate($this->Schedule);

        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
                'order' => [
                    'name' => 'ASC'
                ]
            ])->where(['status' => 1]);

        $this->loadModel('Job');
        $companyid = $this->request->query['company'];

        if ($companyid != 'all') {
            $searchgigs = $this->Job->find('all', ['order' => [
                'title' => 'ASC']])->where(['company_id' => $companyid]);
        }
        else {
            $searchgigs = $this->Job->find('all', ['order' => [
                'title' => 'ASC']]);
        }

        $gigs = $this->Job->find('all',['order' => [
            'title' => 'ASC']]);

        $base_url = Configure::read('dev_base_url');
        $page = 'search';

        $this->set(compact('schedules', 'companies', 'gigs', 'searchgigs', 'base_url', 'page'));
        $this->set('_serialize', ['schedules','companies', 'gigs', 'searchgigs', 'base_url', 'page']);
    }
    
    public function savedescription() {
        $scheduleid = $_POST["id"];
        $schedulename = $_POST["name"];
        $summernote = $_POST["description"];
        
        $this->loadModel('Schedule');
        $schedule = $this->Schedule->get($scheduleid);

        if($schedulename == 'shift_description') {
            $schedule->shift_description = $summernote;
        } else {
            $schedule->comment = $summernote;
        }
        $this->Schedule->save($schedule);

        echo 1;
        die();
    }
    
    function getdescription() {
        $scheduleid = $_POST["id"];
        $type = $_POST["type"];
        $this->loadModel('Schedule');
        $schedule = $this->Schedule->get($scheduleid);

        if($type == 'description') {
            echo $schedule->shift_description;
        }
        else {
            echo $schedule->comment;
        }
        die();
    }
            
    function process() {
        if (isset($_POST["companyid"])) {
            $companyid = $_POST["companyid"];
            
            $conn = ConnectionManager::get('default');
            if($companyid == 'all')
            {
                $stmt = $conn->prepare('SELECT id,title FROM job');
                $stmt->execute();
                $gigs = $stmt->fetchAll('assoc');
                
                if($gigs)
                {
                $html="<span style='font-size:12px; display: block;'>Gig</span>";
                $html.="<select name='gig' class='form-control input-sm' style='width:160px;'>";
                $html.="<option value='all'>All</option>";
                foreach($gigs as $i => $gig)
                {
                     $html.="<option value=".$gig['id'].">".$gig['title']."</option>";
                }
                $html.="</select>";
            }
            }
            else
            {
                $this->viewBuilder()->template('allschedule');
                $stmt = $conn->prepare('SELECT id,title FROM job where company_id="'.$companyid.'"');
                $stmt->execute();
                $gigs = $stmt->fetchAll('assoc'); 
                
                if($gigs)
                {
                    $html="<span style='font-size:12px; display: block;'>Gig</span>";
                    $html.="<select name='gig' class='form-control input-sm' style='width:160px;'>";
                    $html.="<option value='all'>All</option>";
                    foreach($gigs as $i => $gig)
                    {
                         $html.="<option value=".$gig['id'].">".$gig['title']."</option>";
                    }
                    $html.="</select>";
                }
                else
                {
                    $html="<span style='font-size:12px; display: block;'>Gig</span>";
                    $html.="<select name='gig' class='form-control input-sm' style='width:160px;'>";
                    $html.="<option value=''>No gigs for selected company</option>";
                    $html.="</select>";
                }
            }
            echo $html;
            die();
        }
    }
    
    function addgigprocess() {
        if (isset($this->request->data['companyid'])) {
            $companyId = $this->request->data['companyid'];
            $this->viewBuilder()->template('allschedule');

            $this->loadModel('Job');
            $gigs = $this->Job->findByCompanyId($companyId);

            if($gigs) {
                $html="<span>Gig</span>";
                $html.="<select name='gig' class='form-control' id='gigId'>
                        <option value=''>Select a Gig</option>";
                foreach($gigs as $i => $gig) {
                     $html.="<option value=".$gig->id.">".$gig->title."</option>";
                }
                $html.="</select>";
            } else {
                $html="<span>Gig</span>";
                $html.="<select name='gig' class='form-control' id='gigId'>";
                $html.="<option value=''>No gigs for selected company</option>";
                $html.="</select>";
            }
            echo $html;
            die();
        }
    }
    
    public function delete($id = null) {
       $this->viewBuilder()->template('allschedule');

        $conn = ConnectionManager::get('default');

        $company = $this->request->data['company'];
        $to = $this->request->data['to'];
        $from = $this->request->data['from'];
        $gig = $this->request->data['gig'];
        //$sid = $_POST['sid'];
        $sids = $this->request->data['sid'];

        $shceduleTable = TableRegistry::get('Schedule');

        $ret = 0;
        foreach($sids as $sid) {
            $shcedule = $shceduleTable->get($sid);
            $shcedule->status = '0';
            if ($shceduleTable->save($shcedule)) {
                $ret = 1;
            }
        }

        $sch_arra = array(
            'ret' =>$ret,
            'company' => $company,
            'to' => $to,
            'from' => $from,
            'gig' => $gig
        );
        echo json_encode($sch_arra);
        die();
    }

    /**
     *
     */
    public function add() {

        $schedule = $this->Schedule->newEntity();
        $conn = ConnectionManager::get('default');
        
        if ($this->request->is('post')) {
       
            $datefrom = new \DateTime($this->request->data['from']);
            $timefrom = new \DateTime($this->request->data['from_time']);
            $dateto = new \DateTime($this->request->data['to']);
            $timeto = new \DateTime($this->request->data['to_time']);
            $mergefrom = new \DateTime($datefrom->format('Y-m-d') .' '.$timefrom->format('H:i:s'));
            $mergeto = new \DateTime($dateto->format('Y-m-d') .' '.$timeto->format('H:i:s'));
            $shift_from = new \DateTime(date("Y-m-d H:i:s", strtotime($mergefrom->format('Y-m-d H:i:s'))), new \DateTimeZone('Europe/Stockholm'));
            $shift_from->setTimezone(new \DateTimeZone('UTC'));
            $shift_to = new \DateTime(date("Y-m-d H:i:s", strtotime($mergeto->format('Y-m-d H:i:s'))), new \DateTimeZone('Europe/Stockholm'));
            $shift_to->setTimezone(new \DateTimeZone('UTC'));
            
            $this->request->data['from_time'];
            $schedule->gig_id = $this->request->data['gig'];
            $schedule->gigstr = $this->request->data['candidate'];
            $schedule->shift_from = $shift_from->format('Y-m-d H:i');
            $schedule->shift_to = $shift_to->format('Y-m-d H:i');
            $schedule->shift_description = $this->request->data['description'];
            $schedule->internal_user_id =  $this->Auth->user('id');

            if ($result = $this->Schedule->save($schedule)) {
                $this->Flash->success(__('Shift added successfully! Add another shift by pressing "ADD SHIFT" one more time'));
            } else {
                $this->Flash->success(__('The shift could not be updated. Please, try again.'));
            }

            /*** Save Schedule template */
            $this->loadModel('ScheduleGigTemplate');
            $this->loadModel('GigTemplate');

            $scheduleTemplate = $this->ScheduleGigTemplate->findByScheduleId($result->id)->toArray();
            $formJson = $this->GigTemplate->findByGigId($result->gig_id)->toArray();
            if($scheduleTemplate) {
                /** edit should be in edit page */
                $newScheduleTemplate = $this->ScheduleGigTemplate->find();
            }
            else {
                $newScheduleTemplate = $this->ScheduleGigTemplate->newEntity();

                $newScheduleTemplate->schedule_id = $result->id;
                $newScheduleTemplate->form_json = $formJson[0]->form_json;
                $this->ScheduleGigTemplate->save($newScheduleTemplate);
            }

            /** save in ScheduleDataField table*/
            $this->loadModel('GigTemplateShiftData');
            $this->loadModel('ScheduleDataField');

            $gigTemplate = $this->GigTemplateShiftData->find()
                                ->select(['id'])
                                ->where(['gig_template_id' => $formJson[0]->id, 'is_delete' => 0])
                                ->toArray();

            foreach($gigTemplate as $gig) {
                $newScheduleData = $this->ScheduleDataField->newEntity();
                $newScheduleData->schedule_id = $result->id;
                $newScheduleData->gig_template_shift_data_id = 	$gig->id;
                $newScheduleData->data_field_value = "";
                $this->ScheduleDataField->save($newScheduleData);
            }
        }

        $companyid=$this->request->data['company'];
        $stmtnew = $conn->prepare('SELECT id,title FROM job');
             
        $stmtnew->execute();
        $this->loadModel('Job');
        $gigs = $this->Job->find('all')->where(['company_id' => $companyid]);
                
        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
                    'order' => [
                        'name' => 'ASC'
                    ]
                ])->where(['status' => 1]);
        
        $this->loadModel('User');
        $candidates = $this->User->find('all')->where(['user_type' => '1']);

        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
        $user_id = $user_data['id'];
        $base_url = Configure::read('dev_base_url');

        $this->set(compact('schedule','user_id', 'companies','base_url', 'candidates','gigs'));
        $this->set('_serialize', ['schedule', 'user_id','companies','base_url', 'candidates','gigs']);
    }

    public function edit() {
        $id = $this->request->data['id'];
        $name = $this->request->data['name'];
        $value = $this->request->data['value'];
        
        $this->loadModel('Schedule');
        $schedule = $this->Schedule->get($id);
        
        if($name == "shift_from" || $name == "shift_to" || $name == "start" || $name == "stop") {
            $date = new \DateTime($value, new \DateTimeZone('Europe/Stockholm'));
            $date->setTimezone(new \DateTimeZone('UTC'));
            $schedule->$name = $date->format('Y-m-d H:i');
        }
        elseif($name == "attested") {
            $attest = $schedule->attested;
            if($attest == '' || $attest==0) {
                $schedule->attested = 1;
            }
            else {
                $schedule->attested = 0;
            }
        }
        elseif($name == "not_work") {
            $schedule->not_work = ($schedule->not_work == 0 ? 1 : 0);
        }
        else{
            $schedule->$name = $value;
        }
        
        if ($this->Schedule->save($schedule)) {
            echo 1;
            die();
        } 
    }
    
    public function find($team= null) {
        $this->autoRender = false;
        $team = $_GET['term'];
        $this->loadModel('User');
      
        $candidates = $this->User->find('all', array(
        'conditions' => array('User.name LIKE' => '%' . $team . '%', 'user_type' => '1'),
        'fields' => array('name', 'id')));

         $resultArr = array();
         $i =0;
            foreach($candidates as $candidate) {
                    $resultArr[$i]['id']=$candidate['id'];
                    $resultArr[$i]['label']=$candidate['id']." ".$candidate['name'];
                    $resultArr[$i]['value']=$candidate['name'];
                    $i++;
    
            }

            echo json_encode($resultArr);
            exit();
    }
    
    public function changedatefrom()
    {
        $conn = ConnectionManager::get('default');
        
        $stmt = $conn->prepare('SELECT * from schedule where shift_from >"2016-11-01" AND shift_from <"2017-03-26"');

        $stmt->execute();
        $schedules = $stmt->fetchAll('assoc');
        
        foreach($schedules as $schedule)
        {
            $this->loadModel('Schedule');
            $schedulechange = $this->Schedule->get($schedule['id']);
            
            $newdate =date('Y-m-d H:i:s', strtotime($schedule['shift_from'] . " +1 hours"));
            
            $schedulechange->shift_from = $newdate;
            
            $this->Schedule->save($schedulechange);
            print_r($schedulechange->id."saved</br>");
        }
        die();
    }
    
    public function changedateto()
    {
        $conn = ConnectionManager::get('default');
        
        $stmt = $conn->prepare('SELECT * from schedule where shift_to >"2016-11-01" AND shift_to <"2017-03-26"');

        $stmt->execute();
        $schedules = $stmt->fetchAll('assoc');
        
        foreach($schedules as $schedule)
        {
            $this->loadModel('Schedule');
            $schedulechange = $this->Schedule->get($schedule['id']);
            
            $newdate =date('Y-m-d H:i:s', strtotime($schedule['shift_to'] . " +1 hours"));
            
            $schedulechange->shift_to = $newdate;
            
            $this->Schedule->save($schedulechange);
            print_r($schedulechange->id."saved</br>");
        }
        die();
    }
    
    public function checkschedule()
    {
        $this->loadModel('Schedule');
        
        $schedules = $this->Schedule->find('all')->where(['gigstr' => $this->request->data['value'] , 'status' => 1]);
        
        foreach($schedules as $schedule) {
            $formatdate= date('Y-m-d' , strtotime($schedule->shift_from));
            $backdate= date('Y-m-d' , strtotime($schedule->shift_to));
            $fromdate = date('Y-m-d' , strtotime($this->request->data['fromdate']));
            $todate = date('Y-m-d' , strtotime($this->request->data['todate']));
            
            if($formatdate == $fromdate || $formatdate == $todate || $backdate == $fromdate || $backdate == $todate)
            {
                echo 1;
                die();
            }
        }
        echo 0;
        die();
    }
    
    public function checkgigstr()
    {
        $this->loadModel('Schedule');
        
        $id = $this->request->data['id'];
        $gigtr = $this->Schedule->get($id);
        
        $name = $this->request->data['name'];
        $date = date('Y-m-d' , strtotime($this->request->data['fromdate']));
        $dateformat = date('Y-m-d' , strtotime($gigtr->$name));
        
        if($dateformat != $date)
        {
            $schedules = $this->Schedule->find('all')->where(['gigstr' => $gigtr->gigstr , 'id !=' => $id,'status' => 1]);

            foreach($schedules as $schedule) {
                $formatdate= date('Y-m-d' , strtotime($schedule->shift_from));
                $backdate= date('Y-m-d' , strtotime($schedule->shift_to));

                if($formatdate == $date || $backdate == $date)
                {
                        echo 1;
                        die();
                    }
                }
        }
        echo 0;
        die();
    }

    public function gigslist() {

        $this->loadModel('User');

        $users = $this->User->find()->select(['id', 'name'])->where(['is_delete' => 0]);

        $user_arr = array();
        foreach($users as $key => $user) {
            $user_arr[$key] = $user['id'] . ' ' . $user['name'];
        }

        $this->set(compact('user_arr'));
        $this->set('_serialize', ['user_arr']);

    }

    public function map() {

        $this->viewBuilder()->layout('map');
        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT s.checkin_lat, s.checkin_long, u.name, s.checkin, g.title
                            FROM schedule s
                            LEFT JOIN user u ON s.gigstr = u.id
                            LEFT JOIN job g ON s.gig_id = g.id
                            WHERE s.checkout IS NULL AND s.checkin_lat <> ""
                            AND s.status != 0');
        $stmt->execute();
        $locations = $stmt->fetchAll('assoc');

        $this->set(compact('locations'));
        $this->set('_serialize', ['locations']);
    }

    public function checkinmap() {

        $conn = ConnectionManager::get('default');
        $id = $this->request->query['id'];

        $stmt = $conn->prepare('SELECT s.checkin_lat, s.checkin_long, u.name, s.checkin, s.checkout,
                            s.checkout_lat, s.checkout_long, g.title
                            FROM schedule s
                            LEFT JOIN user u ON s.gigstr = u.id
                            LEFT JOIN job g ON s.gig_id = g.id
                            WHERE s.id = ' . $id . ' AND s.status != 0');
        $stmt->execute();
        $locations = $stmt->fetch('assoc');

        $this->set(compact('locations'));
        $this->set('_serialize', ['locations']);
    }

    public function attest() {

        $this->loadModel('ScheduleDataField');
        $this->viewBuilder()->template('attestschedule');
        $this->paginate = [
            'conditions' => ['Schedule.status !=' => 0, 'Schedule.gigstr IS NOT' => NULL, 'Schedule.checkout IS NOT' => NULL, 'Schedule.attested =' => 0],
            'contain' => ['Job','User'],
            'order' => [
                'Schedule.shift_from' => 'ASC'
            ],
            'limit' => 30
        ];
        $schedules = $this->paginate($this->Schedule);

        foreach($schedules as $schedule) {
            if($schedule->gigstr_rating == 0) {
                $shifts = $this->getLastGigstrRatingCount($schedule->id, $schedule->gigstr);
                $count = 0;
                foreach($shifts as $shift) {
                    if($shift['gigstr_rating'] != 0) {
                        break;
                    }
                    $count ++;
                }
                $schedule->gigstr_rating = $count . ' shift(s) ago';
            }

             $dataFieldArr = $this->ScheduleDataField->find()
                ->hydrate(false)
                ->select(['ScheduleDataField.data_field_value', 'g.field_title', 'g.field_type'])
                ->join([
                   'table' => 'gig_template_shift_data',
                   'alias' => 'g',
                   'type' => 'LEFT',
                   'conditions' => 'g.id = ScheduleDataField.gig_template_shift_data_id'
               ])
                ->where(['schedule_id' => $schedule->id])
                ->toArray();

            if(!empty($dataFieldArr)) {
                foreach($dataFieldArr as $arr) {
                    if($arr['g']['field_type'] != 'title') {
                        $firstDataField = $dataFieldArr[0]['g']['field_title'];
                        //. " : " . $dataFieldArr[0]['data_field_value'];
                        break;
                    }
                }
            } else {
                $firstDataField = "";
            }
            $schedule->firstDataField = $firstDataField;

            $scheduleImages = $this->getScheduleImage($schedule->id);
            $schedule->attestImages = $scheduleImages;

            $start = new DateTime($schedule->start);
            $end = new DateTime($schedule->stop);
            $interval = $start->diff($end);

            $min =  $interval->format('%i');
            $mins = round($min / 60 * 100);
            $schedule->timeDiff = $interval->format('%h') . '.' . ($mins < 10 ? '0'.$mins : $mins);

            $shift_from = new DateTime($schedule->shift_from, new DateTimeZone('UTC'));
            $shift_from->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->from_popup = $shift_from->format('Y-m-d H:i');

            $shift_to = new DateTime($schedule->shift_to, new DateTimeZone('UTC'));
            $shift_to->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->to_popup = $shift_to->format('Y-m-d H:i');

            $checkin = new DateTime($schedule->checkin, new DateTimeZone('UTC'));
            $checkin->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->checkin_popup = $checkin->format('Y-m-d H:i');

            $checkout = new DateTime($schedule->checkout, new DateTimeZone('UTC'));
            $checkout->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->checkout_popup = $checkout->format('Y-m-d H:i');
        }

        $this->loadModel('Job');
        $gigs = $this->Job->find('all',['order' => [
            'title' => 'ASC']]);

        if (isset($_GET["company"])) {
            $companyId = $_GET["company"];
            $searchGigs = $this->Job->find('all',['order' => [
                'title' => 'ASC']])->where(['company_id' => $companyId]);
        }
        else {
            $searchGigs = $this->Job->find('all',['order' => [
                'title' => 'ASC']]);
        }

        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
            'order' => [
                'name' => 'ASC'
            ]
        ])->where(['status' => 1]);

        $countries = $this->getCountry();
        $base_url = Configure::read('dev_base_url');

        $this->loadModel('User');
        $users = $this->User->find()
            ->select(['id', 'name']);
        $userArr = array( 0 => '');
        foreach($users as $key => $user) {
            $userArr[$user['id']] = $user['id'] . ' ' . $user['name'];
        }

        $this->set(compact('schedules', 'searchGigs', 'companies', 'countries', 'base_url', 'userArr'));
        $this->set('_serialize', ['schedules', 'searchGigs', 'companies', 'countries', 'base_url', 'userArr']);
    }


    public function attestSearch() {

        $this->viewBuilder()->template('attestschedule');
        $country = $this->request->query['country'];
        $company = $this->request->query['company'];
        $gig = $this->request->query['gig'];
        $gigstr = $this->request->query['gigstr'];
        $attested = '';
        if(isset($this->request->query['attested'])) {
            $attested = $this->request->query['attested'];
        }

        $searchFilter = array(
            'Schedule.status !=' =>  0,
            'Schedule.checkout IS NOT' => NULL
        );

        if($country != 'all') {
            $searchFilter['Job.country = '] = $country;
        }
        if($company != 'all') {
            $searchFilter['Job.company_id = '] = (int)$company;
        }
        if($gig != 'all') {
            $searchFilter['Schedule.gig_id = '] = (int)$gig;
        }
        if(empty($gigstr)) {
            $searchFilter['Schedule.gigstr IS NOT'] = NULL;
        } else {
            $searchFilter['Schedule.gigstr = '] = (int)$gigstr;
        }

        if($attested) {
            $searchFilter['Schedule.attested = '] = 1;
        } else {
            $searchFilter['Schedule.attested = '] = 0;
        }

        $this->paginate = [
            'conditions' => $searchFilter,
            'contain' => ['Job','User'],
            'order' => [
                'shift_from' => 'ASC'
            ],
            'limit' => 30
        ];

        $this->loadModel('ScheduleDataField');
        $schedules = $this->paginate($this->Schedule);

        foreach($schedules as $schedule) {
            if($schedule->gigstr_rating == 0) {
                $shifts = $this->getLastGigstrRatingCount($schedule->id, $schedule->gigstr);
                $count = 0;
                foreach($shifts as $shift) {
                    if($shift['gigstr_rating'] != 0) {
                        break;
                    }
                    $count ++;
                }
                $schedule->gigstr_rating = $count . ' shift(s) ago';
            }

            $dataFieldArr = $this->ScheduleDataField->find()
                ->hydrate(false)
                ->select(['ScheduleDataField.data_field_value', 'g.field_title'])
                ->join([
                    'table' => 'gig_template_shift_data',
                    'alias' => 'g',
                    'type' => 'LEFT',
                    'conditions' => 'g.id = ScheduleDataField.gig_template_shift_data_id'
                ])
                ->where(['schedule_id' => $schedule->id])
                ->toArray();

            if(!empty($dataFieldArr)) {
                foreach($dataFieldArr as $arr) {
                    if($arr['g']['field_type'] != 'title') {
                        $firstDataField = $dataFieldArr[0]['g']['field_title'];
                        //. " : " . $dataFieldArr[0]['data_field_value'];
                        break;
                    }
                }
            } else {
                $firstDataField = "";
            }
            $schedule->firstDataField = $firstDataField;

            $scheduleImages = $this->getScheduleImage($schedule->id);
            $schedule->attestImages = $scheduleImages;

            $start = new DateTime($schedule->start);
            $end = new DateTime($schedule->stop);
            $interval = $start->diff($end);

            $min =  $interval->format('%i');
            $mins = round($min / 60 * 100);
            $schedule->timeDiff = $interval->format('%h') . '.' . ($mins < 10 ? '0'.$mins : $mins);


            $shift_from = new DateTime($schedule->shift_from, new DateTimeZone('UTC'));
            $shift_from->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->from_popup = $shift_from->format('Y-m-d H:i');

            $shift_to = new DateTime($schedule->shift_to, new DateTimeZone('UTC'));
            $shift_to->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->to_popup = $shift_to->format('Y-m-d H:i');

            $checkin = new DateTime($schedule->checkin, new DateTimeZone('UTC'));
            $checkin->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->checkin_popup = $checkin->format('Y-m-d H:i');

            $checkout = new DateTime($schedule->checkout, new DateTimeZone('UTC'));
            $checkout->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $schedule->checkout_popup = $checkout->format('Y-m-d H:i');
        }

        $this->loadModel('Job');
        $gigs = $this->Job->find('all',['order' => [
            'title' => 'ASC']]);

        if (isset($_GET["company"])) {
            $companyId = $_GET["company"];
            $searchGigs = $this->Job->find('all',['order' => [
                'title' => 'ASC']])->where(['company_id' => (int)$companyId]);
        }
        else {
            $searchGigs = $this->Job->find('all',['order' => [
                'title' => 'ASC']]);
        }

        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
            'order' => [
                'name' => 'ASC'
            ]
        ])->where(['status' => 1]);

        $countries = $this->getCountry();
        $base_url = Configure::read('dev_base_url');

        $this->loadModel('User');
        $users = $this->User->find()
            ->select(['id', 'name']);
        $userArr = array(
            0 => ''
        );
        foreach($users as $key => $user) {
            $userArr[$user['id']] = $user['id'] . ' ' . $user['name'];
        }

        $this->set(compact('schedules', 'searchGigs', 'companies', 'countries', 'base_url', 'userArr'));
        $this->set('_serialize', ['schedules', 'searchGigs', 'companies', 'countries', 'base_url', 'userArr']);

    }

    /**
     * check the attest check box
     */
    public function attested() {
        $sids = $this->request->data['sid'];
        $allIds = $this->request->data['allIds'];

        $this->loadModel('Schedule');

        if($sids) {
            foreach ($sids as $sid) {
                $schedule = $this->Schedule->get($sid);
                $schedule->attested = 1;
                $this->Schedule->save($schedule);
            }
        }
        else {
            foreach($allIds as $id) {
                $schedule = $this->Schedule->get($id);
                $schedule->attested = 0;
                $this->Schedule->save($schedule);
            }
        }

        echo 1;
        die();
    }

    public function updateAttested() {

        $update_comment = $this->request->data['update_comment'];
        $scheduleid = $this->request->data['scheduleid'];
        $update_start = $this->request->data['update_start'];
        $update_stop = $this->request->data['update_stop'];

        $schedule = $this->Schedule->get($scheduleid);
        $schedule->comment = $update_comment;

        $date = new \DateTime($update_start, new \DateTimeZone('Europe/Stockholm'));
        $date->setTimezone(new \DateTimeZone('UTC'));
        $schedule->start = $date->format('Y-m-d H:i:s');

        $date = new \DateTime($update_stop, new \DateTimeZone('Europe/Stockholm'));
        $date->setTimezone(new \DateTimeZone('UTC'));
        $schedule->stop = $date->format('Y-m-d H:i:s');
        $result = array();

        $start = new DateTime($schedule->start);
        $end = new DateTime($schedule->stop);
        $interval = $start->diff($end);
        $min =  $interval->format('%i');
        $mins = round($min / 60 * 100);

        $startTime = $start->format('H:i');
        $endTime = $end->format('H:i');

        if($this->Schedule->save($schedule)) {
            $result['val'] = 1;
            $result['timeDiff'] = $interval->format('%h') . '.' . ($mins < 10 ? '0'.$mins : $mins);
            $result['startTime'] = $startTime;
            $result['endTime'] = $endTime;

            $shift_from = new DateTime($schedule->shift_from, new DateTimeZone('UTC'));
            $shift_from->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $result['from'] = $shift_from->format('Y-m-d H:i');

            $shift_to = new DateTime($schedule->shift_to, new DateTimeZone('UTC'));
            $shift_to->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $result['to'] = $shift_to->format('Y-m-d H:i');

            $checkin = new DateTime($schedule->checkin, new DateTimeZone('UTC'));
            $checkin->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $result['checkin'] = $checkin->format('Y-m-d H:i');

            $checkout = new DateTime($schedule->checkout, new DateTimeZone('UTC'));
            $checkout->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $result['checkout'] = $checkout->format('Y-m-d H:i');

            $startSch = new DateTime($schedule->start, new DateTimeZone('UTC'));
            $startSch->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $result['startTime'] = $startSch->format('H:i');

            $endSch = new DateTime($schedule->stop, new DateTimeZone('UTC'));
            $endSch->setTimezone(new DateTimeZone('Europe/Stockholm'));
            $result['endTime'] = $endSch->format('H:i');

            echo json_encode($result);
            die();
        }
        else {
            $result['val'] = 0;
            echo json_encode($result);
            die();
        }
    }

    /**
     *  update the admin rating for rating pupup
     */
    public function updateRating() {
        $popupRating = $this->request->data['popupRating'];
        $scheduleid = $this->request->data['scheduleId'];
        $popupComment = $this->request->data['popupComment'];

        $schedule = $this->Schedule->get($scheduleid);
        $schedule->internal_rating = $popupRating;
        $schedule->internal_rating_comment = $popupComment;

        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
        $userId = $user_data['id'];

        $schedule->internal_user_id = $userId;

        $result = array();

        $this->loadModel('User');
        $user = $this->User->get($schedule->gigstr);

        if($this->Schedule->save($schedule)) {
            $result['val'] = 1;
            $result['internal_rating'] = $schedule->internal_rating;
            $result['internal_rating_comment'] = $schedule->internal_rating_comment;
            $result['gigstr_rating'] = $schedule->gigstr_rating;
            $result['gigstr_rating_comment'] = $schedule->gigstr_rating_comment;
            $result['name'] = $user->name;
            echo json_encode($result);
            die();
        }
        else {
            echo 0;
            die();
        }
    }

    /**
     * save gig template
     */
    public function addGigTemplate() {
        $fromJson = $this->request->data['fromJson'];
        $desc = $this->request->data['desc'];
        $gigId = $this->request->data['gigId'];
        $conn = ConnectionManager::get('default');

        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
        $user = $user_data['id'];

        $this->loadModel('GigTemplateShiftData');
        $this->loadModel('GigTemplate');
        $template = $this->GigTemplate->find()
            ->where(['gig_id' => $gigId])->first();

        if(!$template) { // add new gig template

            $fromArr = json_decode($fromJson, true);
            $jsonForm = json_decode(json_encode($fromArr));

            foreach($jsonForm as $arr) {
                if($arr->type == 'checkbox-group' || $arr->type == 'select') {
                    $values = $arr->values;
                    foreach($values as $key => $val) {
                        if($val->value == "") {
                            $label = str_replace(' ', '', $val->label);
                            $val->value = $label;
                        }
                    }
                }
            }

            $arrayShift = json_decode(json_encode($jsonForm), true);
            $shiftJson = json_encode($arrayShift);

            $gigTemplate = $this->GigTemplate->newEntity();
            $gigTemplate->gig_id = $gigId;
            $gigTemplate->description = $desc;
            $gigTemplate->form_json = $shiftJson;
            $gigTemplate->created_by = $user;
            $result = $this->GigTemplate->save($gigTemplate);
            $gigTemplateId = $result->id;

            foreach ($arrayShift as $key => $shift) {

                if(isset($shift['required'])) {
                    $required = "1";
                } else { $required = "0"; }

                $shiftLabel = html_entity_decode(str_replace('<br>', '', $shift['label']));

                if ($shift['type'] == 'checkbox-group') {
                    $values = $shift['values'];

                    /*save checkbox group title*/
                    $shiftData = $this->GigTemplateShiftData->newEntity();
                    $shiftData->gig_template_id = $gigTemplateId;
                    $shiftData->field_name = $shift['name'];
                    $shiftData->field_type = 'title';
                    $shiftData->field_data = '';
                    $shiftData->field_title = trim($shiftLabel);
                    $shiftData->checkbox_value = '';
                    $shiftData->field_order = $key;
                    $shiftData->required = $required;
                    $shiftTitle = $this->GigTemplateShiftData->save($shiftData);
                    $checkBoxParent = $shiftTitle->id;

                    /*save checkboxes*/
                    foreach ($values as $checkKey => $value) {

                        $valueLabel = html_entity_decode(str_replace('<br>', '', $value['label']));

                        $shiftData = $this->GigTemplateShiftData->newEntity();
                        $shiftData->gig_template_id = $gigTemplateId;
                        $shiftData->field_name = $shift['name'];
                        $shiftData->field_type = $shift['type'];
                        $shiftData->field_data = $value['label'];
                        $shiftData->field_title = trim($valueLabel);
                        $shiftData->checkbox_value = $value['value'];
                        $shiftData->field_order = $key;
                        $shiftData->checkbox_order = $checkKey;
                        $shiftData->checkbox_parent = $checkBoxParent;
                        $shiftData->required = $required;
                        $this->GigTemplateShiftData->save($shiftData);
                    }
                }
                else {
                    if ($shift['type'] == 'select') {
                        $field_value = serialize($shift['values']);
                    } else { $field_value = ""; }

                    $shiftData = $this->GigTemplateShiftData->newEntity();
                    $shiftData->gig_template_id = $gigTemplateId;
                    $shiftData->field_name = $shift['name'];
                    $shiftData->field_type = $shift['type'];
                    $shiftData->field_data = $field_value;
                    $shiftData->field_title = trim($shiftLabel);
                    $shiftData->checkbox_value = '';
                    $shiftData->field_order = $key;
                    $shiftData->required = $required;
                    $this->GigTemplateShiftData->save($shiftData);
                }
            }

            $shiftJson = array(
                'form_json' => json_decode($shiftJson),
                'status' => 'save'
            );
            echo(json_encode($shiftJson));
            die();
        }
        else { // update
            $fromArr = json_decode($fromJson, true);
            $jsonForm = json_decode(json_encode($fromArr));

            foreach($jsonForm as $arr) {
                if($arr->type == 'checkbox-group' || $arr->type == 'select') {
                    $values = $arr->values;
                    foreach($values as $key => $val) {
                        if($val->value == "") {
                            $label = str_replace(' ', '', $val->label);
                            $val->value = $label;
                        }
                    }
                }
            }

            $arrayShift = json_decode(json_encode($jsonForm), true);
            $shiftJson = json_encode($arrayShift);
            $templateId = $template->id;
            $gigTemplate = $this->GigTemplate->get($templateId);
            $gigTemplate->description = $desc;
            $gigTemplate->form_json = $shiftJson;
            $gigTemplate->modified_by = $user;
            $result = $this->GigTemplate->save($gigTemplate);
            $gigTemplateId = $result->id;
            $fieldString = "";

            foreach ($arrayShift as $key => $shift) {

                if (isset($shift['required'])) {
                    $required = "1";
                } else {  $required = "0"; }
                $shiftLabel = html_entity_decode(str_replace('<br>', '', $shift['label']));

                switch($shift['type']) {
                    case 'checkbox-group':

                        $check = $this->GigTemplateShiftData->find()
                            ->select(['id'])
                            ->where(['gig_template_id' => $gigTemplateId, 'field_name' =>                                               $shift['name']])->toArray();

                        if(!empty($check)) { //update

                            //title
                            $stmt = $conn->prepare('UPDATE gig_template_shift_data SET field_title = "'.                               trim($shiftLabel) .'", field_order = '. $key .'
                        WHERE field_name = "'. $shift['name'] .'" AND
                        gig_template_id = '. $gigTemplateId .' AND checkbox_value = ""');
                            $stmt->execute();

                            $values = $shift['values'];
                            $checkBoxVal = "";

                            foreach ($values as $value) {

                                $checkCheckboxValue = $this->GigTemplateShiftData->find()
                                    ->select(['id'])
                                    ->where(['gig_template_id' => $gigTemplateId, 'field_name' =>                                               $shift['name'], 'checkbox_value' => $value['value']])->first();

                                $valueLabel = html_entity_decode(str_replace('<br>', '', $value['label']));

                                if(!$checkCheckboxValue) { //add new checkbox option
                                    $shiftData = $this->GigTemplateShiftData->newEntity();
                                    $shiftData->gig_template_id = $gigTemplateId;
                                    $shiftData->field_name = $shift['name'];
                                    $shiftData->field_type = 'checkbox-group';
                                    $shiftData->field_data = $value['value'];
                                    $shiftData->field_title = trim($valueLabel);
                                    $shiftData->checkbox_value = $value['value'];
                                    $shiftData->field_order = $key;
                                    $shiftData->required = $required;
                                    $this->GigTemplateShiftData->save($shiftData);
                                }
                                else { //update
                                    $stmt = $conn->prepare('UPDATE gig_template_shift_data SET field_title = "' . trim($valueLabel) . '", required = "' . $required . '",  field_order = ' . $key . '
                        WHERE field_name = "' . $shift['name'] . '" AND
                        gig_template_id = ' . $gigTemplateId . ' AND checkbox_value = "' . $value['value'] . '"');
                                    $stmt->execute();
                                }
                                $checkBoxVal .= $value['value'] . " ";
                            }

                            /*delete checkbox value*/
                            $deleteItem = $conn->prepare('SELECT DISTINCT checkbox_value, id FROM gig_template_shift_data WHERE gig_template_id = '. $gigTemplateId.' AND field_name = "'. $shift['name'] .'" AND field_type = "checkbox-group"');
                            $deleteItem->execute();
                            $deleteCheck = $deleteItem->fetchAll('assoc');

                            foreach($deleteCheck as $item) {
                                if (strpos($checkBoxVal, $item['checkbox_value']) !== false) {
                                } else {
                                    $gigTemplateShift = $this->GigTemplateShiftData->get($item['id']);
                                    $gigTemplateShift->is_delete = 1;
                                    $this->GigTemplateShiftData->save($gigTemplateShift);
                                }
                            }
                            /*end delete*/
                        }
                        else { //add
                            $values = $shift['values'];
                            /*save checkbox group title*/
                            $shiftData = $this->GigTemplateShiftData->newEntity();
                            $shiftData->gig_template_id = $gigTemplateId;
                            $shiftData->field_name = $shift['name'];
                            $shiftData->field_type = 'title';
                            $shiftData->field_data = '';
                            $shiftData->field_title = trim($shiftLabel);
                            $shiftData->checkbox_value = '';
                            $shiftData->field_order = $key;
                            $shiftData->required = $required;
                          //  $this->GigTemplateShiftData->save($shiftData);
                            $shiftTitle = $this->GigTemplateShiftData->save($shiftData);
                            $checkBoxParent = $shiftTitle->id;

                            foreach ($values as $value) {
                                $valueLabel = html_entity_decode(str_replace('<br>', '', $value['label']));

                                $shiftData = $this->GigTemplateShiftData->newEntity();
                                $shiftData->gig_template_id = $gigTemplateId;
                                $shiftData->field_name = $shift['name'];
                                $shiftData->field_type = $shift['type'];
                                $shiftData->field_data = $value['label'];
                                $shiftData->field_title = trim($valueLabel);
                                $shiftData->checkbox_value = $value['value'];
                                $shiftData->field_order = $key;
                                $shiftData->checkbox_parent = $checkBoxParent;
                                $shiftData->required = $required;
                                $this->GigTemplateShiftData->save($shiftData);
                            }
                        }
                        break;

                    default:
                        if ($shift['type'] == 'select') {
                            $field_value = serialize($shift['values']);
                        } else { $field_value = ""; }

                        $text = $this->GigTemplateShiftData->find()
                            ->select(['id'])
                            ->where(['gig_template_id' => $gigTemplateId, 'field_name' =>                                               $shift['name']])->toArray();

                        if(!empty($text)) {
                            $stmt = $conn->prepare("UPDATE gig_template_shift_data SET field_title = '".                               trim($shiftLabel) ."', required = '". $required."', field_data = '". $field_value ."',  field_order = ". $key ."  WHERE field_name = '". $shift['name'] ."' AND gig_template_id = ". $gigTemplateId);
                            $stmt->execute();
                        }
                        else { // add new
                            $shiftData = $this->GigTemplateShiftData->newEntity();
                            $shiftData->gig_template_id = $gigTemplateId;
                            $shiftData->field_name = $shift['name'];
                            $shiftData->field_type = $shift['type'];
                            $shiftData->field_data = $field_value;
                            $shiftData->field_title = trim($shiftLabel);
                            $shiftData->checkbox_value = '';
                            $shiftData->field_order = $key;
                            $shiftData->required = $required;
                            $this->GigTemplateShiftData->save($shiftData);
                        }
                        break;
                }
                $fieldString .= $shift['name'] . " ";
            }

            /*delete shift*/
            $deleteItem = $conn->prepare('SELECT DISTINCT field_name, id FROM gig_template_shift_data WHERE gig_template_id = '. $gigTemplateId);
            $deleteItem->execute();
            $delete = $deleteItem->fetchAll('assoc');

            foreach($delete as $item) {
                if (strpos($fieldString, $item['field_name']) !== false) {
                } else {
                    $gigTemplateShift = $this->GigTemplateShiftData->get($item['id']);
                    $gigTemplateShift->is_delete = 1;
                    $this->GigTemplateShiftData->save($gigTemplateShift);
                }
            }

            $shiftJson = array(
                'form_json' => json_decode($shiftJson),
                'status' => 'update'
            );
            echo(json_encode($shiftJson));
            die();
        }
    }

    /**
     * shift value for attest popup
     */
    public function getDataShiftValue() {
        $scheduleId = $this->request->data['scheduleId'];

        $this->loadModel('ScheduleDataField');
        $dataFieldArr = $this->ScheduleDataField->find()
            ->hydrate(false)
            ->select(['ScheduleDataField.id', 'ScheduleDataField.data_field_value', 'g.field_name', 'g.field_type', 'g.field_data', 'g.field_title','g.checkbox_value', 'g.id'])
            ->join([
                'table' => 'gig_template_shift_data',
                'alias' => 'g',
                'type' => 'LEFT',
                'conditions' => 'g.id = ScheduleDataField.gig_template_shift_data_id'
            ])
            ->where(['ScheduleDataField.schedule_id' => $scheduleId])
            ->toArray();

        $dataFieldHtml = '<input type="hidden" id="scheduleId" name="scheduleId" value="'.$scheduleId .'"/>
            <form id="data_shift_form" action="" method="post" class="form-horizontal group-border">
            <div class="row">';

        foreach($dataFieldArr as $fieldArr) {

            switch($fieldArr['g']['field_type']) {

                case 'textarea':
                    $fieldHtml = '<div class="form-group">
                <label class="col-lg-3 col-md-3 control-label">'. $fieldArr['g']['field_title'] .'</label>
                <div class="col-lg-8 col-md-9">
                <textarea  class="form-control"id="dataField_'. $fieldArr['id'] .'" name="'. $fieldArr['id'] .'">'. $fieldArr['data_field_value'] .'</textarea></div></div>';
                    break;

                case 'number':
                    $fieldHtml = '<div class="form-group">
                <label class="col-lg-3 col-md-3 control-label">'. $fieldArr['g']['field_title'] .'</label>
                <div class="col-lg-8 col-md-9"><input type="number"  class="form-control" value="'. $fieldArr['data_field_value'] .'" id="dataField_'. $fieldArr['id'] .'" name="'. $fieldArr['id'] .'"></div></div>';
                    break;

                case 'checkbox-group':
                    $fieldHtml = '<div class="form-group checkbox_outer"> <div class="checkbox-custom col-md-10 col-md-offset-1">
                    <input type="hidden" value="0" name="'. $fieldArr['id'] .'">
                    <input id="checkbox_'.$fieldArr['id'].'" class="check" value="1" type="checkbox" '. ($fieldArr['data_field_value'] == "1" ? "checked" : "") .' name="'. $fieldArr['id'] .'">
                    <label for="checkbox_'.$fieldArr['id'].'">'.$fieldArr['g']['field_data'].'</label></div></div>';
                    break;

                case 'select':
                    $fieldHtml = '<div class="form-group">
                    <label class="col-lg-3 col-md-3 control-label">'. $fieldArr['g']['field_title'] .'</label>
                    <div class="col-lg-8 col-md-9"><select id="select_'. $fieldArr['id'] .'"  class="form-control" name="'.$fieldArr['id'].'">';
                    $options = unserialize($fieldArr['g']['field_data']);

                    foreach($options as $option) {
                        $fieldHtml .= '<option value="'. $option['value'] .'" '. ($fieldArr['data_field_value'] == $option['value'] ? "selected": "") .'>'. $option['label'] .'</option>';
                    }
                    $fieldHtml .= '</select></div></div>';
                    break;

                case 'title':
                    $fieldHtml = '<div class="col-md-12 checkbox_group"><label>'.$fieldArr['g']['field_title'] .'</label></div>';
                    break;

                default :
                    $fieldHtml = "";
                    break;
            }
            $dataFieldHtml .= $fieldHtml;
        }

        $dataFieldHtml .= '
                        </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4  text-center">
                        <button id="data_schedule_update" class="btn btn-success btn-block" type="button">Update</button>
                    </div>
                </div>
                </form>';

        echo  $dataFieldHtml;
        die();
    }

    public function updateDataShiftValue() {
        $fromData = $this->request->data['fromData'];
        $scheduleId = $this->request->data['scheduleId'];
        $dataArr = array();
        parse_str($fromData, $dataArr);
        $this->loadModel('ScheduleDataField');
        $user = $this->Auth->user('id');

        foreach($dataArr as $key => $data) {
            $ScheduleDataField = $this->ScheduleDataField->get($key);
            $ScheduleDataField->data_field_value = $data;
            $ScheduleDataField->modified_by = $user;
            $this->ScheduleDataField->save($ScheduleDataField);
        }

        $dataFieldArr = $this->ScheduleDataField->find()
            ->hydrate(false)
            ->select(['ScheduleDataField.data_field_value', 'g.field_title'])
            ->join([
                'table' => 'gig_template_shift_data',
                'alias' => 'g',
                'type' => 'LEFT',
                'conditions' => 'g.id = ScheduleDataField.gig_template_shift_data_id'
            ])
            ->where(['schedule_id' => $scheduleId])
            ->toArray();

        if(!empty($dataFieldArr)) {
            $firstDataField = $dataFieldArr[0]['g']['field_title'] . " : " . $dataFieldArr[0]['data_field_value'];
        } else {
            $firstDataField = "";
        }

        echo '<a data-toggle="modal" data-scheduleid="'. $scheduleId .'" href="#data_shift">'. $firstDataField. '</a>';
        die;
    }

    /**
     * check and shor error in ADD SHIFT button
     */
    public function validateShiftForm() {
        $fromJson = $this->request->data['from_json'];
        $gigId = $this->request->data['gigId'];

        $this->loadModel('GigTemplate');
        $template = $this->GigTemplate->find()
            ->where(['form_json' => $fromJson, 'gig_id' => $gigId])->first();

        if($template) {
            echo "1";
        } else {
            echo "0";
        }
        die;
    }

    /**
     * load shift data form for edit
     */
    public function getShiftForm() {
        $gigId = $this->request->data['gigId'];

        $this->loadModel('GigTemplate');
        $shiftArr = $this->GigTemplate->find()
            ->select(['form_json', 'id'])
            ->where(['gig_id' => $gigId])
            ->toArray();

        if(!empty($shiftArr)) {
            $shiftJson = array(
                'form_json' => json_decode($shiftArr[0]->form_json),
                'gig_template_id' => $shiftArr[0]->id
            );
            echo(json_encode($shiftJson));
        } else {
            echo "0";
        }
        die;
    }

    private function getLastGigstrRatingCount($shiftId, $gigstr) {
        $conn = ConnectionManager::get('default');

        $stmt = $conn->prepare('SELECT s.id, s.gigstr_rating, s.shift_from, s.checkout FROM schedule s
                        WHERE s.id != '.$shiftId .' AND s.status != 0
                        AND s.gigstr = '. $gigstr .'
                        AND s.checkout IS NOT NULL
                        ORDER BY s.checkout DESC');
        $stmt->execute();
        $gigstrs = $stmt->fetchAll('assoc');

        return $gigstrs;

    }

    private function getCountry() {

        $conn = ConnectionManager::get('default');
        $stmt = $conn->prepare('SELECT * FROM  country
                            WHERE user_country = 1');
        $stmt->execute();
        $country = $stmt->fetchAll('assoc');

        return $country;
    }

    private function getScheduleImage($scheduleId) {

        //$scheduleId = $this->request->data['scheduleId'];
        $this->loadModel('ScheduleImage');

        $images = $this->ScheduleImage->find()
            ->select(['image'])
            ->where(['schedule_id' => $scheduleId]);

        return $images->count();

    }

    public function scheduleImage() {

        $scheduleId = $this->request->data['scheduleId'];
        $this->loadModel('ScheduleImage');

        $images = $this->ScheduleImage->find()
            ->select(['image'])
            ->where(['schedule_id' => $scheduleId]);

        echo json_encode($images);
        die();

    }

}

