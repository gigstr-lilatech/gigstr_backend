<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use stdClass;

/**
 * Candidate Controller
 *
 * @property \App\Model\Table\CandidateTable $Candidate
 */
class CandidateController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        //$this->RequestHandler->addInputType('json', ['json_decode', true]);
//        if (!$this->Auth->user()) {
//            return $this->redirect(['controller' => 'user', 'action' => 'login']);
//        }
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        $this->paginate = [
            'fields' => ['user_id', 'phone_number', 'country', 'city', 'manual_payment', 'created', 'app_mode_preference', 'app_mode', 'verified_company', 'is_verified', 'User.email', 'User.name'],
            'contain' => ['User'],
            'sortWhitelist'=>['Candidate.user_id', 'User.name', 'User.email', 'Candidate.phone_number', 'Candidate.country', 'Candidate.city', 'Candidate.app_mode_preference', 'Candidate.app_mode', 'Candidate.manual_payment', 'Candidate.created', 'Candidate.verified_company', 'Candidate.is_verified'],
            'conditions' => ['User.is_delete' => 0],
            'order' => ['Candidate.user_id' => 'desc'],
            'limit' => 30
        ];

        $base_url = Configure::read('dev_base_url');
        $candidates = $this->paginate($this->Candidate);
        $this->loadModel('Country');

        foreach($candidates as $candidate) {
         //   $candidate->candidate_rating = $this->getUserAvg($candidate->user_id);

            $candidate->country_name = "";
            if(!empty($candidate->country)) {
                $country = $this->Country->findByCountryCode($candidate->country)->first();
                $candidate->country_name = $country->country_name;
            }
        }

        $this->set(compact('candidates', 'base_url'));
        $this->set('_serialize', ['candidates', 'base_url']);

    }

    private function getUserAvg($user) {

        $this->loadModel('Schedule');
        $rating = $this->Schedule->find()
            ->select('internal_rating')
            ->where(['gigstr' => $user, 'internal_rating !=' => 0])
            ->toArray();

        $rates = 0;

        foreach($rating as $rate) {
            $rates +=  $rate->internal_rating;
        }
        $total = count($rating);

        if($total > 0) {
            $avg = round($rates / $total, 1);
            return $avg;
        } else {
            return null;
        }

    }
    
    /**
     * View method
     *
     * @param string|null $id Candidate id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $gig = $this->Candidate->get($id, [
            'contain' => ['Company', 'Applicant', 'Inbox', 'Sentbox']
        ]);
        $this->set('gig', $gig);
        $this->set('_serialize', ['data']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $gig = $this->Candidate->newEntity();
        if ($this->request->is('post')) {
            $gig = $this->Candidate->patchEntity($gig, $this->request->data);
            if ($this->Candidate->save($gig)) {
                $this->Flash->success(__('The gig has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gig could not be saved. Please, try again.'));
            }
        }
        $companies = $this->Candidate->find('list', ['limit' => 200]);
        $this->set(compact('gig', 'companies'));
        $this->set('_serialize', ['gig']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Candidate id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $gig = $this->Candidate->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gig = $this->Candidate->patchEntity($gig, $this->request->data);
            if ($this->Candidate->save($gig)) {
                $this->Flash->success(__('The gig has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The gig could not be saved. Please, try again.'));
            }
        }
        $companies = $this->Candidate->Company->find('list', ['limit' => 200]);
        $this->set(compact('gig', 'companies'));
        $this->set('_serialize', ['gig']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Candidate id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);

        $userTable = TableRegistry::get('User');
        $user = $userTable->get($id); // Return article with id 12

        $user->status = '2';
        $userTable->save($user);
        $this->Flash->success(__('Candidate has been deleted.'));
        return $this->redirect(['action' => 'index']);
    }

    /**
     * listAll method
     * @param string|null $id Candidate id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function listAll() {
        if ($this->request->is('post')) {
            $this->paginate = ['page' => 1,
                'limit' => 10000,
                'fields' => [
                    'id', 'title', 'description', 'sub_title1', 'sub_title2', 'gig_image'
                ],
                'sortWhitelist' => [
                    'id', 'title', 'description', 'sub_title1', 'sub_title2', 'gig_image'
            ]];
            $this->set('data', $this->paginate($this->Candidate));
            $return = new \stdClass();
            $return->status = 0;
            $this->set('return', $return);
            $this->set('_serialize', ['return', 'data']);
        }
    }


    /**
     * @param $id
     * @param int $chat_id
     */
    public function profile($id, $chat_id = 0) {

        $this->loadModel('User');
        $query = $this->Candidate->findByUserId($id);
        $row = $query->first();

        $candidate = $this->Candidate->get($row->id, [
            'contain' => ['User']
        ]);

        $this->loadModel('Schedule');
        $rating = $this->Schedule->find()
            ->select('internal_rating')
            ->where(['gigstr' => $row->user_id, 'internal_rating !=' => 0])
            ->toArray();

        $rates = 0;

        foreach($rating as $rate) {
            $rates +=  $rate->internal_rating;
        }
        $total = count($rating);

        if($total > 0) {
            $avg = round($rates / $total, 1);
            $candidateRating = array(
                'rateAvg' => $avg,
                'rateCount' => $total
            );
        } else {$candidateRating = array('rateAvg' => 0, 'rateCount' => 0); }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $email = $this->request->data['email'];
                $user_id_of_email = $this->User->checkExisting($email);

            if($email !=null)
            {
                if ($user_id_of_email > 0) {
                    if ($user_id_of_email != $id) {

                        $this->Flash->error(__('The modified email address already existing in the database. Please try with another.'));
                        return $this->redirect(['action' => 'profile', $row->user_id]);
                    } else {

                    }
                }
            }
         //   $this->request->data['user_country'] = $this->request->data['country'];
//            if (trim($this->request->data['dob']) == '') {
//                $this->request->data['dob'] = NULL;
//            }
            
            $day = $month = $year = '';
            $dob = array();
            $dob = explode('-', $this->request->data['dob']);
            if (count($dob) >0) {
                if (count($dob) > 2) {
                    $year = $dob[0];
                    $month = $dob[1];
                    $day = $dob[2];
                }else{
                    $year = date('Y');
                    $month = $dob[0];
                    $day = $dob[1];
                }
            }

            unset($this->request->data['dob']);
            $this->request->data['year'] = $year;
            $this->request->data['month'] = $month;
            $this->request->data['day'] = $day;
            
            if (!isset($this->request->data['car_driver_license'])) {
                $this->request->data['car_driver_license'] = 0;
            }
            if (!isset($this->request->data['manual_payment'])) {
                $this->request->data['manual_payment'] = 0;
            }
            if (!isset($this->request->data['is_verified'])) {
                $this->request->data['is_verified'] = 0;
            }
            if (!isset($this->request->data['verified_company'])) {
                $this->request->data['verified_company'] = 0;
            }

            $query_user = $this->User->query();
            $query_user->update()
                    ->set(['email' => $this->request->data['email'], 'name' => $this->request->data['name']])
                    ->where(['id' => $id])
                    ->execute();
            unset($this->request->data['email']);
            unset($this->request->data['name']);

            $data = $this->request->data;
            $data['modified'] = date('Y-m-d H:i:s');
            $query = $this->Candidate->query();
            $query->update()
                    ->set($data)
                    ->where(['id' => $row->id])
                    ->execute();

            $this->Flash->success(__('Candidate details updated.'));
            return $this->redirect(['action' => 'profile', $row->user_id]);
        }

        $user_data = $this->User->get($candidate->user_id);
        $user_id = $this->Auth->user('id');

        $referrers = new stdClass;
        if($user_data->referrer_id) {
            $query = $this->User->findById($user_data->referrer_id);
            $referr = $query->first();

            $referrers->id = $user_data->referrer_id;
            $referrers->name = $referr->name;
        }

        $base_url = Configure::read('dev_base_url');

        $this->loadModel('CandidateExperiences');
        $this->loadModel('CandidateEducation');
        $this->loadModel('CandidateSkills');
        $this->loadModel('CandidateLanguages');
        $this->loadModel('Skills');
        $this->loadModel('Languages');
        $this->loadModel('Country');
        $this->loadModel('PayoutData');
        $this->loadModel('Payout');
        $this->loadModel('GigCandidate');
        $this->loadModel('Company');

        if(empty($candidate->country_name)) {
            $country = $this->Country->findByCountryCode(trim($candidate->country))->first();
            $country = $country->country_name;
        } else {
            $country = $candidate->country_name;
        }
        $lngCountry = $this->Country->findByCountryCode(trim($candidate->lng_country))->first();
        $candidate->lng_country = $lngCountry->country_name;

        $candidate->address =  ($candidate->address_co ? $candidate->address_co. ', ' : '') . $candidate->city . ', <br/>' . ($candidate->zip_code ? $candidate->zip_code . ', ' : "") . $country;

        /** @var  $experiences */
        $experiences = $this->CandidateExperiences->findByCandidateId($candidate->id)->toArray();
        $experience = "";
        foreach($experiences as $exp) {
            $experience .= $exp->employer . " - " . $exp->position . "</br>";
        }
        $candidate->experiences = $experience;

        /** @var  $education */
        $education = $this->CandidateEducation->findByCandidateId($candidate->id)->toArray();
        $edu = "";
        foreach($education as $canEdu) {
            $edu .= $canEdu->school . " - " . $canEdu->type . (!empty($canEdu->subject) ? " - " . $canEdu->subject : "") . "</br>";
        }
        $candidate->education = $edu;

        /** @var  $skills */
        $skills = $this->CandidateSkills->find('All',[
            'conditions' => ['CandidateSkills.candidate_id' => $candidate->id],
            'contain' => ['Skills'],
        ])->toArray();
        $skillArr = "";
        foreach($skills as $key => $skill) {
            $skillArr .= $skill->skill->skill;
            $skillArr .= (count($skills) > ($key + 1) ? ", " : "");
        }
        $candidate->skills = $skillArr;

        /** @var  $language */
        $language = $this->CandidateLanguages->find('all', [
            'conditions' => ['CandidateLanguages.candidate_id' => $candidate->id],
            'contain' => ['Languages'],
        ])->toArray();
        $langArr = "";
        foreach($language as $key => $lang) {
            $langArr .= $lang->language->language;
            $langArr .= (count($language) > ($key + 1)? ", " : "");
        }
        $candidate->language = $langArr;

        /** @var  $payout */
        $payout = $this->PayoutData->find('all', [
            'conditions' => ['PayoutData.user_id' => $candidate->user_id],
            'contain' => ['Payout', 'Country'],
        ])->toArray();
        $payoutData = "";
        $payoutCountry = "";
        foreach($payout as $pay) {
            $payoutObj = $this->Payout->findById($pay->payout->title_id)->first();
            $payoutData .= $payoutObj->value . ' - ' . $pay->value .  "</br>";
            $payoutCountry = $pay->country->country_name;
        }
        $candidate->payout = $payoutData;
        $candidate->payoutCountry = $payoutCountry;

        if($row->company_id == 0) {
            $candidate->company = "";
        }
        else {
            $comp =$this->Company->findById($row->company_id)->first();
            $candidate->company = $comp->name;
        }

        /** @var  $activity */
        $activity = $this->GigCandidate->find()
            ->select(['GigCandidate.selection_date', 'GigCandidate.gig_id', 'GigCandidate.selection', 'g.skills_id', 's.skill', 'g.title'])
            ->contain(['Gig'])
            ->join([
                'g' => [
                    'table' => 'gig',
                    'type' => 'INNER',
                    'conditions' => 'GigCandidate.gig_id = g.id'
                ],
                's' => [
                    'table' => 'skill',
                    'type' => 'LEFT',
                    'conditions' => 's.id = g.skills_id'
                ]
            ])
            ->where(['user_id' => $candidate->user_id])
            ->order(['selection_date' => 'DESC']);

        $activityLog = [];
        foreach($activity as $log) {
            $temp = [];
            $temp['add'] = (!empty($log->g['title']) ? $log->g['title'] : $log->s['skill']);
            $temp['selection'] = ucwords(strtolower($log->selection));
            $temp['date'] = $log->selection_date->format('Y-m-d');
            $temp['gig_id'] = $log->gig_id;
            array_push($activityLog, $temp);
        }

        $user_name = $this->Auth->user('name');
        $this->set(compact('candidate', 'user_data', 'base_url', 'user_id', 'user_name', 'referrers', 'candidateRating', 'activityLog'));
    }


    public function apply() {
        $this->loadModel('Applicant');
        if ($this->request->is('post')) {
            $return = new \stdClass();
            $applicant = $this->Applicant->newEntity();

            $applicant->gig_id = $this->request->data['gig_id'];
            $applicant->user_id = $this->request->data['user_id'];
            $applicant->status = 1;

            if ($this->Applicant->save($applicant)) {
                $return->status = 0;
                $this->set('return', $return);
                $this->set('_serialize', $return);
            } else {
                $return->status = 202;
                $this->set('return', $return);
                $this->set('_serialize', $return);
            }
        }
    }


    /**
     * Not use anymore since API v2
     * chat has removed
     */
    public function getUnread() {

        if ($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $chat_id = $this->request->data['chat_id'];
            $candidate_id = $this->request->data['candidate_id'];
            $conn = ConnectionManager::get('default');

            $stmt = $conn->prepare(
                'SELECT m.*, u.name, um.id AS unread_id '
                . 'FROM `message` m '
                . 'INNER JOIN unread_message um ON um.message_id = m.id '
                . 'INNER JOIN user u ON m.created_by = u.id '
                . 'WHERE m.`chat_id` = ' . $chat_id . ' AND um.`user_id` = ' . $user_id . ' '
            );

            $stmt->execute();
            $results = $stmt->fetchAll('assoc');
            $html = '';

            if(!empty($results)){
                foreach ($results AS $result) {
                    if ($result['created_by'] == $candidate_id)
                        $html .= '<span class="right_msg">' . $this->makeClickableLinks(base64_decode($result['message'])) . '</span><br/><span class="chat_time_right">' . date('Y-m-d', strtotime($result['created'])) . ', ' . $result['name'] . '</span><br/>';
                    else
                        $html .= '<span class="left_msg">' . $this->makeClickableLinks(base64_decode($result['message'])) . '</span><br/><span class="chat_time_left">' . date('Y-m-d', strtotime($result['created'])) . ', ' . $result['name'] . '</span><br/>';
                }

                $stmt = $conn->prepare(
                    'DELETE um FROM unread_message um INNER JOIN message m ON m.id = um.message_id WHERE m.chat_id = '
                    . $chat_id . ' AND um.user_id = ' . $user_id . ';'
                );

                $stmt->execute();
            }
            echo $html;
            // echo json_encode($return);
            die();
        }
    }

    public function makeClickableLinks($text) {
        //return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href=\'$1" target="_blank">$1</a>', $s);
        //$reg_exUrl = "/(http|https|ftp|ftps|www.)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

        $text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"ginascheme://$3\" >$3</a>", $text);
        $text = preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
        $text = str_replace('http', 'ginascheme', $text);
        $text = str_replace('>ginascheme://', '>', $text);
        return($text);
    }


    /**
     *
     */
    public function ajaxSearch() {
        $this->loadModel('Country');
        $keyWord = $this->request->data['search_keyword'];

        $this->paginate = [
            'fields' => ['user_id', 'phone_number', 'country', 'city', 'manual_payment', 'created', 'app_mode_preference', 'app_mode', 'verified_company', 'is_verified', 'User.email', 'User.name'],
            'order' => [
                'User.id' => 'desc'
            ],
            'contain' => ['User'],
            'conditions' => [
                'User.is_delete' => 0,
                'OR' => ['User.name like' => "%{$keyWord}%", 'User.email like'  => "%{$keyWord}%", 'Candidate.city like' => "%{$keyWord}%"]
            ],
            'limit' => 1000
        ];

        $candidates = $this->paginate($this->Candidate);
        foreach($candidates as $candidate) {
            //$candidate->candidate_rating = $this->getUserAvg($candidate->user_id);
            $candidate->country_name = "";
            if(!empty($candidate->country)) {
                $country = $this->Country->findByCountryCode($candidate->country)->first();
                $candidate->country_name = $country->country_name;
            }
        }
        echo json_encode($candidates);
        die;
    }


    public function verifyCandidate() {

        $candidateId = $this->request->data['candidateId'];
        $candidateObj = $this->Candidate->get($candidateId);
        $candidateObj->is_verified = 1;
        $this->Candidate->save($candidateObj);

        echo "1";
        die();
    }

}
