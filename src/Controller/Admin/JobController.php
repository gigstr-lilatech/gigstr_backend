<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use App\Controller\Datetime;
/**
 * Company Controller
 *
 * @property \App\Model\Table\CompanyTable $Company
 */
class JobController extends AppController {

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        
        $this->paginate = [
            'contain' => ['Company'],
            'order' => ['Job.id' => 'DESC'],
            'limit' => 30000
        ];
        $jobs = $this->paginate($this->Job);

        foreach($jobs as $job) {
            $this->loadModel('Country');
            $country = $this->Country->findByCountryCode($job->country)->first();
            $job->country = $country->country_name;
        }
        
        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
                    'order' => [
                        'name' => 'ASC'
                    ]
                ])->where(['status' => 1]);
        
        $base_url = Configure::read('dev_base_url');

        $this->loadModel('Country');
        $country = $this->Country->find()
            ->select(['country_code', 'country_name'])->where(['user_country' => 1]);

        $this->set(compact('jobs', 'companies','base_url', 'country'));
        $this->set('_serialize', ['jobs', 'companies','base_url', 'country']);
    }
    
    
    public function add()
    {
        $this->viewBuilder()->template('index');
        $job = $this->Job->newEntity();
        
        
        if ($this->request->is('post')) {
       
                $job->company_id = $this->request->data['company'];
                $job->title = $this->request->data['title'];
                $job->create_date = date('Y-m-d h:i:s');
                $job->create_by = $_SESSION['Auth']['User']['id'];
            $job->country = $this->request->data['country'];

                if ($this->Job->save($job)) {
                    echo 200;
                    die();
                } else {
                   echo 400;
                   die();
                }
            }
        
        $this->loadModel('Company');
        $companies = $this->Company->find('all', [
                    'order' => [
                        'name' => 'ASC'
                    ]
                ])->where(['status' => 1]);
        
        $this->paginate = [
            'contain' => ['Company'],
            'limit' => 1000000
        ];
        $jobs = $this->paginate($this->Job);

        $country = array(
            'SE' => 'Sweden',
            'NO' => 'Norway',
            'DK' => 'Denmark'
        );
        
        $this->set(compact('jobs','companies', 'country'));
        $this->set('_serialize', ['jobs', 'companies', 'country']);
    }
    
}
