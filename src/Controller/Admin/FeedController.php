<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Feed Controller
 *
 * @property \App\Model\Table\FeedTable $Feed
 *
 * @method \App\Model\Entity\Feed[] paginate($object = null, array $settings = [])
 */
class FeedController extends AppController
{

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('SimpleImage');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index() {

        $ourWorld = $this->Feed->find()
            ->where(['section' => 'client', 'is_delete' => 0])
            ->order(['feed_order' => 'DESC'])
            ->toArray();

        $gigHelp = $this->Feed->find()
            ->where(['section' => 'help-gig', 'is_delete' => 0])
            ->order(['feed_order' => 'DESC'])
            ->toArray();

        $base_url =  Configure::read('dev_base_url');
        $this->set(compact('ourWorld', 'gigHelp', 'base_url'));
        $this->set('_serialize', ['ourWorld', 'gigHelp', 'base_url']);
    }

    /**
     * View method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $feed = $this->Feed->get($id, [
            'contain' => []
        ]);

        $this->set('feed', $feed);
        $this->set('_serialize', ['feed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $feed = $this->Feed->newEntity();
        if ($this->request->is('post')) {
            $feed = $this->Feed->patchEntity($feed, $this->request->getData());
            if ($this->Feed->save($feed)) {
                $this->Flash->success(__('The feed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feed could not be saved. Please, try again.'));
        }
        $this->set(compact('feed'));
        $this->set('_serialize', ['feed']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $feed = $this->Feed->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $feed = $this->Feed->patchEntity($feed, $this->request->getData());
            if ($this->Feed->save($feed)) {
                $this->Flash->success(__('The feed has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feed could not be saved. Please, try again.'));
        }
        $this->set(compact('feed'));
        $this->set('_serialize', ['feed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Feed id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $feed = $this->Feed->get($id);
        if ($this->Feed->delete($feed)) {
            $this->Flash->success(__('The feed has been deleted.'));
        } else {
            $this->Flash->error(__('The feed could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Save feed and element
     */
    public function saveFeedForm() {
        $feedForm = $this->request->data['feedForm'];
        $feedArr = array();
        parse_str($feedForm,$feedArr);
        $section = $this->request->data['section'];
        $this->loadModel('FeedItem');
        $return = array();

        $session = $this->request->session();
        $user_data = $session->read('Auth.User');
        $userId = $user_data['id'];

       // var_dump($feedArr);die;

        if(!empty($feedArr['feedId'])) { //update

            $feed = $this->Feed->get($feedArr['feedId']);
            $feed->title = $feedArr['title'];
            $feed->url_action = $feedArr['componentAction'];
            $feed->url = $feedArr['componentUrl'];
            $feed->modified_by = $userId;
            $feedSave = $this->Feed->save($feed);
            $itemIdArr = array();

            if(isset($feedArr['item']) && !empty($feedArr['item'])) {
                $order = 0;
                $feedItemArr = array_reverse($feedArr['item']);

                foreach ($feedItemArr as $item) {

                    if(empty($item['itemId'])) { // add new item
                        $feedItem = $this->FeedItem->newEntity();
                    }
                    else {
                        $feedItem = $this->FeedItem->get($item['itemId']);
                    }

                    $feedItem->feed_id = $feedSave->id;
                    $feedItem->title = $item['imageTitle'];
                    $feedItem->description = $item['desc'];
                    $feedItem->link_type = $item['linkType'];
                    $feedItem->url = $item['link'];
                    $feedItem->image = $item['image'];
                    $feedItem->image_thumbnail = $item['thumbnail'];
                    $feedItem->item_order = $order;
                    $saveItem = $this->FeedItem->save($feedItem);

                    $itemIdArr[$item['count']] = $saveItem->id;
                    $order ++;
                }
            }
            $return['itemId'] = $itemIdArr;
            $return['feedId'] = $feedSave->id;
            $return['status'] = 'update';
            $return['newFeed'] = array();
        }
        else { // add
            $feed = $this->Feed->newEntity();
            $feed->section = $section;
            $feed->ui_type = $feedArr['uiComponent'];
            $feed->title = $feedArr['title'];
            $feed->url_action = $feedArr['componentAction'];
            $feed->url = $feedArr['componentUrl'];
            $feed->created_by = $userId;
            $feedSave = $this->Feed->save($feed);

            $feedSave->feed_order = $feedSave->id;
            $this->Feed->save($feedSave);

            $itemIdArr = array();
            if($feedArr['uiComponent'] == 'carousel') {

                $feedItemArr = array_reverse($feedArr['item']);

                foreach ($feedItemArr as $key => $item) {
                    $feedItem = $this->FeedItem->newEntity();
                    $feedItem->feed_id = $feedSave->id;
                    $feedItem->title = $item['imageTitle'];
                    $feedItem->description = $item['desc'];
                    $feedItem->link_type = $item['linkType'];
                    $feedItem->url = $item['link'];
                    $feedItem->image = $item['image'];
                    $feedItem->image_thumbnail = $item['thumbnail'];
                    $feedItem->item_order = $key;
                    $saveItem = $this->FeedItem->save($feedItem);

                    $countVal = (empty($item['count']) ? 0 : $item['count']);
                    $itemIdArr[$countVal] = $saveItem->id;
                }
            }
            $return['itemId'] = $itemIdArr;
            $return['status'] = 'add';
            $return['feedId'] = $feedSave->id;
            $return['newFeed'] = array(
                'id' => $feedSave->id,
                'title' => $feedSave->title,
                'ui_type' => $feedSave->ui_type,
                'section' => $feedSave->section,
                'feed_order' => $feedSave->feed_order
            );

        }

        echo json_encode($return);
        die();
    }

    /**
     * Load component items
     */
    public function loadItems() {

        $componentId = $this->request->data['componentId'];
        $this->loadModel('FeedItem');

        $componentItems = $this->FeedItem->find()
            ->where(['feed_id' => $componentId, 'is_delete' => 0])
            ->order(['item_order' => 'DESC'])
            ->toArray();

        $componentHeaders = $this->Feed->get($componentId, ['conditions' => ['is_delete' => 0]]);

        $feed = array(
            'componentHeaders' => $componentHeaders,
            'componentItems' => $componentItems
        );

        echo(json_encode($feed));
        die();

    }


    public function upload() {

        if (!empty($_FILES)) {
            /* Path to Images folder */
            $dir = WWW_ROOT . 'img' . DS . 'feed_images';
            $f = explode('.', $_FILES['file']['name']);
            $ext = '.' . end($f);
            /* Generate a Name in my case i use ID  & slug */
            $filename = time(). "_" . rand(000000, 999999);

            /* Save image in the thumbnail folders and replace if exist */
            move_uploaded_file($_FILES['file']['tmp_name'], $dir . DS . $filename . $ext);

            $fn = $filename . $ext;

            $this->SimpleImage->load($dir . DS . $fn);
            $this->SimpleImage->resize(200, 150);
            $this->SimpleImage->save($dir . DS . 'thumbnail-' . $fn);

            $success_message = array('success' => 200,
                'filename' => $fn,
                'thumbnail' => 'thumbnail-' . $fn);

            echo json_encode($success_message);
            die();
        }
    }


    public function removeCarouselItem() {

        $this->loadModel('FeedItem');
        $itemId = $this->request->data['itemId'];
        $feedItem = $this->FeedItem->get($itemId);
        $feedItem->is_delete = 1;

        if($this->FeedItem->save($feedItem)) {
            echo 1;
        } else { echo 0; }

        die();
    }


    public function removePanel() {

        $panelId = $this->request->data['panelId'];

        $panel = $this->Feed->get($panelId);
        $panel->is_delete = 1;
        if($this->Feed->save($panel)) {
            echo 1;
        } else { echo 0; }

        die();
    }


    public  function saveFeedItemOrder() {
        $feedForm = $this->request->data['feedForm'];
        $feedArr = array();
        parse_str($feedForm,$feedArr);
        $this->loadModel('FeedItem');

        if(isset($feedArr['item']) && !empty($feedArr['item'])) {
            $order = 0;
            $feedItemArr = array_reverse($feedArr['item']);

            foreach ($feedItemArr as $item) {
                if(!empty($item['itemId'])) {
                    $feedItem = $this->FeedItem->get($item['itemId']);
                    $feedItem->item_order = $order;
                    $this->FeedItem->save($feedItem);
                }
                $order ++;
            }
        }
        echo 1;
        die();
    }


    public function saveClientFeedOrder() {
        $feedForm = $this->request->data['feedForm'];

        $feedClientArr = array();
        parse_str($feedForm,$feedClientArr);
        $order = 0;
        $feedArr = array_reverse($feedClientArr['feed']);

        foreach($feedArr as $key => $arr) {

            $panel = $this->Feed->get($arr);
            $panel->feed_order = $order;

            $this->Feed->save($panel);
            $order ++;
        }
        echo 1;
        die;
    }


}
