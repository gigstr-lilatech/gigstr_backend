<?php

namespace App\Controller\Admin;
use Cake\Event\Event;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
/**
 * Company Controller
 *
 * @property \App\Model\Table\CompanyTable $Company
 */
class CompanyController extends AppController {


    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        $company = $this->Company->find('All')
            ->where(['status' => '1'])
            ->order(['modified' => 'DESC']);

        $this->set('company', $company);
        $this->set('_serialize', ['company']);
    }

    /**
     * View method
     *
     * @param string|null $id Company id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $company = $this->Company->get($id, [
            'contain' => ['Gig']
        ]);
        $this->set('company', $company);
        $this->set('_serialize', ['company']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $company = $this->Company->newEntity();
        if ($this->request->is('post')) {
            $company = $this->Company->patchEntity($company, $this->request->data);
            if ($this->Company->save($company)) {
                $this->Flash->success(__('The company has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The company could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('company'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Company id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $company = $this->Company->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $company = $this->Company->patchEntity($company, $this->request->data);
            if ($this->Company->save($company)) {
                $this->Flash->success(__('The company has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The company could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('company'));
        $this->set('_serialize', ['company']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gig id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {
        $this->loadModel('Gig');
        $this->request->allowMethod(['post', 'delete']);

        $companyTable = TableRegistry::get('Company');
        $company = $companyTable->get($id); // Return article with id 12

        $company->status = '2';
        $companyTable->save($company);

        $query = $this->Gig->query();
        $query->update()
                ->set(['status' => '2'])
                ->where(['company_id' => $id])
                ->execute();

        $this->Flash->success(__('Company has been deleted.'));
        return $this->redirect(['action' => 'index']);
    }

    public function create() {
        $company_name = $this->request->query['company'];

        if ($this->Company->exists(['name' => $company_name])) {
            echo json_encode(array('status' => '108'));
            die();
        } else {
            $companyTable = TableRegistry::get('Company');
            $company = $companyTable->newEntity();

            $company->name = $company_name;
            $company->status = 1;

            if ($companyTable->save($company)) {
                echo json_encode(array('status' => '200', 'id' => $company->id, 'name' => $company_name));
                die();
            } else {
                echo json_encode(array('status' => '404'));
                die();
            }
        }
    }

}
