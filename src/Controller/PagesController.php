<?php

namespace App\Controller;

use App\Controller\AppController;

class PagesController extends AppController {

    var $name = 'Pages';
    
    function display() {

        $path = func_get_args();
        
        $count = count($path);
        if (!$count) {
            $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        switch ($page) {
            case 'about':
                $this->_about();
                $this->render('about');
                break;
            case 'terms':
                $this->_terms();
                $this->render('terms');
                break;
            default:
                $this->render(implode('/', $path));
        }
    }

    function _about() {
        //$this->layout = 'default';
    }

    function _terms() {
        
    }

    function events() {
        
    }

}
