/**
 * Created by yasith on 1/22/18.
 */
//'use strict';

var mysql     =    require('mysql');
var mySQLEvents = require('mysql-events');
var admin = require('firebase-admin');
var atob = require('atob');


/*var ZongJi = require('zongji');
var zongji = new ZongJi({
    host     : 'localhost',
    user     : 'root',
    password : 'gr-fWg5><b+9+R*t',
    debug: true
});
zongji.start({
    includeEvents: ['tablemap', 'writerows', 'updaterows', 'deleterows']
});
zongji.on('binlog', function(evt) {
    console.log("+++++Inside binlog event++++");
    evt.dump();
});*/



var pool      =    mysql.createPool({
 connectionLimit : 100, //important
 host     : 'localhost',
 user     : 'root',
 password : 'gr-fWg5><b+9+R*t',
 database : 'gigs_staging',
 debug    :  false
 });

var dsn = {
    host:    'localhost',
    user:     'root',
    password: 'gr-fWg5><b+9+R*t'
};
const settings = {
    includeEvents: ['rotate','tablemap', 'writerows', 'updaterows', 'deleterows']
};
//var serviceAccount = require("/opt/webapp/gigstr_backend/node_app/gigstr-staging-firebase-adminsdk-y9xx0-41049c68e6.json");
var serviceAccountLive = require("/opt/webapp/gigstr/gigstr_backend/node_app/gigstr-1176-firebase-adminsdk-ldqum-edb22eca06.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccountLive),
    databaseURL: "https://gigstr-staging.firebaseio.com"
});

var mysqlEventWatcher = mySQLEvents(dsn, settings);

console.log(mysqlEventWatcher);

var watcher = mysqlEventWatcher.add(
    'gigs_staging.push_notification',
    function (oldRow, newRow, event) {
        if (oldRow === null) {
            //insert code goes here

            var jsonContent = JSON.stringify(newRow);
            var pushJson = JSON.parse(jsonContent);
            var body = pushJson.fields.payload;
            var device_type = pushJson.fields.device_type;
            var token = pushJson.fields.token;
            var options = {
                priority : "high",
                "content_available": true,
                timeToLive : 60 * 60 * 24
            };

            console.log('insert');
            console.log(pushJson.fields.id);

            var name = JSON.parse(pushJson.fields.key_value);
            var redirect = pushJson.fields.redirect;

            if(redirect === 'CHAT_INBOX') {
                var title = name.name;
            }
            else {
                 title = "Gigstr";
            }

            var payload = {
                notification: {
                    title: title,
                    body: body.toString('utf8'),
                    sound : "default",
                    badge : pushJson.fields.badge_count
                },
                data: {
                    mode : pushJson.fields.mode,
                    redirect : redirect,
                    resource : pushJson.fields.key_value,
                    title: title,
                    body: body.toString('utf8'),
                    sound : "default",
                    badge : pushJson.fields.badge_count
                }
            };

            var topicPayload = {
                notification: {
                    title: "Gigstr",
                    body: body,
                    sound : "default"
                },
                data: {
                    mode : pushJson.fields.mode,
                    redirect : pushJson.fields.redirect,
                    resource : pushJson.fields.key_value,
                    title: "Gigstr",
                    body: body,
                    sound : "default"
                }
            };

            if(pushJson.fields.topic) { // send to topics
                var topic = pushJson.fields.topic;

                if(pushJson.fields.response === '') {
                    admin.messaging().sendToTopic(topic, topicPayload)
                    .then(function (response) {
                        // See the MessagingTopicResponse reference documentation for the contents of response.
                        pool.getConnection(function (err, connection) {
                            connection.query("UPDATE push_notification SET response = '1' WHERE id = " + pushJson.fields.id, function (err, rows) {
                                connection.release();
                            });
                        });
                        console.log("Successfully sent topic : " + topic, response);
                    })
                    .catch(function (error) {
                        console.log("Error sending topic : " + topic, error);
                    });
                }
            }
            else { // send to single device

                if(pushJson.fields.response === '') {
                    admin.messaging().sendToDevice(token, payload, options).then(function (response) {
                        console.log(response.results);
                        pool.getConnection(function (err, connection) {
                            connection.query("UPDATE push_notification SET response = " + response.successCount + " WHERE id = " + pushJson.fields.id, function (err, rows) {
                                connection.release();
                            });
                        });
                    }).catch(function (error) {
                        console.log("Error sending message:", error);
                    });
                }
            }
        }
        //row deleted
        if (newRow === null) {
        }
        //row updated
        if (oldRow !== null && newRow !== null) {
            //update code goes here
        }

        //detailed event information
        // console.log(event)
    },
    'match this string or regex'
);


setInterval(function() {
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    console.log('*********************************');
    console.log(date+' '+time);
    console.log(mysqlEventWatcher.zongji.connection.state);
    console.log('*********************************');
}, 30 * 1000);

