//------------- login.js -------------//
$(document).ready(function () {

    //validate login form 
    $("#password-form").validate({
        ignore: null,
        ignore: 'input[type="hidden"]',
                errorPlacement: function (error, element) {
                    var place = element.closest('.input-group');
                    if (!place.get(0)) {
                        place = element;
                    }
                    if (place.get(0).type === 'checkbox') {
                        place = element.parent();
                    }
                    if (error.text() !== '') {
                        place.after(error);
                    }
                },
        errorClass: 'help-block',
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            confirmpassword: {
                required: true,
                equalTo: "#password"
            }
        },
        messages:{
            password: "",
            confirmpassword: ""
        },
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();

            if ($('#password').val() == "" || $('#confirmpassword').val() == "") {
                $("#error-message").show().text("These passwords don’t match. Please try again");
            } else {
                if (errors) {
                    $("#error-message").show().text("Password must be at least 6 characters. Please try again.");
                } else {
                    $("#error-message").hide();
                }
            }

        },
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        }
    });

});