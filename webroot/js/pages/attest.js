$(document).ready(function () {

    $('#company').change(function () {
        var companyid = jQuery('select[name=company]').val();
        $.ajax({
            type: "POST",
            url: base_url + "admin/schedule/process",
            data: {companyid: companyid}
        }).done(function (data) {
            $('#response').show();
            $("#response").html(data);
            $('#gigselect').css('display', 'none');
        });
    });

    $('#multi_attest').click(function() {

        var sid = $.map($('input[name="attest_check"]:checked'), function(c){return c.value; });
        var allIds = $.map($('input[name="attest_check"]'), function(c){return c.value; });

        if(sid == '') {
            sid = '';
        }
        $.ajax({
            url: base_url + 'admin/schedule/attested/',
            type:'POST',
            data:{ sid : sid, allIds: allIds },
            success:function(data){
                if(data == 1) {
                    successMsg('3000', 'Selected Shift Updated.');
                    location.reload();
                }
            }
        });
    });

    $('#shift_report').on('show.bs.modal', function (e) {

        var scheduleid = $(e.relatedTarget).data('scheduleid');
        var from = $(e.relatedTarget).data('from');
        var to = $(e.relatedTarget).data('to');
        var checkin = $(e.relatedTarget).data('checkin');
        var checkout = $(e.relatedTarget).data('checkout');
        var comment = $(e.relatedTarget).data('comment');
        var start = $(e.relatedTarget).data('start');
        var stop = $(e.relatedTarget).data('stop');

        $('#from_popup').val(from);
        $('#to_popup').val(to);
        $('#checkin_popup').val(checkin);
        $('#checkout_popup').val(checkout);
        $('#start_popup').val(start);
        $('#stop_popup').val(stop);
        $('#popup_comment').val(comment);
        $('#scheduleId').val(scheduleid);
        $('#mapUrl').attr('href', '/admin/schedule/checkinmap?id=' + scheduleid);

        $('.popup_calender').datetimepicker({
            format: "YYYY-MM-DD HH:mm",
            widgetPositioning: {
                vertical: 'bottom'
            },
            useCurrent: false
        });
    });

    $('#popup_schedule').click(function() {

        var update_start = $('#start_popup').val();
        var update_stop = $('#stop_popup').val();
        var update_comment = $('#popup_comment').val();
        var scheduleid = $('#scheduleId').val();

        $.ajax({
            url: base_url + 'admin/schedule/updateAttested/',
            type:'POST',
            data:{ scheduleid : scheduleid, update_start: update_start, update_stop : update_stop,                          update_comment : update_comment },
            success:function(data){
                var result = jQuery.parseJSON(data);
                if(result.val == 1) {
                    var td = '<a id="timeDiff-'+scheduleid+'" href="#shift_report" data-toggle="modal" data-from="'+result.from+'" data-to="'+result.to+'" data-checkin="'+result.checkin+'" data-checkout="'+result.checkout+'" data-scheduleid="'+scheduleid+'" data-comment="'+update_comment+'" data-start="'+update_start+'" data-stop="'+update_stop+'">' +
                        '<span data-html="true" data-toggle="tooltip" data-placement="bottom" title="'+ result.startTime + ' - ' + result.endTime + '' + update_comment+'">' +
                        result.timeDiff
                        +'</span></a>';

                    $(document).ajaxComplete(function () { $('[data-toggle="tooltip"]').tooltip(); });
                    $('#shiftHours-' + scheduleid).html(td);
                    $('#timeDiff-'+scheduleid).tooltip('hide');
                    $('#shift_report').modal('hide');
                    successMsg('3000', 'Selected Shift Updated.');
                }
            }
        });
    });

    $('#success_rate').on('show.bs.modal', function (e) {

        $('#rate_alert').hide();
        var scheduleid = $(e.relatedTarget).data('scheduleid');
        var gigstr_rating = $(e.relatedTarget).data('gigstr_rating');
        var gigstr_rating_comment = $(e.relatedTarget).data('gigstr_rating_comment');
        var internal_rating = $(e.relatedTarget).data('internal_rating');
        var internal_rating_comment = $(e.relatedTarget).data('internal_rating_comment');
        var gigstr_name = $(e.relatedTarget).data('gigstr_name');

        $('#gigstr_rating').addClass('gig_rate_'+scheduleid);
        $('.gig_rate_'+scheduleid).rating('update', gigstr_rating);

        $('#internal_rating').addClass('int_rate_'+scheduleid);
        $('.int_rate_'+scheduleid).rating('update', internal_rating);

        $('#gigstr_comment').val(gigstr_rating_comment);
        $('#internal_comment').val(internal_rating_comment);
        $('#rating_gigstr').html('Rating by ' + gigstr_name);
        $('#scheduleId').val(scheduleid);
    });

    $('#popup_rate').click(function() {
        var popup_rating = $('#internal_rating').val();
        var popup_comment = $('#internal_comment').val();
        var scheduleid = $('#scheduleId').val();

        if(popup_comment == '' || popup_rating == 0) {
            $('#rate_alert').show();
            return false;
        }

        $.ajax({
            url: base_url + 'admin/schedule/updateRating/',
            type:'POST',
            data: {popupRating: popup_rating, popupComment: popup_comment, scheduleId: scheduleid},
            success:function(data) {
                var result = jQuery.parseJSON(data);
                if(result.val == 1) {
                    if(result.internal_rating > 0) {
                        var starRate = '<a class="star_rate" href="#success_rate" data-toggle="modal" ' +
                            'data-gigstr_name="'+result.name+'" data-scheduleid="'+scheduleid+'"' +
                            'data-gigstr_rating="'+result.gigstr_rating+'"' +
                            'data-gigstr_rating_comment="'+result.gigstr_rating_comment+'"' +
                            'data-internal_rating="'+ result.internal_rating +'" data-internal_rating_comment ="'+result.internal_rating_comment+'" >';
                        starRate += '<div data-html="true" data-toggle="tooltip" data-placement="bottom" title="'+ result.internal_rating_comment +'">';
                        starRate += '<span>';
                        for(var i = 0; i < result.internal_rating; i++) {
                            starRate += '&#9733';
                        }
                        starRate += '</span>';
                        for(var j = 0; j < (5 - result.internal_rating); j++) {
                            starRate += '&#9733';
                        }
                        starRate += '</div></a>';
                        $('#attest_rate_' + scheduleid).html(starRate);
                        $(document).ajaxComplete(function () { $('[data-toggle="tooltip"]').tooltip(); });
                    }
                }
                $('#success_rate').modal('hide');
                successMsg('3000', 'Selected Shift Updated.');
            }
        });
    });

    $('.select2-minimum').select2({
        placeholder: 'Select state',
        // data: gigList,
        minimumInputLength: 1
    });

    $('#attest_image').on('show.bs.modal', function(e) {

        var scheduleId = $(e.relatedTarget).data('scheduleid');
        //var scheduleImage = $(e.relatedTarget).data('images');

        var slider = '<ol class="carousel-indicators dotstyle center">';

        var controllers = '<a class="left carousel-control" href="#carousel-schedule" data-slide="prev"> ' +
            '<i class="fa fa-angle-left"></i></a>' +
            '<a class="right carousel-control" href="#carousel-schedule" data-slide="next">' +
            '<i class="fa fa-angle-right"></i></a>';

        var images = '<div class="carousel-inner">';

        $.ajax({
            url: base_url + 'admin/schedule/scheduleImage/',
            type:'POST',
            data:{ scheduleId: scheduleId },
            success:function(data){
                if(data) {
                    $('#carousel-schedule').addClass('carousel slide');
                    var attImage = jQuery.parseJSON(data);
                    $.each(attImage, function(i, item) {
                        if(i == 0) {
                            var active = "active";
                        } else {
                            active = "";
                        }

                        slider += '<li class="'+ active +'" data-target="#carousel-schedule" data-slide-to="'+ i +'"><a href="#">slide1</a></li>';

                        images += '<div data-slide-no="'+ i +'" class="item ' +active + '">' +
                            '<img class="img-responsive" src="' + base_url + 'img/schedule_images/'+item.image+'" alt="Image2"></div>';
                    });

                    slider += '</ol>';
                    images += '</div>';
                    slider += images;
                    slider += controllers;

                    $(document).ajaxComplete(function () { $('#carousel-schedule').html(slider); });

                }
            }

        });


    });

    $('#data_shift').on('show.bs.modal', function (e) {
        var scheduleId = $(e.relatedTarget).data('scheduleid');

        $.ajax({
            url: base_url + 'admin/schedule/getDataShiftValue/',
            type: 'POST',
            data: {scheduleId: scheduleId},
            success: function (data) {
                $('#data_shift_body').html(data);
            }
        });
    });

    $(document).ajaxComplete(function () {
        $('#data_schedule_update').click(function() {
            var fromData = $('#data_shift_form').serialize();
            var scheduleId = $('#scheduleId').val();

            $.ajax({
                url: base_url + 'admin/schedule/updateDataShiftValue/',
                type: 'POST',
                data: {fromData: fromData, scheduleId: scheduleId},
                success: function (data) {
                    $('#data_shift_td_' + scheduleId).html(data);
                    /*$('[data-toggle="tooltip"]').tooltip();*/
                    $('#data_shift').modal('hide');
                    successMsg('3000', 'Selected Shift Updated.');
                }
            });

        });
    });

});


function successMsg(time, msg) {
    $.extend($.gritter.options, {
        position: 'bottom-left'
    });
    $.gritter.add({
        title: 'Done !!!',
        text: msg,
        time: time,
        close_icon: 'l-arrows-remove s16',
        icon: 'glyphicon glyphicon-ok',
        class_name: 'success-notice'
    });
}
