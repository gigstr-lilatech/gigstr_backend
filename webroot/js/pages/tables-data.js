//------------- tables-data.js -------------//
$(document).ready(function () {


    //------------- Data tables -------------//
    //Data table for gigs
    $('#basic-datatables-gigs').dataTable({
        "language": {
            "sSearch": "",
            "sLengthMenu": "<span>_MENU_</span>",
            "searchPlaceholder": "Search"
        },
        "sDom": "<'row'<'col-md-6 col-xs-12 'l><'col-md-6 col-xs-12'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>",
        bFilter: true,
        "sDom" :'fptip',
        "iDisplayLength": 25,
        "columnDefs": [
            {"orderable": false
            }
        ]
    });
    
     $('#basic-datatables-jobs').dataTable({
        "language": {
            "sSearch": "",
            "sLengthMenu": "<span>_MENU_</span>",
            "searchPlaceholder": "Search"
        },
        "sDom": "<'row'<'col-md-6 col-xs-12 'l><'col-md-6 col-xs-12'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>",
        bFilter: true,
        "sDom" :'fptip',
        "iDisplayLength": 30,
        "order": [[ 1, "desc" ]],
         "bDeferRender": true,
        "columnDefs": [
            {"orderable": true
            }
        ]
    });

    // Data table for candidates
    $('#basic-datatables-candidates').dataTable({
        "language": {
            "sSearch": "",
            "sLengthMenu": "<span>_MENU_</span>",
            "searchPlaceholder": "Search"
        },
        "sDom": "<'row'<'col-md-6 col-xs-12 'l><'col-md-6 col-xs-12'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>",
        bFilter: true,
        "order": [[ 1, "desc" ]],
        "sDom" :'fptip',
        "iDisplayLength": 25,
        "columnDefs": [
            { "orderable": false,
               // "targets": [5,6]
            }
        ]
    });

      // Data table for chat
    $('#basic-datatables-chat').dataTable({
        "language": {
            "sSearch": "",
            "sLengthMenu": "<span>_MENU_</span>",
            "searchPlaceholder": "Search"
        },
        "sDom": "<'row'<'col-md-6 col-xs-12 'l><'col-md-6 col-xs-12'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>",
        "dom": '<"pull-left"f><"pull-right"l>tip',
        "bFilter": true,
        "bSort": false,
        "bPaginate" : true,
        "sDom" :'fptip',
        "iDisplayLength": 30,
        "columnDefs": [
            { "orderable": false,
                //"targets": 1
                }
        ]

    });

    // Data table for companies
    $('#basic-datatables-companies').dataTable({
        "language": {
            "sSearch": "",
            "sLengthMenu": "<span>_MENU_</span>",
            "searchPlaceholder": "Search"
        },
        "sDom": "<'row'<'col-md-6 col-xs-12 'l><'col-md-6 col-xs-12'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>",
        bFilter: true,
        "sDom" :'fptip',
        "iDisplayLength": 25,
        "columnDefs": [
            {"orderable": false,
                //"targets": [2, 4]
            }
        ]
    });

    //Data table for applicants
    $('#basic-datatables-applicants').dataTable({
        "language": {
            "sSearch": "",
            "sLengthMenu": "<span>_MENU_</span>",
            "searchPlaceholder": "Search"
        },
        "sDom": "<'row'<'col-md-6 col-xs-12 'l><'col-md-6 col-xs-12'f>r>t<'row'<'col-md-4 col-xs-12'i><'col-md-8 col-xs-12'p>>",
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "sDom" :'fptip',
        "iDisplayLength": 25,
        "bInfo": false,
        "bAutoWidth": false,
        "ordering": false,
        "columnDefs": [
            {"orderable": false, "targets": 2}
        ]
    });

});




$(window).ready(function() {
    $('.loading').show(0).delay(1000).hide(0);
});