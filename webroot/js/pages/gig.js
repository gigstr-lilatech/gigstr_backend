//------------- login.js -------------//
$(document).ready(function () {

    $('#summernote_gig').summernote({
        height: 200,
        disableDragAndDrop: true,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']]
        ],
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });

    $('#push_message_ajax').attr('disabled', 'disabled');

    $('#add_company').click(function () {
        $(this).hide();
        $('.save_comp').show();
    });

    $('#push_message').keydown(function () {

        if (!$('#push_message').val()) {
            $('#push_message_ajax').attr('disabled', 'disabled');
        } else {
            $('#push_message_ajax').removeAttr('disabled');

        }
    });
    
    $('#push_message').blur(function () {

        if (!$('#push_message').val()) {
            $('#push_message_ajax').attr('disabled', 'disabled');
        } else {
            $('#push_message_ajax').removeAttr('disabled');

        }
    });

    $('#active').change(function () {
        //alert('sdsdfsfdsf');
        if ($(this).is(":checked")) {
            $('#push_message').removeAttr('disabled');
        } else {
            $('#push_message').attr('disabled', 'disabled');
        }
    });

    $('#company_name').keydown(function () {
        var company_name = $('#company_name').val();

        if (company_name != '') {
            $('#save_company_ajax').removeAttr('disabled');
        } else {
            $('#save_company_ajax').attr('disabled', 'disabled');
        }
    });

    $('#save_company_ajax').click(function () {
        var company_name = $('#company_name').val().trim();

        if (company_name === '') {
            alert('Company name cannot be blank');
        } else {
            $.ajax({
                url: base_url + "admin/company/create?company=" + company_name,
                dataType: 'json',
                async: false,
                success: function (result) {
                    if (result.status == '200') {
                        $("#error-message").hide();
                        var option_value = '<option value="' + result.id + '">' + result.name + '</option>';
                        $('#company_id').append(option_value);
                        $('#company_id').val(result.id);
                        $('#company_name').val('');
                        $('#company_name').focus();
                        return false;
                    } else {
                        if (result.status == '108') {
                            $('#error-message').html('This Company already exist');
                            $("#error-message").show();
                        } else {
                            $('#error-message').html('Unable to add company now. Please try again.');
                            $("#error-message").show();
                        }
                        return false;
                    }
                },
                error: function (results) {
                    $('#error-message').html('Unable to add company now. Please try again.');
                    $("#error-message").show();
                    // alert('error');
                }
            });
        }

    });


    $('#push_message_ajax').click(function () {
        var push_message = $('#push_message').val();
        var gig_id = $('#gig_id').val();
        var gig_country = $('#country').val();
        if ($('#area_push').prop('checked')) {
            var push_area = 1;
        } else {
            push_area = 0;
        }

        /** Old Push */
        /*$.ajax({
            url: base_url + "push/send",
            dataType: 'json',
            data: { push_message : push_message, gig_id : gig_id, gig_country: gig_country },
            method: "POST",
            async: false,
            success: function (result) {
                if(result.status == '0'){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('#push_success').show();
                    $('#push_error').hide();
                }
                else{
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $('#push_success').hide();
                    $('#push_error').show();
                }
            }
        });*/

        /** New push */
        $.ajax({
            url: base_url + "admin/gig/jobPush",
            dataType: 'json',
            data: { push_message : push_message, gig_id : gig_id, gig_country: gig_country, push_area: push_area},
            method: "POST",
            async: false,
            success: function (result) {
                if(result == 0){
                    successMsg('2000', 'Push message has been sent.');
                }
                else{
                    successMsg('2000', 'Unable to send push message. Please try again.');
                }
            }
        });

    });


    $('#deep_link_btn').click(function() {

        var app_id = $('#gig_id').val();
        var title = $('#title').val();
        var desc = '';

        $.ajax({
            url: base_url + "admin/gig/link",
            dataType: 'json',
            data: { gig_id : app_id},
            method: "POST",
            async: false,
            success: function (data) {
                if(data.link == "NOLINK") {
                    branch.link({
                        tags: [ 'tag1', 'tag2' ],
                        channel: 'mobile_web',
                        feature: 'deepview',
                        stage: 'created link',
                        data: {
                            '$desktop_url': 'https://www.gigstr.com/',
                            'share_gig' : app_id,
                            '$og_title' : title,
                            'user_id' : data.user,
                            'type' : 'gig_share',
                            'open_app': true,
                            '$always_deeplink': true
                        },
                        "source":"web-sdk",
                        "branch_key":"key_live_jbxl88cdgfC29C2FbvS8TpffEulXn15t"
                    },
                    function(err, link) {
                        $.ajax({
                            url: base_url + "admin/gig/link_save",
                            dataType: 'json',
                            data: {gig_id: app_id, link: link},
                            method: "POST",
                            async: false,
                            success: function (result) {

                            }
                        });

                        $('.deep_link').html(link);
                        $("#deep_link_btn").attr("disabled", true);

                    });
                }
                else {
                    $('.deep_link').html(data.link);
                    $("#deep_link_btn").attr("disabled", true);
                }
            }
        });

    });

    $('.gig_activate').click(function() {

        var data = $('#gig_form').serialize();

        var activeCheck = $(this).data("active");
        if(activeCheck == 'Published') {
            var active = '1';
        } else {
            active = '2';
        }

        $.ajax({
            url: base_url + "admin/gig/activate_update",
            dataType: 'json',
            data: { data : data, active: active },
            type: "POST",
            async: false,
            success: function () {
                location.reload();
            }
        });
    });

    /**
     * Active for 7 days
     */
    $('#activate_days').click(function() {
        var gigId = $(this).data("id");
        var active = "1";
        $.ajax({
            url: base_url + "admin/gig/activeDays",
          //  dataType: 'json',
            data: { gigId : gigId, active: active },
            type: "POST",
            success: function (data) {
                $('.setStatus').html('Active');
                $('#activate_days').attr("disabled", true);
                $('#declined_add').attr("disabled", false);
                successMsg('2000', 'Successfully Activate for 14 days.');
            }
        });
    });

    /**
     * Declined Ad
     */
    $('#declined_add').click(function() {
        var gigId = $(this).data("id");
        var active = "5";
        $.ajax({
            url: base_url + "admin/gig/activeDays",
            //  dataType: 'json',
            data: { gigId : gigId, active: active },
            type: "POST",
            success: function (data) {
                $('.setStatus').html('Declined');
                $('#declined_add').attr("disabled", true);
                $('#activate_days').attr("disabled", false);
                successMsg('2000', 'Successfully Declined the Ad.');
            }
        });
    });


    var searchRequest = null;
    var minlength = 0;

    $("#addSearch").keyup(function () {
        var that = this,
            value = $(this).val();

        if (value.length != 0) {
            $('.pagination').hide();
        } else {
            $('.pagination').show();
        }
        var table = $('#addTable');

        if (value.length >= minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "POST",
                url: base_url + "admin/gig/ajaxSearch",
                data: {
                    'search_keyword': value
                },
                success: function(data){
                    if (value==$(that).val()) {

                        var jobs = jQuery.parseJSON(data);
                        var maxSize = jobs.length;
                        var sta = 0;
                        var elements_per_page = 30;

                        if(maxSize < elements_per_page) {
                            var limit = maxSize;
                            $('.pagination_button').css('display', 'none');
                        } else {
                            $('.pagination_button').css('display', 'block');
                            limit = elements_per_page;
                        }
                        if(value.length == 0) {$('.pagination_button').css('display', 'none');}

                        goPagination(sta,limit);

                        function goPagination(sta,limit) {
                            table.html('');
                            for (var i = sta ; i < limit; i++) {

                                var created = new Date(jobs[i]['created']).toISOString().substring(0, 10);
                                var modified = new Date(jobs[i]['modified']).toISOString().substring(0, 10);
                                var application = jobs[i]['application'].length;

                                if(jobs[i]['title'] != "") {
                                    var title = jobs[i]['title'];
                                }
                                else {
                                    title = jobs[i]['skill']['skill'];
                                }

                                var html = '<tr>' +
                                    '<td>' + jobs[i]['id'] + '</td>' +
                                    '<td><a href="'+base_url +'admin/gig/edit/'+ jobs[i]['id'] +'">' + title + '</a></td>' +
                                    '<td>' + jobs[i]['sub_title2'] + '</td>' +
                                    '<td>' + jobs[i]['company']['name'] + '</td>' +
                                    '<td>' + jobs[i]['countryName'] + '</td>' +
                                    '<td>' + jobs[i]['user']['name'] + '</td>' +
                                    '<td>' + jobs[i]['status'] + '</td>' +
                                    '<td>' + created + '</td>' +
                                    '<td>' + modified + '</td>' +
                                    '<td>' + application + '</td>' +
                                    '</tr>';
                                table.append(html);
                            }
                        }

                        $('#nextValue').click(function(){
                            var next = limit;
                            if(maxSize>=next) {
                                limit = limit+elements_per_page;
                                table.empty();
                                goPagination(next,limit);
                            }
                        });
                        $('#PreeValue').click(function(){
                            var pre = limit-(2*elements_per_page);
                            if(pre>=0) {
                                limit = limit-elements_per_page;
                                table.empty();
                                goPagination(pre,limit);
                            }
                        });
                    }
                }
            });
        }
    });


    $('#editor').froalaEditor({
        key: 'aA-7rB-22njmF4bmn==',
        toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'formatUL',  '|', 'insertLink', '|', 'clearFormatting', 'html', '|', 'undo', 'redo'],
        codeBeautifierOptions: {
            end_with_newline: true,
            indent_inner_html: true,
            extra_liners: "['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'ol', 'table', 'dl']",
            brace_style: 'expand',
            indent_char: ' ',
            indent_size: 4,
            wrap_line_length: 0
        },
        height: 300
    });


    $('.candidate_state').change(function() {
        var select = $(this).val();
        if(select != "") {
            var gigId = $(this).data("gig");
            var userId = $(this).data("user");
            var application_id = $(this).data("application");
            var selection_id = $(this).data("selection");

            $.ajax({
                url: base_url + "admin/gig/selectCandidate",
                data: {gigId : gigId, userId: userId, select:select, selection_id:selection_id},
                type: "POST",
                success: function (selection) {
                    var str = select.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                        return letter.toUpperCase();
                    });
                    $('#select_' + application_id).html(str);
                }
            });
        }
    });

});


function successMsg(time, msg) {
    $.extend($.gritter.options, {
        position: 'top-right'
    });
    $.gritter.add({
        title: 'Done !!!',
        text: msg,
        time: time,
        close_icon: 'l-arrows-remove s16',
        icon: 'glyphicon glyphicon-ok',
        class_name: 'success-notice'
    });
}