//------------- chat.js -------------//
$(document).ready(function () {

//    $('#star').raty({
//        score : rating,
//        cancel: true,
//        target     : '#target',
//        targetType : 'number',
//        click       : function(score, evt) {
//            $('#rating').val(score);
//        }
//    });

    /** chat function is not use any more */
    if (chat_id > 0) {
    //    read_chat();
    }

    $("#chatbox").animate({scrollTop: $('#chatbox').prop("scrollHeight")}, 1000);

    $('#chat_send').click(function () {
 
        var chat_msg = $('#chat_msg').val().replace( /\n/g, '<br>' );
        var d = new Date();
        var month = d.getMonth()+1;
        var admin =  $('#admin-user').val();
        var now = d.getFullYear() + '-' + month + '-' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes();

        var chat_html = '<span class="left_msg">' + chat_msg + '</span><br/><span class="chat_time_left">'+ now +', '+ admin +'</span><br/>';
        $('#chatbox').append(chat_html);
        $("#chatbox").animate({scrollTop: $('#chatbox').prop("scrollHeight")}, 1000);
        $('#chat_msg').val('');

      if (chat_id > 0) {

        if (chat_msg == '') {
            //$('#chat_msg').val('Company name cannot be null');
        } 
        else {
            $.ajax({
                url: base_url + "admin/chat/send",
                dataType: 'json',
                method: "POST",
                data: {message: chat_msg, user_id: user_id, chat_id: chat_id},
                async: true,
                success: function (result) {
                    if (result.status == '0') {
                        //var chat_html = '<span class="left_msg">' + chat_msg + '</span><br/><span class="chat_time_left">' + result.created + ', ' + user_name + '</span><br/>';
                        //$('#chatbox').append(chat_html);
                        //$("#chatbox").animate({scrollTop: $('#chatbox').prop("scrollHeight")}, 1000);
                        //$('#chat_msg').val('');
                         return true;
                    } else {
                        $('#chatbox').append('<small class="text-danger">Message not delivered.</small>');
                        $('#chat_msg').val('');
                        return false;
                    }
                },
                error: function (result) {
                    $('#chatbox').append('<small class="text-danger">Message not delivered.</small>');
                    $('#chat_msg').val('');
                    return false;
                }
            });
        }
    }else{
        
    }

    });
    
    /*$('#update_profile').click(function () {
        $('#candidate_rating').val($('#candidate_rating_dummy').val());
        $('#name').val($('#candidate_name').val());
        $('#profile_form').submit();
    });*/
    
    $('#applications').change(function () {
        chat_id = $(this).val();
        $.ajax({
            url: base_url + "admin/chat/listChat",
            dataType: 'html',
            method: "POST",
            data: {user_id: user_id, chat_id: chat_id, candidate_id: candidate_id},
            async: false,
            success: function (result) {
                $('#chatbox').html(result);
                $("#chatbox").animate({scrollTop: $('#chatbox').prop("scrollHeight")}, 1000);
            },
            error: function (results) {

            }
        });
    });
    //$('#basic-datepicker').datepicker('options','format', 'yyyy-mm-dd');
    //$('#basic-datepicker').datepicker('setValue', dob_date);
    //$('#basic-datepicker').datepicker("update");
   // $("#basic-datepicker").focus();
    $("#basic-datepicker").datepicker({
        format: 'yyyy-mm-dd'
    });
});

function read_chat() {
    $.ajax({
        url: base_url + "admin/candidate/getUnread",
        dataType: 'html',
        method: "POST",
        data: {user_id: user_id, chat_id: chat_id, candidate_id: candidate_id},
        async: true,
        success: function (result) {
            $('#chatbox').append(result);
            if(result!='') {
                $("#chatbox").animate({scrollTop: $('#chatbox').prop("scrollHeight")}, 1000);
            }
        },
        error: function (results) {

        }
    });

    //setTimeout(read_chat, 7000);
}
