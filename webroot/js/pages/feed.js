/**
 * Created by yasith on 8/4/17.
 */

$(document).ready(function() {

    /** PANEL **/
    var max_fields      = 99;
    var x = 1;
    var ourWorld = $("#ourWorldPanel");
    var helpGig = $("#gigHelpPanel");
    var addButton = $("#addPanel");
    var addButtonHelp = $("#addPanelHelpGig");

    ourWorld.sortable({
        update: function (event, ui) {
            var feedForm = $('#ourWorldForm').serialize();
            panelOrderAjax(feedForm);
        }
    });

    helpGig.sortable({
        update: function (event, ui) {
            var feedForm = $('#gigHelpForm').serialize();
            panelOrderAjax(feedForm);
        }
    });

    $(addButton).click(function(){
        newPanel(max_fields, ourWorld);
    });

    $(addButtonHelp).click(function(){
        newPanel(max_fields, helpGig);
    });


    /** remove pannel */
    $(ourWorld).on("click",".removePanel", function(e){
        e.preventDefault();

        var panelId = $(this).data("panelid");
        var deleteItem = $(this).parent().closest('li');

        if((deleteItem).hasClass('newComponent')) {
            deleteItem.remove();
        } else {
            deletePanel(panelId, deleteItem);
        }
    });
    $(helpGig).on("click",".removePanel", function(e){
        e.preventDefault();

        var panelId = $(this).data("panelid");
        var deleteItem = $(this).parent().closest('li');
        if((deleteItem).hasClass('newComponent')) {
            deleteItem.remove();
        } else {
            deletePanel(panelId, deleteItem);
        }
    });

    /*** carousel Items add */
    var wrapper =  $ ("#sortable" );
    var add_button = $("#add");
    $(add_button).click(function(e){
        e.preventDefault();
        var
            dropzoneCount = $('.feedDropZone').length,
            dropzoneClass = 'feedDropZone-' + dropzoneCount;
        if(x < max_fields){
            $(wrapper).prepend(
                '<li class="ui-state-default addNew sub_component">' +
                '<div class="panel panel-default">' +
                '<div class="panel-heading"><h4 class="panel-title">' +
                '<a href="javascript:;" class="remove pull-right">' +
                '<i class="fa fa-times" aria-hidden="true"></i></a>' +
                '</h4></div>' +
                '<div class="panel-body">' +
                '<div class="row"><div class="col-lg-6 panel-input">' +
                '<input class="itemId_'+ x +'" type="hidden" name="item['+ x +'][itemId]" value="">' +
                '<input type="hidden" name="item['+ x +'][count]" value="'+ x +'">' +
                '<input class="form-control" name="item['+ x +'][imageTitle]" type="text" placeholder="Image title">' +
                '<input class="form-control" name="item['+ x +'][desc]" type="text" placeholder="Description">' +
                '<select class="form-control" name="item['+ x +'][linkType]">' +
                '<option value="">Select Type</option><option value="web">Web</option><option value="youtube">Youtube</option>' +
                '</select>' +
                '<input class="form-control" name="item['+ x +'][link]" type="text" placeholder="link">' +
                '<input type="hidden" name="item['+ x +'][image]" id="feedImage_'+ x +'">' +
                '<input type="hidden" name="item['+ x +'][thumbnail]" id="feedImageThumbnail_'+ x +'">' +
                '</div><div class="col-lg-6">' +
                '<div class="dropzone small-dropzone feedDropZone ' + dropzoneClass + '" data-count="'+ x +'">' +
                '<div class="dz-message" data-dz-message><p>Drop image &nbsp; (800x600px)</p></div></div>' +
                '</div>' +
                '</div></div></div></li>');
            initDropZone(dropzoneClass);
            x++;
        }
    });

    $(wrapper).on("click",".remove", function(e){
        e.preventDefault();
        $(this).parent().closest('li').remove();
        x--;
    });

    wrapper.sortable({
        update: function (event, ui) {
            var feedForm = $('#feedForm').serialize();
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/feed/saveFeedItemOrder/',
                data: {feedForm: feedForm},
                success: function (data) {
                    if(data == 1) {
                        successMsg('2000', 'Successfully change the Items order.');
                    }
                }
            });
        }
    });

    /*** select component on change type  */
    $('#uiComponent').change(function() {
        var component = $(this).val();
        $('#feedId').val("");
        $('#feedForm input').val("");
        $('#componentAction').val("");
        $('#feedButton').show();
        if(component == "button") {
            $('#componentTitleOuter, #componentActionOuter, #componentUrlOuter').show();
            $('#sortable, #add').hide();
            $('#componentTitleOuter label').html('Button text');
        }
        else if(component == "carousel") {
            $('#componentTitleOuter, #add, #sortable').show();
            $('#componentActionOuter, #componentUrlOuter').hide();
            $('#componentTitleOuter label').html('Component Title');
        }
        else if(component == "link") {
            $('#componentTitleOuter, #componentActionOuter, #componentUrlOuter').show();
            $('#sortable, #add').hide();
            $('#componentTitleOuter label').html('Link text');
        }
    });


    /** Form submit AJAX**/
    $('#feedButton').click(function() {
        var feedForm = $('#feedForm').serialize();
        var section = $('.section.active').data('section');

        $.ajax({
            type: 'POST',
            url: base_url + 'admin/feed/saveFeedForm/',
            data: {feedForm: feedForm, section: section},
            success: function (data) {
                var feed = jQuery.parseJSON(data);
                $('#feedId').val(feed.feedId);
                $.each(feed.itemId, function(k, v) {
                    if(k < 99) {
                        $('.itemId_' + k).val(v);
                    }
                });

                if(feed.newFeed.length != 0) {
                    var newfeedHtml = '<li>' +
                        '<input class="feed_order" type="hidden" name="feed['+ feed.newFeed.id +']" value="'+ feed.newFeed.feed_order +'">' +
                        '<div class="panel panel-default panelMove toggle panelRefresh panelClose">' +
                        '<div class="panel-heading">' +
                        '<h4 class="panel-title">' +
                        '<a data-panelid="'+ feed.newFeed.id +'" href="javascript:;" class="removePanel pull-right">' +
                        '<i class="fa fa-times" aria-hidden="true"></i></a>' +
                        '</h4></div>' +
                        '<div class="panel-body">' +
                        '<p class="text-center">'+ feed.newFeed.title +'</p>' +
                        '<a href="javascript:;" id="'+ feed.newFeed.id +'" class="feed_component feed_component_ajax_'+ feed.newFeed.id +'">' +
                        '<img class="img-responsive center-block" src="/img/'+ feed.newFeed.ui_type +'.png">' +
                        '</a></div></div></li>';

                    $('.newComponent').remove();
                    if(feed.newFeed.section == 'client') {
                        $('#ourWorldPanel').prepend(newfeedHtml);
                    } else {
                        $('#gigHelpPanel').prepend(newfeedHtml);
                    }
                    $('.feed_component_ajax_' + feed.newFeed.id).click(function(){
                        var componentId = $(this).attr('id');
                        loadItems(componentId);

                    });
                }

                if(feed.status == 'add') {
                    successMsg('3000', 'Gig Explore Component Successfully saved.');
                } else {
                    successMsg('3000', 'Gig Explore Component Successfully updated.');
                }
            }
        });

    });


    /*** Load carousel items in update*/
    $('.feed_component').click(function(){
        var componentId = $(this).attr('id');
        loadItems(componentId);

    });


    /*** Call initial drop zone */
    initDropZone();

});


/** Image upload in AJAX success */
$(document).ajaxComplete(function () {

  //  initDropZone();

    $("#sortable").on("click",".removeItem", function(e) {

        var itemId = $(this).data("itemid");
        var deleteItem = $(this).parent().closest('li');
        swal({
                title: "",
                text: "Are you sure you want to delete this ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/feed/removeCarouselItem/',
                data: {itemId: itemId},
                success: function (data) {
                    if(data == 1) {
                        deleteItem.remove();
                        swal("Deleted!", "Carousel item has been deleted.", "success");
                    }
                }
            });

        });
    });
});


function successMsg(time, msg) {
    $.extend($.gritter.options, {
        position: 'top-right'
    });
    $.gritter.add({
        title: 'Done !!!',
        text: msg,
        time: time,
        close_icon: 'l-arrows-remove s16',
        icon: 'glyphicon glyphicon-ok',
        class_name: 'success-notice'
    });
}

function initDropZone(dropzoneClass) {

    var initClass = typeof dropzoneClass !== 'undefined' ? '.' + dropzoneClass : '.feedDropZone';
    Dropzone.autoDiscover = false;
    var minImageWidth = 800, minImageHeight = 600;
    $(initClass).dropzone({
        url: base_url + "admin/feed/upload",
        maxFiles: 1, // Number of files at a time
        maxFilesize: 5, //in MB,
        acceptedFiles: ".jpeg,.jpg,.png",
        addRemoveLinks: true,
        maxfilesexceeded: function (file) {
            /* done($('#image_upload_error > p').html("You have uploaded more than 1 Image. Only the first file will be uploaded!"));
             done($('#image_upload_error').show());*/
        },
        createImageThumbnails: true,
        success: function (file, response) {
            var imgObject = JSON.parse(response);
            var count = $(initClass).data("count");
            $('#feedImage_' + count).val(imgObject.filename);
            $('#feedImageThumbnail_' + count).val(imgObject.thumbnail);
        },
        error: function (file, response) {
        },
        init: function () {
            this.on("thumbnail", function (file) {
                if (file.width < minImageWidth || file.height < minImageHeight) {
                    file.rejectDimensions();
                }else if(file.size/(1024*1024) > 5){
                    file.rejectFilesize();
                }
                else {
                    file.acceptDimensions();
                }
            });
        },
        accept: function (file, done) {
              file.acceptDimensions = done;
             /*file.rejectDimensions = function () {
             done($('#image_upload_error > p').html("Image exceeds image ratio, please try another"));
             done($('#image_upload_error').show());
             };
             file.rejectFilesize = function () {
             done($('#image_upload_error > p').html("Image is too large, please try another"));
             done($('#image_upload_error').show());
             };*/
        }
    });
}

function newPanel(max_fields, outer) {
    var x = 1;
    if(x < max_fields){

        $(outer).prepend('<li class="newComponent">' +
            '<div class="panel panel-default panelMove toggle panelRefresh panelClose">' +
            '<div class="panel-heading">' +
            '<h4 class="panel-title">' +
            '<a href="javascript:;" class="removePanel pull-right">' +
            '<i class="fa fa-times" aria-hidden="true"></i></a>' +
            '</h4></div>' +
            '<div class="panel-body"><p>' +
            '<img class="img-responsive center-block" src="/img/placeholder.png">' +
            '</p></div>' +
            '</div></li>');
        x++;
    }
    $('#uiComponentOuter').show();
    $('#uiComponent').prop('disabled', false);
    $('#feedId').val("");
    $('#feedForm input').val("");
    $('#componentAction, #uiComponent').val("");

    $('#sortable, #add, #componentActionOuter, #componentUrlOuter, #componentTitleOuter').hide();

    $('#sortable').html('<li class="ui-state-default addNew sub_component" id="firstItem">' +
     '<div class="panel panel-default"><div class="panel-heading">' +
     '<h4 class="panel-title"><a href="javascript:;" class="remove pull-right">' +
     '<i class="fa fa-times" aria-hidden="true"></i></a></h4></div>' +
     '<div class="panel-body"><div class="row">' +
     '<div class="col-lg-6 panel-input">' +
     '<input class="itemId_0" type="hidden" name="item[0][itemId]" value=""><input type="hidden" name="item[0][count]" value="0">' +
     '<input class="form-control" name="item[0][imageTitle]" type="text" placeholder="Image title">' +
     '<input class="form-control" name="item[0][desc]" type="text" placeholder="Description">' +
     '<select class="form-control" name="item[0][linkType]"><option value="">Select Action</option><option value="web">Web</option><option value="youtube">Youtube</option></select>' +
     '<input class="form-control" name="item[0][link]" type="text" placeholder="link">' +
     '<input type="hidden" name="item[0][image]" id="feedImage_0">' +
     '<input type="hidden" name="item[0][thumbnail]" id="feedImageThumbnail_0"></div>' +
     '<div class="col-lg-6">' +
     '<div class="dropzone small-dropzone feedDropZone" data-count="0"><div class="dz-message" data-dz-message><p>Drop image &nbsp; (800x600px)</p></div>' +
     '</div></div></div></div></div></li>');
    initDropZone();
}

function panelOrderAjax(feedForm) {
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/feed/saveClientFeedOrder/',
        data: {feedForm: feedForm},
        success: function (data) {
            if(data == 1) {
                successMsg('2000', 'Successfully change the Component order.');
            }
        }
    });
}

function deletePanel(panelId, deleteItem) {

    swal({
            title: "",
            text: "Are you sure you want to delete this ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: base_url + 'admin/feed/removePanel/',
                data: {panelId: panelId},
                success: function (data) {
                    if(data == 1) {
                        deleteItem.remove();
                        swal("Deleted!", "Explore Panel has been deleted.", "success");
                    }
                }
            });

        });
}

function loadItems(componentId) {

    $('#feedButton').show();
    $.ajax({
        type: 'POST',
        url: base_url + 'admin/feed/loadItems/',
        data: { componentId: componentId },
        success : function(data) {
            if(data) {
                var feed = jQuery.parseJSON(data);
                $('#uiComponent').val(feed.componentHeaders.ui_type).prop('disabled', true);
                $('#componentTitle').val(feed.componentHeaders.title);
                $('#componentAction').val(feed.componentHeaders.url_action);
                $('#componentUrl').val(feed.componentHeaders.url);
                $('#componentTitleOuter, #uiComponentOuter').show();
                $('#feedId').val(feed.componentHeaders.id);

                if(feed.componentHeaders.ui_type == 'carousel') {
                    $('#sortable, #add').show();
                    $('#componentActionOuter, #componentUrlOuter').hide();
                    $('#componentTitleOuter label').html('Component Title');
                }
                else if(feed.componentHeaders.ui_type == 'button') {
                    $('#componentActionOuter, #componentUrlOuter').show();
                    $('#sortable, #add').hide();
                    $('#componentTitleOuter label').html('Button text');
                }
                else if(feed.componentHeaders.ui_type == 'link') {
                    $('#componentActionOuter, #componentUrlOuter').show();
                    $('#sortable, #add').hide();
                    $('#componentTitleOuter label').html('Link text');
                }

                var linkType = { ALL:'Select Action', web:'Web', youtube: 'Youtube' };

                var html = '';
                var dropzoneClass = '';
                var dropZone = [];
                $.each(feed.componentItems, function(key,value){
                     dropzoneClass = 'feedDropZone-' + value.id;

                    html += '<li class="ui-state-default addNew sub_component">' +
                        '<div class="panel panel-default"><div class="panel-heading">' +
                        '<h4 class="panel-title"><a data-itemid="'+ value.id +'" href="javascript:;" class="removeItem pull-right">' +
                        '<i class="glyphicon glyphicon-remove" aria-hidden="true"></i></a></h4></div>' +
                        '<div class="panel-body"><div class="row"><div class="col-lg-6 panel-input">' +
                        '<input class="itemId_'+value.id+'" type="hidden" value="'+value.id+'" name="item['+value.id+'][itemId]">' +
                        '<input type="hidden" name="item['+value.id+'][count]" value="'+value.id+'">' +
                        '<input value="'+value.title+'" class="form-control" name="item['+value.id+'][imageTitle]" type="text" placeholder="Image title">' +
                        '<input value="'+value.description+'" class="form-control" name="item['+value.id+'][desc]" type="text" placeholder="Description">' +
                        '<select class="form-control" name="item['+value.id+'][linkType]">';
                    $.each(linkType, function(link, type) {
                        html += '<option ';
                        if(link == value.link_type) {
                            html += ' selected ';
                        }
                        html += 'value="'+ link + '">'+type+'</option>';
                    });
                    html += '</select>' +
                        '<input value="'+value.url+'" class="form-control" name="item['+value.id+'][link]" type="text" placeholder="link">' +
                        '<input value="'+value.image +'" type="hidden" name="item['+value.id+'][image]" id="feedImage_'+value.id+'">' +
                        '<input value="'+value.image_thumbnail +'" type="hidden" name="item['+value.id+'][thumbnail]" id="feedImageThumbnail_'+value.id+'">' +
                        '</div><div class="col-lg-6">' +
                        '<div class="row">' +
                        '<div class="col-lg-12">' +
                        '<img class="img-responsive feed-item-img" src="' + base_url +'img/feed_images/'+ value.image_thumbnail +'" alt="feed item image">' +
                        '</div><div class="col-lg-12">' +
                        '<div class="dropzone small-dropzone feedDropZone feed-item '+ dropzoneClass +'" data-count="'+value.id+'">' +
                        '<div class="dz-message" data-dz-message><p class="feed-item-text">Drop image &nbsp; (800x600px)</p></div></div>' +
                        '</div></div></div></div></div></div></li>';
                    dropZone.push(value.id);
                });
                $('#sortable').html(html);

                $(document).ajaxComplete(function() {
                    for (var i = 0; i < dropZone.length; i++) {
                        initDropZone('feedDropZone-' + dropZone[i]);
                    }
                });
            }
        }


    });
}





