$(document).ready(function () {


    $('#update_profile').click(function () {
        $('#candidate_rating').val($('#candidate_rating_dummy').val());
        $('#name').val($('#candidate_name').val());
        $('#profile_form').submit();
    });

    $('#delete_candidate').click(function() {
        var check =  confirm('Do you want to delete User?');
        if(check == true){
            var user = $(this).attr('rel');

            $.ajax({
                url: base_url + "admin/user/deleteUser",
                dataType: 'json',
                data: { user : user },
                method: "POST",
                async: false,
                success: function (data) {
                    if(data == "0") {

                        successMsg('3000', 'Profile has been deleted.');
                        setTimeout(function() { window.location.replace(base_url + "admin/candidate");
                        }, 2000);
                      //  window.location.replace(base_url + "admin/candidate");
                    }
                    else {
                        $('#delete_mg').css('display','block');
                    }
                }
            });
        }
        else {
            return false;
        }
    });


    var searchRequest = null;
    var minlength = 0;

    $("#candidateSearch").keyup(function () {
        var that = this,
            value = $(this).val();

        if(value.length != 0) {
            $('.pagination').hide();
        } else { $('.pagination').show(); }
        var table =  $('#candidateTable');

        if (value.length >= minlength ) {
            if (searchRequest != null)
                searchRequest.abort();

            searchRequest = $.ajax({
                type: "POST",
                url: base_url + "admin/candidate/ajaxSearch",
                data: {
                    'search_keyword' : value
                },
                success: function(data){
                    //we need to check if the value is the same
                    if (value==$(that).val()) {

                        var candidates = jQuery.parseJSON(data);
                        var maxSize = candidates.length;
                        var sta = 0;
                        var elements_per_page = 30;

                        if(maxSize < elements_per_page) {
                            var limit = maxSize;
                            $('.pagination_button').css('display', 'none');
                        } else {
                            $('.pagination_button').css('display', 'block');
                            limit = elements_per_page;
                        }
                        if(value.length == 0) {$('.pagination_button').css('display', 'none');}

                        goPagination(sta,limit);

                        function goPagination(sta,limit) {
                            table.html('');
                            for (var i = sta ; i < limit; i++) {

                                var date = new Date(candidates[i]['created']).toISOString().substring(0, 10);

                                var app_mode = '';
                                if(candidates[i]['app_mode'] == 1) { app_mode = 'Work'}
                                else if (candidates[i]['app_mode'] == 2) { app_mode = 'Hire'}

                                var app_mode_preference = '';
                                if(candidates[i]['app_mode_preference'] == 1) { app_mode_preference = 'Work'}
                                else if (candidates[i]['app_mode_preference'] == 2) { app_mode_preference = 'Hire'}

                                var html = '<tr>' +
                                    '<td>' + candidates[i]['user_id'] + '</td>' +
                                    '<td><a href="' + base_url + 'admin/candidate/profile/' + candidates[i]['user_id'] + '">' + candidates[i]['user']['name'] + '</a></td>' +
                                    '<td>' + (candidates[i]['user']['email'] == null ? "" : candidates[i]['user']['email']) + '</td>' +
                                    '<td>' + (candidates[i]['phone_number'] == null ? "" : candidates[i]['phone_number']) + '</td>' +
                                    '<td>' + candidates[i]['country_name'] + '</td>' +
                                    '<td>' + (candidates[i]['city'] == null ? "" : candidates[i]['city']) + '</td>' +
                                    '<td>' + app_mode + '</td>' +
                                    '<td>' + app_mode_preference + '</td>' +
                                    '<td>' + candidates[i]['is_verified'] + '</td>' +
                                    '<td>' + candidates[i]['verified_company'] + '</td>' +
                                    '<td>' + candidates[i]['manual_payment'] + '</td>' +
                                    '<td>' + date + '</td>' +
                                    '</tr>';
                                table.append(html);
                            }
                        }

                        $('#nextValue').click(function(){
                            var next = limit;
                            if(maxSize>=next) {
                                limit = limit+elements_per_page;
                                table.empty();
                                goPagination(next,limit);
                            }
                        });
                        $('#PreeValue').click(function(){
                            var pre = limit-(2*elements_per_page);
                            if(pre>=0) {
                                limit = limit-elements_per_page;
                                table.empty();
                                goPagination(pre,limit);
                            }
                        });

                    }
                }
            });
        }
    });


    $('#verify').click(function() {
        var candidateId = $(this).data("id");

        $.ajax({
            url: base_url + "admin/candidate/verifyCandidate",
            data: { candidateId : candidateId },
            type: "POST",
            success: function () {
                $('.setStatus').html('Verified');
                $('#verify').attr("disabled", true);
                window.location.reload();
            }
        });
    });


    function successMsg(time, msg) {
        $.extend($.gritter.options, {
            position: 'top-right'
        });
        $.gritter.add({
            title: 'Done !!!',
            text: msg,
            time: time,
            close_icon: 'l-arrows-remove s16',
            icon: 'glyphicon glyphicon-ok',
            class_name: 'success-notice'
        });
    }


});
