//------------- forms-advanced.js -------------//
$(document).ready(function () {
    //------------- WYSIWYG summernote -------------//
    $('#summernote').summernote({
        height: 200,
        disableDragAndDrop: true,
        toolbar: [
             ['style', ['bold', 'italic', 'underline', 'clear']]
        ],
       /* callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }*/
    });


    var minImageWidth = 800, minImageHeight = 600;
    $("#my-awesome-dropzone").dropzone(
            {
                url: base_url + "admin/gig/upload",
                maxFiles: 1, // Number of files at a time
                maxFilesize: 5, //in MB,
                acceptedFiles: ".jpeg,.jpg",
                addRemoveLinks: true,
                maxfilesexceeded: function (file) {
                    //done('You have uploaded more than 1 Image. Only the first file will be uploaded!');
                    done($('#image_upload_error > p').html("You have uploaded more than 1 Image. Only the first file will be uploaded!"));
                    done($('#image_upload_error').show());
                },
                createImageThumbnails: true,
                success: function (file, response) {
                    obj = JSON.parse(response);
                    $('#gig_image').val(obj.filename);
                    //alert('Image uploaded successfully');
                },
                error: function (file, response) {
                    console.log(response);
                    //alert(file.size);
                },
                init: function () {
                    // Register for the thumbnail callback.
                    // When the thumbnail is created the image dimensions are set.
                    this.on("thumbnail", function (file) {
                        // Do the dimension checks you want to do
                        if (file.width < minImageWidth || file.height < minImageHeight) {
                            file.rejectDimensions();
                        }else if(file.size/(1024*1024) > 5){
                            file.rejectFilesize();
                        }
                        else {
                            file.acceptDimensions();
                        }
                    });
                },
                // Instead of directly accepting / rejecting the file, setup two
                // functions on the file that can be called later to accept / reject
                // the file.
                accept: function (file, done) {
                    file.acceptDimensions = done;
                    file.rejectDimensions = function () {
                        done($('#image_upload_error > p').html("Image exceeds image ratio, please try another"));
                        done($('#image_upload_error').show());
                    };
                    file.rejectFilesize = function () {
                        done($('#image_upload_error > p').html("Image is too large, please try another"));
                        done($('#image_upload_error').show());
                    };
                    // Of course you could also just put the `done` function in the file
                    // and call it either with or without error in the `thumbnail` event
                    // callback, but I think that this is cleaner.
                }
            }
    );
    
    $("#create_gig").validate({
		
		rules: {
			title: {
				required: true
			},
			sub_title1: {
				required: true
			},
			sub_title2: {
				required: true
			},
			summernote: {
				required: true
			}
		}
	});
        var count = 0;



    $("#form").validate({
		
		rules: {
			company: {
				required: true
			},
			from: {
				required: true
			},
			to: {
				required: true
			},
            summernote: {
				required: true
			},
             gig: {
				required: true
             }
        },
        submitHandler: function (form) {
            var from = $('#from').val();
            var to = $('#to').val();
            var gigstr = $('#candidate').val();
            var msg = '<div class="alert alert-success fade in"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button><i class="fa fa-check alert-icon "></i>Shift added successfully</div>';

            if (gigstr !== "") {
                $.ajax({
                type: "POST",
                url: base_url + "admin/schedule/checkschedule",
                data: { value : gigstr, fromdate: from, todate: to }
                }).done(function(data){
                    count = count + 1;
                    if(data === "1"){
                        $.confirm({
                            title: "Warning",
                            text: "Selected Gigstr is already scheduled for selected Shift Start Date or Shift End Date. Schedule shift anyway?",
                            confirm: function (button) {
                                $.ajax({
                                    url: form.action,
                                    type: form.method,
                                    data: $(form).serialize(),
                                    success: function (response) {
                                       //$('#success-message').css('display','none').show().append(msg);
                                        $.extend($.gritter.options, {
                                            position: 'bottom-left'
                                        });
                                        $.gritter.add({
                                            title: 'Done',
                                            text: 'Shift added successfully.',
                                            time: '',
                                            close_icon: 'l-arrows-remove s16',
                                            icon: 'glyphicon glyphicon-ok',
                                            class_name: 'success-notice'
                                        });
                                    }
                                });
                            },
                            cancel: function (button) {
                            },
                            confirmButton: "Confirm",
                            cancelButton: "Cancel"
                        });
                    }
                        else {
                            $.ajax({
                                url: form.action,
                                type: form.method,
                                data: $(form).serialize(),
                                success: function(response) {
                                    //$('#success-message').css('display','none').show().append(msg);
                                    $.extend($.gritter.options, {
                                        position: 'bottom-left'
                                    });
                                    $.gritter.add({
                                        title: 'Done',
                                        text: 'Shift added successfully.',
                                        time: '',
                                        close_icon: 'l-arrows-remove s16',
                                        icon: 'glyphicon glyphicon-ok',
                                        class_name: 'success-notice'
                                    });
                                }
                            });
                        }
                    }
                );
                }
                else {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: $(form).serialize(),
                        success: function(response) {
                           // $('#success-message').css('display','none').show().append(msg);
                            $.extend($.gritter.options, {
                                position: 'bottom-left'
                            });
                            $.gritter.add({
                                title: 'Done',
                                text: 'Shift added successfully.',
                                time: '',
                                close_icon: 'l-arrows-remove s16',
                                icon: 'glyphicon glyphicon-ok',
                                class_name: 'success-notice'
                            });
                        }
                    });
                }
            }
	    });
	});
