$(document).ready(function () {
            
              $('#idaddgigbutton').click(function () {
                        $('#addgigbutton').css('display', 'none');
                        $('#addgig').show();
                    });
                    
        $('#title').keydown(function () {
        var title = $('#title').val();

        if (title != '') {
            $('#error-message').css('display', 'none');
            $('#save_job_ajax').removeAttr('disabled');
        } else {
            $('#save_job_ajax').attr('disabled', 'disabled');
        }
    });
    
    $('#save_job_ajax').click(function () {
        
        var title = $('#title').val();
        var company= $('select[name="company"]').val();
        var country = $('select[name="country"]').val();
        
        
        if (title === '') {
            $('#error-message').html('Title can not be empty.');
            $("#error-message").show();
        } else {
            $.ajax({
                    type: "POST",
                    url: base_url + "admin/job/add",
                    data: { title : title, company:company, country:country }
                        }).done(function(data){
                            if (data === '200') {
                                $('#success-message').html('The gig has been saved.');
                                $("#success-message").show();
                                location.reload();
                            }
                            else
                            {
                                $('#error-message').html('The gig can not be saved.');
                                $("#error-message").show();
                            }
                        });
        }
        
    });
 
  });