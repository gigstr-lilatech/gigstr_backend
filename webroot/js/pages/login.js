//------------- login.js -------------//
$(document).ready(function () {

    //validate login form 
    $("#login-form").validate({
        ignore: null,
        ignore: 'input[type="hidden"]',
                errorPlacement: function (error, element) {
                    var place = element.closest('.input-group');
                    if (!place.get(0)) {
                        place = element;
                    }
                    if (place.get(0).type === 'checkbox') {
                        place = element.parent();
                    }
                    if (error.text() !== '') {
                        place.after(error);
                    }
                },
        errorClass: 'help-block',
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            password: {
                required: "",
                minlength: ""
            },
            email: "",
        },
        invalidHandler: function (form, validator) {
            var errors = validator.numberOfInvalids();

            if ($('#email').val() == "" || $('#password').val() == "") {
                $("#error-message").show().text("Please fill all fields");
            } else {
                if (errors) {
                    $("#error-message").show().text("Invalid username or password, try again");
                } else {
                    $("#error-message").hide();
                }
            }

        },
        highlight: function (label) {
            $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (label) {
            $(label).closest('.form-group').removeClass('has-error');
            label.remove();
        }

    });

});