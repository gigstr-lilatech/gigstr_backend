$(document).ready(function () {

    var today = '';
    $('#company').change(function(){
        var companyid=jQuery('select[name=company]').val();
        $.ajax({
        type: "POST",
        url: base_url + "admin/schedule/addgigprocess",
        data: { companyid : companyid }
            }).done(function(data){
                $("#gigId").html(data);
            });
    });

    $('#from').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
        }).on('change',function() {
            var date = $('#from').val();
            $('#to').val(date);

            return;
    });

    $('#to').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight: 'TRUE',
        //startDate: today,
       // endDate: 0,
        autoclose: true
    });

    $('#from_time').timepicker({
           upArrowStyle: 'fa fa-angle-up',
           downArrowStyle: 'fa fa-angle-down',
           showMeridian: false,
    });

    $('.shiftfromdate').datepicker({
       format: 'yyyy-mm-dd'
    });
    $('.shiftfromdate').datepicker({
       format: 'yyyy-mm-dd'
    });
            
    $('#to_time').timepicker({
           upArrowStyle: 'fa fa-angle-up',
           downArrowStyle: 'fa fa-angle-down',
           showMeridian: false
    });

    $( "#tags" ).autocomplete({
        source: "find",
        select: function (event, ui) {
            $('#candidate').val(ui.item.id);
          }
    });

    $('#tags').focusout(function() {
        //check_gig = $("#tags").val() != '';
        if($('#candidate').val() != '' && $("#tags").val() == '') {
            $('#candidate').val('');
        }
        else if ($('#candidate').val() != '' && $("#tags").val() != '') {
           // alert('candidate-' + $('#candidate').val());
        }
        else {
            $('#candidate').val('');
        }

    });

    $('.add_shift').submit(function(e) {
        var fromDate = $('#from').val();
        var fromTime = $('#from_time').val();
        var toDate = $('#to').val();
        var toTime = $('#to_time').val();

        $shiftData = $('#shiftData').val();
        $('#dataShift').val($shiftData);

        var fromtime = fromDate + ' ' + fromTime;
        var totime = toDate + ' ' + toTime;
        var diff = get_time_diff(fromtime, totime);

        if(diff < 0) {
            $('#errormassageto').css('display', 'block');
            return false;
        }
    });

});


function get_time_diff( fromtime, totime ) {

    //var datetime = typeof fromtime !== 'undefined' ? fromtime : "2014-01-01 01:02:03.123456";
    var   datetime = new Date( fromtime ).getTime();
    //var to = typeof totime !== 'undefined' ? totime : "2014-01-01 01:02:03.123456";
    var   to = new Date( totime ).getTime();
    if( isNaN(datetime) ) {
        return "";
    }
    var milisec_diff = to - datetime;
    return milisec_diff;
}


$(document).ready(function () {

    var options = {
        disableFields: ['autocomplete','button', 'text', 'header', 'radio-group', 'file', 'date', 'paragraph', 'hidden'],
        disabledAttrs: ["placeholder", "description", "access", "value", "subtype", "maxlength", "toggle", "inline", "other", "multiple", "min", "max", "step", "rows" ],
        disabledActionButtons: ['data', 'save'],
        typeUserEvents: {
            text: {
                onadd: function(fld) {
                    var $patternField = $('.form-control', fld);
                    $patternField.prop('disabled', true);
                }
            },
            number: {
                onadd: function(fld) {
                    var $patternField = $('.form-control', fld);
                    $patternField.prop('disabled', true);
                }
            }
        }
    };
    var dragndrop = $('#dragndrop');
    var formBuilder = dragndrop.formBuilder(options);


    $('#gig_template').click(function() {

        var from_json = formBuilder.actions.getData('json');
        var gig_id = $('#gigId').val();
        var desc = $('#editor').val();
        var gig_template_id = $('#gig_template_id').val();

        if(gig_id == null || gig_id == "") {
            $('#gig_err').html('<div class="alert alert-danger fade in">Please select a Gig</div>');
            return false;
        } else {
            $('#gig_err').html('');
        }

        $.ajax({
            url: base_url + 'admin/schedule/addGigTemplate/',
            type: 'POST',
            data: {fromJson: from_json, gigId: gig_id, desc: desc, gig_template_id: gig_template_id},
            success: function (data) {
                var json = $.parseJSON(data);
                var initial_val = JSON.stringify(json.form_json);
                if(json.status == 'save') {
                    $('#gig_err').html('<div class="alert alert-info fade in">Gig Template Successfully saved.</div>').delay(3000).fadeOut();
                    formBuilder.actions.setData(initial_val);
                }
                else {
                    $('#gig_err').show().html('<div class="alert alert-info fade in">Gig Template Successfully updated.</div>').delay(3000).fadeOut();
                    formBuilder.actions.setData(initial_val);
                }
            }
        });
    });


    $('#gigId').change(function() {

        $('#dragndrop_outer').css('display', 'block');
        $('#gig_template_msg').hide();
        var gigId = $(this).val();
        $.ajax({
            url: base_url + 'admin/schedule/getShiftForm/',
            type: 'POST',
            data: { gigId: gigId },
            success: function (data) {
                if(data != "0") {
                    var json = $.parseJSON(data);
                    var initial_val = JSON.stringify(json.form_json);
                    formBuilder.actions.setData(initial_val);
                    $('#gig_template_id').val(json.gig_template_id);
                }
                else {
                    formBuilder.actions.clearFields();
                }
            }
        });
    });


    $('.add_shift').submit(function(e) {


        /*e.preventDefault();
        $.confirm({
            title: "Warning",
            text: "Selected Gigstr is already scheduled for selected Shift Start Date or Shift End Date. Schedule shift anyway?",
            confirm: function (button) {
                /!*$.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success: function (response) {
                        $('#success-message').css('display','none').show().append(msg);
                    }
                });*!/
            },
            cancel: function (button) {
            },
            confirmButton: "Confirm",
            cancelButton: "Cancel"
        });*/



/*
        var from_json = formBuilder.actions.getData('json');
        var gigId = $('#gigId').val();

        if(from_json == '[]') {
        }
        else {
            $.ajax({
                url: base_url + 'admin/schedule/validateShiftForm/',
                type: 'POST',
                data: { gigId: gigId, from_json: from_json },
                success: function (data) {
                    if(data == "0") {
                        $('#gig_err').show().html('<div class="alert alert-info fade in">Please save a Gig template.</div>').delay(4000).fadeOut();
                        return false;
                    }
                }
            });
        }*/

    });



    $('#editor').froalaEditor({
        key: 'aA-7rB-22njmF4bmn==',
        toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'formatUL',  '|', 'clearFormatting', 'html', '|', 'undo', 'redo'],
        codeBeautifierOptions: {
            end_with_newline: true,
            indent_inner_html: true,
            extra_liners: "['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'ol', 'table', 'dl']",
            brace_style: 'expand',
            indent_char: ' ',
            indent_size: 4,
            wrap_line_length: 0
        },
        height: 300
    })


});
