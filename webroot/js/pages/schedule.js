$(document).ready(function () {

    $('#company').change(function () {

        var companyid = jQuery('select[name=company]').val();
        $.ajax({
            type: "POST",
            url: base_url + "admin/schedule/process",
            data: {companyid: companyid}
        }).done(function (data) {
            $('#response').show();
            $("#response").html(data);
            $('#gigselect').css('display', 'none');
        });
    });

    $('#from').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#to').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('#from_time').timepicker({
        upArrowStyle: 'fa fa-angle-up',
        downArrowStyle: 'fa fa-angle-down'
    });
    $('#to_time').timepicker({
        upArrowStyle: 'fa fa-angle-up',
        downArrowStyle: 'fa fa-angle-down'
    });

    $('#basic-datatables-schedules').on('focus', '.shiftfromdate', function () {
        var i = 0;
        $(this).datetimepicker({
            format: "YYYY-MM-DD HH:mm",
            widgetPositioning: {
                vertical: 'bottom'
            },
            useCurrent: false
        }).on("dp.hide", function (e) {
            var id = $(this).attr('id');
            var splitarray = id.split("_");
            var idnew = splitarray['1'];
            var name = $(this).attr('name');
            var value = $("#" + id).val();
            if (i === 0) {
                if (name === 'shift_from' || name === 'shift_to') {
                    $.ajax({
                        url: base_url + 'admin/schedule/checkgigstr/',
                        type: 'POST',
                        data: {id: idnew, fromdate: value, name: name},
                        dataType: 'text',
                        success: function (data) {
                            if (data === "1") {
                                $.confirm({
                                    title: "Warning",
                                    text: "Selected Gigstr is already scheduled for selected Shift Start Date or Shift End Date.",
                                    confirmButton: "Ok",
                                    cancelButton: false
                                });
                            }

                        }
                    });
                }

                $.ajax({
                    url: base_url + 'admin/schedule/edit/',
                    type: 'POST',
                    data: {id: idnew, name: name, value: value},
                    dataType: 'text',
                    success: function (data) {
                        if (data === "1") {
                            $("#" + id).val(value);
                            successMsg('3000', 'Date has been saved.');
                        }
                        else {
                            $('#error-message').css('display', 'block').fadeOut(5000);
                        }
                    }
                });

            }
            i++;
        });
    });

    $('#basic-datatables-schedules').on('focus', '.gig_id', function () {

        $(this).change(function () {
            var id = $(this).attr('id');
            var splitarray = id.split("_");
            var idnew = splitarray['1'];
            var name = $(this).attr('name');
            var value = $('select[id=' + id + ']').val();
            $.ajax({
                url: base_url + 'admin/schedule/edit/',
                type: 'POST',
                data: {id: idnew, name: name, value: value},
                dataType: 'text',
                success: function (data) {
                    if (data === "1") {
                        successMsg('3000', 'Gig has been saved.');
                    }
                    else {
                        $('#error-message').css('display', 'block').fadeOut(5000);
                    }
                }
            });

        });
    });

    $('#basic-datatables-schedules').on('focus', '.autocomplete', function () {

        $(this).autocomplete({
            source: "find",
            select: function (event, ui) {
                var value = ui.item.id;
                var candidatename = ui.item.value;
                var name = 'gigstr';
                var id = $(this).attr('id');
                var splitarray = id.split("_");
                var idnew = splitarray['1'];
                var fromdate = $("#from_" + idnew).val();
                $.ajax({
                    url: base_url + 'admin/schedule/checkschedule/',
                    type: 'POST',
                    data: {value: value, fromdate: fromdate},
                    dataType: 'text',
                    success: function (data) {
                        if (data === "1") {
                            $.confirm({
                                title: "Warning",
                                text: "Selected Gigstr is already scheduled for selected Shift Start Date or Shift End Date.",
                                confirmButton: "Ok",
                                cancelButton: false
                            });
                        }
                        $.ajax({
                            url: base_url + 'admin/schedule/edit/',
                            type: 'POST',
                            data: {id: idnew, name: name, value: value},
                            dataType: 'text',
                            success: function (data) {
                                if (data === "1") {
                                    $("#" + id).val(candidatename);
                                    var gigid = '<a href="'+ base_url +'admin/candidate/profile/'+ value +'" style="text-decoration: underline;">'+ value + '</a>';
                                    $('#idgigstr_' + idnew).html(gigid);
                                    successMsg('3000', 'Gigster has been saved.');
                                }
                                else {
                                    $('#error-message').css('display', 'block').fadeOut(5000);
                                }
                            }
                        });
                    }
                });
            }
        });

        $(this).focusout(function () {
            var id = $(this).attr('id');
            var splitarray = id.split("_");
            var idnew = splitarray['1'];
            var name = $(this).attr('name');
            var value = $("#" + id).val();
            if (value.length === 0) {
                var value = "";
                $.ajax({
                    url: base_url + 'admin/schedule/edit/',
                    type: 'POST',
                    data: {id: idnew, name: name, value: value},
                    dataType: 'text',
                    success: function (data) {
                        if (data === "1") {
                            successMsg('3000', 'Gigster has been saved.');
                        }
                        else {
                            $('#error-message').css('display', 'block').fadeOut(5000);
                        }
                    }
                });
            }
        });

    });

    /** DEPRECATED */
    $('#basic-datatables-schedules').on('focus', '.attestcheckx', function () {

        $(this).click(function () {

            var id = $(this).attr('id');
            var splitarray = id.split("_");
            var idnew = splitarray['1'];
            var name = 'attested';
            var value = 'value';
            var attval = $(this).is(':checked') ? 'Attested' : 'Not Attested';

            $.ajax({
                url: base_url + 'admin/schedule/edit/',
                type: 'POST',
                data: {id: idnew, name: name, value: value},
                dataType: 'text',
                success: function (data) {
                    if (data === "1") {
                        if(attval == 'Attested') {
                            successMsg('3000', 'Selected shift Attested.');
                        }
                        else {
                            successMsg('3000', 'Selected shift not Attested.');
                        }
                    }
                }
            });



        });
    });

    $('.commentadd').change(function () {
        var id = $(this).attr('id');
        var splitarray = id.split("_");
        var idnew = splitarray['1'];
        var name = $(this).attr('name');
        var value = $("#" + id).val();
        $.ajax({
            url: base_url + 'admin/schedule/edit/',
            type: 'POST',
            data: {id: idnew, name: name, value: value},
            dataType: 'text',
            success: function (data) {
                if (data === "1") {
                    location.reload();
                }
            }
        });

    });

    $('.gigster').change(function () {
        var id = $(this).attr('id');
        var splitarray = id.split("_");
        var idnew = splitarray['1'];
        var name = $(this).attr('name');
        var value = $("#" + id).val();
        $.ajax({
            url: base_url + 'admin/schedule/edit/',
            type: 'POST',
            data: {id: idnew, name: name, value: value},
            dataType: 'text',
            success: function (data) {
                if (data === "1") {
                    location.reload();
                }
                else {
                    alert('No user for giver user id');
                }
            }
        });

    });

    $('#summernote').summernote({
        height: 200,
        disableDragAndDrop: true,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']]
        ],
        /*callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }*/
    });

    $('#my_modal').on('show.bs.modal', function (e) {
        var id = $(e.relatedTarget).data('id');
        var name = $(e.relatedTarget).data('name');
        var type = $(e.relatedTarget).data('type');
        if(name == 'shift_description') {
            $('#descLabel').html('Shift Description');
        } else {
            $('#descLabel').html('Comment');
        }

        $.ajax({
            url: base_url + 'admin/schedule/getdescription/',
            type: 'POST',
            data: {id: id, type: type},
            dataType: 'text',
            success: function (data) {
                $(e.currentTarget).find('input[name="scheduleid"]').val(id);
                $(e.currentTarget).find('input[name="schedulename"]').val(name);

                $('#editor').froalaEditor('html.set', data);
            }
        });
    });

    $("#btnadd").click(function (e) {
        var id = $("#scheduleid").val();
        var name = $("#schedulename").val();
        var description =  $('#editor').froalaEditor('html.get', true);
        var shiftData = $('#shiftData').val();
        if(typeof shiftData == 'undefined' && shiftData == '') {
            shiftData = "";
        }

        $.ajax({
            url: base_url + 'admin/schedule/savedescription/',
            type: 'POST',
            data: {id: id, name: name, description: description, shiftData: shiftData},
            dataType: 'text',
            success: function (data) {
                if (data === "1") {
                    if (name === 'shift_description') {
                        $('#my_modal').modal('toggle');
                        var desc = $(description).text();
                        $('#iddescription_' + id).html(desc.substr(0, 6) + '...');
                        successMsg('3000', 'Description Saved successfully.');
                    }
                    else {
                        $('#my_modal').modal('toggle');
                        desc = $(description).text();
                        $('#idcomment_'+id).html(desc.substr(0, 6)+'...');
                        successMsg('3000', 'Comment Saved successfully.');
                    }
                }
                else {
                    $('#error-message').css('display', 'block').fadeOut(5000);
                }
            }
        });
    });

    //$('#multi_delete').attr("disabled", "disabled");
    $('#multi_delete').css('pointer-events', 'none');

    $('input[name="schedule_delete"]').change(function() {
        $('#multi_delete').css('pointer-events', 'auto');
    });

    $('#multi_delete').click(function(e) {
        e.preventDefault();
        var company = getUrlVars()["company"];
        /* var to = getUrlVars()["to"];
         var gig = getUrlVars()["gig"];
         var from = getUrlVars()["from"];*/

        if (typeof company != "undefined") {
            var to = getUrlVars()["to"];
            var gig = getUrlVars()["gig"];
            var from = getUrlVars()["from"];
            var gigstr = getUrlVars()["gigstr"];
        }
        else {
            to = "";
            gig = "";
            from = "";
            company = "";
            gigstr = "";
        }
        var sid = $.map($('input[name="schedule_delete"]:checked'), function(c){return c.value; });

        var result = confirm('Delete all selected shifts?');
        if(result == true) {
            $.ajax({
                type:"POST",
                url: base_url + 'admin/schedule/delete/',
                //url:$(this).attr('href'),
                data:{"sid" : sid, "company" : company, "to" : to,  "gig" : gig,  "from" : from, "gigstr" : gigstr },
                dataType:"text",
                success:function(data){
                    var returnedData = JSON.parse(data);
                    if(returnedData.ret == 1)
                    {
                        if(returnedData.company == '') {
                            window.location.href = base_url + 'admin/schedule/allschedule';
                            successMsg('3000', 'Selected shift(s) deleted.');
                        }
                        else {
                            window.location.href = base_url + 'admin/schedule/search?company=' + returnedData.company + '&gig=' + returnedData.gig + '&from=' + returnedData.from + '&to=' + returnedData.to + '&gigstr=' + gigstr;
                            successMsg('3000', 'Selected shift(s) deleted.');
                        }
                    }
                    else
                    {
                        $('#error-message').css('display','block').fadeOut(5000);
                    }
                }
            });
        }
        else {
            return false;
        }
    });

    var list = "/admin/schedule/gigslist";
    $.getJSON( list, function( json ) {
        var  gigList = json.user_arr;
        $( "#gigstr" ).autocomplete({
            source: gigList
        });
    });


    $('#editor').froalaEditor({
        key: 'aA-7rB-22njmF4bmn==',
        toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'formatUL',  '|', 'clearFormatting', 'html', '|', 'undo', 'redo'],
        codeBeautifierOptions: {
            end_with_newline: true,
            indent_inner_html: true,
            extra_liners: "['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre', 'ul', 'ol', 'table', 'dl']",
            brace_style: 'expand',
            indent_char: ' ',
            indent_size: 4,
            wrap_line_length: 0
        },
        height: 300
    })


});

function attestCheck(gigId, name) {
    var value = 'value';
    $.ajax({
        url: base_url + 'admin/schedule/edit/',
        type: 'POST',
        data: {id: gigId, name: name, value: value},
        dataType: 'text',
        success: function (data) {
            if (data === "1") {
                successMsg('3000', 'Value has been saved.');
            }
        }
    });
}


function successMsg(time, msg) {
    $.extend($.gritter.options, {
        position: 'top-right'
    });
    $.gritter.add({
        title: 'Done !!!',
        text: msg,
        time: time,
        close_icon: 'l-arrows-remove s16',
        icon: 'glyphicon glyphicon-ok',
        class_name: 'success-notice'
    });
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function(m,key,value) {
            vars[key] = value;
        });
    return vars;
}