//------------- chatinbox.js -------------//
$(document).ready(function () {
    
    $('#autoreply').on('change', function() {
        if($(this).prop('checked') === true){
            $.ajax({
                    url: base_url + "admin/chat/autoreply",
                    dataType: 'html',
                    method: "POST",
                    data:{checked: "true"},
                    async: false,
                    success: function (result) {
                        $('#warning').css('display','none');
                        $('#warning2').show();
                    },
                });
           
           }
        else
        {
            $.ajax({
                    url: base_url + "admin/chat/autoreply",
                    dataType: 'html',
                    method: "POST",
                    data:{checked: "flase"},
                    async: false,
                    success: function (result) {
                        $('#warning').css('display','none');
                        $('#warning2').css('display','none');
                    },
                });
        }
    });


    $('#basic-datatables-chat tbody').on('click', '.unreadChat', function () {

        var chat = $(this).attr("chat");
        var gig = $(this).attr("gig");
        var count = $(this).attr("count");

        $.ajax({
            url: base_url + "admin/chat/unread",
            dataType: 'html',
            method: "POST",
            data:{chat: chat, gig: gig},
            success: function (result) {
                if(result == '1') {
                    $('.chats-' + count).addClass('strong');
                }
                else{
                    $('.chats-' + count).removeClass('strong');
                }
            }
        });
    });


    $('#chatfrom').datepicker({
        format: 'yyyy-mm-dd'
    });

    $('#chatto').datepicker({
        format: 'yyyy-mm-dd'
    });

});

